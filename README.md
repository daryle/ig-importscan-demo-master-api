IG-ImportScan
=============

#### Sphinx Trouble Shooting References

Directory Path:
```
/usr/local/sphinx/etc
```


----

###### Files:

sphinx.conf 
 - main config for us dataset indexes.

sphinx-temp.conf 
 - pointer config for us dataset.

sphinx-latin-n.conf : main config for latin dataset indexes.
sphinx-latin-temp-n.conf : pointer config for latin dataset indexes.

where n is the server id. (e.g. sphinx-latin-temp-s6.conf)

----

###### Sphinx Ports:

9940 - main us port
9930 - pointer us port

9945 - main latin port
9935 - pointer latin port

----

Command to view running sphinx daemon.

```
ps x | grep searchd
```

You will see something like this.

```
root@n3 [/usr/local/sphinx/etc]# ps x | grep searchd
 7479 pts/0    S+     0:00 grep searchd
 7691 ?        S      1:28 searchd --config /usr/local/sphinx/etc/sphinx-temp.conf
18954 ?        S      0:33 searchd --config sphinx-temp-latin-s3.conf
20830 ?        S    166:11 searchd --config /usr/local/sphinx/etc/sphinx.conf
29922 ?        S      2:41 searchd --config sphinx-latin-s3.conf

```

----

Rebooting Sphinx Search

```
searchd -c /usr/local/sphinx/sphinx.conf --stop; sleep 5; searchd -c /usr/local/sphinx/sphinx.conf;
```

---

Sphinx Indexes Server Assignment

Ref:

https://docs.google.com/a/importgenius.com/spreadsheet/ccc?key=0Aohw1cxrnDzYdGdrRGFPNXhQY25XMVZPYVItUE43dkE#gid=0
