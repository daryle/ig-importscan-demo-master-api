<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|    http://codeigniter.com/user_guide/general/hooks.html
|
*/


/**
 * Show profilers only if dev mode cookie is detected
 */
if (isset($_COOKIE['devmode'])) {
	$hook['pre_controller'] = array(
		'class' => 'Benchmark',
		'function' => 'start',
		'filename' => 'Benchmark.php',
		'filepath' => 'hooks',
	);

	$hook['post_system'] = array(
		'class' => 'Benchmark',
		'function' => 'end',
		'filename' => 'Benchmark.php',
		'filepath' => 'hooks',
	);

	$hook['post_controller'][] = array(
		'class' => 'Benchmark',
		'function' => 'profiler',
		'filename' => 'Benchmark.php',
		'filepath' => 'hooks',
	);

	$hook['display_override'][] = array(
		'class' => 'Benchmark',
		'function' => 'display_override',
		'filename' => 'Benchmark.php',
		'filepath' => 'hooks',
	);
}

/* End of file hooks.php */
/* Location: ./system/application/config/hooks.php */