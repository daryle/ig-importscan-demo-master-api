<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the "Database Connection"
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
| ['hostname'] The hostname of your database server.
| ['username'] The username used to connect to the database
| ['password'] The password used to connect to the database
| ['database'] The name of the database you want to connect to
| ['dbdriver'] The database type. ie: mysql.  Currently supported:
         mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
| ['dbprefix'] You can add an optional prefix, which will be added
|        to the table name when using the  Active Record class
| ['pconnect'] TRUE/FALSE - Whether to use a persistent connection
| ['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
| ['cache_on'] TRUE/FALSE - Enables/disables query caching
| ['cachedir'] The path to the folder where cache files should be stored
| ['char_set'] The character set used in communicating with the database
| ['dbcollat'] The character collation used in communicating with the database
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the "default" group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
| AWS
*/

$active_record = TRUE;
$active_group = "live";

switch (S_ID) {
  case 'aws': 
    $active_group   = "core"; 
    #$igdata    = "54.221.214.142";
    $igdata     = "sql-igdata-master.importgenius.com";
    $core       = "sql-core-master.importgenius.com"; # Core sql

    break;
  case 'dev_local':
    $active_group   = "core";
    $igdata     = "localhost";
    $core       = "localhost";
    break;
  default:
    $active_group   = "core";
    $igdata     = "sql-igdata-master.importgenius.com";
    $core       = "localhost"; # Core sql
    break;
}

/**
 * DEFFAULT
 */
$db['default']['hostname'] = $igdata;
$db['default']['username'] = "iscan";
$db['default']['password'] = "ajnin_edoc_12";
$db['default']['database'] = "iglatindata";
$db['default']['dbdriver'] = "mysql";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = APPPATH."/cache";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

/**
 * LIVE
 */
$db['live']['hostname'] = $core;
$db['live']['username'] = "iscan";
$db['live']['password'] = "ajnin_edoc_12";
$db['live']['database'] = "core";
$db['live']['dbdriver'] = "mysql";
$db['live']['dbprefix'] = "";
$db['live']['pconnect'] = FALSE;
$db['live']['db_debug'] = TRUE;
$db['live']['cache_on'] = FALSE;
$db['live']['cachedir'] = "cache";
$db['live']['char_set'] = "utf8";
$db['live']['dbcollat'] = "utf8_general_ci";

/**
 * CORE
 */
$db['core']['hostname'] = $core;
$db['core']['username'] = "iscan";
$db['core']['password'] = "ajnin_edoc_12";
$db['core']['database'] = "core";
$db['core']['dbdriver'] = "mysql";
$db['core']['dbprefix'] = "";
$db['core']['pconnect'] = FALSE;
$db['core']['db_debug'] = TRUE;
$db['core']['cache_on'] = FALSE;
$db['core']['cachedir'] = "cache";
$db['core']['char_set'] = "utf8";
$db['core']['dbcollat'] = "utf8_general_ci";

/**
 * IGSAVE
 */
$db['igsave']['hostname'] = $igdata;
$db['igsave']['username'] = "iscan";
$db['igsave']['password'] = "ajnin_edoc_12";
$db['igsave']['database'] = "iglatindata";
$db['igsave']['dbdriver'] = "mysql";
$db['igsave']['dbprefix'] = "";
$db['igsave']['pconnect'] = FALSE;
$db['igsave']['db_debug'] = TRUE;
$db['igsave']['cache_on'] = FALSE;
$db['igsave']['cachedir'] = "cache";
$db['igsave']['char_set'] = "utf8";
$db['igsave']['dbcollat'] = "utf8_general_ci";

/**
 * IGSAVE2
 */
$db['igsave2']['hostname'] = $igdata;
$db['igsave2']['username'] = "iscan";
$db['igsave2']['password'] = "ajnin_edoc_12";
$db['igsave2']['database'] = "igdata";
$db['igsave2']['dbdriver'] = "mysql";
$db['igsave2']['dbprefix'] = "";
$db['igsave2']['pconnect'] = FALSE;
$db['igsave2']['db_debug'] = TRUE;
$db['igsave2']['cache_on'] = FALSE;
$db['igsave2']['cachedir'] = "cache";
$db['igsave2']['char_set'] = "utf8";
$db['igsave2']['dbcollat'] = "utf8_general_ci";

/**
 * BIGDATA_US
 */
$db['bigdata_us']['hostname'] = $igdata;
$db['bigdata_us']['username'] = "iscan";
$db['bigdata_us']['password'] = "ajnin_edoc_12";
$db['bigdata_us']['database'] = "igdata";
$db['bigdata_us']['dbdriver'] = "mysql";
$db['bigdata_us']['dbprefix'] = "";
$db['bigdata_us']['pconnect'] = FALSE;
$db['bigdata_us']['db_debug'] = TRUE;
$db['bigdata_us']['cache_on'] = FALSE;
$db['bigdata_us']['cachedir'] = APPPATH."/cache";
$db['bigdata_us']['char_set'] = "utf8";
$db['bigdata_us']['dbcollat'] = "utf8_general_ci";

/**
 * BIGDATA
 */
$db['bigdata']['hostname'] = $igdata;
$db['bigdata']['username'] = "iscan";
$db['bigdata']['password'] = "ajnin_edoc_12";
$db['bigdata']['database'] = "iglatindata";
$db['bigdata']['dbdriver'] = "mysql";
$db['bigdata']['dbprefix'] = "";
$db['bigdata']['pconnect'] = FALSE;
$db['bigdata']['db_debug'] = TRUE;
$db['bigdata']['cache_on'] = FALSE;
$db['bigdata']['cachedir'] = APPPATH."/cache";
$db['bigdata']['char_set'] = "utf8";
$db['bigdata']['dbcollat'] = "utf8_general_ci";

if (S_ID=='dev') {
/**
 * Kanban Beta Server
 */

$db['igsave_k']['hostname'] = "localhost";
$db['igsave_k']['username'] = "iscan";
$db['igsave_k']['password'] = "ajnin_edoc_12";
$db['igsave_k']['database'] = "igdata";
$db['igsave_k']['dbdriver'] = "mysql";
$db['igsave_k']['dbprefix'] = "";
$db['igsave_k']['pconnect'] = FALSE;
$db['igsave_k']['db_debug'] = TRUE;
$db['igsave_k']['cache_on'] = FALSE;
$db['igsave_k']['cachedir'] = "cache";
$db['igsave_k']['char_set'] = "utf8";
$db['igsave_k']['dbcollat'] = "utf8_general_ci";

$db['bigdata_k']['hostname'] = "localhost";
$db['bigdata_k']['username'] = "iscan";
$db['bigdata_k']['password'] = "ajnin_edoc_12";
$db['bigdata_k']['database'] = "iglatindata";
$db['bigdata_k']['dbdriver'] = "mysql";
$db['bigdata_k']['dbprefix'] = "";
$db['bigdata_k']['pconnect'] = FALSE;
$db['bigdata_k']['db_debug'] = TRUE;
$db['bigdata_k']['cache_on'] = FALSE;
$db['bigdata_k']['cachedir'] = "cache";
$db['bigdata_k']['char_set'] = "utf8";
$db['bigdata_k']['dbcollat'] = "utf8_general_ci";

}

/* End of file database.php */
/* Location: ./system/application/config/database.php */