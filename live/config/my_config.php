<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Asset Loader and Minifier

$config['static_url'] = $config['base_url'];
$config['resource_folder'] = 'resources/iscan4l/live/';
$config['expires'] = 60 * 60 * 24 * 14;
$config['html_expires'] = 0;

if (strpos($config['static_url'], 'iscan.com')) {
    $config['server1_ip'] = "127.0.0.1";
} else {
    $config['server1_ip'] = "144.202.219.10";
}

//$config['static_url'] = 'http://d9i9520poph3p.cloudfront.net/';
if ($config['proto'] == 'https') {
	//$config['static_url'] = 'https://d9i9520poph3p.cloudfront.net/';
} 

if (preg_match("/^(((s[1-5s]{1})|www)\.)?(importgenius\.com)*$/", $_SERVER['HTTP_HOST'])) {
    $config['static_url'] = 'http://d3efumwg9zxd72.cloudfront.net/';

    if ($config['proto'] == 'https')
        $config['static_url'] = 'https://d3efumwg9zxd72.cloudfront.net/';
}

if (strpos($config['static_url'], 'ec2-54-205-133-3.compute-1.amazonaws.com')) {
    $config['export_url'] = "ec2-54-221-28-82.compute-1.amazonaws.com/iscan4l.php";
} else {
    $config['export_url'] = "files.importgenius.com";
}


if (strpos($config['static_url'], 'kanban.importgenius.com') !== FALSE) {
    $config['export_url'] = "kanban.importgenius.com/iscan4lx.php";
}

$config['proto'] = str_replace('httpss', 'https', $config['proto']);
$config['static_url'] = str_replace('httpss', 'https', $config['static_url']);

$config['css']['entry'] = array(
    array('path' => 'css/iscan2/', 'file' => 'entry.css')
    , array('path' => 'css/iscan2/', 'file' => 'login.css')
    , array('path' => 'css/flexigrid/', 'file' => 'flexigrid.css')
    , array('path' => 'css/iscan2/', 'file' => 'vmap.css')
);

$config['css']['default'] = array(
    array('path' => 'css/iscan2/', 'file' => 'style.css')
    /* 	,array('path'=>'css/iscan2/','file'=>'calendar.css') */
    , array('path' => 'css/datepicker/', 'file' => 'datepicker.css')
    , array('path' => 'css/flexigrid/', 'file' => 'flexigrid.css')
    , array('path' => 'css/iscan2/', 'file' => 'thickbox.css')
    , array('path' => 'css/iscan2/', 'file' => 'vmap.css')
);

//3 ways to handle google analytics  for better loading time need to test for best result

$config['js']['entry'] = array(
    array('path' => 'js/', 'file' => 'jquery-144.js')
    , array('path' => 'js/', 'file' => 'alphanumeric.js')
    , array('path' => 'js/', 'file' => 'flexigrid.js')
    , array('path' => 'js/', 'file' => 'thickbox.js')
);

$config['js']['default'] = array(
    array('path' => 'js/', 'file' => 'jquery-144.js')
    , array('path' => 'js/', 'file' => 'cdropdown.js')
    , array('path' => 'js/', 'file' => 'alphanumeric.js')
    , array('path' => 'js/', 'file' => 'flexigrid.js')
    , array('path' => 'js/', 'file' => 'swfobject.js')
    , array('path' => 'js/', 'file' => 'thickbox.js')
    , array('path' => 'js/', 'file' => 'datepicker.js')
    , array('path' => 'js/', 'file' => 'tooltip.js')
    , array('path' => 'js/', 'file' => 'zipcoderange.js')
    /*
      ,array('path'=>'js/jscalendar/','file'=>'calendar_stripped.js')
      ,array('path'=>'js/jscalendar/lang/','file'=>'calendar-en.js')
      ,array('path'=>'js/jscalendar/','file'=>'calendar-setup_stripped.js')
     */
);

$config['js']['gmap'] = array(
    array('path' => 'js/', 'file' => 'gmap.js')
);

$config['js']['script4'] = array(
    array('path' => 'js/', 'file' => 'script4.js')
    , array('path' => 'js/', 'file' => 'alibaba.js')
//    , array('path' => 'js/', 'file' => 'googleads.js')
);

$config['js']['jit'] = array(
    array('path' => 'lib/jit/', 'file' => 'jit.js')
    , array('path' => 'lib/jquery/', 'file' => 'jquery.simpletip.js')
    , array('path' => 'lib/jit/', 'file' => 'jit.ie.js')
);

//list ccs3 and html5 compatible browsers

$config['css3_browsers'] = array(
    "Internet Explorer" => 7
    , "Firefox" => 3.5
    , "Opera" => 9
    , "Safari" => 4
    , "Chrome" => 4
);

//set available language abbreviations
$config['lang_abbr'] = "en";
$config['lang_default_abbr'] = "en";

$config['lang_uri_abbr'] = array(
    "en" => "english"
    , "cn" => "zhongwen"
);
$config['lang_desc'] = array(
    "en" => "English"
    , "cn" => "中文"
);

$config['allcountries'] = 10;

//version testing for loading alternate version of views or uri's
$config['versions'] = array();
/*
$config['versions']['home'] =
	array
	(
		 1 => array('file' => 'home.b','percent' => 3)
		,2 => array('file' => 'home.c','percent' => 1)
	);
*/

