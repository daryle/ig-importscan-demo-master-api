<?

	function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}
	
	function fsize($x)
	{
	
	$x = $x / 1024;

	if ($x>1024)
		$fz = number_format($x/1024,1)." Mb";
	else
		$fz = number_format($x)." Kb";

	return $fz;
	}
	
	function fwrap($str)
	{
	$str = "<span style='white-space: normal; margin: 0'>$str</span>";
	return $str;
	}
	
	function toTime($time)
	{

	$rtext = "";

	if($time >= 31556926){
      $rtext .= floor($time/31556926)." years ";
      $time = ($time%31556926);
    }
    if($time >= 86400){
      $rtext .= floor($time/86400)." days ";
      $time = ($time%86400);
    }
    if($time >= 3600){
      $rtext .= floor($time/3600)." hours ";
      $time = ($time%3600);
    }
    if($time >= 60){
      $rtext .= floor($time/60)." minutes ";
      $time = ($time%60);
    }
    
    	$rtext .= $time." seconds ";
    	
    	return $rtext;
	
	}

	function toKG($val,$unit)
	{
	
		$val = (int)$val;
	
		if(is_int($val))
		{
			if ($unit=='LB' || $unit =='L')
				$val = $val/2.2;
			else if($unit == 'LT' || $unit == 'T')
				$val = $val * 1016.05;
			else if($unit == 'ST' || $unit == 'S')
				$val = $val * 907.185;
			else if($unit == 'ET' || $unit == 'E' || $unit == "MT" || $unit == "M")
				$val = $val * 1000;
			else if($unit == 'GM' || $unit == "G")
				$val = $val * 0.001;
			else if($unit == 'OZ' || $unit == "O")
				$val = $val * 0.0283495;
		}
		else
		{
			$val = 0;
		}
		return number_format($val);//." KG";
	}

	function toLB($val,$unit)
	{
		
		$val = (int)$val;
	
		if ($unit=='KG' || $unit =='K')
				$val = $val*2.2;
		else if($unit == 'LT' || $unit == 'T')
			$val = $val * 2240;
		else if($unit == 'ST' || $unit == 'S')
			$val = $val * 2000;
		else if($unit == 'ET' || $unit == 'E' ||$unit == "MT" || $unit == "M")
			$val = $val * 2205;
		else if($unit == 'GM' || $unit == "G")
			$val = $val * 0.00220462;
		else if($unit == 'OZ' || $unit == "O")
			$val = $val * 0.0625;


		return number_format($val);//." LB";

	}
	
	function hlite2($row,$qlite,$cfields="us")
	{
			$cell = array();
			
			if($cfields == "us")
			{
				$all = array(
					'containernum'
					,'product'
					,'consname'
					,'consaddr'
					,'shipname'
					,'shipaddr'
					,'ntfyname'
					,'ntfyaddr'
					,'uport'
					,'fport'
					,'billofladingnbr'
					,'carriercode'
					,'vesselname'
					,'countryoforigin'
					,'marks'
					,'masterbilloflading'
					,'usdisport_name'
					,'continent_name'
					,'zipcode'
				
				);
			}
			else
			{
				$all = $cfields;
			}
			
			//echo "<pre>";
		
			foreach ($row as $key => $item) {
				//value of $qlite[$key][0][0] is NOT | AND | OR
				if ((isset($qlite[$key]) && $key != 'zipcode' && $qlite[$key][0][0] != 'NOT') || isset($qlite['all']) ) {
						
						$k = $key;
						
						if (!isset($qlite[$k]) && in_array($k, $all)==true) {
							$k = 'all';
						}
						
						if (isset($qlite[$k])) foreach ($qlite[$k] as $qitem) {
								
							//print_r($qitem); 
							
							if ($k=='consname_n'||$k=='shipname_n') {
								continue;	
							}

							$pattern = array();
							$rep = array();
							$qries = explode(' ',$qitem[1]);

							foreach ($qries as $q) {
								$nstr = strtoupper(trim($q));

								$allowed = "/[^a-zA-Z0-9., ]/i";
								$nstr = preg_replace($allowed,'',$nstr);

								if ($nstr=='') {
									continue;
								}
					
								$pattern[] = "/^($nstr)[^a-zA-Z<>]/i";
								$rep[] = " <b>$nstr</b> ";
				
								$pattern[] = "/[^a-zA-Z<>]($nstr)[^a-zA-Z<>]/i";
								$rep[] = " <b>$nstr</b> ";
				
								$pattern[] = "/[^a-zA-Z<>]*($nstr)[^a-zA-Z<>]*$/i";
								$rep[] = " <b>$nstr</b> ";
				
								$item = preg_replace($pattern,$rep,$item);
							}
						}
						
				}
						
				$cell[$key] = $item;
			}
			
		//print_r($cell); exit;
		return $cell;
	}


	function cleanR3($row)
	{
		$cell = array();
		
		$pattern = array();
		$rep = array();
			
		$pattern[] = "/[^a-zA-Z0-9.-]/i";
		$rep[] = " ";
		
		foreach ($row as $key=>$item)
		{

			if ($key=='grossweight') continue;
			{
				$cell['weight1'] = "\"".toLB($row['grossweight'],'KG')."\"";
				$cell['weight2'] = "\"".toKG($row['grossweight'],'LB')."\"";
				continue;
			}

			continue;

			$val = preg_replace($pattern,$rep,$item);
			
			$cell[$key] = "\"".trim($val)."\"";
 
		}

			//$cell['cc_name'] = '';//preg_replace($pattern,$rep,"$row[carriercode] - $row[company_name]");
			//$cell['cc_address'] = '';//preg_replace($pattern,$rep,"$row[address] $row[city] $row[state], $row[czip]");
			
/*
			echo "<pre>";
			print_r($cell); 
			echo "</pre>";
			exit;
*/
			
		return $cell;
	}

	function cleanR4($row)
	{
		$cell = array();
		
		$pattern = array();
		$rep = array();
			
		//$pattern[] = "/[^a-zA-Z0-9.- ]/i";
		//$rep[] = " ";
		
		foreach ($row as $key=>$item)
			{
			if ($key=='weight') continue;
			if ($key=='weightunit')
			{
				$cell['weight1'] = toLB($row['weight'],$row['weightunit']);
				$cell['weight2'] = toKG($row['weight'],$row['weightunit']);
			continue;
			}

			if (
			$key=='address'
			|| $key=='city'
			|| $key=='carriercode'
			|| $key=='company_name'
			|| $key=='czip'
			|| $key=='state'
			)
			continue;
			
			//$val = preg_replace($pattern,$rep,$item);
			$val = $item;
			$val = str_replace("="," ",$val);
			$val = trim($val);
			if ($val!='') $val = substr($val,0,1000);
			$cell[$key] = $val;
			}
			
		//$cell['cc_name'] = "$row[carriercode] - $row[company_name]";
		//$cell['cc_address'] = "$row[address] $row[city] $row[state], $row[czip]";
			
			
		return $cell;
	}

	
	function cleanR($row)
	{
		$cell = array();
		
		$pattern = array();
		$rep = array();
			
		$pattern[] = "/[^a-zA-Z0-9.-]/i";
		$rep[] = " ";
		
		foreach ($row as $key=>$item)
			{
			
			$val = preg_replace($pattern,$rep,$item);
			
			$cell[$key] = "\"".trim($val)."\"";
			}
		return $cell;
	}

	function cleanR2($row)
	{
		$cell = array();
		
		$pattern = array();
		$rep = array();
			
		$pattern[] = "/[^a-zA-Z0-9.-]/i";
		$rep[] = " ";
		
		foreach ($row as $key=>$item)
			{
			
			$val = preg_replace($pattern,$rep,$item);
			$val = trim($val);
			if ($val!='') $val = substr($val,0,1000);
			$cell[$key] = $val;
			}
		return $cell;
	}



	
	function sortbylen($val_1, $val_2)
	{
		// initialize the return value to zero
		$retVal = 0;
		
		// compare lengths
		$firstVal = strlen($val_1);
		$secondVal = strlen($val_2);
		
		if($firstVal < $secondVal)
		{
		$retVal = 1;
		}
		else if($firstVal > $secondVal)
		{
		$retVal = -1;
		}
		return $retVal;
	}
	
	function rp($str,$isError=false)
		{
			if ($isError)
				$c = 'error';
			else
				$c = 'comment';
		
			return "<div class='$c'>$str</div>";
		
		}

	function json_exit($json)
	{
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" ); 
	header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" ); 
	header("Cache-Control: no-cache, must-revalidate" ); 
	header("Pragma: no-cache" );
	header("Content-type: text/x-json");
	if (isset($json)) echo json_encode($json);
	exit;		
	}
	
	function buildFilename($q,$max=100)
	{
	
		$fn = "";

			foreach (unserialize($q['qlite']) as $qkey => $ql)
			{
			

			foreach ($ql as $qs) 
				{
				
				$nw = "";
				
				if ($qkey=='masterbillofladingind')
					{
					switch ($qs[1])
						{
						case 'H': $qs[1] = 'house-only'; break;
						case 'M': $qs[1] = 'master-only'; break;
						}
					}
				
				if (isset($qs[3])) 
					{
					if ($qs[3] != '') $qs[1] = $qs[3];
					}
				
				if ($fn != ''||$qs[0]=='NOT')
					$nw .= $qs[0]."-".$qs[1];
				else
					$nw .= $qs[1];
				
				$nw = strtolower(trim(preg_replace('/[^a-zA-Z0-9]+/', '-', $nw)));


				if ($fn != '' && $nw != '')
					$nw = "-".$nw;

				
				if (strlen($fn.$nw)<$max || $fn == '')
					{
					$fn .= $nw;
					}
				
				
				}
				
			
			} //end of qlite


			$filters = array();

			if (isset($q['filters']))
				{
				$filters = unserialize($q['filters']);
				if (!is_array($filters))
					$filters = array();
				}


			foreach ($filters as $fl)
			{
			
			//$response .= "$fl[field] ".isset($fl['values']);
			
			$nw = "";
			
			switch ($fl['field'])
				{
				case "zipcode_s":
					if (isset($fl['from']) && isset($fl['to']))
						{
						//if ($fn!='') $nw .= '-';
						$nw .= "zip-range-$fl[from]-$fl[to]";
						}
					break;	
				}


			if ($fn != '' && $nw != '')
				$nw = "-".$nw;
				
			if (strlen($fn.$nw)<$max || $fn == '')
				{
				$fn .= $nw;
				}
				
			
			} //end unserialize filter
			
			//$fn .= "-".date("Ymd",$q['qfrom'])."-".date("Ymd",$q['qto']); 
			
			if (substr($fn, -1,1)=='-') $fn = substr($fn,0,strlen($fn)-1);
			
			return $fn;
			
			
		
	}
	
	function buildResponse($q,$extra="")
		{

				$response = '';
				$exclude = false;
			

				$response .= "Your search {$extra}for ";
				
				foreach (unserialize($q['qlite']) as $qkey => $ql)
				{
				
				if ($response!="Your search {$extra}for ") $response .= ' and ';
				
				if ($qkey=='zipcode') 
				{
					foreach($ql as $zp){
					$first = substr($zp[1], 0, 5);
					$last = substr($zp[1], -5);
					$resultx = $first.' - '.$last;
					$response .= "Searched by zipcode ";
					continue;
					}
				}
				$qs_r = array();
				foreach ($ql as $qs) 
					{
					//$qs_r[] = $qs[1];
					
					if (($qs[1]=='NOT AVAILABLE'||$qs[1]=='N A')&&$qs[0]=='NOT'&&$qkey=='consname') 
						{
						$exclude = true;
						continue;
						}

					if (isset($qs[3])) 
						{
						if ($qs[3] != '') $qs[1] = $qs[3];
						}
					if (count($qs_r)||$qs[0]=='NOT')
						$qs_r[] = $qs[0]." <b class='hlite'>".$qs[1]."</b>";
					
					else
						if($qs[0]==''){
							$first = substr($qs[1], 0, 5);
							$last = substr($qs[1], -5);
							$resultx = $first.' - '.$last;
							$qs_r[] = " <b class='hlite'>".$resultx."</b>";
						}else{
							$qs_r[] = " <b class='hlite'>".$qs[1]."</b>";
						}
										
/*
*/
					}
					
				if (!count($qs_r)) continue;	

					
				$response .= implode(', ',$qs_r)." ";
				
				$qs_f['product'] = 'Products';
				$qs_f['consname'] = 'Consignee';
				$qs_f['consname_n'] = 'Consignee';
				$qs_f['consaddr'] = 'Consignee Address';
				$qs_f['shipname'] = 'Shipper';
				$qs_f['shipname_n'] = 'Shipper';
				$qs_f['shipaddr'] = 'Shipper Address';
				$qs_f['ntfyname'] = 'Notify Name';
				$qs_f['uport'] = 'US Port';
				$qs_f['fport'] = 'Foreign Port';
				$qs_f['billofladingnbr'] = 'Bill of Lading';
				$qs_f['carriercode'] = 'Carrier Code';
				$qs_f['vesselname'] = 'Vessel Name';
				$qs_f['containernum'] = 'Container Number';
				$qs_f['countryoforigin'] = 'Country of Origin';
				$qs_f['origin'] = 'Country of Origin';
				$qs_f['marks'] = 'Marks';
				$qs_f['distancefromzip'] = 'Distance from Zip code';
				$qs_f['ziprange'] = 'Zip code range';
				$qs_f['masterbillofladingind'] = 'House vs Master';
				$qs_f['placereceipt'] = "Place of Receipt";
			 
				if (isset($qs_f[$qkey]))
					$response .= ' in '.$qs_f[$qkey].' ';				
				}
				
				
/*
				if ($exclude==false)
					$response .= " , including <b>blank consignees</b>, ";
*/
					
				$response .= " generated <b>".number_format($q['qresult'])."</b> results in $q[qtime] seconds. ";
				
/*
				if ($exclude)
					$response .= " <br /><br /><span class='tip'>(Tip: You can uncheck <b>Exclude blank consignees</b> to try and generate more results)</span>";
*/
				
				return $response;		
		
		}

		function buildResponseMobile($q,$extra="")
		{

				$response = '';
				$exclude = false;
			

				$response .= "Your search {$extra}for ";
				
				foreach (unserialize($q['qlite']) as $qkey => $ql)
				{
				
				if ($response!="Your search {$extra}for ") $response .= ' and ';
				
				if ($qkey=='zipcode') 
					{
					$response .= "Searched by zipcode ";
					continue;
					}
					
				$qs_r = array();
				foreach ($ql as $qs) 
					{
					//$qs_r[] = $qs[1];
					
					if (($qs[1]=='NOT AVAILABLE'||$qs[1]=='N A')&&$qs[0]=='NOT'&&$qkey=='consname') 
						{
						$exclude = true;
						continue;
						}

					if (isset($qs[3])) 
						{
						if ($qs[3] != '') $qs[1] = $qs[3];
						}
					if (count($qs_r)||$qs[0]=='NOT')
						$qs_r[] = $qs[0]." <b>".$qs[1]."</b>";
					else
						$qs_r[] = " <b>".$qs[1]."</b>";
					
/*
*/
					}
					
				if (!count($qs_r)) continue;	

					
				$response .= implode(', ',$qs_r)." ";
				
				$qs_f['product'] = 'Products';
				$qs_f['consname'] = 'Consignee';
				$qs_f['consname_n'] = 'Consignee';
				$qs_f['consaddr'] = 'Consignee Address';
				$qs_f['shipname'] = 'Shipper';
				$qs_f['shipname_n'] = 'Shipper';
				$qs_f['shipaddr'] = 'Shipper Address';
				$qs_f['ntfyname'] = 'Notify Name';
				$qs_f['uport'] = 'US Port';
				$qs_f['fport'] = 'Foreign Port';
				$qs_f['billofladingnbr'] = 'Bill of Lading';
				$qs_f['carriercode'] = 'Carrier Code';
				$qs_f['vesselname'] = 'Vessel Name';
				$qs_f['containernum'] = 'Container Number';
				$qs_f['countryoforigin'] = 'Country of Origin';
				$qs_f['origin'] = 'Country of Origin';
				$qs_f['marks'] = 'Marks';
				$qs_f['distancefromzip'] = 'Distance from Zip code';
				$qs_f['ziprange'] = 'Zip code range';
				$qs_f['masterbillofladingind'] = 'House vs Master';
				$qs_f['placereceipt'] = "Place of Receipt";
 
				if (isset($qs_f[$qkey]))
					$response .= ' in '.$qs_f[$qkey].' ';				
				
				}
				
				
/*
				if ($exclude==false)
					$response .= " , including <b>blank consignees</b>, ";
*/
					
				$response .= " generated <b>".number_format($q['qresult'])."</b> results in $q[qtime] seconds. ";
				
/*
				if ($exclude)
					$response .= " <br /><br /><span class='tip'>(Tip: You can uncheck <b>Exclude blank consignees</b> to try and generate more results)</span>";
*/
				
				return $response;		
		
		}
		
	    function strtonumber( $str, $dec_point=null, $thousands_sep=null )
	    {
	        if( is_null($dec_point) || is_null($thousands_sep) ) {
	            $locale = localeconv();
	            if( is_null($dec_point) ) {
	                $dec_point = $locale['decimal_point'];
	            }
	            if( is_null($thousands_sep) ) {
	                $thousands_sep = $locale['thousands_sep'];
	            }
	        }
	        $number = (float) str_replace($dec_point, '.', str_replace($thousands_sep, '', $str));
	        if( $number == (int) $number ) {
	            return (int) $number;
	        } else {
	            return $number;
	        }
	    }
		
		function buildResponseError()
		{
			$message = "<div class=\"comment\">Hmm, you seem to be having trouble finding what you're looking for. Want to chat with a trade data professional to see if we can help? <button type=\"button\" class=\"button\" onclick=\"showCustomerService(); return false\"><span><b style=\"padding-top: 2px; padding-bottom: 2px; font-size: 10px\">Get Help</b></span></button></div>";
			
			return $message;
		}
		
		function strip_html_tags( $text )
		{
			$text = preg_replace(
			array(
			// Remove invisible content
			'@&lt;head[^&gt;]*?&gt;.*?&lt;/head&gt;@siu',
			'@&lt;style[^&gt;]*?&gt;.*?&lt;/style&gt;@siu',
			'@[removed]]*?.*?[removed]@siu',
			'@&lt;object[^&gt;]*?.*?&lt;/object&gt;@siu',
			'@&lt;embed[^&gt;]*?.*?&lt;/embed&gt;@siu',
			'@&lt;applet[^&gt;]*?.*?&lt;/applet&gt;@siu',
			'@<noframes[^>]*?.*?</noframes>@siu',
			'@<noscript[^>]*?.*?</noscript>@siu',
			'@<noembed[^>]*?.*?</noembed>@siu',
			// Add line breaks before and after blocks
			'@</?((address)|(blockquote)|(center)|(del))@iu',
			'@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
			'@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
			'@</?((table)|(th)|(td)|(caption))@iu',
			'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
			'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
			'@</?((frameset)|(frame)|(iframe))@iu',
			),
			array(
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			"\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
			"\n\$0", "\n\$0",
			),
			$text );
			return strip_tags( $text );
		}

			
?>
