<?php
/******************************************/
/**
* Display logging information via firePHP
*/


if(!function_exists('console'))
{
    function console($message,$label = 'console') {
        
        // Load CI
        $ci = get_instance();
        $ci->load->library('firephp');
        $ci->firephp->log($message);
    }

}
/******************************************/