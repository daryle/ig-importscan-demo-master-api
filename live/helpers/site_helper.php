<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Get domain
 *
 * Return the domain name only based on the "base_url" item from your config file.
 *
 * @access    public
 * @return    string
 */
if ( ! function_exists('get_domain'))
{
	function get_domain()
	{
		$CI =& get_instance();
		return preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $CI->config->slash_item('base_url'));
	}
}

if ( ! function_exists('get_tld'))
{
	function get_tld()
	{
		$CI =& get_instance();
		return preg_replace('@(?:.+://)?(?:[^/]*\.)?([^/]+\.[^/]+)(?:/?.*)?@', '$1', get_domain());
	}
}



if (!function_exists("paging")) {
	function paging($page,$rp,$total,$limit)
	{
			$limit -= 1;

			$mid = floor($limit/2);

			if ($total>$rp)
				$numpages = ceil($total/$rp);
			else
				$numpages = 1;

			if ($page>$numpages) $page = $numpages;

				$npage = $page;

			while (($npage-1)>0&&$npage>($page-$mid)&&($npage>0))
				$npage -= 1;

			$lastpage = $npage + $limit;

			if ($lastpage>$numpages)
				{
				$npage = $numpages - $limit + 1;
				if ($npage<0) $npage = 1;
				$lastpage = $npage + $limit;
				if ($lastpage>$numpages) $lastpage = $numpages;
				}

			while (($lastpage-$npage)<$limit) $npage -= 1;

			if ($npage<1) $npage = 1;

			//echo $npage; exit;

			$paging['first'] = 1;
			if ($page>1) $paging['prev'] = $page - 1; else $paging['prev'] = 1;
			$paging['start'] = $npage;
			$paging['end'] = $lastpage;
			$paging['page'] = $page;
			if (($page+1)<$numpages) $paging['next'] = $page + 1; else $paging['next'] = $numpages;
			$paging['last'] = $numpages;
			$paging['total'] = $total;
			$paging['iend'] = $page * $rp;
			$paging['istart'] = ($page * $rp) - $rp + 1;

			if (($page * $rp)>$total) $paging['iend'] = $total;

			return $paging;
	}
}

if ( ! function_exists('version'))
{

	function version($hit,$target)
	{

	$CI =& get_instance();

	$versions = config_item('versions');

	if (!$versions) show_error('No versions not configured.');

	if (!isset($versions[$target])) show_error("Versions not available for $target");

	$views = $versions[$target];

	$ver = FALSE; // means original

	foreach ($views as $v)
		{
		$ky = key($views);
		if (10 - ($hit % 10) <= $v['percent']) $ver = $ky;
		next($views);
		}


	return $ver;

	}
}

if ( ! function_exists('resource_url'))
{
	function resource_url($uri = '')
	{
		$CI =& get_instance();

		$url = config_item('static_url');

		if (!$url) $url = base_url();

		return $url.config_item('resource_folder').$uri;
	}
}

if ( ! function_exists('static_url'))
{
	function static_url($uri = '')
	{
		$CI =& get_instance();

		$url = trim(config_item('static_url').$CI->config->item('index_page'),'/');

		if (!$url) $url = base_url();

		return $url."/".$uri;
	}
}


if ( ! function_exists('canon_url'))
{
	function canon_url($uri = '')
	{

		$CI =& get_instance();

		if (is_array($uri))
		{
			$uri = implode('/', $uri);
		}


		$base_url = $CI->config->slash_item('canon_url');
		$https = $CI->config->item('https');

		if (is_array($https))
		{
		if (in_array($uri,$https))
			{
			$base_url = str_replace("http://","https://",$base_url);
			}
		}

		if ($uri == '')
		{
			return $base_url.$CI->config->item('index_page');
		}
		else
		{
			$suffix = ($CI->config->item('url_suffix') == FALSE) ? '' : $CI->config->item('url_suffix');
			return $base_url.$CI->config->slash_item('index_page').trim($uri, '/').$suffix;
		}


	}

}


if ( ! function_exists('url_friendly'))
{
	function url_friendly($url)
		{
			$url = strtolower(trim(preg_replace('/[^a-zA-Z0-9]+/', '-', $url), '-'));
			return $url;
		}
}

if ( ! function_exists('de_url_friendly'))
{
	function de_url_friendly($url)
		{
			$allowed = "/[-]/i";
			$url = preg_replace($allowed," ",$url);
			$url = strtolower($url);
			return $url;
		}
}

if (!function_exists('hlite'))
{

	function hlite($row,$qlite)
	{

			$cell = array();

			//print_r($qlite); exit;

			foreach ($row as $key => $item)
				{
					if (isset($qlite[$key])&&$key!='zipcode')
						{

						foreach ($qlite[$key] as $qitem)
							{

								if ($key=='consname_n'||$key=='shipname_n') continue;

								$pattern = array();
								$rep = array();
								$qries = explode(' ',$qitem);
								foreach ($qries as $q)
								{
								$nstr = strtoupper(trim($q));

								$allowed = "/[^a-zA-Z0-9., ]/i";
								$nstr = preg_replace($allowed,'',$nstr);

								if ($nstr=='') continue;

								$pattern[] = "/^($nstr)[^a-zA-Z<>]/i";
								$rep[] = " <b class='hlite'>$nstr</b> ";

								$pattern[] = "/[^a-zA-Z<>]($nstr)[^a-zA-Z<>]/i";
								$rep[] = " <b class='hlite'>$nstr</b> ";

								$pattern[] = "/[^a-zA-Z<>]*($nstr)[^a-zA-Z<>]*$/i";
								$rep[] = " <b class='hlite'>$nstr</b> ";

								$item = preg_replace($pattern,$rep,$item);
								}
							}

						}

					$cell[$key] = $item;
				}
			return $cell;
	}

}

if (!function_exists('row_cleaner'))
{

	function row_cleaner($row)
	{

		//return $row;

		if (!$row) return $row;

		$cell = array();

		//print_r($row); exit;

		foreach ($row as $key=>$item)
			{
			$val = $item;

			switch($key)
			{
				case "product":
					$product = $val;
			 		$product = preg_replace("/[^a-zA-Z0-9 ]/i"," ",$product);
			 		$product = preg_replace("/[0-9][a-z]/i","$1 $2",$product);
			 		$product = preg_replace("/[a-z][0-9]/i","$1 $2",$product);
					$val = $product;

				break;
				default:
					$val = preg_replace("/[,-]$/i","",$val);
				break;
			}

			$val = trim($val);
			$cell[$key] = $val;
			}
		return $cell;
	}


}

if (!function_exists('is_ajax'))
{
	function is_ajax()
		{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			{
			return true;
			}
		return false;
		}
}

if (!function_exists('compute_discount'))
{
	function compute_discount($plan,$val)
		{

		$amount = $val;

		if (strpos($val,"%"))
		{
			$val = str_replace("%","",$val);
			$val = $plan * ($val/100);
			$amount = $plan - $val;
		}

		return $amount;
		}
}

if (!function_exists('search_filters'))
{

	function search_filters($country)
	{
		$countries = array(
					'us' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee (Importer)',
								'consaddr'			=>	'Consignee Address',
								'shipname'			=>	'Shipper (Supplier)',
								'shipaddr'			=>	'Shipper Address',
								'ntfyname'			=>	'Notify Party',
								'all'				=>	'All',
								'uport'				=>	'US Port',
								'fport'				=>	'Foreign Port',
								'billofladingnbr'	=>	'Bill of Lading',
								'placereceipt'		=> 	'Place of Receipt',
								'carriercode'		=> 'Carrier Code',
								'vesselname'			=> 'Vessel Name',
								'containernum'		=> 'Container Number',
								'countryoforigin'			=> 'Country of Origin',
								'marks'				=> 'Marks',
								'distancefromzip'	=> 'Distance from Zipcode',
								'ziprange'			=> 'Zipcode Range',
							),
					'ar' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								'consaddr'			=>	'Consignee Address',
								'origin'			=>  'Country of Origin',
								'destination'		=>  'Destination Country',
								'all'				=>	'All',
								'hscode'			=>  'HS Code',
								'embarkation'		=>  'Embarkation',

							),
					'cl' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								'consaddr'			=>	'Consignee Address',
								'origin'			=>  'Country of Origin',
								'destination'		=>  'Destination Country',
								'all'				=>	'All',
								'hscode'			=>  'HS Code',
								'embarkation'		=>  'Embarkation',
							),
					'co' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								'consaddr'			=>	'Consignee Address',
								'shipname'			=>	'Shipper',
								'shipaddr'			=>	'Shipper Address',
								'origin'			=>  'Country of Origin',
								'all'				=>	'All',
								'hscode'			=>  'HS Code',
								//'embarkation'		=>  'Embarkation',
							),
					'cr' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								'consaddr'			=>	'Consignee Address',
								'shipname'			=>	'Shipper',
								'shipaddr'			=>	'Shipper Address',
								'all'				=>	'All',
								'hscode'			=>  'HS Code',
							),
					'in' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								'consaddr'			=>	'Consignee Address',
								'shipname'			=>	'Shipper',
								'shipaddr'			=>	'Shipper Address',
								'origin'			=>  'Country of Origin',
								'all'				=>	'All',
								'hscode'			=>  'HS Code',
								//'embarkation'		=>  'Embarkation',
							),
					'ec' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								'consaddr'			=>	'Consignee Address',
								'shipname'			=>	'Shipper',
								//'shipaddr'			=>	'Shipper Address',
								'port'				=>	'Port',
								'all'				=>	'All',
								'vessel'			=>  'Vessel Name',
								'origin'			=>  'Country of Origin',
								'hscode'			=>  'HS Code',
							),
					'pa' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								//'consaddr'			=>	'Consignee Address',
								'shipname'			=>	'Shipper',
								'destination'		=>  'Destination Country',
								//'shipaddr'			=>	'Shipper Address',
								'port'				=>	'Port',
								'all'				=>	'All',
								'origin'			=>  'Country of Origin',
								'hscode'			=>  'HS Code',

							),
					'py' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								//'consaddr'			=>	'Consignee Address',
								'shipname'			=>	'Shipper',
								//'port'				=>	'Port',
								'all'				=>	'All',
								'origin'			=>  'Country of Origin',
								'destination'		=> 	'Destination Country',
								'hscode'			=>  'HS Code',
							),
					'pe' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								//'consaddr'			=>	'Consignee Address',
								'shipname'			=>	'Shipper',
								//'shipaddr'			=>	'Shipper Address',
								//'port'				=>	'Port',
								'all'				=>	'All',
								//'vessel'			=>  'Vessel Name',
								'origin'			=>  'Country of Origin',
								'hscode'			=>  'HS Code',
							),
					'uy' => array(
								'product'			=>	'Product',
								'consname'			=>	'Consignee',
								'shipname'			=>	'Shipper',
								//'consaddr'			=>	'Consignee Address',
								'all'				=>	'All',
								'origin'			=>  'Country of Origin',
								'hscode'			=>  'HS Code',
							),
					've' => array(
								'consname'			=>	'Consignee',
								'shipname'			=>	'Shipper',
								'origin'			=>  'Country of Origin',
								'all'				=>	'All',
								'hscode'			=>  'HS Code',
								'hscodedesc'		=>  'HS Description',
								'embarkation'		=>  'Embarkation',

							)
					);


		return element($country,$countries);
	}

}


if (!function_exists('find_occurences'))
{

	function find_occurences($string, $find,$s,$e) {
		if (strpos(strtolower($string), strtolower($find)) !== FALSE) {
			$pos = -1;
			$merge_found = "";
			for ($i=0; $i<substr_count(strtolower($string), strtolower($find)); $i++) {
				$pos = strpos(strtolower($string), strtolower($find), $pos+1);
				//$positionarray[] = $pos;
				$found = substr($string,$pos+$s,$e);

				$merge_found .= str_replace("0000000000","",str_replace("000000000000000000","",$found));

			}

			return $merge_found;
		}
		else {
			return FALSE;
		}

	}
}

if (! function_exists('country_name'))
{
	function country_name($code)
	{
		$countries = array(
			'us'	=>	'USA',
			'ar'	=>	'Argentina',
			'cl'	=> 	'Chile',
			'co'	=>	'Colombia',
			'ec'	=>	'Ecuador',
			'pa'	=>	'Panama',
			'pe'	=>	'Peru',
			'py'	=>	'Paraguay',
			'uy'	=>	'Uruguay',
			've'	=>	'Venezuela',
			'in'	=>	'India',
			'cr'	=>	'Costa Rica',
			'ru'	=>	'Russia'
			);

		return element($code,$countries);
	}
}

if (! function_exists('server_id'))
{
	function server_id($sid)
	{
		$servers = array(
			 's2'	=>	'Venus'
			,'s3'	=>	'Earth'
			,'s4'	=>	'Mars'
			,'s5'	=>	'Jupiter'
			,'s6'	=>	'Satrun'
			,'de'	=>	'Dev'
			);

		return element($sid,$servers);
	}
}

if (! function_exists('countryOrigin'))
{
	function countryOrigin($country="",$origin="")
	{
		$retVal = false;

		$origin = strtolower($origin);

		switch($country)
		{
			case 'cl':
				$origins = array(
					 'eeuu' 					=> 'Estados Unidos'
					,'e.e.u.u' 					=> 'Estados Unidos'
					,'us' 						=> 'Estados Unidos'
					,'usa' 						=> 'Estados Unidos'
					,'u s' 						=> 'Estados Unidos'
					,'u s a' 					=> 'Estados Unidos'
					,'u.s.a' 					=> 'Estados Unidos'
					,'united states of america' => 'Estados Unidos'
					,'united states' 			=> 'Estados Unidos'
					,'america' 					=> 'Estados Unidos'
				);

				$retVal = element($origin,$origins);
				break;
		}

		return $retVal;
	}
}
