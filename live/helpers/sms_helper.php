<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('send_sms'))
{
	function sms_send($users = array(), $message = '')
	{
		$CI =& get_instance();
		$CI->load->plugin('twilio');		
			
		/* Twilio REST API version */
		//$ApiVersion = $CI->config->item('ApiVersion');
		
		/* Set our AccountSid and AuthToken */
		//$AccountSid = $CI->config->item('AccountSid');
		//$AuthToken = $CI->config->item('AuthToken');
		
		/* Outgoing Caller ID you have previously validated with Twilio */
		//$sender = $CI->config->item('number');
		
		// Twilio REST API version
		$ApiVersion = "2010-04-01";
	
		// Set our AccountSid and AuthToken
		$AccountSid = "AC10bdd3cefa9e70ae09f76ba7d44b35da";
		$AuthToken = "5d817b9cbd805dc44dfa94683fce906c";
		
		/* Outgoing Caller ID you have previously validated with Twilio */
		$sender = "310-895-9869";
		
		// Instantiate a new Twilio Rest Client
		$client = new TwilioRestClient($AccountSid, $AuthToken);
		
		$logs = "";		
		foreach ($users as $user) {
	
			// Send a new outgoinging SMS by POST'ing to the SMS resource
			// YYY-YYY-YYYY must be a Twilio validated phone number
			$response = $client->request("/$ApiVersion/Accounts/$AccountSid/SMS/Messages", 
				"POST", array(
				"To" => $user['phone'],
				"From" => $sender,
				"Body" => $message
			));
			
			if($response->IsError)
				$logs['result'][] = "Error: {$response->ErrorMessage}";
			else
				$logs['result'][] = "Sent message to ".$user['name'];		
				
				
		}

		return $logs;
	}
}


/* End of file sms_helper.php */
/* Location: ./system/app/iscan4/helpers/sms_helper.php */