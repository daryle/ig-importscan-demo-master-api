<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//header
$lang['home'] = "Home";
$lang['tour'] = "Tour";
$lang['plans_n_pricing'] = "Plans &amp; Pricing";
$lang['contact'] = "Contact Us";
$lang['talk_to_us'] = "Talk to us";
$lang['signup'] = "Join Now";
$lang['signin'] = "Sign In";
$lang['submit'] = 'Submit';
$lang['choose_language'] = "Choose Language";
$lang['subscribe'] = 'Subscribe';
$lang['live_chat'] = 'Live Chat';
$lang['call_number'] = '1 888 843 0272';

//titles h1s h2s h3s
$lang['making_waves'] = "Making Waves";
$lang['our_trade_data_blog'] = "Our Trade Data Blog";
$lang['we_can_help_you'] = 'We can help you';
$lang['featured_videos'] = 'Featured Videos';
$lang['in_the_press'] = 'In The Press';
$lang['watch_more_video_tours'] = 'Watch More Video Tours';
$lang['who_uses_import_genius'] = 'Who uses Import Genius';
$lang['as_featured_in'] = 'As Featured In';

//inquiry form
$lang['name'] = 'Name';
$lang['business_name'] = 'Business Name';
$lang['email'] = 'Email';
$lang['phone'] = 'Phone';
$lang['message'] = 'Message';

//footer
$lang['latest_posts'] = 'Latest Posts';
$lang['company_profiles'] = 'Company Profiles';
$lang['follow_us'] = 'Follow Us';
$lang['on_our_blog_feed'] = 'Blog Feed';
$lang['have_questions'] = 'Have questions? We\'ll reply quickly!';
$lang['call_us'] = 'Or Call Us';
$lang['type_your_info_below'] = 'Type your info below';
$lang['copyright'] = "All Rights Reserved. Copyright";
$lang['records_in_our_db'] = "Records in our database and counting";
$lang['back_to_top'] = "Back to top";
$lang['privacy_policy_by'] = "Privacy Policy by";
$lang['hours_open'] = "Hours: M-F 9 am - 8 pm, U.S. Eastern Time";
$lang['shanghai'] = "Shanghai, China";
$lang['tel'] = "Tel";
$lang['mobile_no'] = "Mobile No.";

//page titles
$lang['page_title'] = "Import Genius : U.S. Customs Database and Competitive Intelligence Tools";
$lang['product_tour'] = "Product Tour";
$lang['choose_a_plan'] = "Choose a plan";
$lang['rss_lang'] = "en-us";
$lang['valid'] = "Valid";
$lang['and'] = "and";
$lang['choose'] = "Choose";
$lang['tradebase'] = "Tradebase";
$lang['account_created'] = "Account Created";
$lang['activation'] = "Activation by eSignature";
$lang['reviews'] = "Media &amp; Reviews";
$lang['terms_of_service'] = "Terms of Service";
$lang['detailed_info'] = "Detailed Company Information, Imports & Exports";

//common content
$lang['processing'] = "Processing, please wait ...";
$lang['empower'] = "Empowering Information";
$lang['page'] = "Page";
$lang['may_i_help'] = "Hi, may I help you with something?";
$lang['sales_agent_is'] = "An ImportGenius.com sales agent is available to answer your question.";
$lang['yes_chat'] = "Yes, let's chat";
$lang['no_thanks'] = "No, thanks.";
$lang['sincerely'] = "Sincerely";
$lang['ig_subscription'] = "ImportGenius.com Subscription";
$lang['m-priority'] = "1.0";
$lang['l-priority'] = "0.9";

//tab titles
$lang['faq'] = 'Frequently Asked Questions';
$lang['what_is'] = "What is Import Genius";
$lang['who_uses_us'] = "Who uses us";
$lang['available_fields'] = "Available Data Fields";
$lang['schedule_demo'] = "Schedule Live Demo";
$lang['submit_a_ticket'] = "Submit a Ticket";
$lang['claim_profile'] = "Claim your profile";

//sub content might be reused or moved if too many

	//home
	$lang['companies_in_our_db'] = "Companies in our database";
	$lang['instant_search'] = "Instant Search"; //reused in tradebase
	
	//tour
	$lang['videos'] = "Videos";
	$lang['videos_desc'] = "Watch videos to learn how Import Genius works";
	
	$lang['screenshots'] = "Screenshots";
	$lang['screenshots_desc'] = "See screenshots to learn about our application";
	
	$lang['presentation'] = "Presentation";
	$lang['presentation_desc'] = "View our interactive presentation";
	$lang['download_video'] = "Download Video";
	$lang['right_click_save'] = "Right click to save";
	
		//pics
		$lang['ship_manifest_search_pic'] = "<strong>Shipping Manifest Search</strong> reveals supplier info for most U.S. companies.";
		$lang['visual_map_pic'] = "<strong>Visual Mapping</strong> lets you explore the connections between trading companies.";
		$lang['email_alert_pic'] = "<strong>Email Alerts</strong> notify you when a new shipment matches a search you've specified.";
		$lang['identify_sales_pic'] = "<strong>Identify Sales Leads</strong> based on their actual shipping histories, location and more.";
	
	//reviews
	$lang['what_people'] = "What people are saying about us";
	$lang['source'] = "Source";

	//signup
	$lang['instantly_spill'] = 'Instantly search <b class="rec_count">{rec_count}</b> ocean freight records to monitor U.S. importers, research suppliers, generate sales leads and more.';
	$lang['view_all_us_imports'] = 'View All U.S. Imports!';
	$lang['per_month'] = "per month";
	
	//contact
	$lang['top_10_q'] = 'Top 10 Questions';
	$lang['ready_for_demo'] = "Ready for a live demo?";
	$lang['ready_spill'] = "Fill out this form and we'll contact you right away to set up a live demo of Import Genius.";
	$lang['need_to_report']	= "Need to report a problem?";
	$lang['submit_ticket_spill'] = "If you're an Import Genius user who would like to report a problem or offer feedback on the service, we want to hear from you:";
	
	
	//tradebase
	$lang['tradebase_slogan'] = "The World's Trade Database";
	$lang['business_intelligence_by'] = "Business Intelligence By";
	$lang['find_etc'] = "Find Suppliers / Importers / Products";
	$lang['lp_banner_importers'] = 'Import Genius provides <b class="rec_count2">{rec_count}</b> records on what companies are importing into the U.S. ';
	$lang['lp_banner_suppliers'] = 'Import Genius provides <b class="rec_count2">{rec_count}</b> records on what companies are exporting to the U.S. ';
	
	//tradebase labels
	$lang['browse'] = "Browse";
	$lang['search'] = "Search";
	$lang['importer'] = "Importer";
	$lang['supplier'] = "Supplier";
	$lang['port'] = "Port";
	$lang['product'] = "Product";
	$lang['all'] = "All";
	$lang['record_s'] = "record(s)";
	$lang['company_name'] = "Company Name";
	$lang['shipment_records'] = "Shipment Records";
	$lang['legend'] = "Legend";
	$lang['business_profile'] = "Business Profile";
	$lang['port_name'] = "Port Name";
	$lang['results_found'] = "results found";


	//tradebase view could be big might move to separate file
	$lang['is_an_importer'] = "is an importer in the United States";
	$lang['which_imports'] = "which <b>imports goods</b> from the following countries";
	$lang['is_a_supplier'] = "is an overseas supplier";
	$lang['that_exports'] = "which <b>exports goods</b> to importers in the following states";
	$lang['monthly_shipping'] = "Monthly Shipping Activity";
	$lang['monthly_comparison'] = "Monthly Comparsion";
	$lang['weekly_comparison'] = "Weekly Comparsion";
	$lang['daily_comparison'] = "Daily Comparsion";
	$lang['yearly_comparison'] = "Yearly Comparsion";
	$lang['sample_shipment'] = "Sample Shipment Data";
	$lang['visual_connection_map'] = "Visual Connection Map";
	$lang['company_profile'] = "Company Profile";
	$lang['view_more_details'] = "View more detailed information about";
	
	//mp tabs
	
	//mp_profile
	$lang['to_view_complete'] = "to view complete data about this company.";
	$lang['to_view_connected'] = "to view {c} more connected companies";
	$lang['importer_profile'] = "Importer Profile";
	$lang['supplier_profile'] = "Supplier Profile";
	$lang['also_importer'] = "This company is also an importer.";
	$lang['also_supplier'] = "This company is also a supplier.";
	$lang['view_importer_profile'] = "View Importer Profile";
	$lang['view_supplier_profile'] = "View Supplier Profile";
	$lang['imports_products_from'] = "imports products from";
	$lang['imports_products_to'] = "exports products to";
	$lang['address'] = "Address";
	$lang['total_in_database'] = "Total shipments in our database";
	$lang['latest_shipment'] = "Latest Shipment";
	$lang['known_suppliers'] = "known suppliers";
	$lang['known_importers'] = "known importers";
	
	//mp_sample
	$lang['to_view_more_shipping'] = "to view more shipping data from this company.";
	$lang['bill_of_lading'] = "Bill of Lading";
	$lang['bill_of_lading_no'] = "Bill of Lading No.";	
	$lang['arrival_date'] = "Arrival Date";	
	$lang['voyage_no'] = "Voyage No.";	
	$lang['vessel_name'] = "Vessel Name";	
	$lang['shipper'] = "Shipper";	
	$lang['consignee'] = "Consignee";	
	$lang['notify_party'] = "Notify Party";	
	$lang['port_of_loading'] = "Port of Loading";	
	$lang['port_of_discharge'] = "Port of Discharge";	
	$lang['declaration_of_goods'] = "Declaration of Goods";	
	$lang['other_information'] = "Other Information Available";	
	$lang['other_information_spill'] = "Gross Weight,
								Number of Units,
								Volume,
								Country of Origin,
								Carrier Code,
								Ship Registered In,
								Container Number,
								Marks & Numbers";	

	//mp map
	$lang['visual_mapping'] = "Visual Mapping";	
	$lang['visual_mapping_spill'] = "Our new Visual Mapping technology gives you an instant snapshot of any company's trade connections.";	
	$lang['watch_video'] = "Watch Video";	
	
	//mp claim
	$lang['is_this_yours'] = "Is this your company?";	
	$lang['is_this_spill'] = "Import Genius subscribers include key purchasing agents at some of the most important companies in the United States. Claim your company profile to make sure they can find you.";	

	
	
//common links
$lang['read_more_reviews'] = "Read More Reviews";
$lang['older_posts'] = "Older Posts";
$lang['newer_posts'] = "Newer Posts";
$lang['read_more_blogs'] = 'Read More Posts';
$lang['read_more'] = 'Read More';
$lang['learn_more'] = "Learn More";
$lang['join_now'] = "Join Now";

//widget
$lang['learn_more_about_us'] = "Learn more about us";
$lang['about_us_spill'] = "<b>Import Genius</b> provides timely, detailed shipment data for every container that enters the United States. Our user base is diverse: importers, exporters, entrepreneurs, freight forwarders, bankers, private investigators, foreign factories, and more.";
$lang['viewed_companies'] = "Viewed Companies";
$lang['latest_shipping_activity'] = "Latest Shipping Activity";
$lang['previous_searches'] = "Previous Searches";
$lang['top_product_searches'] = "Top Product Searches";
$lang['results_found'] = "results found";

//metadescs
$lang['contact_metadesc'] = "ImportGenius can be reached by e-mail at info@importgenius.com, by phone at (888) 843-0272, or by live chat on this page.";
$lang['home_metadesc'] = "Searchable online database of U.S. customs records. Find out who your competitors buy from overseas, and who your suppliers sell to in the U.S.";
$lang['tour_metadesc'] = "Because we give you detailed information on every shipments entering the United States through an easy-to-use search interface. Find out who your competitors buy from, where your suppliers sell, and who else you should be working with overseas.";

//custom titles
$lang['contact_title'] = "Contact Us - Import Genius  : Contact ImportGenius at 888-843-0272 or by e-mail at info@importgenius.com";
