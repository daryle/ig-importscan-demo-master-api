$(document).ready(function()
{
	$('html').click(function(e){
		var target = $(e.target);
		
		if(target.is('#co_dropdown') == false && target.is('.dropdown') == false && target.is('.cicon') == false)
		{
			$('.co_lists').slideUp('fast');
		}
	})

	$(".co_dropdown a.active").click(function(e){
		e.preventDefault();
		$('.co_lists').slideToggle('fast');
	})

	$(".co_dropdown ul li a").click(function(){
		$('.co_lists').hide();
	})
	
	$('.co_dropdown').hide().fadeIn('fast');
	
});
