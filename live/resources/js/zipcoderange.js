$(document).ready(function(){

	var boolZipRangeDisable = false;
	var objZipRangeElem;
	
	initMain();
	
	function initMain()
	{
		initFilters();
		initAddBtn();
		initSubBtn();
		return;
	}

	function initAddBtn()
	{
		$(".addb").click(function(event) 
		{
			if ($('.fcond').length<100) 
			{ 
				reccursiveAddFilter();
				if($(".filters", $(this).parent()).val() == "ziprange")
				{
					setBitwiseLimit($(this).parent());
				}	
			}
		});
		return;
	}
	
	function initSubBtn()
	{
		$(".subb").click(function() 
		{
			removeFilter(this);
		});
		
		return;
	}
	
	function initFilters()
	{
		var boolChangeFlag = false;
	//	//console.log("START - initFilters");
		$(".filters").change(function() 
		{
			boolChangeFlag = false;
	
			if($(this).val() == "ziprange")
			{
				boolZipRangeDisable = true;
				boolChangeFlag = true;
				setBitwiseLimit($(this).parent());
			}
			else if(($("option[value='ziprange']", this).length > 0)  && boolZipRangeDisable && ($(this).val() != "ziprange"))
			{
				boolZipRangeDisable = false;
				boolChangeFlag = true;
				addZipRangeOption();
			}
			updateFilters(boolChangeFlag);
		});
		
		updateFilters(boolChangeFlag);
	//	//console.log("END - initFilters");
		return;
	}
	
	function setBitwiseLimit(elem)
	{
			var objParent = $(elem)
			var boolFilterPos = checkFilterPosition(objParent);
		
			var objCondSel = (boolFilterPos) ? $(".cond_select", objParent) : $(".cond_select", objParent.next());
			
			objCondSel.val("AND");
			objCondSel.attr('disabled', 'disabled');
			
			// To enable 
			$('.someElement').removeAttr('disabled');

			// OR you can set attr to "" 
			$('.someElement').attr('disabled', '');
			return;
	}
	
	function checkFilterPosition(elem)
	{
		var intConLen = $("#conditions>.fcond").length;
		
		var intConIdx = $("#conditions>.fcond").index($(elem));
		
		if((intConLen > 1) && (parseInt(intConIdx+1, 10) == intConLen))
		{
			// fcond is the last on in the group
			return true;
		}
		
		return false;
	}
	
	function updateFilters(boolChange)
	{
		if(boolChange = null) boolChange = false;

		checkZipRange();
		
		if(((checkZipRangeExists() == true)  && (boolZipRangeDisable==boolChange)) && (checkZipRangeExists(false) == false))
		{
			boolZipRangeDisable = false;
			//console.log("Adding ZipRange...");
			addZipRangeOption();
		}
		return;
	}
	
	function checkZipRange()
	{
		if(boolZipRangeDisable)
		{
			//console.log("Removing ZipRange...");
			removeZipRangeOption()
		}
		return;
	}
	
	function reccursiveAddFilter()
	{
		// Clone Filter Element
		var nelem = $('#clonebank>.fcond').clone().appendTo('#conditions'); 
		
		checkZipRange();
		
		// Bind Add New filter button to new element
		$(".addb", nelem).click(function(event) 
		{
			reccursiveAddFilter();
			
			checkZipRange();
			initFilters();
			
			if($(".filters", $(this).parent()).val() == "ziprange")
			{
				setBitwiseLimit($(this).parent());
			}	
			
		});
		
		// Bind Remove filter button to new element
		$(".subb", nelem).click(function(event) 
		{
			removeFilter(this);
			
			checkZipRange();
			initFilters();
		});
		hideFirst(); 
		return;
	}
	
	function removeFilter(elem)
	{
		if ($('.fcond').length>1) 
		{ 
			var boolChangeFlag = false;
			$(elem).parent().remove(); 
			//console.log('START - removeFilter');
			$(".filters", $(elem).parent()).each(function(idx, selElem) 
			{
				boolChangeFlag = false;
				//console.log(selElem);
				//console.log($(selElem).val());
				
				if($(selElem).val() == "ziprange")
				{
					boolZipRangeDisable = false;
					addZipRangeOption();
				}
			});
			$(elem).parent().remove(); 
			hideFirst(); 
			initSubBtn();
		}
			//console.log('END - removeFilter');
		return;
	}

	function removeZipRangeOption()
	{
		//console.log("START - removeZipRangeOption");
		// Remove all Zip Range on Conditions container
		$.each( $("#conditions"), function(i, elem) 
		{
			   $(".filters option[value='ziprange']", elem).each(function(idx, element) 
			   {
					if($(element).is(":selected") != true)
				   {
						if(objZipRangeElem == null)
						{
							objZipRangeElem = $(element).clone();
						}
						
						$(element).remove();
					}
			   });
		});	
	//	//console.log("END - removeZipRangeOption");
		return;
	}	
	
	function addZipRangeOption()
	{
		//console.log("START -  addZipRangeOption");
		$.each( $("#conditions"), function(i, divElem) 
		{
				var intSelCount = 0;
				var intSelElemCount = 0;
			   $(".filters", divElem).each(function(idx, selectElem) 
			   {
					intSelCount++;
					//console.log("intSelCount: " + intSelCount);
					if($("option[value='ziprange']", selectElem).length <= 0)
					{
						intSelElemCount++
						//console.log("intSelElemCount: " + intSelElemCount);
						$(objZipRangeElem).clone().appendTo(selectElem);
					}	
			   });
		});	
		
		// Re-enable bitwise operators
		$("#conditions>.fcond>.cond_select").removeAttr('disabled');
		//console.log("END -  addZipRangeOption");
		return;
	}
	
	function checkZipRangeExists(boolContAllFlag)
	{
		if(boolContAllFlag = null) boolContAllFlag = true;
		var boolZipRangeFlag = false;
		var intZipCounter = 0;
		$.each( $("#conditions"), function(i, elem) 
		{
			   $(".filters option[value='ziprange']", elem).each(function(idx, element) 
			   {
					if($(element).is(":selected") != boolContAllFlag)
				   {
						intZipCounter++;
					}
			   });
		});	
		//console.log("intZipCounter: " + intZipCounter);
		if(intZipCounter > 0)
		{
			boolZipRangeFlag = true;
		}

		return boolZipRangeFlag;
	}
	
});