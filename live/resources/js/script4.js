$('.alpha').alphanumeric();
$('.alpha2').alphanumeric({allow: '.-,* ', allowpaste: true});
$('.numeric').numeric();

$('.email').alphanumeric({allow: '.@-_', allowpaste: true});


var defTo = 10; //default timeout for all ajax
var flexTo = 300; //search timeout
var exTo = 3600; //export timeout
var lpid = 0;

var Cookie = {
    set: function(cName, cValue) {
        document.cookie = cName + "=" + cValue + ";path=/";
    },
    get: function(cName) {
        thisCookie = document.cookie.split("; ");
        for (i = 0; i < thisCookie.length; i++)
        {
            if (cName == thisCookie[i].split("=")[0])
            {
                return thisCookie[i].split("=")[1];
            }
        }
        return 0;
    },
    del: function(cName) {
        var cookie_date = new Date();  // current date & time
        cookie_date.setTime(cookie_date.getTime() - 1);

        document.cookie = cName += "=; expires=" + cookie_date.toGMTString() + ";path=/";
    },
    clear: function() {
        var cookie_date = new Date();  // current date & time
        cookie_date.setTime(cookie_date.getTime() - 1);
        document.cookie = "expires=" + cookie_date.toGMTString() + ";path=/";
    }
}

function doDateCheck(from, to) {
    if (!from || !to)
        return true;
    var sd = Date.parse(from);
    var ed = Date.parse(to);
    if (sd <= ed)
    {
        var diff = (ed - sd) / (1000 * 60 * 60 * 24);
        if (diff > 366)
        {
            //alert('Date beyond 366 days range. \n Adjust From or To fields to enable selected date');
            return false;
        }
        return true;
    }
    else
    {
        return false;
    }
}

function onSelect(cal, date)
{
    var p = cal.params;
    var update = (cal.dateClicked);

    if (!update)
        return false;

    if (update && p.inputField) {
        p.inputField.value = cal.date.print(p.ifFormat);
        if (typeof p.inputField.onchange == "function")
            p.inputField.onchange();
    }

    if (cal.dateClicked)
        cal.callCloseHandler();
}



function disallowDate(d, s) {


    d += '';

    var date = Date.parse(d.substring(0, 15));

    if (date > tlimit)
    {
        return true;
    }

    if (date < flimit)
    {
        return true;
    }

    if (s == 1)
    {
        if (date < Date.parse($('.from').val()))
            return true;
    }
    /*
     var tday = new Date();
     if (tlimit)
     {
     if (date>tlimit) return false;
     }
     if (date>tday) return false;
     if (date<flimit) return false;
     return false;
     */
}
;

function disallowDate2(d, s)
{
    d += '';

    var date = Date.parse(d.substring(0, 15));

    if (date > tlimit_ex)
    {
        return true;
    }

    if (date < flimit_ex)
    {
        return true;
    }

    if (s == 1)
    {
        if (date < Date.parse($('.from_ex').val()))
            return true;
    }
}
;

if ($.fn.DatePicker) {

    $('.from').DatePicker(
            {
                calendars: 1
                , format: 'm/d/Y'
                , current: $('.from').val()
                , onBeforeShow: function() {
                    $('.from').DatePickerSetDate($('.from').val(), true)
                            .DatePickerLayout()
                            ;
                }
                , onChange: function(f)
                {
                    $('.from').val(f);
                    $('.from').DatePickerHide();
                }
                , onRender: function(date) {
                    return {
                        disabled: disallowDate(date, 0)
                    }
                }

            }
    )
            .attr("readonly", true)
            ;

    $('.to').DatePicker(
            {
                calendars: 1
                , format: 'm/d/Y'
                , current: $('.to').val()
                , onBeforeShow: function() {
                    $('.to').DatePickerSetDate($('.to').val(), true);
                }
                , onChange: function(f)
                {
                    $('.to').val(f);
                    $('.to').DatePickerHide();

                }
                , onRender: function(date) {
                    return {
                        disabled: disallowDate(date, 1)
                    }
                }

            }
    )
            .attr("readonly", true)
            ;

    $('.from_ex').DatePicker(
            {
                calendars: 1
                , format: 'm/d/Y'
                , current: $('.from_ex').val()
                , onBeforeShow: function() {
                    $('.from_ex').DatePickerSetDate($('.from_ex').val(), true)
                            .DatePickerLayout()
                            ;
                }
                , onChange: function(f)
                {
                    $('.from_ex').val(f);
                    $('.from_ex').DatePickerHide();
                }
                , onRender: function(date) {
                    return {
                        disabled: disallowDate2(date, 0)
                    }
                }

            }
    )
            .attr("readonly", true)
            ;

    $('.to_ex').DatePicker(
            {
                calendars: 1
                , format: 'm/d/Y'
                , current: $('.to_ex').val()
                , onBeforeShow: function() {
                    $('.to_ex').DatePickerSetDate($('.to_ex').val(), true);
                }
                , onChange: function(f)
                {
                    $('.to_ex').val(f);
                    $('.to_ex').DatePickerHide();

                }
                , onRender: function(date) {
                    return {
                        disabled: disallowDate2(date, 1)
                    }
                }

            }
    )
            .attr("readonly", true)
            ;
}

function resetform(from) //clear form
{
    var form = $(from).parents('form');
    $(form).trigger('reset');
    $('.fields', form).show();
    $('button[type=button]:first', form).trigger('focus');
    $('.response', form).empty();
    return false;
}

function showform(from) //show form if unsuccessful
{
    var form = $(from).parents('form');
    $('.fields', form).show();
    $('button[type=button]:first', form).trigger('focus');
    $('.response', form).empty();
    return false;
}

function deleteSearch(svid)
{

    if (!svid)
    {
        alert('Please select a saved search');
        return false;
    }


    if (!confirm('Delete saved search?'))
        return false;

    var dt = [{name: "sid", value: svid}];

    $("#flexsave").flexOptions({params: dt}).flexReload();

}

function deleteSub(svid)
{

    if (!svid)
    {
        alert('Please select a subscription');
        return false;
    }


    if (!confirm('Delete subscription?'))
        return false;

    var dt = [{name: "sid", value: svid}, {name: "cmd", value: "delete"}];

    $("#flexsub").flexOptions({params: dt}).flexReload();

}

function statSub(sobj)
{


    var dt = [{name: "sid", value: sobj.value}, {name: "cmd", value: "statchange"}, {name: "status", value: sobj.checked}];

    $("#flexsub").flexOptions({params: dt}).flexReload();

}

var cancelexp = 0;
var exdone = 0;

$('#progcancel').click
        (
                function()
                {
                    if (cancelexp)
                        return true;
                    cancelexp = 1;
                    $('b', this).text("Cancelling, please wait...");
                    $.get(mod + 'export/cancel/' + pid);
                }
        );

function getStatus()
{
    //if(typeof country == "undefined")
    //	country = "";

    if (pid == 0)
        return true;
    //return true;
    $.ajax({
        type: 'POST',
        url: mod + 'export/status2/' + pid,
        dataType: 'json',
        success: function(data) {
            procJSON(data, '#exform');
        },
        timeout: (flexTo * 1000),
        complete: function(data, textStatus) {
            if (textStatus == 'timeout')
                onTimeout(action, form);
        },
        error: function(data)
        {
            try
            {
                //onError(data);
            }
            catch (e)
            {
            }
            var naction = 'continue2';
            procJSON({'action': naction}, '#exform');
        }
    });

}

var eto = 0;
var statTo = 3000;



function procJSON(json, form)
{
    var rs = '';

    if (json.response)
        rs = json.response;

    //Compute Results
    $('.pLast').show();

    try
    {
        if (json.rows.length > 0)
        {

            var xrp = json.rows.length;
            var xtotal = json.total;
            var xtpage = Math.round(xtotal / xrp);

            if (xtpage > 200)
                $('.pLast').hide();

        }
    }
    catch (e)
    {

    }
    switch (json.action)
    {
        case 'continue2':

            if (pid == 0 && json.pid > 0)
            {
                if ($('span.exb b').length == 0)
                {

                    $('span.exb')
                            .append(' <b>1</b>');

                }
            }



            if (!exdone && !cancelexp) //not done or canceled
            {


                pid = json.pid;

                $('#exform').hide();
                $('.prog_div').show();
                $('.progbar-inner').width(json.percent + "%");

                clearTimeout(eto);
                eto = setTimeout("getStatus()", statTo);

                $('#exstatus').html(rs);

            } else if (cancelexp) { //if canceled

                pid = 0;
                cancelexp = 0;
                $('#progcancel b').text("Cancel");
                $('span.exb b').remove();
                $('#exform .response').empty();
                $('#exform .fields').show();
                $('#exform').show();

                $('.prog_div').hide();

            }

            return false;
            break;

        case 'newcard':
            location.href = root + 'newcard';
            break;

        case 'reset':
            rs += "<br /><p><button type='button' class='button' onclick='return resetform(this)'><span><b>Submit again?</b></span></button></p>";
            break;

        case 'retry':
            rs += "<br /><p><button type='button' class='button' onclick='return showform(this)'><span><b>Submit again?</b></span></button></p>";
            break;

        case 'saverep':
            rs += "<br /><p><button type='button' class='button' onclick='return resetform(this)'><span><b>Submit again?</b></span></button></p>";
            $('#flexsave').flexReload();
            break;

        case 'savesub':
            rs += "<br /><p><button type='button' class='button' onclick='return resetform(this)'><span><b>Submit again?</b></span></button></p>";
            $('#flexsub').flexReload();
            break;

        case 'closereset':
            rs += "<br /><p><button type='button' class='button' onclick='$(\"#TB_closeWindowButton\").trigger(\"click\"); resetform(this); return false;'><span><b>Close</b></span></button></p>";
            $('#flexsave').flexReload();
            break;

        case 'closesub':
            rs += "<br /><p><button type='button' class='button' onclick='$(\"#TB_closeWindowButton\").trigger(\"click\"); resetform(this); return false;'><span><b>Close</b></span></button></p>";
            $('#flexsub').flexReload();
            break;

        case 'reload':
            self.location.reload();
            break;

        case 'exdone':

            exdone = 1;
            lpid = json.pid;

            rs += "<p>";
            rs += "<button type='button' class='button' onclick='resetExport(); xpid = " + json.pid + ";$(\".etabs a:eq(1)\").trigger(\"click\");'><span><b>Download or Email this file</b></span></button>";
            rs += " <button type='button' id='generate-again-b' class='button' onclick='resetExport();'><span><b>Generate another?</b></span></button>";
            rs += "</p>";

            $('.response', form).html(rs);

            $('.prog_div').hide();
            $('#exform').show();
            //$('#exform .fields').hide();

            if ($('#exform:visible').length)
            {
                if (lpid)
                {
                    $.get(mod + 'export/viewed/' + lpid);
                    $('span.exb b').remove();
                    lpid = 0;
                }
            }

            return false;

            break;

        case 'exmail':
            exdone = 1;
            lpid = pid;
            xpid = 0;
            pid = 0;

            rs += "<p><button type='button' id='generate-again-b' class='button' onclick='$(\".etabs a:eq(0)\").trigger(\"click\");'><span><b>Generate another?</b></span></button></p>";

            $('.prog_div').hide();

            $('#exform').show();
            $('#exform .fields').hide();

            $('.exmail').show();
            $('#ehistory').flexigrid(ehis).flexReload();


            $('#last-export').html(rs);
            $('.etabs a:eq(1)').trigger('click');

            if ($('#last-export:visible').length)
            {
                if (lpid)
                {
                    $.get(mod + 'export/viewed/' + lpid);
                    $('span.exb b').remove();
                    lpid = 0;
                }
            }

            resetExport();

            return false;

            break;

        case 'newvmap':

            rs += "<button type='button' onclick='vstart=0; return showform(this);' class='button nsearch'><span><b>New Search</b></span></button>";

            $('.resultindex,#resultbar-inner').empty();

            var ri = '';
            var nctype = $('select[name=ctype]').val();

            if (json.db.length)
            {
                for (x = 0; x < json.db.length; x++)
                {
                    ri += "<div class='ri'><a href='#' rel='" + json.db[x].id + "'><span>" + json.db[x].name + " <br />" + json.db[x].nodes + " Shipment(s) </span></a></div>";
                }

                vend = +json.start + 10;
                if (vend > json.limit)
                    vend = json.limit;

                $('#resultbar-inner').html("Displaying <b>" + (+json.start + 1) + "</b> to <b>" + vend + "</b> of <b>" + json.limit + "</b> results. Did you mean one of the following results?");


            }

            vstart = +json.start;
            vlimit = +json.limit;

            ri += "<div class='rindexcontrol'>";
            if (vstart > 0)
                ri += "<button type='button' onclick='vstart=" + (+vstart - 10) + "; $(\".fsearch\").trigger(\"submit\"); return false;' class='button'><span><b>Back</b></span></button> ";
            if ((vstart + 10) < vlimit)
                ri += "<button type='button' onclick='vstart=" + (+vstart + 10) + "; $(\".fsearch\").trigger(\"submit\"); return false;' class='button'><span><b>Next</b></span></button>";
            ri += "</div>";

            $('.resultindex').html(ri);
            $('.ri a').click
                    (
                            function()
                            {

                                if (vloading)
                                    return false;

                                $('.ri a.selected').removeClass('selected');
                                $(this).addClass('selected');
                                this.blur();

                                //if(country == "") country = "us";


                                $('#nwb').attr('href', mod + 'vmap/render/' + country + '/' + nctype + '/' + this.rel).show();

                                page = 0;
                                jroot = this.rel;
                                ctype = nctype;
                                getJSON();

                                return false;
                            }
                    );

            $('.ri a:first').trigger('click');

            break;


        case 'newsearch':
            rs += "<button type='button' onclick='return showform(this);' class='button nsearch'><span><b>New Search</b></span></button>";
            
            if ($('.fsearch div.response').html())
                $('.fsearch div.response').html(rs);
            //$('#sresults').show();

            sid = json.sid;

            if (sid)
            {
                //load sid in params
                var dt = [{name: "sid", value: sid}];

                $("#flex1").flexOptions({params: dt});
            }


            try
            {
                $('#nstotal').html(json.nstotal);
                $('#nsleft').html(json.nsleft.toString());
            } catch (e) {
            }

            if (json.from)
                $('#daterange input.from').val(json.from);
            if (json.to)
                $('#daterange input.to').val(json.to);

            if (json.total)
            {
                //$('span.svb, span.exb').fadeTo(0,1);
                $('span.exb, span.svb').removeClass('grayed');

                Alibaba.offer(json);
            }

            var tnto = json.total;

            if (tnto > 100)
            {
                tnto = 100;
                $('#rec1').attr('checked', 'checked');
            }
            else
            {
                $('#rec0').attr('checked', 'checked');
            }

            $('#nfname').val("");
            $('#nfrom').val(1);
            $('#nto').val(tnto);
            $('#exform .fields').show();
            $('.etabs a:eq(0)').trigger('click');


            return json;

            break;

        case 'limitreach':
            rs += "<br/><button type='button' class='button' onclick='upgrade(); return false;'><span><label><b>Upgrade my account</b></label></span></button>";
            $('.fsearch div.response').html(rs);
            //$('#sresults').show();
            break;

    }

    $('.response', form).html(rs);
    //$('.response button',form).trigger('focus');

}


function resetExport()
{
    showform("#ge-submit");
//showform($(".exmail"));
//$(".exmail").hide();
}

function onTimeout(action, form)
{
    switch (action)
    {
        default:
            {
                var rs = "<div>Your connection timed-out.</div>";
                rs += "<br /><p><button type='button' class='button' onclick='return showform(this)'><span><b>Submit again?</b></span></button></p>";
                break;
            }
    }

    $('.response', form).html(rs);

}

function resetGrid()
{
    $('#flex1 a, #flex1 tr').unbind();
    $('#flex1').empty();
}

function onError(data)
{

    if ($.trim(data.responseText) == '')
        return true;

    $('#prog').val(0);
    if (data.responseText)
        $('.errorcontent').html(data.responseText);
    else
        $('.errorcontent').html(data);

    $('.errorbox').show();
}


$('.form').submit(
        function()
        {
            var form = this;
            if (!validate(form))
                return false;
            var action = $("input[name='action']", form).val();

            var dt = $(this).serializeArray();
            dt[dt.length] = {name: 'sid', value: sid};

            var ndefTo = defTo;



            if (action == 'export/email')
            {
                if (filterE($(".email", this).val()) == false)
                {
                    $('.response', this).html('Please enter a valid email');
                    return false;
                }
                $("input[name='exf']", this).val($("#edata input:checked").val());

                action += "/" + $('#email-pid').val();

                dt = $(this).serializeArray();
                ndefTo = exTo; //override default timeout for export generation


            }
            else if (action == 'main/generate')
            {
                var dt2 = $('.fsearch').serializeArray();

                if (dt2.length)
                {
                    for (var pi = 0; pi < dt2.length; pi++)
                        dt[dt.length] = dt2[pi];
                }



                dt[dt.length] = {name: 'sortname', value: $('.sorted').attr('abbr')};
                dt[dt.length] = {name: 'sortorder', value: $('.sorted div').attr('class').substr(1)};
                ndefTo = exTo; //override default timeout for export generation



            }
            else if (action == 'main/generate2')
            {
                var dt2 = $('.fsearch').serializeArray();

                if (dt2.length)
                {
                    for (var pi = 0; pi < dt2.length; pi++)
                        dt[dt.length] = dt2[pi];
                }

                dt[dt.length] = {name: 'sortname', value: $('.sorted').attr('abbr')};
                dt[dt.length] = {name: 'sortorder', value: $('.sorted div').attr('class').substr(1)};
                ndefTo = exTo; //override default timeout for export generation
            }
            else if (action == 'export/generate')
            {
                exdone = 0;
                var dt2 = $('.fsearch').serializeArray();

                if (dt2.length)
                {
                    for (var pi = 0; pi < dt2.length; pi++)
                        dt[dt.length] = dt2[pi];
                }



                dt[dt.length] = {name: 'sortname', value: $('.sorted').attr('abbr')};

                dt[dt.length] = {name: 'sortorder', value: $('.sorted div').attr('class').substr(1)};
                ndefTo = exTo; //override default timeout for export generation
                $('.progbar-inner').width("0%");
                cancelexp = 0;
                clearTimeout(eto);


                //eto = setTimeout("getStatus()",statTo);
            }

            $('.fields', this).slideUp('fast');
            $('.response', this).html("<div class='load exloading'>Processing, please wait...</div>");

            $.ajax({
                type: 'POST',
                url: mod + action,
                data: dt,
                dataType: 'json',
                success: function(data) {
                    procJSON(data, form);
                },
                timeout: (ndefTo * 1000),
                complete: function(data, textStatus) {
                    if (textStatus == 'timeout')
                        onTimeout(action, form);
                },
                error: function(data)
                {
                    try
                    {
                        onError(data);
                    }
                    catch (e)
                    {
                    }
                    var naction = 'retry';
                    procJSON({'action': naction}, form);

                }
            });

            return false;
        }
);

function retryExport(id)
{
    if (pid != 0)
    {
        if (!confirm("This will cancel current export, Do you wish to proceed?"))
        {
            return false;
        }
    }

    $.get(mod + 'export/retry/' + id + '/' + pid,
            function(data)
            {
                if (data.action == 'continue2')
                {
                    pid = data.pid;
                    exdone = 0;
                    continueExport();
                    $('.etabs a:eq(0)').trigger('click');
                }
            }
    );

    return false;
}

function continueExport()
{
    $('.progbar-inner').width("0%");
    cancelexp = 0;
    $('#exform').hide();
    $('.exmail').hide();
    $('#exstatus').html("Processing, please wait...");
    $('span.exb b').remove();
    $('span.exb')
            .append(' <b>1</b>')
            .removeClass('grayed');

    getStatus();
}

var fht = 320;

fht = $(window).height() - 362;

if (fht < 320)
    fht = 320;


var flexopt = {
    dataType: 'json',
    width: 'auto',
    url: mod + 'main/maketable2',
    colModel: [
        {display: 'View', width: 60, name: 'entryid', sortable: true, align: 'center'},
        {display: 'Product Description', name: 'product_s', width: 200, sortable: true, align: 'left', hide: false},
        {display: 'Consignee', name: 'consname_s', width: 180, sortable: true, align: 'left'},
        {display: 'Shipper', name: 'shipname_s', width: 120, sortable: true, align: 'left'},
        {display: 'Arrival Date', name: 'actdate', width: 80, sortable: true, align: 'center'},
        {display: 'Gross Weight (LB)', name: 'weight', width: 100, sortable: true, align: 'right', hide: false},
        {display: 'Gross Weight (KG)', name: 'weight', width: 100, sortable: true, align: 'right', hide: true},
        {display: 'Quantity', name: 'manifestqty', width: 100, sortable: true, align: 'right', hide: false},
        {display: 'Foreign Port', name: 'fport_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'US Port', name: 'uport_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'Carrier Code', name: 'carriercode_s', width: 120, sortable: true, align: 'left', hide: true},
        {display: 'Vessel Name', name: 'vesselname_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'Country of Origin', name: 'countryoforigin_s', width: 140, sortable: true, align: 'left', hide: false},
        {display: 'Marks &amp; Numbers', name: 'mark_s', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'Consignee Address', name: 'consaddr', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Shipper Address', name: 'shipaddr', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Zip Code', name: 'zipcode_s', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'No. of Containers', name: 'con_num_count', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'Container Number', name: 'containernum', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Distribution Port', name: 'usdisport_s', width: 120, sortable: false, align: 'left', hide: false},
        {display: 'House vs Master', name: 'masterbillofladingind_s', width: 120, sortable: true, align: 'left'},
        {display: 'Master B/L', name: 'masterbilloflading_s', width: 120, sortable: true, align: 'left'},
        {display: 'In-Bond Entry Type', name: 'inboundentrytype_s', width: 240, sortable: true, align: 'left'},
        {display: 'Carrier Code / Name', name: 'carriercode', width: 120, sortable: false, align: 'left'},
        {display: 'Carrier Address', name: 'carrier_address', width: 120, sortable: false, align: 'left'},
        {display: 'Place of Receipt', name: 'placereceipt', width: 120, sortable: false, align: 'left', hide: false},
        {display: 'Notify Name', name: 'ntfyname', width: 120, sortable: false, align: 'left', hide: true},
        {display: 'Notify Address', name: 'ntfyaddr', width: 120, sortable: false, align: 'left', hide: true}
    ],
    buttons: [
        {name: 'Export/Email Report', bclass: 'exb', onpress: callExport}
        , {name: 'Save Search', bclass: 'svb', onpress: callSave}
        , {name: 'Create Email Alert', bclass: 'svb svb2', onpress: callSub}
        , {separator: true}
        , {name: 'Modify Display Fields', bclass: 'tgb', onpress: function() {
                $('a.togclick').trigger('click');
            }}
    ],
    blockOpacity: 0.7,
    sortname: "actdate",
    sortorder: "desc",
    usepager: true,
    rpOptions: [10, 15, 20, 25, 40, 100],
    useRp: true,
    rp: 15,
    autoload: false,
    onToggleCol: togback,
    onSuccess: tkbox,
    preProcess: procJSON,
    onError: onError,
    showToggleBtn: false,
    minheight: 320,
    timeout: flexTo,
    onTimeout: flexTimeout,
    height: fht,
    pagestat: 'Displaying {from} to {to} of {total} shipment records for this search term'
};



var flexopt2 = {
    dataType: 'json',
    width: 'auto',
    url: mod + 'main/maketable2',
    colModel: [
        {display: 'View', width: 60, name: 'entryid', sortable: false, align: 'center'},
        {display: 'Product Description', name: 'product_s', width: 200, sortable: false, align: 'left', hide: false},
        {display: 'Consignee', name: 'consname_s', width: 180, sortable: false, align: 'left'},
        {display: 'Shipper', name: 'shipname_s', width: 120, sortable: false, align: 'left'},
        {display: 'Arrival Date', name: 'actdate', width: 80, sortable: false, align: 'center'},
        {display: 'Gross Weight (LB)', name: 'weight', width: 100, sortable: false, align: 'right', hide: false},
        {display: 'Gross Weight (KG)', name: 'weight', width: 100, sortable: false, align: 'right', hide: true},
        {display: 'Quantity', name: 'manifestqty', width: 100, sortable: false, align: 'right', hide: false},
        {display: 'Foreign Port', name: 'fport_s', width: 120, sortable: false, align: 'left', hide: false},
        {display: 'US Port', name: 'uport_s', width: 120, sortable: false, align: 'left', hide: false},
        {display: 'Carrier Code', name: 'carriercode_s', width: 120, sortable: false, align: 'left', hide: true},
        {display: 'Vessel Name', name: 'vesselname_s', width: 120, sortable: false, align: 'left', hide: false},
        {display: 'Country of Origin', name: 'countryoforigin_s', width: 140, sortable: false, align: 'left', hide: false},
        {display: 'Marks', name: 'mark_s', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Consignee Address', name: 'consaddr', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Shipper Address', name: 'shipaddr', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Zip Code', name: 'zipcode_s', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'No. of Containers', name: 'con_num_count', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Container Number', name: 'containernum', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Distribution Port', name: 'usdisport_s', width: 120, sortable: false, align: 'left', hide: false},
        {display: 'House vs Master', name: 'masterbillofladingind_s', width: 120, sortable: false, align: 'left'},
        {display: 'Master B/L', name: 'masterbilloflading_s', width: 120, sortable: false, align: 'left'},
        {display: 'In-Bond Entry Type', name: 'inboundentrytype_s', width: 240, sortable: false, align: 'left'},
        {display: 'Carrier Code / Name', name: 'carriercode', width: 120, sortable: false, align: 'left'},
        {display: 'Carrier Address', name: 'carrier_address', width: 120, sortable: false, align: 'left'},
        {display: 'Place of Receipt', name: 'placereceipt', width: 120, sortable: false, align: 'left'},
        {display: 'Notify Name', name: 'ntfyname', width: 120, sortable: false, align: 'left', hide: true},
        {display: 'Notify Address', name: 'ntfyaddr', width: 120, sortable: false, align: 'left', hide: true}
    ],
    buttons: [
        {name: 'Export/Email Report', bclass: 'exb', onpress: function() {
                $('.promo3').trigger('click');
            }}
        , {name: 'Save Search', bclass: 'svb', onpress: function() {
                $('.promo3').trigger('click');
            }}
        , {name: 'Create Email Alert', bclass: 'svb svb2', onpress: function() {
                $('.promo3').trigger('click');
            }}
        , {separator: true}
        , {name: 'Modify Display Fields', bclass: 'tgb', onpress: function() {
                $('a.togclick').trigger('click');
            }}
    ],
    blockOpacity: 0.7,
    sortname: "actdate",
    sortorder: "desc",
    usepager: true,
    rpOptions: [10, 15, 20, 25, 40, 100],
    useRp: true,
    rp: 15,
    autoload: false,
    onToggleCol: togback,
    onSuccess: tkbox,
    preProcess: procJSON,
    onError: onError,
    showToggleBtn: false,
    minheight: 320,
    timeout: flexTo,
    onTimeout: flexTimeout,
    height: fht,
    pagestat: 'Displaying {from} to {to} of {total} shipment records for this search term'
};

var flexopt3 = {
    dataType: 'json',
    width: 'auto',
    url: mod + 'main/maketable2',
    colModel: [
        {display: 'View', width: 60, name: 'entryid', sortable: true, align: 'center'},
        {display: 'Product Description', name: 'product_s', width: 200, sortable: true, align: 'left', hide: false},
        {display: 'Consignee', name: 'consname_s', width: 180, sortable: true, align: 'left'},
        {display: 'Shipper', name: 'shipname_s', width: 120, sortable: true, align: 'left'},
        {display: 'Arrival Date', name: 'actdate', width: 80, sortable: true, align: 'center'},
        {display: 'Gross Weight (LB)', name: 'weight', width: 100, sortable: true, align: 'right', hide: false},
        {display: 'Gross Weight (KG)', name: 'weight', width: 100, sortable: true, align: 'right', hide: true},
        {display: 'Quantity', name: 'manifestqty', width: 100, sortable: true, align: 'right', hide: false},
        {display: 'Foreign Port', name: 'fport_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'US Port', name: 'uport_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'Carrier Code', name: 'carriercode_s', width: 120, sortable: true, align: 'left', hide: true},
        {display: 'Vessel Name', name: 'vesselname_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'Country of Origin', name: 'countryoforigin_s', width: 140, sortable: true, align: 'left', hide: false},
        {display: 'Marks', name: 'mark_s', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'Consignee Address', name: 'consaddr', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Shipper Address', name: 'shipaddr', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Zip Code', name: 'zipcode_s', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'No. of Containers', name: 'con_num_count', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'Container Number', name: 'containernum', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Distribution Port', name: 'usdisport_s', width: 120, sortable: false, align: 'left', hide: false},
        {display: 'House vs Master', name: 'masterbillofladingind_s', width: 120, sortable: true, align: 'left'},
        {display: 'Master B/L', name: 'masterbilloflading_s', width: 120, sortable: true, align: 'left'},
        {display: 'In-Bond Entry Type', name: 'inboundentrytype_s', width: 240, sortable: true, align: 'left'},
        {display: 'Carrier Code / Name', name: 'carriercode', width: 120, sortable: false, align: 'left'},
        {display: 'Carrier Address', name: 'carrier_address', width: 120, sortable: false, align: 'left'},
        {display: 'Place of Receipt', name: 'placereceipt', width: 120, sortable: false, align: 'left'},
        {display: 'Notify Name', name: 'ntfyname', width: 120, sortable: false, align: 'left', hide: true},
        {display: 'Notify Address', name: 'ntfyaddr', width: 120, sortable: false, align: 'left', hide: true}
    ],
    buttons: [
        {name: 'Export/Email Report', bclass: 'exb', onpress: function() {
                $('.promo3').trigger('click');
            }}
        , {name: 'Save Search', bclass: 'svb', onpress: callSave}
        , {name: 'Create Email Alert', bclass: 'svb svb2', onpress: callSub}
        , {separator: true}
        , {name: 'Modify Display Fields', bclass: 'tgb', onpress: function() {
                $('a.togclick').trigger('click');
            }}
    ],
    blockOpacity: 0.7,
    sortname: "actdate",
    sortorder: "desc",
    usepager: true,
    rpOptions: [10, 15, 20, 25, 40, 100],
    useRp: true,
    rp: 15,
    autoload: false,
    onToggleCol: togback,
    onSuccess: tkbox,
    preProcess: procJSON,
    onError: onError,
    showToggleBtn: false,
    minheight: 320,
    timeout: flexTo,
    onTimeout: flexTimeout,
    height: fht,
    pagestat: 'Displaying {from} to {to} of {total} shipment records for this search term'
};

var lcountry = $('input[name=country]').val()
var countrymod = lcountry == "us" ? "main" : lcountry;
var datatype = '';

if ($('input[name=datatype]').val() == "ex")
{
    datatype = "ex";
}


var flexopt4 = {
    preProcess: function(data)
    {
//		console.log(data)
        //return data;
    },
    dataType: 'json',
    width: 'auto',
    url: mod + countrymod + '/maketable2',
    colModel: colModel,
    buttons:
            [
                {name: 'Export/Email Report', bclass: 'exb', onpress: callExport}
                , {name: 'Save Search', bclass: 'svb', onpress: callSave}
                , {name: 'Create Email Alert', bclass: 'svb svb2', onpress: callSub}
                , {separator: true}
                , {name: 'Modify Display Fields', bclass: 'tgb', onpress: function() {
                        $('a.togclick').trigger('click');
                    }}
            ],
    blockOpacity: 0.7,
    sortname: arivalDate,
    sortorder: "desc",
    usepager: true,
    rpOptions: [10, 15, 20, 25, 40, 100],
    useRp: true,
    rp: 15,
    autoload: false,
    onToggleCol: togback,
    onSuccess: tkbox,
    preProcess: procJSON,
            onError: onError,
    showToggleBtn: false,
    minheight: 320,
    timeout: flexTo,
    onTimeout: flexTimeout,
    height: fht,
    pagestat: 'Displaying {from} to {to} of {total} shipment records for this search term'
}

var flexopt5 = {
    dataType: 'json',
    width: 'auto',
    url: mod + 'main/maketable2',
    colModel: [
        {display: 'View', width: 60, name: 'entryid', sortable: true, align: 'center'},
        {display: 'Product Description', name: 'product_s', width: 200, sortable: true, align: 'left', hide: false},
        {display: 'Consignee', name: 'consname_s', width: 180, sortable: true, align: 'left'},
        {display: 'Shipper', name: 'shipname_s', width: 120, sortable: true, align: 'left'},
        {display: 'Arrival Date', name: 'actdate', width: 80, sortable: true, align: 'center'},
        {display: 'Gross Weight (LB)', name: 'weight', width: 100, sortable: true, align: 'right', hide: false},
        {display: 'Gross Weight (KG)', name: 'weight', width: 100, sortable: true, align: 'right', hide: true},
        {display: 'Quantity', name: 'manifestqty', width: 100, sortable: true, align: 'right', hide: false},
        {display: 'Foreign Port', name: 'fport_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'US Port', name: 'uport_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'Carrier Code', name: 'carriercode_s', width: 120, sortable: true, align: 'left', hide: true},
        {display: 'Vessel Name', name: 'vesselname_s', width: 120, sortable: true, align: 'left', hide: false},
        {display: 'Country of Origin', name: 'countryoforigin_s', width: 140, sortable: true, align: 'left', hide: false},
        {display: 'Marks', name: 'mark_s', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'Consignee Address', name: 'consaddr', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Shipper Address', name: 'shipaddr', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Zip Code', name: 'zipcode_s', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'No. of Containers', name: 'con_num_count', width: 100, sortable: true, align: 'left', hide: false},
        {display: 'Container Number', name: 'containernum', width: 100, sortable: false, align: 'left', hide: false},
        {display: 'Distribution Port', name: 'usdisport_s', width: 120, sortable: false, align: 'left', hide: false},
        {display: 'House vs Master', name: 'masterbillofladingind_s', width: 120, sortable: true, align: 'left'},
        {display: 'Master B/L', name: 'masterbilloflading_s', width: 120, sortable: true, align: 'left'},
        {display: 'In-Bond Entry Type', name: 'inboundentrytype_s', width: 240, sortable: true, align: 'left'},
        {display: 'Carrier Code / Name', name: 'carriercode', width: 120, sortable: false, align: 'left'},
        {display: 'Carrier Address', name: 'carrier_address', width: 120, sortable: false, align: 'left'},
        {display: 'Place of Receipt', name: 'placereceipt', width: 120, sortable: false, align: 'left'},
        {display: 'Notify Name', name: 'ntfyname', width: 120, sortable: false, align: 'left', hide: true},
        {display: 'Notify Address', name: 'ntfyaddr', width: 120, sortable: false, align: 'left', hide: true}
    ],
    buttons: [
        {name: 'Export/Email Report', bclass: 'exb', onpress: function() {
                $('.promo2').trigger('click');
            }}
        , {name: 'Save Search', bclass: 'svb', onpress: callSave}
        , {name: 'Create Email Alert', bclass: 'svb svb2', onpress: function() {
                $('.promo2').trigger('click');
            }}
        , {separator: true}
        , {name: 'Modify Display Fields', bclass: 'tgb', onpress: function() {
                $('a.togclick').trigger('click');
            }}
    ],
    blockOpacity: 0.7,
    sortname: "actdate",
    sortorder: "desc",
    usepager: true,
    rpOptions: [10, 15, 20, 25, 40, 100],
    useRp: true,
    rp: 15,
    autoload: false,
    onToggleCol: togback,
    onSuccess: tkbox,
    preProcess: procJSON,
    onError: onError,
    showToggleBtn: false,
    minheight: 320,
    timeout: flexTo,
    onTimeout: flexTimeout,
    height: fht,
    pagestat: 'Displaying {from} to {to} of {total} shipment records for this search term'
};

if (typeof useflex2 != 'undefined')
    flexopt = flexopt2;
if (typeof useflex3 != 'undefined')
    flexopt = flexopt3;
if (typeof useflex4 != 'undefined')
    flexopt = flexopt4;
if (typeof useflex5 != 'undefined')
    flexopt = flexopt4;

$('#flex1').flexigrid(flexopt);

//console.log(flexopt);

if (typeof useflex2 != 'undefined')
    setTimeout('trialsort()', 1000);

function trialsort() {
    $('#sresults .hDiv th div').click
            (
                    function()
                    {
                        $('.promo3').trigger('click');
                    }
            );
}

function flexTimeout()
{
    var rs = "<div class='error'>Your connection timed-out.</div>"
    rs += "<button type='button' onclick='return showform(this);' class='button nsearch'><span><b>Try Again?</b></span></button>";
    if ($('.fsearch div.response').html())
        $('.fsearch div.response').html(rs);

}

$(document).ready(
        function()
        {
            $('span.svb').addClass('grayed');
            if (pid)
                continueExport();

            $('input[name=splashOption]').click(function(e) {
                var publish = 0;
                var action = $(this).attr('alt');

                if (this.checked)
                    publish = 1;

                $.ajax({
                    url: action,
                    dataType: 'json',
                    data: {publish: publish},
                    type: 'POST',
                    success: function()
                    {

                    }
                })
            })

            $('.upgrade_now').click(function(e) {
                $('.cartpane').show();
                $('.splash-wrapper').hide();

            })

            if (getFilter)
                getFilters();


        }
);

var fltr = [];

function setFilter(dtype)
{
    var html = [];

    $.each(fltr, function(n, i) {
        if (n == "f_" + dtype)
        {
            $.each(i, function(x, y) {
                html.push("<option value=\"" + y.filter_name + "\">" + y.display + "</option>");
            })
        }
    });

    $('.filters').html(html.join(""));
    //$('.filters').trigger('liszt:updated');
}

function getFilters()
{
    $.get(root + 'iscan4l.php/filters/getFilters/' + country, function(data) {
        try
        {
            fltr = data;
            setFilter("im")
        }
        catch (e)
        {
        }
    })
}


function export_fields()
{
    $('input[name=export_fields]').val('');
    var export_fields = [];

    if (typeof country === 'undefined')
        country = 'us';

    if (country != "us")
    {
        $('input[name=vf]:checked').each(function() {
            var v = $(this).attr('class').split(' ');
            export_fields.push(v[0]);
        })

        $('input[name=export_fields]').val("'" + export_fields.join("','") + "'");
    }

}

var sid = 0;
var vstart = 0;
var vlimit = 0;

$('.fsearch').submit
        (
                function()
                {
                    //console.log(stype);

                    if (stype == 0) //New search
                    {
                        //validate query

                        var nogo = false;
                        var fcond = $('.fcond').length;

                        if (fcond <= 2)
                            $('.cond_select').val('AND');

                        $('.fsearch span:visible input.ntb').each
                                (
                                        function()
                                        {
                                            if (!$.trim(this.value).length)
                                            {
                                                alert('Please enter a valid ' + this.title);
                                                this.focus();
                                                nogo = true;
                                                return false;
                                            }

                                            if ($.trim(this.value) == "-")
                                            {
                                                alert('Please enter a valid ' + this.title);
                                                this.focus();
                                                nogo = true;
                                                return false;
                                            }
                                        }
                                );

                        if (nogo)
                            return false;

                        var dt = $('.fsearch').serializeArray();

                        sid = 0;

                        $("#flex1").flexOptions({params: dt});

                    }
                    else if (stype == 1) //Visual Map search
                    {

                        if (vloading)
                            return false;

                        var ctype = $('select[name=ctype]').val();
                        var cname = $('input[name=cname]').val();
                        var dtype_map = $('input[name=datatype_map]:checked').val();

                        if (typeof dtype_map == "undefined")
                        {
                            dtype_map = "im";
                        }

                        if (!$.trim(cname).length)
                        {
                            alert('Please enter a valid company name');
                            $('input[name=cname]').trigger('focus');
                            return false;
                        }


                        $('.fields', this).hide();
                        $('.response', this).html("<div class='load exloading'>Processing, please wait...</div>");

                        var form = this;
                        var country = $('input[name=country]').val();
                        if (vstart < 0)
                            vstart = 0;

                        $('.resultindex').empty();

                        var dt = [
                            {name: "ctype", value: ctype},
                            {name: 'cname', value: cname},
                            {name: 'start', value: vstart},
                            {name: 'country', value: country},
                            {name: 'dtype', value: dtype_map}
                        ];
                        $.ajax({
                            type: 'POST',
                            url: mod + 'vmap/makevmap',
                            data: dt,
                            dataType: 'json',
                            success: function(data) {
                                procJSON(data, form);
                            },
                            timeout: (flexTo * 1000),
                            complete: function(data, textStatus) {
                                if (textStatus == 'timeout')
                                    flexTimeout();
                            },
                            error: function(data)
                            {
                                try
                                {
                                    procJSON(data, form);
                                }
                                catch (e)
                                {
                                }
                                var naction = 'retry';
                                procJSON({'action': naction}, form);
                                onError(data);

                            }
                        });


                        return false;
                    }
                    else if (stype == 3) //Saved search
                    {

                        if ($('#flexsave tr.trSelected').length == 0)
                        {
                            alert('Please select a saved search');
                            return false;
                        }

                        var ssid = $('#flexsave tr.trSelected').attr('id').substr(3);

                        var dt = [{name: "sid", value: ssid}];

                        $("#flex1").flexOptions({params: dt});
                    }
                    else if (stype == 2) //Sub search
                    {

                        if ($('#flexsub tr.trSelected').length == 0)
                        {
                            alert('Please select a subscribed search');
                            return false;
                        }

                        var ssid = $('#flexsub tr.trSelected').attr('id').substr(3);

                        var dt = [{name: "sid", value: ssid}, {name: 'stype', value: 2}];

                        $("#flex1").flexOptions({params: dt});
                    }
                    else if (stype == 4) //Sub search
                    {

                        if ($('#flexhis tr.trSelected').length == 0)
                        {
                            alert('Please select a previous search');
                            return false;
                        }

                        var ssid = $('#flexhis tr.trSelected').attr('id').substr(3);

                        var dt = [{name: "sid", value: ssid}, {name: 'stype', value: 3}];

                        $("#flex1").flexOptions({params: dt});
                    }


                    $('span.exb, span.svb').addClass('grayed');

                    $('.fields', this).hide();
                    $('.response', this).html("<div class='load exloading'>Processing, please wait...</div>");
                    $('#flex1').flexOptions({newp: 1}).flexReload();
                    return false;
                }
        );

function upgrade()
{
    $('#upgradeb').trigger('click');
}

function callExport()
{
    $('#exportb').trigger('click');

    $('span.exb b').remove();

    if (exdone && lpid) //flag as viewed
    {
        if (lpid)
        {
            $.get(mod + 'export/viewed/' + lpid);
            lpid = 0;
        }
    }
}

function callSave()
{
    if (!$('#sresults .bDiv tbody:visible').length)
    {
        return false;
    }
    $('#saveb, #savepane .response button').trigger('click');
}

function callSub()
{
    if (!$('#sresults .bDiv tbody:visible').length)
    {
        return false;
    }
    $('#subscribeb, #subscribepane .response button').trigger('click');
}



var dtype = "im";

function tkbox()
{



    $('div.flexigrid a.view').each
            (
                    function()
                    {
                        //this.href += '?TB_iframe=true&height=480&width=760';
                    }
            );
    tb_init('div.flexigrid a.view');
    cfields[country](dtype);

    export_fields();


}

$('.vfields input[name=vf]').click
        (
                function()
                {

                    var vc = $('.vfields input:checked').length;
                    if (vc < 3)
                    {
                        alert('Atleast 3 fields need to be visible');
                        return false;
                    }
                    var vn = this.value;
                    var sh = this.checked;



                    $('#flex1').flexToggleCol(vn, sh);

                    export_fields();



                }
        );

count_mfields();
export_fields();


function count_mfields()
{
    var vc = $('.vfields input:checked').length;

    $('input[name=check]').removeAttr('checked');

    if (typeof mfields != "undefined")
    {
        if (vc == mfields)
        {
            $('input[name=check]').attr('checked', 'checked');

        }
    }

}

$('input[name=check]').click(function() {
    var p = this;

    if (p.checked)
    {
        $('.vfields input[name=vf]').each(function() {
            if ($(this).parent().is(':visible'))
            {
                var vn = this.value;
                $(this).attr('checked', 'checked');

                var sh = this.checked;
                $('#flex1').flexToggleCol(vn, sh);
            }
        })
    }
    else
    {

        $('.vfields input[name=vf]').each(function() {
            var obj = this;

            if ($(obj).parent().is(':visible'))
            {
                var vn = obj.value;

                if (vn > 3)
                {
                    $(obj).removeAttr('checked');

                    var sh = obj.checked;

                    $('#flex1').flexToggleCol(vn, sh);
                }
            }
        })
    }
    export_fields();
})

function togback(cid, visible)
{
    $('.vfields input[value=' + cid + ']').each
            (
                    function() {
                        this.checked = visible;
                    }
            );
}

$('form.email').submit
        (
                function()
                {
                    if (filterE($(".email", this).val()) == false)
                    {
                        $('.response', this).html('Please enter a valid email');
                        return false;
                    }
                    $("input[name='exf']", this).val($("#edata input:checked").val());
                    var dt = $(this).serializeArray();
                    var form = this;
                    $('.fields', this).slideUp();
                    $('.response', this).html("<div class='exloading'>Processing, please wait...</div>");
                    $.post
                            (
                                    mod + 'main/email',
                                    dt,
                                    function(data)
                                    {
                                        $('.response', form).html(data + "<p><a href='#' onclick='return resetform(this)'>Submit another?</a></p>");
                                        $('a', form).trigger('focus');
                                    }
                            );
                    return false;
                }
        );

function filterE(str) {
    var at = "@"
    var dot = "."
    var lat = str.indexOf(at)
    var lstr = str.length
    var ldot = str.indexOf(dot)
    if (str.indexOf(at) == -1)
        return false;
    else if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr)
        return false;
    else if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr)
        return false;
    else if (str.indexOf(at, (lat + 1)) != -1)
        return false;
    else if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot)
        return false;
    else if (str.indexOf(dot, (lat + 2)) == -1)
        return false;
    else if (str.indexOf(" ") != -1)
        return false;
    return true
}

function validate(form)
{
    var vf = true;


    $('.error', this).removeClass('error');

    $('.rtb', form).each
            (
                    function()
                    {
                        if ($.trim(this.value) == this.dv || $.trim(this.value) == '')
                        {
                            alert(this.title + ' is required.');
                            this.focus();
                            $(this).addClass('rtb-error');
                            vf = false;
                            return false;
                        }
                    }
            );

    if (!vf)
        return vf;

    $('.confirm', form).each
            (
                    function()
                    {
                        var pass = $('input.password', form).val();
                        if (this.value != pass)
                        {
                            alert(this.value + ' = ' + pass);
                            alert('Could not confirm password!');
                            $('.password', form).val('');
                            this.value = '';
                            vf = false;
                            $('.password', form).trigger('focus');
                            return false;
                        }
                    }
            );

    if (!vf)
        return vf;

    $('.emailtb', form).each
            (
                    function()
                    {
                        if (!filterE(this.value))
                        {
                            alert('Please enter a valid email address');
                            this.value = '';
                            $(this).addClass('rtb-error');
                            this.focus();
                            vf = false;
                            return false;
                        }
                    }
            );

    if (!vf)
        return vf;

    $('.agree', form).each
            (
                    function()
                    {
                        if (!this.checked)
                        {
                            alert('You must agree to our Terms and Conditions to sign-up!');
                            this.focus();
                            vf = false;
                            return false;
                        }
                    }
            );

    if (!vf)
        return vf;

    $(".rb", form).each
            (
                    function()
                    {
                        var isSel = false;

                        $(".rb", form).each
                                (
                                        function()
                                        {
                                            if (this.checked)
                                            {
                                                $(".rb[@name=" + this.name + "]", form).each
                                                        (
                                                                function()
                                                                {
                                                                    this.isSel = true;
                                                                }
                                                        );

                                            }
                                        }
                                );

                        $(".rb", form).each
                                (
                                        function()
                                        {
                                            if (!vf)
                                                return vf;
                                            if (!this.isSel)
                                            {
                                                alert('Please select atleast one option');
                                                this.focus();
                                                vf = false;
                                                return false;
                                            }
                                        }
                                );
                    }
            );

    return vf;
}

//if (!($.browser.msie&&$.browser.version==6)) $('.errorbox').fadeTo(0,0.9);

var stype = 0;

var fsave = {
    dataType: 'json',
    width: 749,
    height: 100,
    url: mod + 'main/get_saves/' + $('input[name=country]').val(),
    showToggleBtn: false,
    nohresize: true,
    usepager: true,
    useRp: false,
    timeout: defTo,
    colModel: [
        {display: 'Date', name: 'sv_date', width: 100}
        , {display: 'Search Name', name: 'sv_name', width: 320}
        , {display: 'Delete', name: 'sv_date', width: 40, align: 'center'}
    ],
    onSuccess: function() {
        $("#flexsave").flexOptions({params: false})
    },
    singleSelect: true,
    autoload: true
}

var fsub = {
    dataType: 'json',
    width: 749,
    height: 100,
    url: mod + 'main/get_subs/' + $('input[name=country]').val(),
    showToggleBtn: false,
    nohresize: true,
    usepager: true,
    useRp: false,
    timeout: defTo,
    colModel: [
        {display: 'Date', name: 'su_date', width: 100}
        , {display: 'Subscription Name', name: 'su_name', width: 320}
        , {display: 'Sent To', name: 'su_sentto', width: 120}
        , {display: 'Status', name: 'su_status', width: 40, align: 'center'}
        , {display: 'Delete', name: 'su_id', width: 40, align: 'center'}
    ],
    onSuccess: function() {
        $("#flexsub").flexOptions({params: false})
    },
    singleSelect: true,
    autoload: true
}

if (typeof ealert_readonly != 'undefined') {
    fsub = {
        dataType: 'json',
        width: 749,
        height: 100,
        url: mod + 'main/get_subs/' + $('input[name=country]').val(),
        showToggleBtn: false,
        nohresize: true,
        usepager: true,
        useRp: false,
        timeout: defTo,
        colModel: [
            {display: 'Date', name: 'su_date', width: 100}
            , {display: 'Subscription Name', name: 'su_name', width: 320}
            , {display: 'Sent To', name: 'su_sentto', width: 120}
            , {display: 'Status', name: 'su_status', width: 40, align: 'center'}
        ],
        onSuccess: function() {
            $("#flexsub").flexOptions({params: false})
        },
        singleSelect: true,
        autoload: true
    }
}

var fhis = {
    dataType: 'json',
    width: 749,
    height: 100,
    url: mod + 'main/get_his/' + $('input[name=country]').val(),
    showToggleBtn: false,
    nohresize: true,
    usepager: true,
    useRp: false,
    timeout: defTo,
    colModel: [
        {display: 'Date', name: 'qdate', width: 100}
        , {display: 'Search Description', name: 'su_name', width: 320}
    ],
    onSuccess: function() {
        $("#flexhis").flexOptions({params: false})
    },
    singleSelect: true,
    autoload: false
}


$('.ptitle a').click
        (
                function()
                {
                    var n = $('.ptitle a').index(this);

                    stype = n;

                    $(this).addClass('psel').siblings('a').removeClass('psel');
                    $('.fpane:eq(' + n + ')').show().siblings('div.fpane').hide();

                    $('.fsearch .response button').trigger('click');

                    $('.fpane:visible input:first').trigger('focus');

                    $('#datatype_vmap').show();

                    this.blur();

                    if (n == 0 || n == 2)
                        $('#flexsub tr.trSelected').removeClass('trSelected');
                    if (n == 3)
                        $('#flexsave').flexigrid(fsave);
                    if (n == 2)
                        $('#flexsub').flexigrid(fsub);
                    if (n == 4)
                        $('#flexhis').flexigrid(fhis).flexReload();

                    if (n == 1)
                    {
                        $('#sresults').hide();
                        $('#vresults').show();
                        $('#togview').hide();

                        $('option[value=shipname]').show();
                        $('option[value=consname]').show();

                        if (country == "pe")
                        {
                            $('#datatype_vmap').hide();
                        }

                    }
                    else
                    {
                        $('#vresults').hide();
                        $('#sresults').show();
                    }


                    if (n == 0) {
                        if ($('select[name=followed_companies]').length > 0) {
                            $('select[name=followed_companies]').focus();
                        }
                    }

                    return false;
                }
        );

$('a.togclick').click
        (
                function()
                {
                    $(this).toggleClass('togminus');
                    $('#togview').slideToggle('fast');
                    this.blur();
                    return false;
                }
        );


$.extend($.easing, {
    def: "easeOutQuad",
    easeOutBounce: function(x, t, b, c, d) {
        if ((t /= d) < (1 / 2.75)) {
            return c * (7.5625 * t * t) + b;
        } else if (t < (2 / 2.75)) {
            return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
        } else if (t < (2.5 / 2.75)) {
            return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
        } else {
            return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
        }
    }
});

$(document).ready(function()
{

    $('.hlight').animate({top: 0}, 'slow', 'easeOutBounce');
    $('.filterload').hide();
    $('#filtertable').show();

    if ($('table th[abbr=fport_s] div').length)
    {
        $('table th[abbr=fport_s] div').append('<a href="#" data-placement="above" class="help hhelp" rel="The last port the shipment passes through before arriving in the U.S.">&nbsp;</a>');
    }
    if ($('table th[abbr=countryoforigin_s] div').length)
    {
        $('table th[abbr=countryoforigin_s] div').append('<a href="#" data-placement="above" class="help hhelp" rel="The country of the last foreign port the shipment passes through before arriving in the U.S.">&nbsp;</a>');
    }

    // DataType Options

    $('input[name=datatype_map]').click(function(e) {

        $(this).attr('checked', 'checked');
        $(this).siblings().removeAttr('checked');

    })

    $('input[name=datatype]').click(function(e) {
        var v = $(this).val();

        dtype = v;

        //searchFilter[country](v);

        setFilter(dtype);

        $('#limit_label').html('');

        $('#flex1').empty();

        if (v == "ex")
        {
            $("th[abbr*='adate'] > div").html('Export Date');
            $('.export_date').show();
            $('.import_date').hide();
            $('#range_label_im').hide();
            $('#range_label_ex').show();
            $('.arrival_date').text('Export Date');

        }
        else if (v == "im")
        {
            $('.export_date').hide();
            $('.import_date').show();
            $('#range_label_im').show();
            $('#range_label_ex').hide();
            $("th[abbr*='adate'] > div").html('Arrival Date');
            $('.arrival_date').text('Arrival Date');
        }
    });

    /**
     * google analytics first login
     */
    function processFirstLogin() {
        var terms = $('input[name=terms]').val(),
                accountType = $('input[name=account_type]').val(),
                amount = +$('input[name=amount]').val();

        accountType = accountType.replace(/[^a-z0-9]/gi, '');
        if (typeof String.prototype.toLowerCase !== "undefined") {
            accountType = accountType.toLowerCase();
        }

        google_analytics('send', {
            'hitType': 'event', // Required.
            'eventCategory': 'Goal', // Required.
            'eventAction': 'Load', // Required.
            'eventLabel': 'First Login',
            'eventValue': amount,
            'nonInteraction': true,
            'hitCallback': function() {
                doAjax();
                $('.footer').append('<span style="color:#ced7de;">Data Sent</span>');
            }
        });
    }

    function doAjax() {
        var welcomeURL = '';
        if (root.indexOf('importgenius.com')) {
            welcomeURL = root + 'users/updateOriginalWelcome';
        } else {
            welcomeURL = root + 'iscan4l.php/users/updateOriginalWelcome';
        }

        $.ajax({
            url: welcomeURL,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.original_welcome) {
                    //session was updated successfully
                }
            }
        });
    }

    if (!location.href.match(/login$/)) {
        var isFirstLogin = $('input[name=user_welcome]').val(),
                payType = parseInt($('input[name=paytype]').val(), 10),
                /**
                 * Pay Types that should not be included
                 * 6: Courtesy account
                 * 8: Child
                 */
                payTypeAllowed = (payType === 6 || payType === 8) ? false : true;

        if (isFirstLogin === '0' && payTypeAllowed) {
            processFirstLogin();
        }
    }
});

// Set Default Fields
var cons_vh = $('.vfields input.consname').val();
var ship_vh = $('.vfields input.shipname').val();
var shipaddr_vh = $('.vfields input.shipaddr').val();
var product_vh = $('.vfields input.product').val();
var productid_vh = $('.vfields input.productid').val();
var brandfob_vh = $('.vfields input.brandfob').val();
var brandid_vh = $('.vfields input.brandid').val();
var incoterms_vh = $('.vfields input.incoterms').val();
var countries_name_vh = $('.vfields input.countries_name').val();
var brandid_vh = $('.vfields input.brandid').val();
var attributes_vh = $('.vfields input.attributes').val();
var brandqnty_vh = $('.vfields input.brandqnty').val();
var variety_vh = $('.vfields input.variety').val();
var operationtype_vh = $('.vfields input.operationtype').val();
var consname_vh = $('.vfields input.consname').val();
var consaddr_vh = $('.vfields input.consaddr').val();
var embarkation_vh = $('.vfields input.embarkation').val();
var countries_name_vh = $('.vfields input.countries_name').val();
var incoterms_vh = $('.vfields input.incoterms').val();
var brand_vh = $('.vfields input.brand').val();
var variety_vh = $('.vfields input.variety').val();
var netweight_vh = $('.vfields input.netweight').val();
var packagetype_vh = $('.vfields input.packagetype').val();
var countries_name_vh = $('.vfields input.countries_name').val();
var acquisition_vh = $('.vfields input.acquisition').val();
var advalorem_vh = $('.vfields input.advalorem').val();
var mdsestate_vh = $('.vfields input.mdsestate').val();
var paytype_vh = $('.vfields input.paytype').val();
var origin_vh = $('.vfields input.origin').val();
var unit_vh = $('.vfields input.unit').val();
var transtype_vh = $('.vfields input.transtype').val();
var hscode_vh = $('.vfields input.hscode').val();
var hscodedesc_vh = $('.vfields input.hscodedesc').val();
var conscontact_vh = $('.vfields input.conscontact').val();
var consemail_vh = $('.vfields input.consemail').val();
var consemail2_vh = $('.vfields input.consemail2').val();
var consfax_vh = $('.vfields input.consfax').val();
var port_vh = $('.vfields input.port').val();

var cfields = {
    ar: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(consname_vh, false);
                $('#flex1').flexToggleCol(embarkation_vh, false);
                $('#flex1').flexToggleCol(acquisition_vh, false);
                $('#flex1').flexToggleCol(incoterms_vh, false);
                $('#flex1').flexToggleCol(advalorem_vh, false);
                $('#flex1').flexToggleCol(brand_vh, false);
                $('#flex1').flexToggleCol(variety_vh, false);
                $('#flex1').flexToggleCol(netweight_vh, false);
                $('#flex1').flexToggleCol(attributes_vh, false);
                $('#flex1').flexToggleCol(productid_vh, false);
                $('#flex1').flexToggleCol(brandid_vh, false);
                $('#flex1').flexToggleCol(brandfob_vh, false);

                $('.consname').parent().hide();
                $('.embarkation').parent().hide();
                $('.acquisition').parent().hide();
                $('.incoterms').parent().hide();
                $('.advalorem').parent().hide();
                $('.brand').parent().hide();
                $('.variety').parent().hide();
                $('.netweight').parent().hide();
                $('.attributes').parent().hide();
                $('.productid').parent().hide();
                $('.brandid').parent().hide();
                $('.brandfob').parent().hide();

            }
            else
            {
                $('#flex1').flexToggleCol(consname_vh, true);
                $('#flex1').flexToggleCol(embarkation_vh, true);
                $('#flex1').flexToggleCol(acquisition_vh, true);
                $('#flex1').flexToggleCol(incoterms_vh, true);
                $('#flex1').flexToggleCol(advalorem_vh, true);
                $('#flex1').flexToggleCol(brand_vh, true);
                $('#flex1').flexToggleCol(variety_vh, true);
                $('#flex1').flexToggleCol(netweight_vh, true);
                $('#flex1').flexToggleCol(attributes_vh, true);
                //$('#flex1').flexToggleCol(countries_name_vh,true);
                $('#flex1').flexToggleCol(productid_vh, true);
                $('#flex1').flexToggleCol(brandid_vh, true);
                $('#flex1').flexToggleCol(brandfob_vh, true);

                $('.consname').parent().show();
                $('.embarkation').parent().show();
                $('.acquisition').parent().show();
                $('.incoterms').parent().show();
                $('.advalorem').parent().show();
                $('.brand').parent().show();
                $('.variety').parent().show();
                $('.netweight').parent().show();
                $('.attributes').parent().show();
                $('.productid').parent().show();
                $('.brandid').parent().show();
                $('.brandfob').parent().show();
            }
        }
        catch (e)
        {
        }
    },
    ve: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(cons_vh, false);
                $('#flex1').flexToggleCol(embarkation_vh, false);
                $('#flex1').flexToggleCol(countries_name_vh, false);
                $('#flex1').flexToggleCol(acquisition_vh, false);
                $('#flex1').flexToggleCol(ship_vh, true);

                $('.cons').parent().hide();
                $('.embarkation').parent().hide();
                $('.countries_name').parent().hide();
                $('.acquisition').parent().hide();
                $('.ship').parent().show();

            }
            else if (datatype == "im")
            {
                $('#flex1').flexToggleCol(cons_vh, true);
                $('#flex1').flexToggleCol(embarkation_vh, true);
                $('#flex1').flexToggleCol(countries_name_vh, true);
                $('#flex1').flexToggleCol(acquisition_vh, true);
                $('#flex1').flexToggleCol(ship_vh, false);

                $('.cons').parent().show();
                $('.embarkation').parent().show();
                $('.countries_name').parent().show();
                $('.acquisition').parent().show();
                $('.ship').parent().hide();
            }
        }
        catch (e)
        {
        }
    },
    co: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('option[value=product]').hide();
                $('#flex1').flexToggleCol(product_vh, false);
                $('#flex1').flexToggleCol(incoterms_vh, false);
                $('#flex1').flexToggleCol(countries_name_vh, false);
                $('#flex1').flexToggleCol(brandid_vh, false);
                $('#flex1').flexToggleCol(brandqnty_vh, false);
                $('#flex1').flexToggleCol(variety_vh, false);
                $('#flex1').flexToggleCol(operationtype_vh, false);
                $('#flex1').flexToggleCol(attributes_vh, false);

                $('.product').parent().hide();
                $('.incoterms').parent().hide();
                $('.countries_name').parent().hide();
                $('.brandid').parent().hide();
                $('.brandqnty').parent().hide();
                $('.variety').parent().hide();
                $('.operationtype').parent().hide();
                $('.attributes').parent().hide();
            }
            else
            {
                $('option[value=product]').show();
                $('#flex1').flexToggleCol(product_vh, true);
                $('#flex1').flexToggleCol(product_vh, true);
                $('#flex1').flexToggleCol(incoterms_vh, true);
                $('#flex1').flexToggleCol(countries_name_vh, true);
                $('#flex1').flexToggleCol(brandid_vh, true);
                $('#flex1').flexToggleCol(brandqnty_vh, true);
                $('#flex1').flexToggleCol(variety_vh, true);
                $('#flex1').flexToggleCol(operationtype_vh, true);
                $('#flex1').flexToggleCol(attributes_vh, true);

                $('.product').parent().show();
                $('.incoterms').parent().show();
                $('.countries_name').parent().show();
                $('.brandid').parent().show();
                $('.brandqnty').parent().show();
                $('.variety').parent().show();
                $('.operationtype').parent().show();
                $('.attributes').parent().show();
            }
        }
        catch (e)
        {
        }
    },
    ec: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(unit_vh, false);
                $('#flex1').flexToggleCol(netweight_vh, false);
                $('#flex1').flexToggleCol(transtype_vh, false);
                $('#flex1').flexToggleCol(hscode_vh, true);
                $('#flex1').flexToggleCol(hscodedesc_vh, false);
                $('#flex1').flexToggleCol(conscontact_vh, false);
                $('#flex1').flexToggleCol(consaddr_vh, false);
                $('#flex1').flexToggleCol(consemail_vh, false);
                $('#flex1').flexToggleCol(consemail2_vh, false);
                $('#flex1').flexToggleCol(consfax_vh, false);
                $('#flex1').flexToggleCol(origin_vh, false);
                //$('#flex1').flexToggleCol(countries_name_vh,true);

                $('.unit').parent().hide();
                $('.netweight').parent().hide();
                $('.transtype').parent().hide();
                $('.hscode').parent().show();
                $('.hscodedesc').parent().hide();
                $('.conscontact').parent().hide();
                $('.consaddr').parent().hide();
                $('.consemail').parent().hide();
                $('.consemail2').parent().hide();
                $('.consfax').parent().hide();
                $('.origin').parent().hide();
                $('.countries').parent().hide();

            }
            else
            {
                $('#flex1').flexToggleCol(unit_vh, true);
                $('#flex1').flexToggleCol(netweight_vh, true);
                $('#flex1').flexToggleCol(hscode_vh, true);
                $('#flex1').flexToggleCol(hscodedesc_vh, true);
                $('#flex1').flexToggleCol(conscontact_vh, true);
                $('#flex1').flexToggleCol(consaddr_vh, true);
                $('#flex1').flexToggleCol(consemail_vh, true);
                $('#flex1').flexToggleCol(consemail2_vh, true);
                $('#flex1').flexToggleCol(consfax_vh, true);
                $('#flex1').flexToggleCol(origin_vh, true);
                //$('#flex1').flexToggleCol(countries_name_vh,true);

                $('.unit').parent().show();
                $('.netweight').parent().show();
                $('.transtype').parent().show();
                $('.hscode').parent().show();
                $('.hscodedesc').parent().show();
                $('.conscontact').parent().show();
                $('.consaddr').parent().show();
                $('.consemail').parent().show();
                $('.consemail2').parent().show();
                $('.consfax').parent().show();
                $('.origin').parent().show();
                $('.countries').parent().show();
            }
        }
        catch (e)
        {
        }
    },
    pa: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(consname_vh, false);
                $('#flex1').flexToggleCol(ship_vh, true);
                $('#flex1').flexToggleCol(port_vh, true);
                $('#flex1').flexToggleCol(shipaddr_vh, false);

                $('.consname').parent().hide();
                $('.ship').parent().hide();
                //$('.origin').parent().hide();
            }
            else
            {
                $('#flex1').flexToggleCol(consname_vh, true);
                $('#flex1').flexToggleCol(ship_vh, false);
                //$('#flex1').flexToggleCol(origin_vh,false);
                $('#flex1').flexToggleCol(shipaddr_vh, false);
                $('#flex1').flexToggleCol(port_vh, false);
                $('.consname').parent().show();
                $('.ship').parent().show();
                //$('.origin').parent().show();
            }
        }
        catch (e)
        {
        }
    },
    cl: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(consname_vh, false);
                $('#flex1').flexToggleCol(ship_vh, true);
                $('#flex1').flexToggleCol(paytype_vh, false);
                $('#flex1').flexToggleCol(origin_vh, false);

                $('.consname').parent().hide();
                $('.shipname').parent().show();
                $('.paytype').parent().hide();
                $('.origin').parent().hide();
            }
            else
            {
                $('#flex1').flexToggleCol(consname_vh, true);
                $('#flex1').flexToggleCol(paytype_vh, true);
                $('#flex1').flexToggleCol(origin_vh, true);
                $('#flex1').flexToggleCol(ship_vh, false);

                $('.shipname').parent().hide();
                $('.consname').parent().show();
                $('.paytype').parent().show();
                $('.origin').parent().show();
            }
        }
        catch (e)
        {
            //console.log(e);
        }
    },
    py: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(brand_vh, false);
                $('#flex1').flexToggleCol(origin_vh, false);

                $('.brand').parent().hide();
                $('.origin').parent().hide();
            }
            else
            {
                $('#flex1').flexToggleCol(origin_vh, true);
                $('#flex1').flexToggleCol(origin_vh, true);

                $('.brand').parent().show();
                $('.origin').parent().show();
            }
        }
        catch (e)
        {
        }
    },
    pe: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(consname_vh, false);
                $('#flex1').flexToggleCol(variety_vh, false);
                $('#flex1').flexToggleCol(netweight_vh, false);
                $('#flex1').flexToggleCol(packagetype_vh, false);
                //$('#flex1').flexToggleCol(countries_name_vh,true);
                $('#flex1').flexToggleCol(acquisition_vh, false);
                $('#flex1').flexToggleCol(embarkation_vh, false);
                $('#flex1').flexToggleCol(advalorem_vh, false);
                $('#flex1').flexToggleCol(mdsestate_vh, false);

                $('.consname').parent().hide();
                $('.variety').parent().hide();
                $('.netweight').parent().hide();
                $('.packagetype').parent().hide();
                //$('.countries_name').parent().hide();
                $('.acquisition').parent().hide();
                $('.embarkation').parent().hide();
                $('.advalorem').parent().hide();
                $('.mdsestate').parent().hide();
            }
            else
            {
                $('#flex1').flexToggleCol(consname_vh, true);
                $('#flex1').flexToggleCol(variety_vh, true);
                $('#flex1').flexToggleCol(netweight_vh, true);
                $('#flex1').flexToggleCol(packagetype_vh, true);
                //$('#flex1').flexToggleCol(countries_name_vh,false);
                $('#flex1').flexToggleCol(acquisition_vh, true);
                $('#flex1').flexToggleCol(embarkation_vh, true);
                $('#flex1').flexToggleCol(advalorem_vh, true);
                $('#flex1').flexToggleCol(mdsestate_vh, true);

                $('.consname').parent().show();
                $('.variety').parent().show();
                $('.netweight').parent().show();
                $('.packagetype').parent().show();
                //$('.countries_name').parent().show();
                $('.acquisition').parent().show();
                $('.embarkation').parent().show();
                $('.advalorem').parent().show();
                $('.mdsestate').parent().show();

            }
        }
        catch (e)
        {
        }
    },
    us: function(datatype)
    {

    },
    uy: function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(consname_vh, false);
                $('#flex1').flexToggleCol(ship_vh, true);

                $('.consname').parent().hide();
                $('.shipname').parent().show();

            }
            else
            {
                $('#flex1').flexToggleCol(consname_vh, true);
                $('#flex1').flexToggleCol(ship_vh, false);

                $('.consname').parent().show();
                $('.shipname').parent().hide();
            }
        }
        catch (e)
        {
        }
    },
    'in': function(datatype)
    {

    },
    'cr': function(datatype)
    {
        try
        {
            if (datatype == "ex")
            {
                $('#flex1').flexToggleCol(consname_vh, false);
                $('#flex1').flexToggleCol(consaddr_vh, false);
                $('#flex1').flexToggleCol(shipaddr_vh, true);
                $('#flex1').flexToggleCol(ship_vh, true);

                $('.consname').parent().hide();
                $('.consaddr').parent().hide();
                $('.shipaddr').parent().hide();
                $('.ship').parent().hide();
            }
            else
            {
                $('#flex1').flexToggleCol(consname_vh, true);
                $('#flex1').flexToggleCol(consaddr_vh, true);
                $('#flex1').flexToggleCol(shipaddr_vh, false);
                $('#flex1').flexToggleCol(ship_vh, false);

                $('.consname').parent().show();
                $('.consaddr').parent().show();
                $('.shipaddr').parent().show();
                $('.ship').parent().show();
            }
        }
        catch (e)
        {
        }
    },
    'ru': function(datatype)
    {

    }
}


var ehis = {
    dataType: 'json',
    height: 340,
    url: mod + 'export/get_items',
    showToggleBtn: false,
    nohresize: true,
    resizable: false,
    usepager: true,
    useRp: false,
    timeout: defTo,
    colModel: [
        {display: 'Actions', name: 'downloaded', width: 100}
        , {display: 'Description', name: 'qdesc', width: 280}
        , {display: 'Status', name: 'status', width: 80}
        , {display: 'Date Requested', name: 'extime', width: 110}
    ],
    onSuccess: function() {
        $("#flexsave").flexOptions({params: false})
    },
    singleSelect: true,
    autoload: false
}

function emailTo(e2)
{
    xpid = e2;
    $('.etabs a:eq(1)').trigger('click')
}

$('.etabs a').click
        (
                function()
                {
                    var n = $('.etabs a').index(this);

                    $(this)
                            .addClass('sel')
                            .siblings().removeClass('sel')
                            ;

                    $('.epane:eq(' + n + ')')
                            .show()
                            .siblings('.epane').hide()
                            ;

                    if (n == 1)
                    {


                        if (xpid != 0)
                        {

                            $('.exmail').hide();

                            $('#last-export').html('<div class="load exloading">Processing, please wait ...</div>');

                            $.get(mod + 'export/prep_for_download/' + xpid,
                                    function(data)
                                    {

                                        if (data.action == 'exmail')
                                        {
                                            var rs = data.response;
                                            //rs += "<p><button type='button' id='generate-again-b' class='button' onclick='$(\".etabs a:eq(0)\").trigger(\"click\"); resetExport();'><span><b>Generate another?</b></span></button></p>";
                                            showform('#sendmail');
                                            $('.exmail').show();
                                            $('#last-export').html(rs);
                                        }
                                    }
                            );

                            xpid = 0;

                        }

                    }

                    if (n == 2)
                    {
                        $('#ehistory').flexigrid(ehis).flexReload();
                    }

                    return false;
                }
        );


/*
 $('.styledSelect ul').mouseleave(function(){
 $('.styledSelect').removeClass('open');
 $('.styledSelect').addClass('overflow');
 })
 */

$('.styledSelect ul li').live('click', function(e) {
    var rel = $(this).attr('rel');
    var cname = $(this).text();
    var disabled = $(this).attr('class');
    var sel = $(this).attr('class');

    if (sel == "active")
    {
        $('.styledSelect').removeClass('overflow');
        $('.styledSelect').slideDown('slow', function(e) {
            $(this).addClass('open');
        })
    }
    else
    {
        if (rel == "us")
            rel = '';
        $('.styledSelect').removeClass('open').addClass('overflow');
        if (disabled != "disabled")
        {

            location.href = optRoot + rel;

            $('.styledSelect ul').prepend('<li rel="' + rel + '">' + cname + '</li>');
            $(this).remove();
        }
        else
        {
            if (rel != "us")
            {
                var uri = cname.toLowerCase();
                //location.href="http://www.importgenius.com/latin/"+uri
                window.open("http://www.importgenius.com/latin/" + uri, "_blank");
            }
        }
    }
})

root = root + 'iscan4l.php/';

$('.globalSettings input[type=checkbox]').click(function() {

    var n = $(this).attr('name');
    var c = this.checked;
    var id = $(this).parent().attr('title');

    var dt = [{name: 'field_id', value: id}, {name: 'fname', value: n}, {name: 'bool', value: c}];

    saveSettings(dt);
})

$('.globalSettings input[type=text]').blur(function() {

    var n = $(this).attr('name');
    var v = $(this).val();
    var id = $(this).parent().attr('title');

    var dt = [{name: 'field_id', value: id}, {name: 'fname', value: n}, {name: 'textval', value: v}];

    saveSettings(dt);
})

$('.globalSettings select[name=align]').change(function() {

    var n = $(this).attr('name');
    var v = $(this).val();
    var id = $(this).parent().attr('title');

    var dt = [{name: 'field_id', value: id}, {name: 'fname', value: n}, {name: 'textval', value: v}];

    saveSettings(dt);
})


function saveSettings(dt)
{
    $('*').addClass('wait');
    $.ajax({
        url: root + 'utility/postSettings',
        data: dt,
        dataType: 'json',
        type: 'post',
        success: function(data)
        {
            $('*').removeClass('wait');
        }
    })
}


$('select[name=countries]').live('change', function() {
    var country_id = $(this).val();
    var dt = [{name: 'country_id', value: country_id}];

    $('*').addClass('wait');
    $.ajax({
        url: root + 'utility/getCountries',
        data: dt,
        dataType: 'json',
        type: 'post',
        success: function(data)
        {
            var edata = data;

            $('.cfields input[type=checkbox]').each(function() {
                $(this).attr('checked', '');
                $(this).parent().css({background: 'transparent'});
            })

            $.each(edata.db, function(i, n) {
                if ($('.cfields input[name=' + n.field_name + ']').length)
                {
                    $('.cfields input[name=' + n.field_name + ']').attr('checked', 'checked');

                    $('.cfields input[name=' + n.field_name + ']').parent().css({background: 'orange'});

                }
            })
            $('*').removeClass('wait');
        }
    })

})

$('.cfields input[type=checkbox]').click(function(e) {

    var v = $(this).val();
    var c = $('select[name=countries]').val();
    var b = (this.checked ? "1" : "0");
    var dt = [{name: 'field_id', value: v}, {name: 'country_id', value: c}, {name: 'bool', value: b}];

    if (this.checked)
        $(this).parent().css({background: 'orange'});
    else
        $(this).parent().css({background: 'transparent'});

    if (c)
    {
        $('*').addClass('wait');
        $.ajax({
            url: root + '/utility/setCountryField',
            data: dt,
            dataType: 'json',
            type: 'post',
            success: function(data)
            {

                $('*').removeClass('wait');
            }
        })
    }
    else
    {
        $('.cfields input[type=checkbox]').each(function() {
            $(this).attr('checked', '');
        })
        alert('Please select country');
    }
})


function showCustomerService()
{
    $('#ehelp').trigger('click');
}

function slideSwitch() {

    var $active = $('.splash-image IMG.active');

    if ($active.length == 0)
        $active = $('.splash-image IMG:last');

    var $next = $active.next().length ? $active.next() : $('.splash-image IMG:first');

    $active.addClass('last-active');

    $next.css({opacity: 0.0})
            .addClass('active')
            .animate({opacity: 1.0}, 1000, function() {
                $active.removeClass('active last-active');
            });
}

$(function() {
    if ($('.splash-image').length > 0)
    {
        setInterval("slideSwitch()", 2000);
    }
});
