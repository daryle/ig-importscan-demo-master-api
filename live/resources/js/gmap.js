var infowindow,
	map,
	marker,
	geocoder,
	scriptIsLoaded = false;

function initialize() 
{
	try
	{

		geocoder = new google.maps.Geocoder();
		//myLatLng = new google.maps.LatLng(-100, 33);
		
		var myOptions = {
		  zoom: 14,
		  //center: myLatLng,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

		marker = new google.maps.Marker({
			map: map,
			title: ''
		});


	}
	catch(e){}

}

function getAddress(addr) 
{
	try
	{
		if(typeof addr == "undefined")
		{
			addr = address;
		}

		geocoder.geocode( { 'address': addr}, function(results, status) {
			
		  if (status == google.maps.GeocoderStatus.OK) {

			var center = results[0].geometry.location,
				//content = address.substring(0,40) + "<br/>" + address.substring(40);
				content = address;
			
			map.setCenter(results[0].geometry.location);

			infowindow = new google.maps.InfoWindow({
				content: "<div class=\"infowindow\" style=\"font-size: 11px; word-wrap: break-word; width: 180px;\"><strong>"+gmap_cname+"</strong><br/>"+content+"</div>"
			});
			
			var marker = new google.maps.Marker({
				map: map,
				position: new google.maps.LatLng(center.lb, center.mb),
				title: 'Map'
			});
			
			infowindow.open(map,marker);
			
		  } else {
			console.info("Geocode was not successful for the following reason: " + status);
			//alert('Location of this address cannot be determined.');
		  }
		});
	
	}
	catch(e){}
}

function loadScript() 
{

	if(scriptIsLoaded == false)
	{
		var script = document.createElement("script");
		
		script.type = "text/javascript";
		script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyAAv1wP9rgBTcejRyS9DNnKTz-h_OaH8Mc&sensor=true&callback=initialize";
		document.body.appendChild(script);
		scriptIsLoaded = true;
	}
	else
	{
		initialize();
	}
}

$(document).ready(function(){
	loadScript();

	setTimeout(function(){
		addr = $.trim(address.substr(0,address.lastIndexOf(' ')));
		getAddress(addr);
	},100);
});


