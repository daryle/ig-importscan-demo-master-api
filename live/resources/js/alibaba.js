function log_action(action) {
        $.ajax({
            url: window.root + 'alibaba/log_action'
            , type: 'POST'
            , dataType: 'json'
            , data: {'action': action, 'keyword': Alibaba.search_terms, 'message' : Alibaba.message}
        });
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

var old_tb_remove = window.tb_remove;

var tb_remove = function() {
    Alibaba.message = '';

    if ($('#TB_iframeContent').contents().find('form.frmAlibaba').length > 0) {
        log_action('5');
    }
    old_tb_remove(); // calls the tb_remove() of the Thickbox plugin
};

$(document).ready(function() {

    // Do not close widget if clicked
    $('#alibaba-offer-widget').click(function(event) {
        event.stopPropagation();
    });

    // Do not close widget if clicked
    $('#alibaba-offer-widget-frame').click(function(event) {
        event.stopPropagation();
        $('#TB_closeWindowButton').hide();
        $('.TB_overlayBG').unbind('click');
		document.onkeydown = "";
		document.onkeyup = "";
        window.log_action('1');
    });

    // Close widget if clicked outside the element
    $(document).click(function() {
        $('a#alibaba-offer-widget-close').trigger('click');
    });
});

$('#alibaba-offer-widget-close').click(function(event) {
    $('#alibaba-offer-widget').hide('fast');
    event.preventDefault();
});

$('#alibaba-offer-widget-never-ask-again').click(function(event) {
    $.ajax({
        url: root + '/alibaba/post'
        , type: 'POST'
        , dataType: 'json'
        , data: {'action': 'never_ask_again'}
        , success: function(data) {
            if (data.action === 'ok') {
                window.aliNeverAsk = '1';
                this.never_ask_again = '1';

                // Google Tracker
                _gaq.push(['_trackEvent', 'AliSourcePro', 'AliSourceProNotInterested', 'Not Interested In AliSourcePro', 0, true]);

            }
            $('#alibaba-offer-widget').hide('fast');
            window.log_action('2');
        }
    });
    event.preventDefault();
});

$('a#alibaba-offer-widget-frame').click(function(event) {
    _gaq.push(['_trackEvent', 'AliSourcePro', 'AliSourceProOpenedForm', 'Opened AliSourcePro Form', 50, true]);
});

$('#alibaba-offer-widget-skip').click(function(event) {
    $('#alibaba-offer-widget').hide('fast');
    window.log_action('0');
    createCookie('skip_alibaba_offer',1,1);
    event.preventDefault();
});


/**
 * Alibaba Object
 *
 * @type object
 */
Alibaba = {
    'aliMemberId': 0,
    'never_ask_again': false,
    'search_terms_string': '',
    'search_terms_array': new Array(),
    'search_terms': new Array(),
    'message' : '',
    'sid' : '',
    'offer': function(json) {
        if ( readCookie('skip_alibaba_offer') === 1 || readCookie('skip_alibaba_offer') === '1') {
            this.never_ask_again = 1;
        }
        else {
            this.never_ask_again = window.aliNeverAsk;
        }

        var has_product_search = false;
        $('select.filters').each(function(index) {
            var s = $('select.filters:eq(' + index + ')').val();
            var v = $('input[title=search keyword]:eq(' + index + ')').val();
            if (s.toLowerCase() === 'product' && v !== '' && has_product_search === false) {
                has_product_search = true;
            }
        });

        // This popup is for product search only.
        if (has_product_search === false)
            return false;



        if (json.total > 0) {


            if (json.sid) {
                if (json.sid === this.sid) {
                    return true;
                }
                this.sid = json.sid;
            }

            if (json.page !== '1') {
                return true;
            }

            var obj = $('input.alpha2');
            var s_string = '';
            var s_string_raw = '';
            var s_arr = new Array();
            var s_arr_raw = new Array();
            var s_raw_string = new Array();

            if (obj.length > 0) {
                n = obj.length - 1;
                s_arr_len = 0;


                for (x = 0; x < n; x++) {
                    var s = $(obj).eq(x).val();
                    if (s !== '') {
                        var st = $('select.filters:eq(' + x + ')').val();
                        if (st.toLowerCase() === 'product') {
                            s_arr[s_arr_len] = s;
                            s_arr_len++;
                        }
                    }
                }


                if (s_arr_len > 0) {
                    for (x = 0; x < s_arr_len; x++) {
                        s_arr_raw = s_arr[x];
                        s_arr[x] = '<span class="product-term">' + s_arr[x] + '</span>';
                        s_raw_string[x] = s_arr_raw;

                        if (x === 0) {
                            s_string = s_arr[x];
                            s_string_raw = s_arr_raw;
                        }
                        else if (s_arr_len > 1 && (x + 1) === s_arr_len) {
                            s_string = s_string + ' and ' + s_arr[x];
                            s_string_raw = s_string_raw + ' and ' + s_arr_raw;
                        }
                        else {
                            s_string = s_string + ', ' + s_arr[x];
                            s_string_raw = s_string_raw + ', ' + s_arr_raw;
                        }

                    }

                    // Google Tracker
                    _gaq.push(['_trackEvent', 'AliSourcePro', 'AliSourceProOfferPopup', 'Viewed AliSourcePro Offer Popout', 0, true]);

                    url = window.root + 'promo/show/rapid-sourcing/' + encodeURI(s_string_raw) + '/TB_iframe?height=412&width=740&inlineId=AliSourceProFrame';

                    if ($.browser.msie && $.browser.version.substr(0, 1) < 8) {
                        url = window.root + 'promo/show/rapid-sourcing/' + encodeURI(s_string_raw) + '/TB_iframe?height=412&width=800&inlineId=AliSourceProFrame';
                    }
                    $('#alibaba-offer-widget-frame').attr('href', url);
                }

                this.search_terms_array = s_arr;
                this.search_terms_string = s_string;
                this.search_terms = s_string_raw;
                $('#alibaba-term').html(s_string);

            }

        if (this.never_ask_again === '1' || this.never_ask_again === 1)
        	{
				$('.response').append(' or <a href="#" onclick="$(\'#alibaba-offer-widget-frame\').trigger(\'click\'); return false;" class="get-quotes-btn">Get quotes for this product</a>');
	            return true;
        	}
        if (window.aliNeverAsk === '1' || window.aliNeverAsk === 1)
        	{
				$('.response').append(' or <a href="#" onclick="$(\'#alibaba-offer-widget-frame\').trigger(\'click\'); return false;" class="get-quotes-btn">Get quotes for this product</a>');
	            return true;
        	}

        $('#alibaba-offer-widget').show();

        }

    }
};
Alibaba.never_ask_again = window.aliNeverAsk;
Alibaba.aliMemberId = window.aliMemberId;

if ($.browser.msie && $.browser.version.substr(0, 1) < 8) {
    switch ($.browser.version.substr(0, 1)) {
        case '6':
            {
                $('#alibaba-offer-widget').insertBefore($('.box1')).css({'top': 160});
                $('span.lc_top_arrow', $('#alibaba-offer-widget')).hide();
                $('.inner-widget p').css({'line-height': 1});
                $('.inner-widget h2').css({'line-height': 1});
                break;
            }
        case '7':
            {
                $('#alibaba-offer-widget').insertBefore($('.box1')).css({'top': 165});
                $('.inner-widget p').css({'line-height': 1});
                $('.inner-widget h2').css({'line-height': 1});
                break;
            }
    }
}