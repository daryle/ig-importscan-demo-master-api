$(document).ready(function(){
	
	$('.hhelp').live({
		mouseenter:	function()
			{
				var temp = "";
				
				var $title = $(this).attr('rel');
					
				temp = "<div class=\"toolTip\">";
				temp+=	"<span class=\"toolContent\">"+$title+"</span>";
				temp+=	"<div class=\"toolArrow\"></div>";
				temp+="</div>";
				
				$('.toolTip').remove();
				$('body').prepend(temp);
				
				var pos = $(this).offset();
				
				var $arrow = $('.toolArrow');
				var $parent = this;
				var $obj = $('.toolTip');

				var offset = 10;
				$('body').css({position: 'relative'});
				
				$('.toolTip').css({
					top: pos.top - ($obj.height() + offset),
					left: pos.left - ($obj.width()/2)
				})
				.fadeIn('fast');
				
				$arrow.css({top:$obj.height()});
				$('span',$obj).html($title);	
				
				//$($parent).removeAttr('title');
			},
		mouseleave: function()
			{
				$('.toolTip').remove();
			}
	})
})
