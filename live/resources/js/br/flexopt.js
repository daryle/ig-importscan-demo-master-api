var fht = 320;

fht = $(window).height()-312;

if (fht<320) fht = 320;

var flexOption = {
    dataType: 'json',
    width: 'auto',
    url: mod+submod+'/maketable2',
    colModel : [
        {display: 'View', width: 60, name : 'entryid', sortable : true, align: 'center'},
        {display: 'Product Description', name : 'product_s', width : 200, sortable : true, align: 'left', hide: false},
        {display: 'Consignee', name : 'consname_s', width : 180, sortable : true, align: 'left'},
        {display: 'Shipper', name : 'shipname_s', width : 120, sortable : true, align: 'left'},
        {display: 'Arrival Date', name : 'actdate', width : 80, sortable : true, align: 'center'},
        {display: 'Gross Weight (LB)', name : 'weight', width : 100, sortable : true, align: 'right', hide: false},
        {display: 'Gross Weight (KG)', name : 'weight', width : 100, sortable : true, align: 'right', hide: true},
        {display: 'Quantity', name : 'quantity_n', width : 100, sortable : true, align: 'right', hide: false},
        //{display: 'Port', name : 'fport_s', width : 120, sortable : true, align: 'left', hide: false},
        //{display: 'Vessel Name', name : 'vesselname_s', width : 120, sortable : true, align: 'left', hide: false},
        {display: 'Country of Origin', name : 'origin_s', width : 120, sortable : true, align: 'left', hide: false},        
        {display: 'Country of Destination', name : 'distination_s', width : 120, sortable : true, align: 'left', hide: false},        
        {display: 'Consignee Address', name : 'consaddr', width : 100, sortable : false, align: 'left', hide: false},
        {display: 'Shipper Address', name : 'shipaddr', width : 100, sortable : false, align: 'left', hide: false},
        ],
    buttons : [
        {name: 'Export/Email Report', bclass: 'exb', onpress : callExport}
        ,{name: 'Save Search', bclass: 'svb', onpress : callSave}
        ,{name: 'Subscribe to this search', bclass: 'svb svb2', onpress: callSub}
        ,{separator: true}
        ,{name: 'Modify Display Fields', bclass: 'tgb', onpress : function () {$('a.togclick').trigger('click');}}
        ],    
    blockOpacity: 0.7,    
    sortname: "actdate",
    sortorder: "desc",
    usepager: true,
    rpOptions: [10,15,20,25,40,100],    
    useRp: true,
    rp: 15,
    autoload: false,
    onToggleCol: togback,
    onSuccess: tkbox,
    preProcess: procJSON,
    onError: onError,
    showToggleBtn: false,
    minheight: 320,
    timeout: flexTo,
    onTimeout: flexTimeout,
    height: fht
    };