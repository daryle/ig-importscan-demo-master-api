jQuery.extend({
	curCSS: function(elem, prop, force) {
			var ret, stack = [], swap = [];
			
			function color(a){
				if ( !jQuery.browser.safari )
				return false;
				
				var ret = document.defaultView.getComputedStyle(a,null);
				return !ret || ret.getPropertyValue("color") == "";
			}
			
			if (prop == "opacity" && jQuery.browser.msie) {
				ret = jQuery.attr(elem.style, "opacity");
				return ret == "" ? "1" : ret;
			}
			
			if (prop.match(/float/i))
				prop = styleFloat;
			
			if (!force && elem.style[prop])
				ret = elem.style[prop];
			
			else if (document.defaultView && document.defaultView.getComputedStyle) {
			
				if (prop.match(/float/i))
					prop = "float";
			
				prop = prop.replace(/([A-Z])/g,"-$1").toLowerCase();
				var cur = document.defaultView.getComputedStyle(elem, null);
			
				if ( cur && !color(elem) )
					ret = cur.getPropertyValue(prop);
				else {
					for ( var a = elem; a && color(a); a = a.parentNode )
						stack.unshift(a);
			
					for ( a = 0; a < stack.length; a++ )
						if ( color(stack[a]) ) {
							swap[a] = stack[a].style.display;
							stack[a].style.display = "block";
							}
					
					try
						{
						document.defaultView.getComputedStyle(elem,null).getPropertyValue(prop)|| "";
						} catch (expn) {
							$('body').hide();
							setTimeout('self.location.reload()',1000);
						}
			
					ret = (prop == "display" && swap[stack.length-1] != null) ?	"none" : (document.defaultView.getComputedStyle(elem,null) != null) ? document.defaultView.getComputedStyle(elem,null).getPropertyValue(prop) : "";
			
					for ( a = 0; a < swap.length; a++ )
						if ( swap[a] != null )
						stack[a].style.display = swap[a];
						}
			
					if ( prop == "opacity" && ret == "" )
						ret = "1";
			
				} else if (elem.currentStyle) {
					var newProp = prop.replace(/\-(\w)/g,function(m,c){return c.toUpperCase();});
					ret = elem.currentStyle[prop] || elem.currentStyle[newProp];
			
					if ( !/^\d+(px)?$/i.test(ret) && /^\d/.test(ret) ) {
						var style = elem.style.left;
						var runtimeStyle = elem.runtimeStyle.left;
						elem.runtimeStyle.left = elem.currentStyle.left;
						elem.style.left = ret || 0;
						ret = elem.style.pixelLeft + "px";
						elem.style.left = style;
						elem.runtimeStyle.left = runtimeStyle;
						}
				}
			
			return ret;
	}
});

