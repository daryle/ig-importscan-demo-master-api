<?php 

class Splash extends Controller
{
	
	function Splash()
	{
		parent::Controller();
		$this->load->model('splash_db');
	}
	
	function index()
	{
		
		$this->load->model('countries_db');
		$user = $this->session->userdata('user');
		
		$data['ptitle'] = "My Account";
		$data['user'] = $this->session->userdata('user');
		$this->load->model('Users_db');
		$cards = $this->Users_db->getCard($data['user']['username']);
		$data['carttotal'] = $this->cart->total();
		
		if (!$cards) 
		{
		
			$fields = array(
						'cid'
						,'firstname'
						,'lastname'
						,'street'
						,'city'
						,'state'
						,'country'
						,'zip'
						,'ctype'
						,'cardno'
						,'refno'
						,'seccode'
						,'expiry'
						);
								
			foreach ($fields as $field) $cards[$field] = '';
		} else {
			$edb = $this->Users_db->getCpStore(array('cp_key'=>md5($cards['cid'])));
			if ($edb)
			{
				$cards['cardno'] = $this->encrypt->decode($edb['cp_val'],$cards['cid']);
				$cards['seccode'] = $this->encrypt->decode($edb['cp_end'],$cards['cid']);
			}
			
		}	
		
		$data['countries'] = $this->countries_db->getListStore(array('userid'=>$user['id']));
		$data['acountries'] = $this->countries_db->getAvailableCountries(array('userid'=>$user['id']));
		$data['card'] = $cards;
		
		$data['content'] = $this->load->view('popup/splash',$data,true);
		
		$this->load->view('template/popup',$data);   
	}
	
	function save()
	{
		$user = $this->session->userdata('user');
		
		$db['noshow'] = $this->input->post('publish');
		$db['id']  = $user['id'];
		
		$this->splash_db->saveSplash($db,$db['id']);
		
		$data['db'] = $db;
		$data['json'] = $data;
		
		$this->load->view('template/ajax2',$data);
	
	}
	
	function setSplashFlag()
	{
		$user = $this->session->userdata('user');
		$splash = $this->session->userdata('splash');
		
		if(empty($splash) && $user['utype'] != 23)
		{
			$this->session->set_userdata('splash',1);
		}
		
		$data['message'] = "session set";
		
		$data['json'] = $data;
		
		$this->load->view('template/ajax2',$data);
	}
		
}
