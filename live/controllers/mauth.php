<?php

class Mauth extends Controller
{
	var $select;

	function __construct()
	{
		parent::__construct();
		ini_set('memory_limit','4G');
		$this->load->model('users_db','User');

		$this->select = "id as userid,
						firstname,
						lastname,
						username,
						business,
						email,
						phone,
						utype,
						paytype,
						street,
						city,
						state,
						country,
						zip,
						job_title,
						modified,
						session_id,
						multi";
	}
	
	
	function post()
	{
		$this->load->model('countries_db');

		$data['success'] 	= "false";
		
		if($this->input->post('request'))
		{
			$dt['username'] 	= $this->input->post('username');
			$dt['password'] 	= trim(md5($this->input->post('password')));
			$dt['deleted'] 		= 0;
	
			$user = $this->User->getMUser($dt,$this->select);
			
			if($user)
			{
				$data['success'] = "true";
				$data['user'] = $user;
				$data['user']['stoken'] = md5(serialize($user));
				//$data['countries'] = array('Argentina','Colombia','Chile','USA','Venezuela','Peru','Paraguay','Uruguay','Costa Rica');
				
				$data['countries'] = array();

				$countries = $this->countries_db->getAvailableCountries(array('userid'=>$user['userid']));

				foreach($countries as $country)
				{
					$data['countries'][] = $country['country_name'];
				}

				$dt2['session_id'] = $data['user']['stoken'];
				$this->User->updateUser($dt2,$dt);
			}
		}	

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);

	}
	
	function search()
    {
		#$vlog = var_export($_POST,true); 
		#log_message('error',$vlog);
		#exit;

		#print_r($_POST); exit();	
		//get user info
		$data['logout'] = "false";

		$mtoken = $this->input->post('mtoken');

		$dt['username'] = $this->input->post('username');
		
		$user = $this->User->getMUser($dt,$this->select);

		if($mtoken != $user['session_id'] && $user['multi'] == 0)
		{
			$data['logout'] = "true";
		}


		//load plugin n model
		$this->load->helper('str2');
		$this->load->model('Shipping2','xdb');

		$country = "us";
	
		$this->xdb->country = $country;
		$this->xdb->switch_db($country);

		$data['country'] = $country;

		//get common post vars
		$page = $this->input->post('page');
		$rp = $this->input->post('rp');
		$sortname = $this->input->post('sortname');
		$sortorder = strtoupper($this->input->post('sortorder'));

		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$start = (($page-1) * $rp);
		$filters = false;

		//load rules
		$rules['tlimit'] =  time();
		$rules['flimit'] = strtotime('1/01/2006');
		$rules['qlimit'] = 3000;
		$rules['dlimit'] = 365;
			
		$flimit = $rules['flimit']-(60*60*24);
		$tlimit = $rules['tlimit'];
		$qlimit = $rules['qlimit'];

		//init response
		$data['response'] = rp('Error processing your query! Please, try again.',1);
		$data['action'] = 'newsearch';
		$data['nstotal'] = $this->xdb->totalSearchbyUserId($user['userid']);
		$data['nsleft'] = $qlimit - $data['nstotal'];
		$data['from'] = $from;
		$data['to'] = $to;
		$data['page'] = 0;
		$data['total'] = 0;
		$data['rows'] = array();

		//does search have sid?
		$sid = $this->input->post('sid');
		$stype = $this->input->post('stype');

		$data['sid'] = $sid;

		if (!$sid)
			{

			//if no sid build query

			//handle if pass limits for new queries
			if ($data['nsleft']<=0)
				{
				if ($user['utype']>12)
				{
					$data['response'] = rp("You have reached your daily limit of <b>$qlimit</b> searches. Request for an <a href='#' onclick='$(\"a.promo2\").trigger(\"click\"); return false;' ><b class='hlite'>upgrade</b></a> now!",1);
				}
				else
					$data['response'] = rp("You have reached your daily limit of <b>$qlimit</b> searches. Request for an <b>upgrade</b> now!",1);
					json_exit($data);
				}

			//return if no range
			if (!$from||!$to)
				{
				$ds['response'] = 'Error with range data';
				$ds['action'] = 'newsearch';
				json_exit($data);
				}

			$q['qfrom'] = strtotime($from);
			$q['qto'] = strtotime($to);

			//switch if wrong
			if ($q['qfrom']>$q['qto'])
				{
				$data['from'] = $to;
				$data['to'] = $from;
				$q['qfrom'] = $to;
				$q['qto'] = strtotime($from);
				}

			//handle if range passes limits
			if ($q['qfrom']<$flimit)
				{
				$data['response'] = rp("You are searching for records that are <strong>beyond</strong> the range of your account type. Please request an <strong>upgrade</strong>  or select a range of dates more recent than <b>".date("m/d/Y",$flimit)."</b>.",1);
				json_exit($data);
				}

			if ($q['qto']>$tlimit)
				{
				$data['response'] = rp("You are searching for records that are <strong>beyond</strong> the range of your account type. Please request an <strong>upgrade</strong>  or select a range of dates more recent than <b>".date("m/d/Y",$tlimit)."</b>.",1);
				json_exit($data);
				}

			$qlite = array();

			//get variables
				$fcond = $this->input->post('cond');
				$qtype = $this->input->post('qtype');
				$qry = $this->input->post('qry');
				$mtype = $this->input->post('mtype');
				$qzip = $this->input->post('qzip');
				$qkeyname = $this->input->post('qkeyname');
				$qzip1 = $this->input->post('qzip1');
				$qzip2 = $this->input->post('qzip2');
				$qmiles = $this->input->post('qmiles');
				$exclude = $this->input->post('exclude');
				$hvm = $this->input->post('hvm');
				
				#var_dump($qry); exit();

				if ($exclude)
					{
/*
					$fcond[] = "NOT";
					$qtype[] = "consname";
					$qry[] = "NOT AVAILABLE";
					$mtype[] = "EXACT";
					$qzip[] = "";
					$qzip1[] = "";
					$qzip2[] = "";
					$qmiles[] = "";
					$qkeyname[] = "";

					$fcond[] = "NOT";
					$qtype[] = "consname";
					$qry[] = "N/A";
					$mtype[] = "EXACT";
					$qzip[] = "";
					$qzip1[] = "";
					$qzip2[] = "";
					$qmiles[] = "";
					$qkeyname[] = "";
*/
					$filters[] = array(
								'field' => 'cons_id'
								,'values' => array(2663619)
								,'values' => array(2663619,3431519)
								,'exclude' => true
								);

					}

				if ($hvm)
					{
					$fcond[] = "AND";
					$qtype[] = "masterbillofladingind";
					$qry[] = "$hvm";
					$mtype[] = "EXACT";
					$qzip[] = "";
					$qzip1[] = "";
					$qzip2[] = "";
					$qmiles[] = "";
					$qkeyname[] = "";
					}

				//build qlite
				for ($n=0;$n<count($qtype);$n++)
				{
					if (!isset($qtype[$n])) continue;
					if (!isset($qry[$n])) continue;

					if ($qtype[$n]=='distancefromzip')
						{
						//zip code query
						$numallow = "/[^0-9]/i";
						$qzip[$n] = preg_replace($numallow,"",$qzip[$n]);
						$qmiles[$n] = preg_replace($numallow,"",$qmiles[$n]);
						
						
						if(is_int($qzip[$n]) == FALSE && strlen($qzip[$n]) != 5)
						{
							$data['response'] = rp("Please enter a valid zip code on the Zip code range.",1);
							json_exit($data);
						}
						
						if ($qzip[$n]!=''&&$qmiles[$n]!='')
							{
							//limit 25 miles for now

							while (strlen($qzip[$n])<5)
								{
								$qzip[$n] = "0".$qzip[$n];
								}

							if (strlen($qzip[$n])>5)
								{
								$data['response'] = rp('Please enter the correct Zip Code 5 digit format.',1);
								json_exit($data);
								}

							if ($qmiles[$n]>5)
								{
								$data['response'] = rp('Please limit Zip Code feature to 5 miles.',1);
								json_exit($data);
								}


							$zcodes = $this->xdb->getZipCodes($qzip[$n],$qmiles[$n]);
							if ($zcodes)
								{
								$qlite['zipcode'][] = array($fcond[$n],implode(' | ',$zcodes),'ANY');
								}

							}
						//json_exit($zcodes);
						}
					else if ($qtype[$n]=='ziprange')
						{

						//zip code query
						$numallow = "/[^0-9]/i";
						$qzip1[$n] = preg_replace($numallow,"",$qzip1[$n]);
						$qzip2[$n] = preg_replace($numallow,"",$qzip2[$n]);
						
						
						if(strlen($qzip1[$n]) != 5 && is_int($qzip1[$n]) == FALSE)
						{
							$data['response'] = rp("Please enter a valid zip code on the Zip code range.",1);
							json_exit($data);
						}
						
						if(strlen($qzip2[$n]) != 5  && is_int($qzip2[$n]) == FALSE)
						{
							$data['response'] = rp("Please enter a valid zip code on the Zip code range.",1);
							json_exit($data);
						}

						if ($qzip1[$n]>$qzip2[$n])
							{
							$qzip1temp = $qzip1[$n];
							$qzip1[$n] = $qzip2[$n];
							$qzip2[$n] = $qzip1temp;
							}

						if ($qzip1[$n]!=''&&$qzip2[$n]!='')
							{

							$filters[] = array(
										'field' => 'zipcode_s'
										,'from' => $qzip1[$n]
										,'to' => $qzip2[$n]
										);



/*

							$limit = 500;

							$zcodes = $this->xdb->getZipRange($qzip1[$n],$qzip2[$n],1000);

							//json_exit($zcodes);

							if (count($zcodes)>$limit)
							{
							$data['response'] = rp("The zip code range generates exceed the $limit zipcodes limit allowed by the system, please modify your range to generate a result.",1);
							json_exit($data);
							}

							if ($zcodes)
								{
								$qlite['zipcode'][] = array($fcond[$n],implode(' | ',$zcodes),'ANY');
								}
*/

							}


						}

					else
						{
						//normal query
						$allowed = "/[^a-z0-9* -]/i";
						$qry[$n] = preg_replace($allowed," ",$qry[$n]);
						if (trim($qry[$n]))
                           $qlite[$qtype[$n]][] = array(
                                     $fcond[$n],
                                     $qry[$n],
                                     $mtype[$n],
                                     isset($qkeyname[$n]) ? $qkeyname[$n] : ''
                                     );
						}

				}

				$query = '';

				$this->load->helper('sphinx');
				$cl = new SphinxClient();

				//build from qlite -> consider exporting to a function
				if(is_array($qtype))
				{
					foreach($qtype as $qt)
					{
						if($qt != "ziprange")
						{
							if(count($qlite) < 1)
							{
								$data['response'] = rp('No results have been found using the search criteria provided.  Please modify your search terms and try again.',1);
								json_exit($data);

							}
						}
					}
				}
				foreach ($qlite as $qkey => $ql)
				{
					//$query .= " @$qkey ";
					$qc = 0;
					foreach ($ql as $qt)
						{
							$qc += 1;

							$qi = $qt[1];

							//$qi = $cl->EscapeString($qi);


							if ($qt[2]=='EXACT') $qi = '"'.$qi.'"';
							if ($qt[2]=='STARTS') $qi = "^$qi";
							if ($qt[2]=='ENDS') $qi = $qi."$";

							$qi = "($qi)";

							if ($qt[0]=='OR'&&$qc>1) $qi = " | $qi ";
							if ($qt[0]=='NOT') $qi = " -$qi ";

							if ($qt[0]=='OR'&&$qc==1&&$query!='') $query .= " | ";
							if ($qc==1 && $qkey != 'all') $query .= " @$qkey ";

							$query .= $qi;
						}
				}


				if ($query==''&&!$filters)
					{
					$data['response'] = rp('No results have been found using the search criteria provided.  Please modify your search terms and try again.',1);
					json_exit($data);
					}

				//show query
				//$data['response'] = rp(print_r($qlite,true));
				//json_exit($data);

				$q['userid'] = $user['userid'];
				$q['query'] = $query;
				$q['qlite'] = serialize($qlite);

				$q['qsortname'] = $sortname;
				$q['qsortorder'] = $sortorder;
				$q['qdate'] = date('Y-m-d H:i:s');
				$q['session'] = $this->session->userdata('session_id');
				$q['ip'] = $_SERVER['REMOTE_ADDR'];
				$q['view'] = 1;

				if (is_array($filters))
					$q['filters'] = serialize($filters);

			} // end of build query
			else
				{
				$q = $this->xdb->getQuery($sid);
				$q['qsortname'] = $sortname;
				$q['qsortorder'] = $sortorder;
				$q['view'] += 1;
				$qlite = unserialize($q['qlite']);
				if (!is_array($qlite)) $qlite = array();

				if ($q['filters'])
					$filters = unserialize($q['filters']);

				if ($stype==2) //sub search
					{
					$sid = 0;
					unset($q['aid']);
					$q['qdate'] = date('Y-m-d H:i:s');
					$q['session'] = $this->session->userdata('session_id');
					$q['qto'] = time();
					$q['ip'] = $_SERVER['REMOTE_ADDR'];
					$q['view'] = 1;
					$q['print'] = 0;
					}

				if ($stype==3) //sub search
					{
					$sid = 0;
					unset($q['aid']);
					$q['qdate'] = date('Y-m-d H:i:s');
					$q['session'] = $this->session->userdata('session_id');
					$q['ip'] = $_SERVER['REMOTE_ADDR'];
					$q['view'] = 1;
					$q['print'] = 0;
					}
					
					//ticket 1261
					$this->load->model('Saved_search_db');
					$sv = $this->Saved_search_db->getSave(array('sv_aid'=>$sid));
					if ($sv) 
					{
						$rules = $this->session->userdata('rules');
						if (isset($rules['dlimit']))
						{
						$rules['tlimit'] = time();
						if ($rules['dlimit']>0)
							{
							$rules['flimit'] = time()-(60*60*24*$rules['dlimit']);
							if ($rules['flimit']< $q['qfrom']) $q['qfrom'] = $rules['flimit'];
							}
						}
						
						if (date('Y-m-d',$q['qto']) == date('Y-m-d',strtotime($sv['sv_date']))) $q['qto'] = time();
	
					}


				}

			//get entryids

			$pagemax = 100000;

			if (($start+$rp)>$pagemax)
				{
				$data['page'] = $page;
				$data['total'] = $q['qresult'];
				$data['response'] = rp('Sorry, our database only allows you to view the first 100,000 records from any search. Please consider changing the date range of your search to reveal records beyond that limit.',1);
				json_exit($data);
				}

			$rs = $this->xdb->getEntries($q['query'],$q['qfrom'],$q['qto'],$start,$rp,$sortname,$sortorder,0,0,0,$filters);

			if (!$rs['error'])
				{
				$q['qresult'] = $rs['total_found'];
			    $q['qtime'] = $rs['time'];
			    $q['co_id'] = $country;
			    $q['s_id'] = defined('S_ID') ? S_ID : "";
				
				if (!$sid)
					{
					$sid = $this->xdb->saveQuery($q);
					$data['sid'] = $sid;
					$data['nstotal'] = $this->xdb->totalSearchbyUserId($user['userid']);
					$data['nsleft'] = $qlimit - $data['nstotal'];
					}

				//update search history
				$this->xdb->updateQuery($sid,$q);


				//get rows
				$rows = array();
				$db = array();


				if ($q['qresult']) $db = $this->xdb->getRows($rs['entries'],false," FIELD(entryid,".implode(',',$rs['entries']).' )',false,false,'igalldata');

				//print_r($rs['entries']); exit;

				if ($user['utype']==17)
				{
					$this->session->unset_userdata('allowedentries');
					$allowedentries = array();
				}




				foreach ($db as $row)
				{

				//trial app user stop at 3 rows
				if ($user['utype']==17&&count($rows)>2)
				{


				$rows[] = array(
						"id" => $row['entryid'],
						"cell" => array(
							"<a href=\"".site_url("promo/show/3")."?TB_iframe=true&height=370&width=600\" class=\"view view2\" title=\"Upgrade From Trial Account Now!\" ></a>"
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: 50px; margin-left: 10px;"></div>'
							,'<div class="blur blur2" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,50).'px"></div>'
							,'<div class="blur blur2" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,50).'px"></div>'
							,'<div class="blur blur2" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,50).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,40).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,30).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: 5px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
						)
					);


				continue;
				}

				if ($user['utype']==17)
				{
					$allowedentries[] = $row['entryid'];
					$this->session->set_userdata('allowedentries',$allowedentries);
				}

				//hlite rows
				$row = hlite2($row,$qlite,"us");

				$rows[] = array(
						"id" => $row['entryid'],
						"cell" => array(
							//"<a href=\"".site_url("main/entry/$sid/".$row['entryid'])."?TB_iframe=true&height=480&width=760\" class=\"view\" title=\"View Shipment Details - ".substr(strip_tags($row['consname']),0,30)."\" ></a>"
							//"<a href=\"".site_url("main/entry/$sid/".$row['entryid'])."?TB_iframe=true&height=480&width=760\" class=\"view\" title=\"View Record ".$row['entryid']." - ".substr(strip_tags($row['consname']),0,30)."\" ></a>"
							 $row['product']
							,$row['consname']
							,$row['shipname']
							/*
							,$row['actdate']
							,toLB($row['weight'],$row['weightunit'])
							,toKG($row['weight'],$row['weightunit'])
							,number_format($row['manifestqty'])." ".$row['manifestunit']
							,strtoupper($row['fport'])
							,strtoupper($row['uport'])
							,$row['carriercode']
							,$row['vesselname']
							,$row['countryoforigin']
							,$row['marks']
							,$row['consaddr']
							,$row['shipaddr']
							,$row['zipcode']
							,$row['con_num_count']
							,$row['containernum']
							,$row['portname']					
							,$row['masterbillofladingind']
							,$row['masterbilloflading']
							,$row['inboundentrytype']." - ".$row['ib_desc']
							,$row['carriercode']." - ".$row['company_name']
							,$row['address'].", ".$row['city']." ".$row['state']." ".$row['czipcode']." ".$row['country']." ".$row['czipcode']
							,$row['placereceipt']
							,$row['ntfyname']
							,$row['ntfyaddr']
							*/
						)
					);

				}


				$scount = $this->session->userdata('fissue');

				if($scount >= 4)
				{
					$data['response']  = buildResponseError();
				}
				else
				{
					$extra = "of Import Data ";
					$data['response'] = rp(buildResponse($q,$extra));
				}
				
				$data['scount'] = $scount;

				$data['rows'] = $rows;
				$data['total'] = $q['qresult'];

				if ($start>$data['total']) $page = 1;

				$data['page'] = $page;



			} else {
				$data['response'] = rp($rs['error'],1);
			}


		//load data to JSON
		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);
    }

    function get_saves()
    {
    	
    	$co_id = $this->input->post('country');
		$co_id = addslashes($co_id);
		$mtoken = $this->input->post('mtoken');

		$data['logout'] = "false";

		if(!$co_id)
		{
			$co_id = "us";
		}

		$dt['username'] = $this->input->post('username');
		$user = $this->User->getMUser($dt,$this->select);

		if($mtoken != $user['session_id'] && $user['multi'] == 0)
		{
			$data['logout'] = "true";
		}

        $this->load->model('Saved_search_db','xdb');
        $cmd = $this->input->post('cmd');

        $page = $this->input->post('page');
        $rp = $this->input->post('rp');
        $sid = $this->input->post('sid');

        $test = $this->input->post('test');

        if(!$test)
        {
        	if ($sid) 
        	{
        		$data['deleted'] = "false";
        		$this->xdb->deleteSave("sv_id = $sid");
        	}
        }
        else
        {
        	$data['catched_posts'] = $_POST;
        }

		if ($co_id == 'us')
			$sdb = $this->xdb->getSaves("sv_userid = $user[userid] AND search_log.co_id IN ('us','')",$page,$rp);
		else
			$sdb = $this->xdb->getSaves("search_log.co_id = '{$co_id}' AND sv_userid = $user[userid]",$page,$rp);

        $data['total'] = $sdb['total'];
        $data['page'] = $sdb['page'];
        
        $data['coid'] = $co_id;

        $db = $sdb['db'];

        $rows = array();

                if (is_array($db)) foreach ($db as $row)
                {

                $rows[] = array(
                        "id" => $row['sv_aid'],
                        "cell" => array(
                             $row['sv_date']
                            ,$row['sv_name']
                            ,$row['sv_id']
                            //,"<input type='button' id='delsearchb' class='subb' onclick='deleteSearch($row[sv_id]); return false;' title='Delete saved search?' />"
                        )
                    );
                }

		if(!$sid)
		{                
        	$data['rows'] = $rows;
		}
		else
		{
			$data['deleted'] = "true";
		}
        $json['json'] = $data;
        $this->load->view('template/ajax2',$json);
    }
    
    function search2()
    {
		$this->load->view('template/json');
	}

	function get_his()
    {
        $this->load->model('Shipping2','xdb');
        $cmd = $this->input->post('cmd');
        $mtoken = $this->input->post('mtoken');

        $data['logout'] = "false";

        $co_id = $this->input->post('country');
		$co_id = addslashes($co_id);

		if(!$co_id)
		{
			$co_id = "us";
		}

		$dt['username'] = $this->input->post('username');
		$user = $this->User->getMUser($dt,$this->select);

		if($mtoken != $user['session_id'] && $user['multi'] == 0)
		{
			$data['logout'] = "true";
		}

        $page = $this->input->post('page');
        $rp = $this->input->post('rp');
        $sid = $this->input->post('sid');

        $order = "qdate desc";
		if ($co_id != 'us')
			$sdb = $this->xdb->getQueries("co_id = '{$co_id}' AND userid = $user[userid] AND qlite !=''",$page,$rp,$order,50);
		else
		$sdb = $this->xdb->getQueries("(co_id = 'us' OR co_id = '') AND userid = $user[userid] AND qlite !=''",$page,$rp,$order,50);

        $data['total'] = $sdb['total'];

        $data['page'] = $sdb['page'];

        $db = $sdb['db'];

        $this->load->helper('str2');

        $rows = array();

                if (is_array($db)) foreach ($db as $row)
                {
				
				$extra = "";
				
				if(isset($row['co_id']) && $row['co_id'])
				{
					if($row['co_id'] == "" || $row['co_id'] == "us")
					{
						$row['co_id'] = "us";
						$row['dtype'] = "im";
					}	
					
					$c = country_name($row['co_id']);
					$dt = $row['dtype'] == "im" ? 'Import' : 'Export';
					$extra = "of {$c} {$dt} Data ";
					
				}

				$description = buildResponseMobile($row,$extra);

		    	$pattern = "|\<b\>[\w\d\s]+\<\/b\>|";
		    	preg_match($pattern,$description,$matches);
		    	
		    	$keyword = "";

		    	if(is_array($matches))
		    	{
		    		if(isset($matches[0]))
		    		{
		    			$keyword = strip_tags($matches[0]);
		    		}
		    	}
                
                $rows[] = array(
                        "id" => $row['aid'],
                        "cell" => array(
                             $row['qdate']
                            ,$description 
                            ,$keyword
                        )
                    );
                }


        $data['rows'] = $rows;
		//$data['db'] = $db;
        $json['json'] = $data;
        $this->load->view('template/ajax2',$json);
    }

    function sub_save()
    {
        $this->load->model('Sub_search_db','xdb');
        $this->load->model('Users_db');
        $this->load->helper('str2');

        $mtoken = $this->input->post('mtoken');

        $dt['username'] = $this->input->post('username');
		$user = $this->User->getMUser($dt,$this->select);

		$data['success'] = "true";

		$data['logout'] = "false";

		if($mtoken != $user['session_id'] && $user['multi'] == 0)
		{
			$data['logout'] = "true";
		}

        //print_r($this->db->last_query()); exit;

        $sv['su_name'] = trim($this->input->post('name'));
        $sv['su_aid'] = $this->input->post('sid');
        $sv['su_date'] = date('Y-m-d h:m:s');
        //$sv['su_userid'] = $user['id'];
		$sv['su_userid'] = $user['userid'];
        $sv['su_sentto'] = $this->input->post('email');
        $sv['su_status'] = 1;
        $country = $this->input->post('country');

        //init vars
        $data['response'] = "Please generate a search before subscribing.";
        $data['action'] = 'savesub';


        if (in_array($user['utype'],array(14,6,8))&&$user['allowalerts']!=1)
        {
            $alert_limit = 3;

            if ($user['alert_limit']) $alert_limit = $user['alert_limit'];

            $sdb = $this->xdb->getSaves("su_userid = $user[id]",1,1);
            if ($sdb['total']>=$alert_limit)
                {
                $data['response'] = "Your account only allows up to $alert_limit subscriptions";
                $data['success'] = "false";
                json_exit($data);
                }

        }

        if(!$sv['su_aid'])
        {
        json_exit($data);
        }

        if (!$sv['su_name'])
        {
        $data['response'] = "Please provide a name for your subscription.";
        $data['success'] = "false";
        json_exit($data);
        }

        $db = $this->xdb->getSave(array("su_name"=>$sv['su_name'],"su_userid"=>$user['userid'],"search_log.co_id"=>$country));

        if ($db)
        {
        $data['response'] = "$sv[su_name] already exists.";
        $data['success'] = "false";
        json_exit($data);
        }

        $db = $this->xdb->getSave(array("su_aid"=>$sv['su_aid'],"su_sentto"=>$sv['su_sentto']));

        if ($db)
        {
	        $data['response'] = "This subscription has already been saved as $db[su_name]";
	        $data['success'] = "false";
	        json_exit($data);
        }

        $this->xdb->saveSearch($sv);

        $data['action'] = "closereset";
        $data['response'] = "$sv[su_name] successfully saved.";

        $json['json'] = $data;
        $this->load->view('template/ajax2',$json);

    }

    function search_save()
    {
        $this->load->model('Saved_search_db','xdb');
		$this->load->model('Users_db');
        $this->load->helper('str2');

        $dt['username'] = $this->input->post('username');
		$user = $this->User->getMUser($dt,$this->select);
		
		$mtoken = $this->input->post('mtoken');
		

		$data['success'] = "true";

		$data['logout'] = "false";
		
		if($mtoken != $user['session_id'] && $user['multi'] == 0)
		{
			$data['logout'] = "true";
		}

        $sv['sv_name'] = trim($this->input->post('reportname'));
        $sv['sv_aid'] = $this->input->post('sid');
        $sv['sv_date'] = date('Y-m-d h:m:s');
        $sv['sv_userid'] = $user['userid'];


        //init vars
        $data['response'] = "Please generate a search before saving.";
        $data['action'] = 'saverep';

        if(!$sv['sv_aid'])
        {
        json_exit($data);
        }

        if (!$sv['sv_name'])
        {
        	$data['success'] = "false";
        	$data['response'] = "Please provide a name for your search.";
        	json_exit($data);
        }

        $db = $this->xdb->getSave(array("sv_name"=>$sv['sv_name'],"sv_userid"=>$user['userid']));

        if ($db)
        {
        	$data['success'] = "false";
       	 	$data['response'] = "$sv[sv_name] already exists.";
        	json_exit($data);
        }

        $db = $this->xdb->getSave(array("sv_aid"=>$sv['sv_aid'],"sv_userid"=>$user['userid']));

        if ($db)
        {
      	  	$data['success'] = "false";
        	$data['response'] = "This search has already been saved as $db[sv_name]";
        	json_exit($data);
        }

        $this->xdb->saveSearch($sv);

        $data['action'] = "closereset";
        $data['response'] = "$sv[sv_name] successfully saved.";

        $json['json'] = $data;
        $this->load->view('template/ajax2',$json);

    }

    function get_subs()
    {

        $this->load->model('Sub_search_db','xdb');
        $cmd = $this->input->post('cmd');

        $co_id = $this->input->post('country');
		$co_id = addslashes($co_id);

		$data['logout'] = "false";

		$mtoken = $this->input->post('mtoken');

		if(!$co_id)
		{
			$co_id = "us";
		}

        $dt['username'] = $this->input->post('username');
		$user = $this->User->getMUser($dt,$this->select);

		if($mtoken != $user['session_id'] && $user['multi'] == 0)
		{
			$data['logout'] = "true";
		}

        $page = $this->input->post('page');
        $rp = $this->input->post('rp');
        $sid = $this->input->post('sid');

        $status = $this->input->post('status');
        if ($status=='true') $status = 1; else $status = 0;

        if ($sid&&$cmd=='delete') $this->xdb->deleteSave("su_id = $sid");
        if ($sid&&$cmd=='statchange') $this->xdb->updateSub("su_id = $sid",array('su_status'=>$status));

		if ($co_id == 'us')
			$sdb = $this->xdb->getSaves("su_userid = $user[userid] AND search_log.co_id IN ('us','')",$page,$rp);
		else
			$sdb = $this->xdb->getSaves("su_userid = $user[userid] AND search_log.co_id IN ('{$co_id}')",$page,$rp);

	
        $data['total'] = $sdb['total'];
        $data['page'] = $sdb['page'];

        $db = $sdb['db'];

        $rows = array();

                if (is_array($db)) foreach ($db as $row)
                {

                $checked = "checked='checked'";
                if ($row['su_status']==0) $checked = "";

                $rows[] = array(
                        "id" => $row['su_aid'],
                        "cell" => array(
                            $row['su_date']
                            ,$row['su_name']
                            ,$row['su_sentto']
                            ,$row['su_id']
                            ,$row['su_status']
                        )
                    );
                }


        $data['rows'] = $rows;

        $json['json'] = $data;
        $this->load->view('template/ajax2',$json);
    }

    function regex()
    {
    	# \<b\>[\w\d\s]+\<\/b\>

    	$subject = "the quicb rown fox <b>jumps</b> over the lazy dog.";
    	$pattern = "|\<b\>[\w\d\s]+\<\/b\>|";
    	preg_match($pattern,$subject,$matches);
    	print_r($matches);
    }

    function entry($sid="",$entryid="",$uname="")
	{
	
		if($_POST)
		{
			$data['posts'] = $_POST;
		}
		
		$mtoken = $this->input->post('mtoken');

		$ig = $this->input->post('sid');
		$id = $this->input->post('entryid');
		$username = $this->input->post('username');

		$data['logout'] = "false";

		if(!$ig) $ig = $sid;
		if(!$id) $id = $entryid;
#echo $uname;
		$dt['username'] = $username;

		if(!$username)
		{
			$username = $uname;
		}
		
		$user = $this->User->getMUser($dt,$this->select);

		if($mtoken != $user['session_id'] && $user['multi'] == 0)
		{
			$data['logout'] = "true";
		}
	
		#print_r($user); exit;
		$this->load->model('Shipping2','mdb');
		$this->load->helper('str2');
		
		$data['row'] = $this->mdb->getEntry($id);
	
#		print_r($data['row']); exit;

		if (!$data['row']) show_404();
		$data['ig'] = $ig;
		$data['query'] = $this->mdb->getQuery($ig);

		$carrier_data = $this->mdb->getCarrier($data['row']['carriercode']);
		
		//var_dump($carrier_data);		
		//var_dump($data['row']['carriercode']);
		$data['row']['carrier'] = Array();
		
		if(!empty($data['row']['carriercode']))
		{
			$data['row']['carrier'] = $carrier_data;	
		}
		$data['json'] = $data;
        #$this->load->view('template/ajax2',$json);		
#		print_r($data);
		$this->load->view('template/ajax2',$data);
	}
}
