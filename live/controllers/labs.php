<? 

class Labs extends Controller {

	function Labs()
	{
	parent::Controller();
	}
	
	
	function index()
	{
		$time = 500;
		sleep($time);
		echo $time;
		exit;
	}
	
	function testsession()
	{
		$this->session->sess_update();
	}
	
	function pinfo()
	{
	echo $this->session->userdata('ip_address');
	echo $this->input->ip_address();
	//$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
	//print_r($_SERVER);
	//phpinfo();
	exit;
	}

	function test2()
	{
		$this->load->model('Shipping2','xdb');
		$this->load->helper('sphinx');
		$query = "@consname_n \"nikeusa\"";
		$result = $this->xdb->getEntries($query,0,0,0,12,0,0,'countryoforigin_s');
		echo "<pre>";
		if ($result['total_found'])
			{
			}
			print_r($result); 
			
		$db = $this->xdb->getRows($result['entries'],"countryoforigin");
		
		print_r($db);
			
			exit;
	
	}
	
	function getcookie()
	{
		echo "<pre>";
		print_r($_COOKIE); exit;
	}

	function test($start=0,$rp=0)
	{

		$this->benchmark->mark('code_start');
		$this->load->model('Shipping2','xdb');
		$this->load->model('Labs_db');
		$this->load->helper('file');
		
		$stat['consignee'] = 0;
		$stat['shipper'] = 0;
		
		$data = $this->Labs_db->getMaxNormal();
		
		if ($data['n_id']<$start) 
		{
			echo "done.";
			exit;
		}
		
		if ($rp>0)
			$data['n_id'] = $rp;
			
		for($x=$start;$x<=$data['n_id'];$x++)
		{
			//consider adding throttling every X using sleep Or Add paging
		
			$n_row = $this->Labs_db->getNormal(array('n_id'=>$x));
			
			if (!isset($n_row['n_keyword'])) continue;
			if ($n_row['n_count']>0) continue;
			
			$query = "@consname_n \"$n_row[n_keyword]\"";
			$result = $this->xdb->getEntries($query,0,0,0,1);
			if ($result['total_found'])
				{
				//insert update as consignee here
				$this->Labs_db->updateNormal(array('n_id'=>$x),array('is_consignee'=>1,'n_count'=>$result['total_found']));
				$stat['consignee'] += 1;
				}
			$query = "@shipname_n \"$n_row[n_keyword]\"";
			$result2 = $this->xdb->getEntries($query,0,0,0,1);
			if ($result2['total_found'])
				{
				//insert update as shipper here
				if ($result['total_found']>$result2['total_found'])
					$this->Labs_db->updateNormal(array('n_id'=>$x),array('is_consignee'=>2,'is_shipper'=>1,'n_count'=>$result2['total_found']+$result['total_found']));
				else
					$this->Labs_db->updateNormal(array('n_id'=>$x),array('is_shipper'=>1,'n_count'=>$result2['total_found']+$result['total_found']));
				$stat['shipper'] += 1;
				}
				$last = $x;
		
				write_file("stat/ni_update.txt",$x);

		}
		
		$this->benchmark->mark('code_end');
		$elapse = $this->benchmark->elapsed_time('code_start', 'code_end');
		
		echo "<pre>";
		print_r($stat);
		print_r("\n$elapse seconds");
		echo "</pre>";
		
		//echo "<script type='text/javascript'>location.href='".site_url("labs/test/".($start+100)."/".($start+200))."';</script>";
		
		exit;

	
	}


	function urlizer($start=0,$rp=0)
	{

		$this->benchmark->mark('code_start');
		$this->load->model('Shipping2','xdb');
		$this->load->model('Labs_db');
		$this->load->helper('file');
		
		$data = $this->Labs_db->getMaxNormal();
		
		if ($data['n_id']<$start) 
		{
			echo "done.";
			exit;
		}
		
		if ($rp>0)
			$data['n_id'] = $rp;
		
		if ($start == 0) $start = 1;
		
		$last = 0;
			
		for($x=$start;$x<=$data['n_id'];$x++)
		{
			$n_row = $this->Labs_db->getNormal(array('n_id'=>$x));
			$url = $n_row['n_sample'];
			$url = strtolower(trim(preg_replace('/[^a-zA-Z0-9]+/', '-', $url), '-'));
			$this->Labs_db->updateNormal(array('n_id'=>$x),array('n_url'=>$url));
			
			$last += 1;

			write_file("stat/ni_urlizer.txt",$last);

		}

		$this->benchmark->mark('code_end');
		$elapse = $this->benchmark->elapsed_time('code_start', 'code_end');
		
		//echo "<pre>";
		print_r("\nProcess $last in $elapse seconds");
		//echo "</pre>";
		
		//echo "<script type='text/javascript'>location.href='".site_url("labs/urlizer/".($start+1000)."/".($start+2000))."';</script>";
		
		exit;

	
	}


	function privatizer($end='')
	{

		$this->benchmark->mark('code_start');
		$this->load->model('Shipping2','xdb');
		$this->load->model('Labs_db');
		$this->load->helper('file');
	
		$fn = "perl/privacy.sh";
	
		$lines = file($fn,FILE_SKIP_EMPTY_LINES);
		//echo $lines[1]; exit;
		
		// Loop through our array, show HTML source as HTML source; and line numbers too.

		$last = 0;
		$first = "";

		foreach ($lines as $line_num => $line) {
		
			//if ($last>$end && $end>0) break;
		
		    //echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
		    $line = str_replace("rm -f ../importers/", "", $line);
		    $line = str_replace("rm -f ../shipments/", "", $line);
		    $line = str_replace(".html", "", $line);
		
		    if ($line=='') continue;
		
			if ($first=='') $first == $line;
			
		    //if ($end == $line) break;
		
			$cond = "@n_url (\"$line\")";
			$db = $this->Labs_db->getCompanies($cond,1,1);

		    if (!isset($db['result'][0])) continue;
		    $n_row = $db['result'][0];
		    
		    $this->Labs_db->updateNormal(array('n_id'=>$n_row['n_id']),array('n_individual'=>1));
		    
		    $last += 1;

		}	

		write_file("stat/ni_privatizer.txt",$last." - ".$first);

		$this->benchmark->mark('code_end');
		$elapse = $this->benchmark->elapsed_time('code_start', 'code_end');

		echo "<pre>";
		print_r("\nProcess $last in $elapse seconds");
		echo "</pre>";
		exit;

    
	}

	function vmtest($ntype,$cname)
	{
		$this->load->model('Labs_db');
		$this->load->helper('sphinx');
		$query = "@vr_$ntype"."_n \"$cname\"";
		$result = $this->Labs_db->getVR($query,1,1000,0,0,"vr_shipname");
		echo "<pre>";
		print_r($result);
			
		exit;
	
	}

	
	function vmapper($start=0,$rp=0)
	{

		$this->benchmark->mark('code_start');
		$this->load->model('Shipping2','xdb');
		$this->load->model('Labs_db');
		$this->load->helper('file');
		
		
		$data = $this->Labs_db->getMaxEntry();
		
		if ($data['entryid']<$start) 
		{
			echo "done.";
			exit;
		}
		
		if ($rp>0)
			$data['entryid'] = $rp;
		
		$last = 0;
			
		for($x=$start;$x<=$data['entryid'];$x++)
		{
			//consider adding throttling every X using sleep Or Add paging
			
			$y = $x + 5000;
			if ($y>$data['entryid']) $y = $data['entryid'];
			
			$c = array();
			
			for ($i=$x;$i<=$y;$i++) $c[] = $i; 
			

/*
			echo '<pre>';
			print_r($c); //exit;
*/

			
			$n_rows = $this->Labs_db->getEntriesbyId($c);
			
			//echo $this->db->last_query(); exit;
			
			foreach ($n_rows as $n_row)
			{
			if ($n_row['consname_n']=='') continue;
			if ($n_row['shipname_n']=='') continue;
			
			$ndata['vr_rel'] = $n_row['consname_n'].'-'.$n_row['shipname_n'];
			$ndata['vr_consname_n'] = $n_row['consname_n'];
			$ndata['vr_consname'] = $n_row['consname'];
			$ndata['vr_consaddr'] = $n_row['consaddr'];
			$ndata['vr_shipname_n'] = $n_row['shipname_n'];
			$ndata['vr_shipname'] = $n_row['shipname'];
			$ndata['vr_shipaddr'] = $n_row['shipaddr'];
			
			if ($this->Labs_db->insertVmap($ndata))
				$last += 1;
			}
			
			unset($n_rows);
			unset($c);	
			
			$x = $y;	

			write_file("stat/crawl.txt",$x);

		}

		$this->benchmark->mark('code_end');
		$elapse = $this->benchmark->elapsed_time('code_start', 'code_end');
		
		//echo "<pre>";
		print_r($last);
		print_r("\n$elapse seconds");
		//echo "</pre>";
		
		//echo "<script type='text/javascript'>location.href='".site_url("labs/test/".($start+100)."/".($start+200))."';</script>";
		
		exit;

	
	}
	
	
	
}	