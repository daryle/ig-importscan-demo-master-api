<?php


class Sendalerts extends Controller {

	function Sendalerts()
	{
		parent::Controller();

	}
	
	/* Send Alerts
	* Everytime there are new shipments added to our database,
	* we trigger this function so that clients who subscribed for a given keyword 
	* will receive email notifications
	*/
	function index($country="us")
	{


		set_time_limit(0);
		ini_set('memory_limit', '-1');
		ini_set('output_buffering', 'Off');
		ini_set('output_buffering', FALSE);
		ini_set('output_buffering', 0);

		$this->load->model('Sub_search_db');
		$this->load->model('Shipping2','xdb');
		$this->load->model('Countries_db');

		$cond = "su_status = 1";

		if($country != "us")
		{
			$this->xdb->country = $country;
			$cond = "su_status = 1 and co_id = '$country'";
		}

		$rs = $this->Sub_search_db->getSaves($cond);
		
		$log['nonewresults'] = 0;
		$log['errorsend'] = 0;
		$log['sphinxerror'] = 0;
		$log['totolprocessed'] = 0;
		
		if (!$rs) return false;
		
		foreach ($rs['db'] as $su)
		{

			if (!($su['utype']<5||$su['allowalerts']==1||in_array($su['utype'],array(16,9,14,6,8,101,102,20,21,22,23,24,26,27,28,29))))
				{
				continue;
				}
			
			if (!$su['status']||$su['suspended']) 
				{
				$this->Sub_search_db->saveLog($su['su_id'],0,'Account Suspended/Disabled');
				continue;
				}
				
			if (trim($su['suspendon'])&&$su['suspendon']!='0000-00-00')
			{
				if (time()>=strtotime($su['suspendon'])) 
				{
					$this->Sub_search_db->saveLog($su['su_id'],0,'Account Expired '.$su['suspendon']);
					continue;
				}
			}
		
			$q = $this->xdb->getQuery($su['su_aid']);

			$q['qto'] = time();
			
			$co_abbre = $su['co_id'];
			$cname = "United States";
			
			if($co_abbre != "")
			{
				$this->xdb->country = $co_abbre;
				$co = $this->Countries_db->getCountry(array('country_avre'=>$su['co_id']));
				if(!$co)
				{
					$cname = $co['country_name'];
				}
			}
			
			$su['cname'] = $cname;

			//echo $cname."<br/>";
			
			$sortname = "actdate";

			if($country != "us")
				$sortname = "adate";

			$ds = $this->xdb->getEntries($q['query'],$q['qfrom'],$q['qto'],0,15,$sortname,"desc");
			//print_r($ds); exit;
			
			
			if ($ds['error']!=false) 
				{
				//add error logging here
				$this->Sub_search_db->saveLog($su['su_id'],0,$ds['error']);
				
				$log['sphinxerror'] += 1;
				continue;
				}

			$qresult = $su['su_qresult'];
			//add override from last log result
			if ($qresult==0) $qresult = $q['qresult'];
		
			if ($ds['total_found']>$qresult)
				{
					
				$su['data_entry'] = array();
				sort($ds['entries']);
				$data_entryid = end($ds['entries']);
				
				$table = "igalldata";

				if($country != "us")
					$table = "igalldata_".$country;

				$su['data_entry'] = $this->xdb->getEntry($data_entryid,$table);

				//echo "<pre>";
				//print_r($su['data_entry']); exit;
				
				//update sub
				$this->Sub_search_db->updateSub("su_id = $su[su_id]",array('su_qresult'=>$ds['total_found']));
				
				//send email
				$email_view = "alert";

				if($country != "us")
				{
					$email_view = "alert_latin";
					$su['cfields'] = $this->xdb->countryFields($country);
				}
					
				$message = $this->load->view('email/'.$email_view,$su,true);

				$this->load->library('email');
				
				$config['mailtype'] = 'html';
				
				$this->email->initialize($config);		
				
				$this->email->from('info@importgenius.com', 'ImportGenius Team');
				$this->email->to($su['su_sentto']);
				//$this->email->to('lovely@importgenius.com');
				
				$e['subject'] = "New Shipment Alert from ImportGenius.com for $su[su_name]";
				//$e['subject'] = "New Shipments on $su[su_name] - ImportGenius Alert";
				$e['content'] = $message;
				$e['country'] = $su['co_id'];
				
				$dmessage = $this->load->view('template/email',$e,true);
				
				$this->email->subject($e['subject']);
				$this->email->message($dmessage);
				
				$mail_sender = $this->email->send();
				
				if ($mail_sender)
					{
					//log if sent
					$this->Sub_search_db->saveLog($su['su_id'],1,'Sent',$ds['total_found']);
					
					} else {
					//log if not sent
					$this->Sub_search_db->saveLog($su['su_id'],0,'Email error');
					
					$log['errorsend'] += 1;
					}
				
				
				}
			else
				{
				//log if no new result
				$this->Sub_search_db->saveLog($su['su_id'],0,'No new results',$ds['total_found']);
				$log['nonewresults'] += 1;
				
				}			
			
			$log['totolprocessed'] += 1;
		} //end loop
		
		echo "<pre>".print_r($log,true)."</pre>";
		exit;
		
	}

	public function testemail($co_id = "us")
	{
		$this->load->model('Sub_search_db');
		$this->load->model('Shipping2','xdb');

		$email_view = "alert";

		if($co_id != "us")
			$email_view = "alert_latin";

		$cond = "su_status = 1";

		if($co_id != "us")
		{
			$this->xdb->country = $co_id;
			$cond = "su_status = 1 and co_id = '$co_id'";
		}

		$su = $this->Sub_search_db->getSaves($cond);

		$table = "igalldata";

		if($co_id != "us")
			$table = "igalldata_".$co_id;
	
		$su['data_entry'] = $this->xdb->getEntry('9691601',$table);

		$su['subject'] = "Testing";
		$su['su_name'] = "Testing";

		$su['cfields'] = $this->xdb->countryFields($co_id);

		//echo "<pre>";
		//print_r($su['cfields']);
		//exit;

 		$message = $this->load->view('email/'.$email_view,$su,true);

		$e['content'] = $message;
		$e['country'] = $co_id;

		$this->load->view('template/email',$e);
	}
	
	
}	
