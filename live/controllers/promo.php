<?php

class Promo extends Controller {

    function Promo() {
        parent::Controller();
        $method = $this->uri->rsegment(2);
        $user = $this->session->userdata('user');
        if (!$user)
            redirect('login');
    }

    function show($id = false, $param2 = FALSE) {
        if (!$id)
            show_404();
        $data['promoid'] = $id;
        $this->load->model('users_db');
        $this->load->model('Shipping2');
        $user = $this->session->userdata('user');

        $planname = $this->users_db->getUtypeAmount(array('data_value' => $user['utype'], 'data_code' => 'U_TYPE'));
        if ($planname) {
            $data['planname'] = $planname['data_display'];

            if (strpos($data['planname'], 'Limited') !== FALSE)
                $data['planname'] = 'Limited';

            if (strpos($data['planname'], 'Trial') !== FALSE)
                $data['planname'] = 'Trial';

            if (strpos($data['planname'], 'Enterprise') !== FALSE)
                $data['planname'] = 'Enterprise';

            if (strpos($data['planname'], 'Basic') !== FALSE)
                $data['planname'] = 'Basic';

            if (strpos($data['planname'], 'Standard') !== FALSE)
                $data['planname'] = 'Standard';

            if (strpos($data['planname'], 'Limited') !== FALSE)
                $data['planname'] = 'Limited';

            if (strpos($data['planname'], 'Plus') !== FALSE)
                $data['planname'] = 'Plus';
        }

        $data['card'] = $this->users_db->getLatestCard(array('username' => $user['username']));
        $data['log'] = $this->users_db->getActivateData(array('tran_id' => $user['id']));
        //print_r($data['card']); exit;

//        if ($id === 'rapid-sourcing' && $param2 !== '') {
            $this->load->library('user_agent');
            $this->load->model('Alibaba_db');
//            $data2 = array();
//            $data2['userid'] = $user['id'];
//            $data2['keyword'] = $param2;
//            $data2['action'] = 1;
//            $data2['message'] = '';
//            $data2['datetime'] = date("Y-m-d H:i:s", time());
//            $data2['browser_name'] = $this->agent->browser();
//            $data2['browser_version'] = $this->agent->version();
//            $this->Alibaba_db->save_log($data2);

            $data['aliMemberId'] = $this->Alibaba_db->get_alibaba_id($user['id']);
            $data['never_ask_again'] = $this->Alibaba_db->get_alibaba_never_ask_again($user['id']);
            $data['param2'] = $param2;
            $data['units'] = $this->Shipping2->getUnits();


        $data['content'] = $this->load->view("promos/$id", $data, true);
        $this->load->view("template/popup", $data);
    }

    function post() {

        $response = $this->input->post('response');
        $promoid = $this->input->post('promoid');
        $user = $this->session->userdata('user');

        if (!$promoid || !$response)
            return false;

        $this->load->model('promo_db');

        $promo = $this->promo_db->getPromo(array('prid' => $promoid));

        $this->promo_db->save($promoid, $user['id'], $response);

        $this->load->library('email');

        $config['mailtype'] = 'html';

        $this->email->initialize($config);

        if ($response == '1') {
            $data['user'] = $user;
            $data['promo'] = $promo;
            $data['message'] = $this->input->post('pmessage');

            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            #$this->email->to('rhonda@importgenius.com,info@importgenius.com,paulopmx@gmail.com');
            //$this->email->to('rhonda@importgenius.com,david@importgenius.com,kanko@importgenius.com,autoemails@importgenius.com,ryan@importgenius.com');
            //$this->email->to('dev@importgenius.com');
            $this->email->to('info@importgenius.com');
            $this->email->bcc('paulopmx@gmail.com, lovely@codeninja.co, rgustafson@importgenius.com');
            $this->email->subject("User " . $user['username'] . " responded Yes for " . $promo['prname'] . " ");
            $message = $this->load->view('email/promo', $data, true);
            $this->email->message($message);

            $this->email->send();
        }

        if ($response == '1' && $promoid == 2) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");
            $message = $this->load->view('email/promo2', '', true);
            $this->email->message($message);

            $this->email->send();
        }

        if ($response == '1' && $promoid == 3) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");
            $message = $this->load->view('email/promo2', '', true);
            $this->email->message($message);

            $this->email->send();
        }

        if ($response == '1' && ($promoid == 5 || $promoid == 8)) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");
            $message = $this->load->view('email/promo8', '', true);
            $this->email->message($message);

            $this->email->send();
        }

        if ($response == '1' && $promoid == 6) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");
            $message = $this->load->view('email/promo6', '', true);
            $this->email->message($message);

            $this->email->send();
        }


        if ($response == '1' && $promoid == 7) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");
            $message = $this->load->view('email/promo7', '', true);
            $this->email->message($message);

            $this->email->send();
        }

        if ($response == '1' && $promoid == 9) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");
            $message = $this->load->view('email/promo9', '', true);
            $this->email->message($message);

            $this->email->send();
        }

        if ($response == '1' && $promoid == 10) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);
            $this->email->bcc('lovely@codeninja.co');

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");


            $message = $this->load->view('email/promo10', $data, true);
            $this->email->message($message);

            $this->email->send();
        }

        if ($response == '1' && $promoid == 11) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);
            $this->email->bcc('lovely@codeninja.co');

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");


            $message = $this->load->view('email/promo11', $data, true);
            $this->email->message($message);

            $this->email->send();
        }

        if ($response == '1' && $promoid == 12) {
            $this->email->from('info@importgenius.com', 'ImportGenius');
            $this->email->reply_to('onlinesales@importgenius.com', 'ImportGenius');
            $this->email->to($user['email'], $user['username']);
            $this->email->bcc('lovely@codeninja.co');

            $this->email->subject("Your request to upgrade your ImportGenius.com account has been received");


            $message = $this->load->view('email/promo12', $data, true);
            $this->email->message($message);

            $this->email->send();
        }


        $dt['response'] = "success";
        $dt['action'] = 'success';
        $json['json'] = $dt;
        $this->load->view('template/ajax2', $json);
    }

}

?>