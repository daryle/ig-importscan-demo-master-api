<?php

class Vmap extends Controller {

	private $country;

	function __construct() {
		parent::__construct();

/*
		//require https
		if (config_item('proto')=='http'&&config_item('domain')=='importgenius.com')
			header("Location: ".str_replace('p://','ps://',site_url($this->uri->uri_string())));
*/

		$method = $this->uri->rsegment(2);
		if ($method=='render2'||$method=='getjson2') return true;
		$this->load->model('Users_db');
		//validate
		$user = $this->session->userdata('user');

		if ($user) {
			$sid = $user['session_id'];

			if (isset($user['multi']))
				$m = $user['multi'];
			else
				$m = 0;

			array_splice($user, 19);

			if (!$m)
				$user['session_id'] = $sid;

			$user['multi'] = $m;

			$g_user = $this->Users_db->getUser($user);
			//print_r($g_user);echo "main";exit;
			if (!$g_user) {
				setcookie("isloggedin", '', time() - 3600, "/");
				if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {

					header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
					header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
					header("Cache-Control: no-cache, must-revalidate");
					header("Pragma: no-cache");
					header("Content-type: text/x-json");
					echo 'Your session has been signed out. <a href="' . site_url('login') . '">Click here to sign-in again</a><div style="display:none">' . gmdate("D, d M Y H:i:s") . '</div>';
					exit;
				} else {
					$this->session->sess_destroy();
					redirect('login');
				}
			}
		} else {
			$this->session->sess_destroy();
			redirect('login');
		}

		if (!($g_user['utype']<5||$g_user['allowvmap']==1||in_array($g_user['utype'],array(16,9,14,6,8,21,22,23)))) return false;


	}


	function render($country = "us",$ctype,$cname,$qid=0,$entryid=0,$dtype="im")
	{
		if (!$cname) show_404();

		$this->load->model('Shipping2','xdb');

		$data['qid'] = $qid;
		$data['entryid'] = $entryid;

		$data['jroot'] = $cname;
		$data['ctype'] = $ctype;
	
		$data['country'] = $country;
		$data['dtype']	 = $dtype;
	
		$data['content'] = $this->load->view('panes/vmap',$data,true);
		$this->load->view('template/popup',$data);

	}

	function render2($ctype,$cname,$qid=0,$entryid=0,$country="us",$dtype="im")
	{

		if (!$cname) show_404();

		$this->load->model('Shipping2','xdb');

		$data['qid'] = $qid;
		$data['entryid'] = $entryid;

		$data['jroot'] = $cname;
		$data['ctype'] = $ctype;
		$data['country'] = $country;
		$data['dtype'] = $dtype;

		$data['content'] = $this->load->view('panes/vmap2',$data,true);
		$this->load->view('template/popup',$data);

	}


	function makevmap()
	{
		//get user info
		$user = $this->session->userdata('user');

		//load plugin n model
		$this->load->helper('str2');
		$this->load->model('Shipping2','xdb');

		$ctype = $this->input->post('ctype');
		$cname = $this->input->post('cname');
        $country = $this->input->post('country');
        $dtype = $this->input->post('dtype');
 
        $this->xdb->country = $country;

		if($country == "us")
		{
			$this->xdb->switch_db($country);
        }

		$allowed = "/[^a-z0-9* ]/i";
		$cname = preg_replace($allowed," ",$cname);

		$json['action'] = 'newvmap';


		$start = $this->input->post('start');
		$rp = 10;

		$query = "@$ctype"." \"$cname\" ";

		$atype = 'shipname';

		if ($ctype == 'shipname') $atype = 'consname';

		$result = $this->xdb->getEntries($query,0,0,$start,$rp,0,0,$ctype."_a",$atype."_a",false,false,$dtype);

		$data['total'] = $result['total_found'];
		$data['time'] = $result['time'];

		$db = false;

		//print_r($result); exit;

		if (!is_array($result['entries']))
		{
			$ctype = "consignee";
			
			if($atype == "consname")
				$ctype = "shipper";
			
			$json['response'] = rp("No results for <b class='hlite'>$cname</b> as $ctype".$result['error'],1);
			$json['db'] = array();
			json_exit($json);
		}

		if($country == "us")
			$table = "igalldata";
		else
			$table = 'igalldata_'.$country;

		$db = $this->xdb->getRows($result['entries'],"$ctype"."_n as id,$ctype as name"," FIELD(entryid,".implode(',',$result['entries']).' )',''," $ctype"."_n != ''",$table,$dtype);


		for ($x=0;$x<count($db);$x++)
		{

			$db[$x]['nodes'] = $result['count'][$x];
		}

		$json['response'] = rp("Searched for <b class='hlite'>$cname</b> in companies, generated $result[total_found] possible results in <b>$result[time]</b> seconds.");
		$json['action'] = 'newvmap';
		$json['limit'] = $result['total_found'];
		$json['start'] = $start;

		$json['db'] = $db;
		$json['table'] = $table;

		$data['json'] = $json;

		$this->load->view('template/ajax2',$data);
	}


	function getjson($ctype,$cname,$start=0,$country='us',$dtype = "im")
	{
		if (!$cname) show_404();

		$ncolor = '#f00';

		if ($ctype=='shipname') $ncolor = '#0f0';

		$this->load->model('Vmap_db','xdb');

        $this->xdb->country = $country;

		if($country == "us")
		{
			$this->xdb->switch_db($country);
		}

		//$company = $this->xdb->getCompany($cname);
		$company = false;

		$data = array();

		$result = $this->xdb->getNormal($cname,$ctype,10,$start,$dtype);

        //print_r($result); exit();

		if (!$company)
		{
			//$company['n_sample'] = '-NOT AVAILABLE-';
			$company['n_sample'] = $result['parent'];
		}


		$rows = $result['db'];

		if (!$rows) show_404();
		
		for ($x=0; $x<count($rows); $x++)
		{
			if($rows[$x]['id'] != $cname)
			{
				$rs = $this->xdb->getNormal($rows[$x]['id'],$rows[$x]['data']['ntype'],5,0,$dtype);
		
				if ($rows[$x]['data']['ntype']=='shipname')
						$rows[$x]['data']['$color'] = "#0f0";
				else
						$rows[$x]['data']['$color'] = "#f00";
				
				$rows[$x]['data']['count']=$rs['total'];
				$rows[$x]['children'] = isset($rs['db']) ? $rs['db'] : '';
			}
		}

		$json = array("id"=>"$cname","name"=>"$company[n_sample]","data"=>array('address'=>$result['parentaddress'],'ntype'=>$ctype,'$color'=>$ncolor,"count"=>$result['total']),"children"=>$rows);
		$data['json']['xcode'] = urlencode(base64_encode($cname));
		$data['json']['vmap'] = $json;

		$this->load->view('template/ajax2',$data);


	}

	function getjson2($ctype,$cname,$start=0,$country = "us",$dtype="im")
	{

		if (!$cname) show_404();

		$cname = base64_decode(urldecode($cname));

		$ncolor = '#f00';

		if ($ctype=='shipname') $ncolor = '#0f0';

		//$this->load->model('Shipping2','xdb');
		$this->load->model('Vmap_db','xdb');
		
		$this->xdb->country = $country;
		
		if($country == "us")
		{
			$this->xdb->switch_db($country);
		}

		//$company = $this->xdb->getCompany($cname);
		$company = false;

		$data = array();

		$result = $this->xdb->getNormal($cname,$ctype,10,$start,$dtype);

		if (!$company)
		{
			//$company['n_sample'] = '-NOT AVAILABLE-';
			$company['n_sample'] = $result['parent'];
		}


		$rows = $result['db'];

		if (!$rows) show_404();

		for ($x=0; $x<count($rows); $x++)
		{
			$rs = $this->xdb->getNormal($rows[$x]['id'],$rows[$x]['data']['ntype'],5,0,$dtype);

			if ($rows[$x]['data']['ntype']=='shipname')
					$rows[$x]['data']['$color'] = "#0f0";
			else
					$rows[$x]['data']['$color'] = "#f00";

			$rows[$x]['data']['count']=$rs['total'];
			$rows[$x]['children'] = isset($rs['db']) ? $rs['db'] : '';
		}

		$json = array("id"=>"$cname","name"=>"$company[n_sample]","data"=>array('address'=>$result['parentaddress'],'ntype'=>$ctype,'$color'=>$ncolor,"count"=>$result['total']),"children"=>$rows);

		$data['json']['xcode'] = urlencode(base64_encode($cname));
		$data['json']['vmap'] = $json;

		$this->load->view('template/ajax2',$data);


	}


	function sphinx($v)
	{
		//show_404();

		//function for testing Sphinx Queries

		$this->load->model('Shipping2','xdb');
		$rows = $this->xdb->getNormal($v);

		if ($rows)

		for ($x=0; $x<count($rows); $x++)
		{
			$rows[$x]['children'] = $this->xdb->getNormal($rows[$x]['id'],'shipname');
		}

		echo "<pre>";
		print_r($rows); exit;

	}

}
