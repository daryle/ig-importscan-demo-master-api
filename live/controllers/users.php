<?php

class Users extends Controller {

	function __construct() {

		parent::__construct();

		$method = $this->uri->rsegment(2);

		//validate
		switch ($method) {
			case 'login':
			case 'auth':
			case 'logout':
			case 'login2':
			case 'auth2':
			case 'newcard':
			case 'sessionstate':
			case 'session':
			case 'choose_account':
			case 'autologin':
			case 'uploadtos':
			case 'processlitle':
				break;
			default:
				$user = $this->session->userdata('user');
				$this->load->model('Users_db');

				if ($user) {
					$sid = $user['session_id'];

					if (isset($user['multi']))
						$m = $user['multi'];
					else
						$m = 0;

					array_splice($user, 19);

					if (!$m)
						$user['session_id'] = $sid;

					$user['multi'] = $m;
				}

				$g_user = $this->Users_db->getUser($user);

				if (!$g_user) {
					$this->session->sess_destroy();

					redirect('login');
				}
				break;
		}

		$this->load->library('encrypt');
	}

	function newcard() {
		$url = "https://importgenius.com/newcard/";

		if (defined('S_ID') && S_ID == 'dev')
			header("location: https://" . get_tld() . "/newcard");
		header("location: {$url}");
	}

	function sessionstate() {
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		echo "<pre>";
		echo $this->session->userdata('session_id') . "\n";
		echo "\nPHP Session\n";
		print_r($_SESSION);
		echo "</pre>";
		exit;
	}

	function session($id = false) {
		redirect('');
	}


	function login() {
		setcookie("version", "", time() - 3600, "/");

		$data = array();
		//load messages if any
		$this->load->model('site_db');

		$message = $this->site_db->getNewMessage();


		if ($message)
			$data['message'] = $message;


		//check if there's a cookie named isloggedin
		$cookie = $this->input->cookie('isloggedin');

		if (!empty($cookie)) {

			$this->load->library('encrypt');
			$this->load->model('Users_db', 'User');
			//decode the cookie to get the username
			$eUser = $this->encrypt->decode($cookie, 'ig_remember_ninja');

			$user = $this->User->getUser(array('username' => $eUser));

			//username was found
			if ($user['username']) {
				//set the post values
				$_POST['nusername'] = $user['username'];
				$_POST['password'] = $user['password'];
				$_POST['encrypted'] = TRUE;
				$_POST['loggedin'] = TRUE;
				$this->auth();
				//user was not found
			} else {
				//remove the cookie
				setcookie("isloggedin", "", time() - 360000, "/");var_dump($user); die();
				redirect('login');
			}
		}

		$user = $this->session->userdata('user');


		if ($user)
			redirect('');

		$data = array();
		//load messages if any
		$this->load->model('site_db');

		$message = $this->site_db->getNewMessage();

		if ($message)
			$data['message'] = $message;

		$data['accounts'] = $this->session->flashdata('accounts');

		$data['pagetitle'] = "Sign In to ImportGenius.com";

		$data['content'] = $this->load->view('panes/login2', $data, true);
		$this->load->vars($data);
		$this->load->view('template/popup');
	}

	function clear_cookies() {
		$s = "";
		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach ($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name = trim($parts[0]);
				$s .= $name . "<br/>";
				setcookie($name, '', time() - 1000);
				setcookie($name, '', time() - 1000, '/');
			}
		}
		$this->session->sess_destroy();
		echo $s;
		echo "<pre>";
		print_r($_COOKIE);
		exit;
		//redirect('login');
	}

	function olderversion() {

		$this->load->model('Users_db', 'User');
		$user = $this->session->userdata('user');
		$user['version'] = "iscan3";
		$this->session->set_userdata('version', "iscan3");
		$this->User->updateUser($user, array('id' => $user['id']));
		$this->session->set_userdata('user', $user);

		redirect('');
	}

	function logout() {
		$this->load->model('Users_db', 'User');
		$this->User->logSession("Logout");
		//$this->User->dropMyTable();

		setcookie("iglogged", '', time() - 3600, "/");
		setcookie("otval", "", time() - 3600, "/");
		setcookie("version", "", time() - 3600, "/");
		//remove the isloggedin cookie
		setcookie("isloggedin", '', time() - 3600, "/");
		$this->session->sess_destroy();
		redirect('login');
	}

	function logout_signup() {
		$user = $this->session->userdata('user');
		$this->load->model('Users_db', 'User');
		$this->User->logSession("Logout");
		setcookie("iglogged", '', time() - 3600, "/");
		$this->session->sess_destroy();

		$url = "https://importgenius.com/upgrade/";

		if (defined('S_ID') && S_ID == 'dev') {
			switch(get_tld()) {
				case "ig.com":
					$url = "https://" . get_tld() . "/upgrade/";
					break;
				default:
					$url = base_url() . "upgrade/";
					break;
			}
		}
		header("Location: {$url}" . md5($user['username']) . "");
	}

	function account() {
		$this->load->model('countries_db');
		$user = $this->session->userdata('user');

		$data['ptitle'] = "My Account";
		$data['user'] = $this->session->userdata('user');
		$this->load->model('Users_db');
		$cards = $this->Users_db->getCard($data['user']['username']);
		$data['carttotal'] = $this->cart->total();

		if (!$cards) {

			$fields = array(
				'cid'
				, 'firstname'
				, 'lastname'
				, 'street'
				, 'city'
				, 'state'
				, 'country'
				, 'zip'
				, 'ctype'
				, 'cardno'
				, 'refno'
				, 'seccode'
				, 'expiry'
			);

			foreach ($fields as $field)
				$cards[$field] = '';
		} else {
			$edb = $this->Users_db->getCpStore(array('cp_key' => md5($cards['cid'])));
			if ($edb) {
				$cards['cardno'] = $this->encrypt->decode($edb['cp_val'], $cards['cid']);
				$cards['seccode'] = $this->encrypt->decode($edb['cp_end'], $cards['cid']);
			}
		}

		$data['countries'] = $this->countries_db->getListStore(array('userid' => $user['id']));
		$data['acountries'] = $this->countries_db->getAvailableCountries(array('userid' => $user['id']));
		$data['card'] = $cards;
		$data['content'] = $this->load->view('panes/account', $data, true);
		$this->load->vars($data);
		$this->load->view('template/popup');
	}

	function post() {

		$opassword = $this->input->post('opassword');
		$npassword = $this->input->post('npassword');
		$cpassword = $this->input->post('cpassword');
		$email = $this->input->post('email');

		$c = "";

		$user = $this->session->userdata('user');
		$dt['username'] = $user['username'];

		$data['action'] = 'reset';

		if ($opassword && $npassword && $cpassword) {
			if (md5($opassword) != $user['password'])
				$c .= "Invalid Original Password. ";

			if ($npassword == '')
				$c .= "Invalid New Password";

			if ($c == '')
				$user['password'] = md5($npassword);
			else {
				$data['response'] = $c;
				$json['json'] = $data;
				$this->load->view('template/ajax2', $json);
				return true;
			}
		}


		if ($email && $email != $user['email']) {
			$user['email'] = $email;
			$data['action'] = 'reload';
		}

		if ($this->session->userdata('user') != $user) {
			$this->load->model('Users_db');
			$this->Users_db->updateUser($user, $dt);
			$this->session->set_userdata('user', $user);
			$c .= "Account Updated.";
		} else
			$c .= "Cannot update password.";


		$data['response'] = $c;
		$json['json'] = $data;
		$this->load->view('template/ajax2', $json);
	}

	function update() {
		$c = "";
		//card db
		$user = $this->session->userdata('user');

		$xcardno = $this->input->post('xcardno');
		$card['cid'] = $this->input->post('cid');
		$card['refno'] = $this->input->post('refno');
		$card['firstname'] = $this->input->post('firstname');
		$card['lastname'] = $this->input->post('lastname');
		$card['username'] = $user['username'];
		$card['street'] = $this->input->post('street');
		$card['city'] = $this->input->post('city');
		$card['state'] = $this->input->post('state');
		$card['country'] = $this->input->post('country');
		$card['zip'] = $this->input->post('zip');
		$card['ctype'] = $this->input->post('ctype');
		$card['cardno'] = $this->input->post('cardnoxx');
		$card['expiry'] = $this->input->post('expiry');
		//$card['seccode'] = $this->input->post('seccode');
		$card['activated'] = 0;
		$card['atype'] = $user['atype'];
		$card['utype'] = $user['utype'];
		$card['clientid'] = $user['id'];

		$this->load->library('validation');

		//add rules

		$rules['firstname'] = 'required|min_length[2]|max_length[200]';
		$rules['lastname'] = 'required|min_length[2]|max_length[200]';
		//$rules['street'] = 'required';
		//$rules['city'] = 'required';
		//$rules['state'] = 'required';
		//$rules['country'] = 'required';
		$rules['zip'] = 'required';
		//$rules['ctype'] = 'required';
		if ($xcardno != $card['cardno']) $rules['cardnoxx'] = 'required|min_length[15]|max_length[20]|callback_creditcard_check';
		$rules['expiry'] = 'required|min_length[3]|max_length[6]';
		//$rules['seccode'] = 'required|min_length[3]|max_length[5]';

		$this->validation->set_rules($rules);

		//set fields


		$fields['firstname'] = 'Billing First Name';
		$fields['lastname'] = 'Billing Last Name';
		$fields['street'] = 'Street';
		$fields['city'] = 'City';
		$fields['state'] = 'State';
		$fields['country'] = 'Country';
		$fields['zip'] = 'Zip';
		//$fields['ctype'] = 'Credit Card Type';
		$fields['cardnoxx'] = 'Credit Card Number';
		$fields['expiry'] = 'Expiry Date';
		//$fields['seccode'] = 'Security Code';

		$this->validation->set_fields($fields);

		$mdata['user'] = $user;
		$mdata['card'] = $card;

		$this->validation->set_error_delimiters('<div class="error_line">', '</div>');

		if ($this->validation->run() == FALSE) {

			$c = "<p><b>No update. The following errors were detected:</b></p>";
			$c .= $this->validation->error_string;
			$c .= "<b>Please click the button below to correct these errors.</b>";
		} else {

			$c = "<p>Credit Card Updated.</p><p>A <strong>confirmation letter</strong> has been sent to your email address.</p>";

			if ($xcardno != $card['cardno']) {
				$c = "<p>Thank you for providing new credit card information for <strong>Import Genius</strong>.</p><p>A <strong>confirmation letter</strong> has been sent to your email address.</p><p>Once we verify your account and confirm the credit card authorization, we will enable full access on your account. This will occur within one business day.</p>";
				$card['cid'] = "";
				$card['refno'] = "IG-" . date('U') . "-" . rand(10, 99);
				$card['created'] = date('Y-m-d h:m:s');

				$mdata['user'] = $user;
				$mdata['card'] = $card;

				$message_admin = $this->load->view('email/newcardadmin', $mdata, true);
				$message_user = $this->load->view('email/newcarduser', $mdata, true);
				$subj_admin = "New";
			} else {
				$mdata['user'] = $user;
				$mdata['card'] = $card;

				$message_admin = $this->load->view('email/editcardadmin', $mdata, true);
				$message_user = $this->load->view('email/editcarduser', $mdata, true);
				$subj_admin = "Updated";
			}

			$this->load->model('Users_db');
			$this->Users_db->saveCard($card);

			$this->load->library('email');

			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			//email admin
			$this->email->from('info@importgenius.com', 'ImportGenius');
			$this->email->to('rhonda@importgenius.com,info@importgenius.com, autoemails@importgenius.com');
			$this->email->subject($subj_admin . ' credit card information ' . $user['username']);

			$this->email->message($message_admin);
			//$this->email->send();

			//email user
			$this->email->from('info@importgenius.com', 'ImportGenius');
			$this->email->to($user['email']);
			$this->email->subject('ImportGenius.com Subscription');

			$this->email->message($message_user);
			$this->email->send();
		}



		$data['action'] = 'retry';
		$data['response'] = $c;
		$json['json'] = $data;
		$this->load->view('template/ajax2', $json);
	}

	/*
	* Validate Credit Card Format
	* Prevent user from submitting credit card number with asterisk (*)
	*/
	function creditcard_check($str)
	{

		if ( preg_match("/^((4\d{3})|(5[1-5]\d{2})|(6011))(-?|\040?)(\d{4}(-?|\040?)){3}|^(3[4,7]\d{2})(-?|\040?)\d{6}(-?|\040?)\d{5}$/i", $str)) {
			return TRUE;
		} else {
			$this->validation->set_message('creditcard_check', 'Invalid Credit Card Number');
			return FALSE;
		}
	}

	function auth() {
		$this->load->model('countries_db');
		$this->load->model('Users_db');
		$this->load->model('export_db');

		$login = $this->input->post('nusername');

		$using_email = false;
		$c = "";

		if(filter_var($login, FILTER_VALIDATE_EMAIL))
		{
			$dt['email'] = $login;
			$using_email = true;
		}
		else
		{
			$dt['username'] = $login;
		}

		$encrypted = $this->input->post('encrypted');

		if (!$encrypted)
			$dt['password'] = md5($this->input->post('password'));
		else
			$dt['password'] = $this->input->post('password');


		if (empty($login) && empty($dt['password'])) {
			$c .= '<h1 class="important">Login Failed</h1>';
			$c .= "Invalid Username/Password";
			$this->session->set_flashdata('login_message', $c);
			//header("location:http://app.importgenius.com/login");
			redirect('login');
		}

		//temp trial solution
		if ($login == 'Trial') {
			$c = "<h1 class='important'>Account expired.</h1> Please call <strong>(888) 843-0272</strong> to subscribe.";
			$this->session->set_flashdata('login_message', $c);
			redirect('login');
			return true;
		}

		$this->load->model('Users_db', 'User');

		$dt['deleted'] = 0;
		$user = $this->User->getUser($dt);

		$c = "";

		if ($user) {

			if($using_email)
			{
				$emails = $this->Users_db->getUsers(
					array('email'=>$login,'password'=>$dt['password'],'suspended'=>0,'status'=>1),
					'username,id'
				);

				if($emails->num_rows() > 1)
				{
					$this->session->set_flashdata('login_message', "<h1 class=\"important\">Sign In Here</h1> Detected multiple accounts.");

					$this->session->set_flashdata('accounts',$emails->result_array());
					redirect('login');
				}

			}

			if($user['contract'] == 0)
			{
				if(in_array($user['utype'],array(26,27,28,29)))
				{

					if($user['payschedule'] == 1)
					{
						if (S_ID == 'aws')
							header("location: http://www.importgenius.com/terms/$user[terms]/upfront/".md5($user['username']));
						else
							header("location: http://scrum.importgenius.com/terms/$user[terms]/upfront/".md5($user['username']));

					}
					else
					{
						if (S_ID == 'aws')
							header("location: http://www.importgenius.com/terms/$user[terms]/1/".md5($user['username']));
						else
							header("location: http://scrum.importgenius.com/terms/$user[terms]/upfront/".md5($user['username']));
					}

					exit;
				}
			}

			unset($dt);

			$dt['username'] = $user['username'];

			// Set Version
			if ($user['version'] == "iscan4" || empty($user['version'])) {
				$version_cookie = $this->input->cookie('version');

				if (empty($version_cookie)) {
					setcookie("version", $user['version'], time() + 365 * 24 * 60 * 60 * 1000, "/");
				}
			}

			// Edu, redirect if not yet verified
			if ($user['suspendon'] != '0000-00-00' && $user['suspended'] == 1 && $user['edu_verification_code'] != '') {
				$this->load->model('edu_db');
				$edu = $this->edu_db->getEntry(array('edu_id' => $user['edu_id']));

				if ($_SERVER['SERVER_NAME'] == 'app.importgenius.com') {
					header("location: https://www.importgenius.com/edu/{$edu['edu_url']}/almostdone/{$user['edu_verification_code']}");
					return true;
				} else {
					header("location: http://{$_SERVER['SERVER_NAME']}/index.php/edu/{$edu['edu_url']}/almostdone/{$user['edu_verification_code']}");
					return true;
				}
			}

			if ($user['suspendon'] != '0000-00-00' && $user['suspended'] == 0) {
				if (time() > strtotime($user['suspendon'])) {
					$user['suspended'] = 1;
					$this->User->updateUser($user, $dt);
				}
			}

			//expired?
			if ($user['status'] == 0 && !($user['utype'] == 101 || $user['utype'] == 102)) {

				$c = "<h1 class='important'>Account disabled.</h1> <p>Please call <strong>(888) 843-0272</strong> for details.</p>If you are a new user, or recently applied for reactivation, please wait 1 business day for your account to be activated.";

				/*
				$acgstart = strtotime('2009-01-14');
				$acguser = strtotime($user['created']);
				$newrule = strtotime('2009-01-19');
				if ($acguser >= $newrule && $user['online'] == 1 && $user['contract'] == 0) {
					$host = "https://www.importgenius.com/activate2/" . md5($user['username']);

					if (defined('S_ID') && S_ID == 'dev') {
						switch(get_tld()) {
							case "ig.com":
								$url = "https://" . get_tld() . "/activate2/" . md5($user['username']);
								break;
							default:
								$url = base_url() . "/activate2/" . md5($user['username']);
								break;
						}
					}
					header("location: {$host}");
					return true;
				}

				if ($acguser < $newrule && $acguser > $acgstart && $user['contract'] == 0) {
					$host = "https://www.importgenius.com/activate2/" . md5($user['username']);

					if (defined('S_ID') && S_ID == 'dev') {
						switch(get_tld()) {
							case "ig.com":
								$url = "https://" . get_tld() . "/activate2/" . md5($user['username']);
								break;
							default:
								$url = base_url() . "/activate2/" . md5($user['username']);
								break;
						}
					}
					header("location: {$host}");

					return true;
				}

				*/

				if(in_array($user['utype'],array(26,27,28,29)) && $user['dateclosed']!=date('Y-m-d'))
				{

					if($user['payschedule'] == 1)
					{
						if (S_ID == 'aws')
							header("location: http://www.importgenius.com/terms/$user[terms]/upfront/".md5($user['username']));
						else
							header("location: http://scrum.importgenius.com/terms/$user[terms]/upfront/".md5($user['username']));

					}
					else
					{
						if (S_ID == 'aws')
							header("location: http://www.importgenius.com/terms/$user[terms]/1/".md5($user['username']));
						else
							header("location: http://scrum.importgenius.com/terms/$user[terms]/upfront/".md5($user['username']));
					}

					exit;
				}
				else
				{

					$this->session->set_flashdata('login_message', $c);
					if ($user['utype'] != 17 && $user['utype'] != 18) {
						$this->attemptlogin($user);

						$host = "https://www.importgenius.com/newcard";

						if (defined('S_ID') && S_ID == 'dev') {
							switch(get_tld()) {
								case "ig.com":
									$host = "https://" . get_tld() . "/newcard";
									break;
								default:
									//temp redirect for sprint10 testing purposes
									$host = "http://scrum.importgenius.com/newcard";
									//$host = base_url() . "/newcard";
									break;
							}
						}
						header("location: {$host}");
					}
					else {
						$host = "https://www.importgenius.com/upgrade/" . md5($user['username']) . "";

						if (defined('S_ID') && S_ID == 'dev') {
							switch(get_tld()) {
								case "ig.com":
									$url = "https://" . get_tld() . "/upgrade/" . md5($user['username']) . "";
									break;
								default:
									$url = base_url() . "/upgrade/" . md5($user['username']) . "";
									break;
							}
						}
						header("location: {$host}");
					}
				}
				return true;

				//$this->session->set_flashdata('login_message', $c);
				//redirect('login');
			}

			//search monitoring
			if ($user['utype'] == 19) {

				$c = "<h1 class='important'>Account restricted.</h1>Your Search Monitoring account with ImportGenius.com does not provide access to our web application for searching trade data.  To upgrade for full access, please call us at  <strong>888 843 0272</strong> or <strong>+1 202 595 3101</strong>";

				$this->session->set_flashdata('login_message', $c);
				redirect('login');
			}

			if (($user['datecancelled'] != '0000-00-00' && $user['status'] == 0) || $user['suspended'] == 1) { //email admin of event
				$this->attemptlogin($user);
			}

			if (($user['datecancelled'] != '0000-00-00' && $user['status'] == 0) || $user['suspended'] == 1) {
				$c = "<h1 class='important'>Suspended.</h1> Please provide new credit card details to regain access to your account. <p>You are now being forwarded to this <a href='https://www.importgenius.com/newcard'>page</a></p>";
				$this->session->set_flashdata('login_message', $c);
				if ($user['utype'] != 17 && $user['utype'] != 18) {

					$host = "https://www.importgenius.com/newcard";

					if (defined('S_ID') && S_ID == 'dev') {
						switch(get_tld()) {
							case "ig.com":
								$host = "https://" . get_tld() . "/newcard";
								break;
							default:
								//temp redirect for sprint10 testing purposes
								$host = "http://scrum.importgenius.com/newcard";
								//$host = base_url() . "/newcard";
								break;
						}
					}
					header("location: {$host}");

				}
				else {
					$host = "https://www.importgenius.com/upgrade/" . md5($user['username']);

					if (defined('S_ID') && S_ID == 'dev') {
						switch(get_tld()) {
							case "ig.com":
								$url = "https://" . get_tld() . "/upgrade/" . md5($user['username']);
								break;
							default:
								$url = base_url() . "/upgrade/" . md5($user['username']);
								break;
						}
					}
					header("location: {$host}");
				}
				return true;
			}

			if ($user['utype'] == 101 || $user['utype'] == 102) {
				//first login
				if ($user['firstname'] == '' || $user['lastname'] == '' || $user['email'] == '' || $user['phone'] == '' || $user['status'] == 0) {

					$host = "https://www.importgenius.com/newuser/" . md5($user['username']);

					if (defined('S_ID') && S_ID == 'dev') {
						switch(get_tld()) {
							case "ig.com":
								$url = "https://" . get_tld() . "/newuser/" . md5($user['username']);
								break;
							default:
								$url = base_url() . "/newuser/" . md5($user['username']);
								break;
						}
					}
					header("location: {$host}");

					return true;
				}



				//third day
				$last = $this->User->getFirstSession($user['id']);
				if (!isset($last['created']))
					$last['created'] = date('Y-m-d H:i:s');
				$diff = number_format((time() - strtotime($last['created'])) / 86400);
				//print_r($diff); exit;
				if ($diff > 3 && $user['utype'] == 101) {
					$host = "https://www.importgenius.com/newuser/" . md5($user['username']) . "/1";

					if (defined('S_ID') && S_ID == 'dev') {
						switch(get_tld()) {
							case "ig.com":
								$url = "https://" . get_tld() . "/newuser/" . md5($user['username']) . "/1";
								break;
							default:
								$url = base_url() . "/newuser/" . md5($user['username']) . "/1";
								break;
						}
					}
					header("location: {$host}");
					return true;
				}

				if ($diff > 30 && $user['utype'] == 102) {
					$host = "https://www.importgenius.com/newuser/" . md5($user['username']) . "/1";

					if (defined('S_ID') && S_ID == 'dev') {
						switch(get_tld()) {
							case "ig.com":
								$url = "https://" . get_tld() . "/newuser/" . md5($user['username']) . "/1";
								break;
							default:
								$url = base_url() . "/newuser/" . md5($user['username']) . "/1";
								break;
						}
					}
					header("location: {$host}");

					return true;
				}
			}

			$referrer = $this->input->cookie('referrer');
			if ($user['referrer'] == '') {
				$user['referrer'] = $referrer;
				$this->User->updateUser($user, $dt);
			}

			if (!$user['welcome'] && strtotime($user['created']) > strtotime('2009-07-30')) {
				$user['welcome'] = 1;
				if (strpos($user['username'], 'echosigntest') === false) {
					$this->session->set_userdata('original_welcome', 0);
				}

				$this->session->set_userdata('showWelcome', true);
				setcookie("etval", "true", time() + 365 * 24 * 60 * 60 * 1000, "/");

				//email tips and tricks removed
			}

			//store session_id
			$user['session_id'] = $this->session->userdata('session_id');
			$this->User->updateUser($user, $dt);
			$rememberme = $this->input->post('rememberme');


			if ($rememberme) { //save a cookie for remember
				$this->setCookie($user['username']);
			}

			$this->session->set_userdata('user', $user);
			$c .= 'Signing-in, please wait...';
			//$c = $this->db->last_query();
			//record session
			$this->User->logSession("Login");

			$dlimit = 0;
			$xlimit = 0;

			$available_countries = $this->countries_db->getAvailableCountries(array('userid' => $user['id'], 'c.country_id !=' => 8), true);

			switch ($user['utype']) {
				case 5: //demo only 200701 allowed
					$flimit = strtotime('01/01/2007');
					$tlimit = strtotime('01/31/2007');
					$qlimit = 5;
					$atype = "Demo";
					break;
				case 11: //demo 2 only 200701 allowed but 10 searches per day
					$flimit = strtotime('01/01/2007');
					$tlimit = strtotime('01/31/2007');
					$qlimit = 10;
					$atype = "Demo";
					break;
				case 6: //standard 45 days
					$dlimit = 45;
					$tlimit = time();
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 40;
					$atype = "Standard";

					if ($available_countries) {
						$qlimit = 80;
					}
					break;
				case 7: //deluxe 185 days
					$dlimit = 185;
					$tlimit = time();
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 80;
					$atype = "Deluxe";

					break;
				case 8: //premium 365 days
					$dlimit = 365;
					$tlimit = time();
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 80;
					$atype = "Premium";

					break;
				case 9: //enterprise
					$tlimit = time();
					$flimit = strtotime('01/01/2006');
					$qlimit = 300;
					$atype = "Enterprise";

					break;
				case 10: //custom
					$dlimit = $user['days'];
					$tlimit = time();
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = $user['perday'];
					$atype = $user['atype'];

					break;
				case 12: //standard 45 days
					$dlimit = 45;

					if (!empty($user['days'])) {
						$dlimit = $user['days'];
					}

					$tlimit = time();
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 40;
					$atype = "Limited";

					break;
				case 101:
				case 102:
					$tlimit = time();
					$flimit = $tlimit - (60 * 60 * 24 * 365);
					$qlimit = 15;
					$atype = "Promo";

					break;
				case 14: //premium 365 days
					$dlimit = 365;
					$tlimit = time();

					if (!empty($user['days'])) {
						$dlimit = $dlimit + ($user['days']);
					}

					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 80;
					$atype = "Standard";

					break;

				case 17: //trial app
					$dlimit = 365;
					$tlimit = time();
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 25;
					$atype = "Trial Account";

					break;

				case 18: //standard trial (no export only)
					$dlimit = 365;
					$tlimit = time();
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 80;
					$atype = "Standard Trial";
					break;
				case 16: //enterprise 2 only from November 2006 data
					$tlimit = time();
					$flimit = strtotime('11/01/2006');
					$qlimit = 300;
					$atype = "Enterprise";

					break;
				case 103: // University
					$tlimit = time();
					$flimit = strtotime('01/01/2006');
					$qlimit = 300;
					$atype = "University Trial";

					if ($user['allowvmap'] == 0 || $user['allowalerts'] == 0 || $user['contract'] == 0) {
						$u_user = array();
						$u_user['allowvmap'] = 1;
						$u_user['allowalerts'] = 1;
						$u_user['contract'] = 1;
						$this->Users_db->updateUser($u_user, array('username' => $user['username']));
					}

					break;
				case 24: // Limited (99)
					$tlimit = time();
					$flimit = strtotime("-6 months", time());
					$qlimit = 3000;
					$atype = "Limited";

					// if ($user['allowvmap'] != 0 || $user['allowalerts'] != 1 || $user['contract'] != 1) {
					// 	$u_user = array();
					// 	$u_user['allowvmap'] = 0;
					// 	$u_user['allowalerts'] = 1;
					// 	$u_user['contract'] = 1;
					// 	$this->Users_db->updateUser($u_user, array('username' => $user['username']));
					// }

					// Maximum export limit
					$this->session->set_userdata('max_export_result', 5000);
					break;
				case 0:
					//$this->session->sess_destroy();

					$c = '<h1 class="important">Login Failed</h1>';
					$c .= "Your account has an invalid user type, please contact us if this error is unexpected.";

					$this->session->set_userdata('user', false);
					$this->session->set_flashdata('login_message', $c);

					redirect('login');
					break;

				case 20:
					$tlimit = time();
					$flimit = strtotime('1/01/2006');
					$qlimit = 80;
					$atype = "Latin Data";
					break;
				case 21: //Limited (199)
					$dlimit = $user['days'];
					$tlimit = time();

					if (empty($user['days'])) {
						$dlimit = 365;
					}
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 25;
					$atype = "Basic";
					break;
				case 22: //Standard (399)
					$tlimit = time();
					$flimit = strtotime('11/01/2006');
					$qlimit = 50;
					$atype = "Standard";
					break;
				case 23: //Enterprise (899)
					$tlimit = time();
					$flimit = strtotime('11/01/2006');
					$qlimit = 100;
					$atype = "Enterprise";
					break;

				case 26: //New Limited (199)
					$dlimit = 180;

					if (!empty($user['days'])) {
						$dlimit = $user['days'];
					}
					$qlimit = 3000;
					$tlimit = time();
					$flimit = strtotime("-6 months", time());

					$atype = "Limited";
					$xlimit = 5000;
					//$xlimit = 50;
					break;
				case 27: //New Standard (199) // Plus&
					$dlimit = 365;
					$tlimit = time();

					if (!empty($user['days'])) {
						//$dlimit = $dlimit + ($user['days']);
						$dlimit = $user['days'] / 12 * $dlimit;
					}
					$flimit = $tlimit - (60 * 60 * 24 * $dlimit);
					$qlimit = 25;
					$atype = "Plus";
					$xlimit = 10000;
					break;
				case 28: //New Enterprise (399)
					$tlimit = time();
					$flimit = strtotime('11/01/2006');
					$qlimit = 50;
					$atype = "Premium";
					$xlimit = 25000;
					break;
				case 29: //Latin Data
					$tlimit = time();
					$flimit = strtotime('1/01/2006');
					$qlimit = 50;
					$atype = "Latin Data";
					$xlimit = 10000;
					break;
				default:
					$tlimit = time();
					$flimit = strtotime('1/01/2006');
					$qlimit = 3000;
					$atype = "Unlimited";
					$xlimit = 0;
			}
                                        //die("ETO: ".$flimit);


			if(in_array($user['utype'],array(26,27,28,29)))
			{
				$xremained = $xlimit;

				$total_export = $this->export_db->getTotalExport(array('uid'=>$user['id'],'status'=>3), $user);

				if($total_export['total'] > 0)
				{
					$xtotal = $total_export['total'];
					$xremained = ($xlimit - $xtotal);

				}

				if ($xremained < 0) $xremained = 0;


				$this->session->set_userdata('xremained',$xremained);

				//if there's extra export credit
				if (isset($user['addon_export']) && $user['addon_export'] > 0)
				{
					$addonexport = $user['addon_export'];

					$this->session->set_userdata('addonexport',$addonexport);

				}
				else $this->session->set_userdata('addonexport',0);

				$user_export['current_export'] = $xremained;
				$this->Users_db->updateUser($user_export, array('id'=>$user['id']));


			}

			$rules['tlimit'] = $tlimit;
			$rules['flimit'] = $flimit;
			$rules['qlimit'] = $qlimit;
			$rules['dlimit'] = $dlimit;
			$rules['xlimit'] = $xlimit;
			$rules['atype'] = $atype;

			$this->session->set_userdata('rules', $rules);

			$data['action'] = 'reload';


		} else {
			$c .= '<h1 class="important">Login Failed</h1>';
			$c .= "Invalid Username/Password";
			$data['action'] = 'reset';
		}



		$this->session->set_flashdata('login_message', $c);

		switch ($data['action']) {
			case 'reset':
				unset($_POST);
				redirect('login');
				break;
			case 'reload':


				$lastcountry = $this->Users_db->getlastcountry();
				$curcountry = $this->Users_db->getMyCountry();
				//removed rowen login for the remember me to work
				if (isset($_POST['loggedin'])) {
					$redirect_url = $_POST['loggedin'] === TRUE ? "" : "login";
					if ($redirect_url !== TRUE)
						unset($_POST['loggedin']);
				}else {
					$redirect_url = '';
				}

				if (is_array($curcountry)) {
					if (isset($curcountry['country_avre']))
						$redirect_url = $curcountry['country_avre'];
				}

				if ($lastcountry) {
					if ($lastcountry !== "us")
						$redirect_url = $lastcountry;
				}

				//random string for logout URL (to avoid firefox and IE caching)
				$this->session->set_userdata('rand', uniqid());
				$this->Users_db->autoAddCountry($user);

				redirect($redirect_url);

				break;
		}
	}

	function setCookie($username) {
		$this->load->library('encrypt');

		$eUser = $this->encrypt->encode($username, 'ig_remember_ninja');

		$this->load->helper('cookie');

		setcookie("isloggedin", $eUser, 2592000 + time(), "/");
	}

	function testarray() {
		$x = array(1, 2, 3, 4, 5);

		$count = 1;
		foreach ($x as $y) {
			$comma = count($x) == $count ? '' : ',';

			echo $y . $comma . "<br/>";

			$count++;
		}

		echo "<br/>" . $count;
		exit();
	}

	function attemptlogin($user) {
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from("info@importgenius.com", 'ImportGenius');
		if (S_ID == 'aws') {
			$this->email->to("info@importgenius.com", 'ImportGenius');
		}
		else {
			$this->email->to("lovely@codeninja.co, rgustafson@importgenius.com");
		}
		//$this->email->bcc("paulo@codeninja.co","Paulo");

		$stype = "cancelled";

		if ($user['suspended'] == 1) {
			$stype = "suspended";

			$lcond['tran_id'] = $user['id'];
			$lcond['event_name'] = "Suspend";


			$lrow = $this->User->getLogData($lcond);

			if ($lrow)
				$user['suspend_date'] = $lrow['log_date'];
		}

		$this->User->save_log($user);

		$message = $this->load->view('email/cancelled_user', $user, true);

		$e['subject'] = "$user[username] - a $stype user just tried signing into their account";
		$e['content'] = $message;

		$dmessage = $this->load->view('template/email', $e, true);

		$this->email->subject($e['subject']);
		$this->email->message($dmessage);
		$this->email->send();
	}

	public function updateOriginalWelcome() {
		if (is_ajax()) {
			$this->session->set_userdata('original_welcome', 1);
			echo json_encode(array('original_welcome' => $this->session->userdata('original_welcome')));
			exit;
		} else {
			echo 'Ajax Only';
			exit;
		}
	}

	public function autologin($hash)
	{
		$this->load->model('Users_db');

		$user = $this->Users_db->getUser(array('md5(username)'=>$hash));

		if(!$user) header("location:http://app.importgenius.com");//redirect('login');

		/*
		if (in_array($user['utype'],array(26,27,28,29)) && $user['dateclosed']!=date('Y-m-d') && $user['cancelled']==1) {

			$this->uploadtos($hash);
			if ($user['status']==0) {
				$this->processlitle($hash, $user['payschedule']);
			}

		}
                else {
                        if ($user['online']!=1) {

                                $this->uploadtos($hash);

                                if ($user['welcome']==0 && $user['status']==0)
                                {
                                        $this->processlitle($hash, $user['payschedule']);
                                }
                        }


                        $this->session->set_userdata('original_welcome', 1);

                        $dt['welcome'] = 1;

                        if($user['contract'] == 0) {

                                $dt['contract'] = 1;
                        }

                        $this->Users_db->updateUser($dt,array('id'=>$user['id']));
                }

		*/


		if ($user['dateclosed']!=date('Y-m-d')) {
			$this->uploadtos($hash);
		}

		if ($user['status']==0) {
			$this->processlitle($hash, $user['payschedule']);
		}

		$_POST['nusername'] = $user['username'];
		$_POST['password']  = $user['password'];
		$_POST['encrypted'] = 1;

		$this->auth();

	}

	function uploadtos($seed) {

        if (S_ID == 'aws')
            $url = "api.importgenius.com/api.php";
        else
            $url = "api.importgenius.com/apidev.php";

        $path = "/terms/upload/$seed";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://" . $url . $path);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
    }

    function processlitle($seed, $payschedule) {


        if (S_ID == 'local') {
            $output = '000';
            return $output;
        }

        if (S_ID == 'aws')
            $url = "api.importgenius.com/api.php";
        else
            $url = "api.importgenius.com/apidev.php";

        if ($payschedule == 1)
            $path = "/process/processprepaid/$seed";
        else
            $path = "/process/processlitle/$seed";


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://" . $url . $path);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);


        return $output;
    }

}

