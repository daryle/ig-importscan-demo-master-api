<?php

class Filters extends Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('filters_db');
	}

	function getFilters($co_id)
	{
		$data['f_ex'] = $this->filters_db->getFilters(array('co_id'=>$co_id,'datatype'=>'ex'));
		$data['f_im'] = $this->filters_db->getFilters(array('co_id'=>$co_id,'datatype'=>'im'));

		$data['json'] = $data;
		$this->load->view('template/ajax2',$data);
	}
}
