<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 * @property Alibaba_db $Alibaba_db
 */
class Alibaba extends Controller {

    private $error_code = array();
    private $rfqDetailUrl = '';

    public function log_action() {
        $keyword = $this->input->post('keyword');
        $action = $this->input->post('action');
        $message = $this->input->post('message');
        $this->load->model('Alibaba_db');
        $user = $this->session->userdata('user');

        if ($action == 5 && trim($keyword) === '') {
            return false;
        }

        $this->load->library('user_agent'); 
        $data = array();
        $data['userid'] = $user['id'];
        $data['keyword'] = $keyword;
        $data['action'] = $action;
        $data['message'] = $message !== FALSE ? $message : '';
        $data['datetime'] = date("Y-m-d H:i:s", time());
        $data['browser_name'] = $this->agent->browser();
        $data['browser_version'] = $this->agent->version();
        $this->Alibaba_db->save_log($data);
    }

    public function __construct() {
        parent::__construct();
        $user = $this->session->userdata('user');

        if (!$user) {
            $this->session->sess_destroy();
            redirect('logout');
        }

        $this->error_code[604] = "You are already a member at Alibaba.com.<br />Please <a href=\"http://us.sourcing.alibaba.com/rfq/request/post_buy_request.htm?src=ibdm_d01_impgen\" target=\"_blank\">sign in</a> to continue.";
//        $this->error_code[200] = "Your sourcing request was submitted!<br /><a href=\"{rfqDetailUrl}\" target=\"_blank\">Check request details</a>";
        $this->error_code[200] = "Your sourcing request was submitted!<br />A confirmation email will be sent to you shortly.";
        $this->load->model('Alibaba_db');
    }

    function post() {
        $user = $this->session->userdata('user');
        $action = $this->input->post('action');
        $data = array();
        $data['action'] = 'retry';


        switch ($action) {
            case 'never_ask_again': {
                    $data['action'] = 'ok';
                    $never_ask_again = 1;
                    $aliMemberId = FALSE;
                    $this->Alibaba_db->save_account($user['id'], $aliMemberId, $never_ask_again);
                    break;
                }
            case 'subscribe': {
                    // aliMemberId

                    if (in_array($user['country'], array(0, '0', '')) === TRUE) {
                        $user['country'] = 'US';
                    }

                    $req['userInfo']['aliMemberId'] = $this->input->post('aliMemberId');
                    $req['userInfo']['countryName'] = $user['country'];
                    $req['userInfo']['firstName'] = $user['firstname'];
                    $req['userInfo']['lastName'] = $user['lastname'];

                    // Demo purpose
                    if (isset($_COOKIE['demo']))
                        $req['userInfo']['email'] = time() . $user['email'];
                    else
                        $req['userInfo']['email'] = $user['email'];

                    $req['userInfo']['companyName'] = $user['business'];
                    $req['userInfo']['telephone'] = $user['phone'];

                    // Product Detail
                    $req['rfqInfo']['rfqName'] = $this->input->post('rfqName');
                    $req['rfqInfo']['rfqDetail'] = $this->input->post('rfqDetail');
                    $req['rfqInfo']['quantity'] = $this->input->post('quantity');
                    $req['rfqInfo']['quantityUnit'] = str_replace("'", '\u0027', $this->input->post('quantityUnit'));
                    $req['rfqInfo']['ip'] = $this->input->ip_address();
                    $req['rfqInfo']['source'] = "importGenius";

                    $retry = FALSE;


                    if (
                        (
                        is_numeric($req['rfqInfo']['quantity']) ||
                        is_float($req['rfqInfo']['quantity']) ||
                        is_int($req['rfqInfo']['quantity'])
                        ) === FALSE) {
                        $data['message'] = '<div class="alibaba-error alibaba-process">Product Quantity must be a number.</div>';
                        $retry = TRUE;
                    }

                    if ($req['rfqInfo']['quantity'] === FALSE OR trim($req['rfqInfo']['quantity']) === '') {
                        $data['message'] = '<div class="alibaba-error alibaba-process">Quantity is required.</div>';
                        $retry = TRUE;
                    }

                    if ($req['rfqInfo']['rfqDetail'] === FALSE OR trim($req['rfqInfo']['rfqDetail']) === '') {
                        $data['message'] = '<div class="alibaba-error alibaba-process">Please describe your requirements briefly.</div>';
                        $retry = TRUE;
                    }

                    if ($req['rfqInfo']['rfqName'] === FALSE OR trim($req['rfqInfo']['rfqName']) === '') {
                        $data['message'] = '<div class="alibaba-error alibaba-process">Product Name is required.</div>';
                        $retry = TRUE;
                    }


                    if ($retry === FALSE) {
                        // Let's reserve the request to DB
                        $db = array();
                        $db['aliMemberId'] = $this->input->post('aliMemberId') === FALSE ? 0 : $this->input->post('aliMemberId');
                        $db['userid'] = $user['id'];
                        $db['success'] = 0;
                        $db['rid_rd'] = 0;
                        $db['id'] = $this->Alibaba_db->save_request(FALSE, $db['userid']);




                        // Send the request to Alibaba
                        $response = $this->Alibaba_db->send_request($req);

                        $this->rfqDetailUrl = isset($response['rfqDetailUrl']) ? $response['rfqDetailUrl'] : 'http://www.alibaba.com';
                        $db['json_request'] = $response['json_request'];
                        $db['json_response'] = $response['json_response'];

                        if (isset($response['aliMemberId'])) {
                            $db['aliMemberId'] = $response['aliMemberId'];
                            $data['aliMemberId'] = $response['aliMemberId'];
                        }

                        $response_good_string = '<div class="alibaba-success alibaba-process">' . str_replace("{rfqDetailUrl}", $this->rfqDetailUrl, $this->error_code[200]) . '</div>';
                        $response_bad_string = '<div class="alibaba-warning alibaba-process">' . str_replace("{rfqDetailUrl}", $this->rfqDetailUrl, $this->error_code[604]) . '</div>';
                        // Response good?
                        if (isset($response['error_code']) AND $response['error_code'] === '200' AND isset($response['error_message']) AND strtolower($response['error_message']) === 'success') {
                            $data['message'] = $response_good_string;
                            $data['action'] = 'ok';

                            if (isset($response['aliMemberId'])) {
                                $response['aliMemberId'] = "" . $response['aliMemberId'] . "";
                            }

                            if (isset($response['rfqDetailUrl'])) {
                                $querystring = parse_url($response['rfqDetailUrl'], PHP_URL_QUERY);
                                parse_str($querystring, $vars);
                                $db['rid_rd'] = isset($vars['rid_rd']) ? $vars['rid_rd'] : 0;
                            }

                            if (isset($data['aliMemberId']) && intval($data['aliMemberId']) > 0) {
                                $data['message'] = $response_good_string;
                                $data['action'] = 'ok';
                            } elseif (isset($response['aliMemberId']) && intval($response['aliMemberId']) > 0) {
                                $data['message'] = $response_good_string;
                                $data['action'] = 'ok';
                            } else {
                                $data['message'] = $response_bad_string;
                                $data['action'] = 'ok';
                            }
                            $db['success'] = 1;
                        } else {
                            if (isset($this->error_code[$response['error_code']]) === TRUE) {
                                $data['message'] = '<div class="alibaba-warning alibaba-process">' . $this->error_code[$response['error_code']] . '</div>';
                                $data['action'] = 'ok';
                            } else {
                                $data['message'] = '<div class="alibaba-error alibaba-process">' . $response['error_message'] . '</div>';
                            }
                        }

                        $this->Alibaba_db->save_request($db['id'], $db['userid'], $db['aliMemberId'], $db['rid_rd'], $db['success'], $db['json_request'], $db['json_response']);

                        if ($db['aliMemberId'] > 0) {
                            $this->Alibaba_db->save_account($db['userid'], $db['aliMemberId']);
                        }
                    }
                    break;
                }
            default: {
                    $data['message'] = '<div class="alibaba-warning alibaba-process">Nothing was done...</div>';
                }
        }

        $json['json'] = $data;
        $this->load->view('template/ajax2', $json);
    }

}
