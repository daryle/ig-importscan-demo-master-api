<?php
    class Billlading extends Controller
    {
        function __construct()
        {
            parent::__construct();
        }
        
        function index()
        {
            $data['date'] = date('Y-m-d H:i:s');

            $data['company_name'] = 'INTERNATIONAL ASPECTS INC';
            
            $this->load->view('export/pdf',$data);
        }
    } 
?>
