<? 

class Grab extends Controller {

	function Grab()
	{
	parent::Controller();
	}
	/*
	function direct($id,$pid,$zip="")
	{
	$this->load->model('Users_db');
	$this->load->model('Export_db');
	
	$user = $this->Users_db->getUser(array('md5(id)'=>$id));
	
	if (!$user)
		exit;
	
	$s = $this->Export_db->get_export($pid);
	
	$fn = "../export/$user[username]/$pid.$s[format]";
	
	if ($zip)
		$fn .= ".zip";
	
	$data = "";
	
	if (file_exists($fn))	
		$data = @file_get_contents($fn,false);
	
	print_r($data); exit;
	
	}
	*/
	
	function direct($id,$pid,$zip="")
	{
	$this->load->model('Users_db');
	$this->load->model('Export_db');
	
	$user = $this->Users_db->getUser(array('md5(id)'=>$id));
	
	
	if (!$user)
		exit;
	
	$s = $this->Export_db->get_export($pid);
	
	
	$this->load->library('s3');
	$userid = $user['id'];
	
	//echo "<pre>";
	//if (!is_dir("../export/$id")) mkdir("../export/$id",0755);
	
	
	// Get the contents of our bucket  
	//$fn = $this->s3->getObject("importgenius","/export/40284/264893.xls",fopen("../temp_export/$id/$pid.$s[format]", "wb"));  
	
	//$fn = "../temp_export/$id/$pid.$s[format]";
	//$object =  $this->s3->getObject("importgenius", "/export/$userid/$pid.$s[format]",$fn);
	
 	$fn =  "$pid.$s[format]";
  
  	if ($zip)
		$fn .= ".zip";
	
	$bucket = "importgenius";
	$uri = "/export/$userid/$fn";
	if (($object = $this->s3->getObject($bucket, $uri)) !== false) {
       //print_r($object);
	   
	 
		
		print_r($object->body);exit;
	  	
    }
	
	//$fn = "../export/$user[username]/$pid.$s[format]";
	
	
	
	
	//$data = "";

	
	
	//if (file_exists($fn))	
		//$data = @file_get_contents($fn,false);
	
	//print_r($data); exit;
	
	
	
	}
	
	function notify($pid)
	{

	$this->load->model('Users_db');
	$this->load->model('Export_db');
	$this->load->helper('str2');
	

	$s = $this->Export_db->get_export($pid);

	//print_r($s); exit();
	
	if (!$s)
		show_404();
		
	if ($s['notify']!=1)
		show_404();
		
	$user = $this->Users_db->getUser(array('id'=>$s['uid']));		
	
	$this->load->library('s3');
	$userid = $user['id'];
	
	$bucket = "importgenius";
	$uri = "/export/$userid/$pid.$s[format]";
	if (($info = $this->s3->getObjectInfo($bucket, $uri)) !== false) {
       //print_r($info);
	   $fn =  "$pid.$s[format]";
	   //exit;
		
		$u['notify'] = 2;
		$this->Export_db->save_export($pid,$u);		
	
		$this->load->library('email');
		
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);		
		
		$this->email->from('info@importgenius.com', 'ImportGenius Team');
		$this->email->to($s['notify_email']);
		
		if ($s['fz1']==0)
			{
				$s['fz1'] = $info['size'];
			}
	
		if ($s['fz2']==0)
			{
				$info2 = $this->s3->getObjectInfo($bucket, $uri.".zip");
				$s['fz2'] = $info2['size'];
			}
	
		$message = $this->load->view('email/notify',$s,true);
		
		$e['subject'] = 'Your Import Genius Exported Data requested '.date('m/d/Y',$s['extime']).' is ready for download';
		$e['content'] = $message;
	
		$dmessage = $this->load->view('template/email',$e,true);
	
		$this->email->subject($e['subject']);
		
		//$this->email->message('<img src="'.base_url().'media/images/rsshead.jpg" /><p>Thank you for using ImportGenius.</p><p>Attach is an exported data of your search results.</p>');
		$this->email->message($dmessage);
		//$this->email->attach($fn);
		
		$this->email->send();
	  	
    }
	else show_404();

	}		

	
	function email($id,$pid,$email,$zip="")
	{

	$this->load->model('Users_db');
	$this->load->model('Export_db');
	
	$user = $this->Users_db->getUser(array('md5(id)'=>$id));
	
	if (!$user)
		exit;

	$s = $this->Export_db->get_export($pid);
	
	
	$this->load->library('s3');
	$userid = $user['id'];
	
	$bucket = "importgenius";
	$uri = "/export/$userid/$pid.$s[format]";
	$fn = "../export/$user[username]/$pid.$s[format]";
	$tfn = "../export/$user[username]/$s[file].$s[format]";
	$fname = "$s[file].$s[format]";
	
	
	if ($zip)
	{
	$fn .= ".zip";
	$tfn .= ".zip";
	$fname .= ".zip";
	$uri .= ".zip";
	}
	
	if (($object = $this->s3->getObject($bucket, $uri,$fn)) !== false) {
		
		//echo "cp \"$fn\" \"$tfn\""; exit;
		
		system("cp \"$fn\" \"$tfn\" > /dev/null 2>&1 &");	
	
		$this->load->library('email');
		
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);		
		
		$this->email->from('info@importgenius.com', 'ImportGenius Team');
		$this->email->to($email);

		$message = $this->load->view('email/export',$user,true);
		
		$e['subject'] = "Thank you for using the Export feature! - $fname attached";
		$e['content'] = $message;

		$dmessage = $this->load->view('template/email',$e,true);

		$this->email->subject($e['subject']);
		
		//$this->email->message('<img src="'.base_url().'media/images/rsshead.jpg" /><p>Thank you for using ImportGenius.</p><p>Attach is an exported data of your search results.</p>');
		$this->email->message($dmessage);
		$this->email->attach($tfn);
		
		$this->email->send();
		
		unlink($fn);
		unlink($tfn);
	
	
	}
	
	
	}
	
	
	function retry($pid="")
	{

		$user = $this->session->userdata('user');
		$username = $user['username'];
		

		$this->load->helper('str2');
		$this->load->model('Export_db');
		$this->load->model('Users_db');
		
		$s = $this->Export_db->get_export_md5($pid);
		
		$user = $this->Users_db->getUser(array('id'=>$s['uid']));
		
		if (!$user) show_404();

		$pid = $s['p_id'];
		$data['pid'] = $s['p_id'];
		$data['action'] = "done";
		
		//exit if pid do not exists
		if (!$s)
			{
			show_404();
			}
		
		//re run this export	
		if ($s)
		{
			//reset a few settings
			$u['status'] = 0;
			$u['extime'] = time();
			$u['ltime'] = time();
			$u['server_id'] = S_ID;
			$u['percent'] = 0;
			$u['retries'] = $s['retries']+1;
			
			if ($s['notify']==2)
				$u['notify'] = 1;
			
			$this->Export_db->save_export($pid,$u);

			//retrieve settings for perl script
			$sid = $s['sid'];
			$start = $s['from'];
			$trp = $s['to'];
			$username = $user['username'];
			$format = $s['format'];
			
			//$fn = "../export/$username/$pid";
			
			//system("cd ../codes; nice -n 10 perl $format"."2.pl $sid $start \"$trp\" \"$fn\" $pid > /dev/null 2>&1 &");
			$fn = "../../../../export/$username/$pid";
			system("cd ../apps/iscan4l/live/perl; nice -n 10 perl $format"."2.pl $sid $start \"$trp\" $fn $pid $s[cc_id] $s[export_fields] > /dev/null 2>&1 &");

			$data['action'] = "continue2";
			
		}

		$json['json'] = $data;
		//$this->load->view('template/ajax2',$json);
	
	}			
	
}	
