<?php

class Utility extends Controller
{
	function Utility()
	{
		parent::Controller();
		$user = $this->session->userdata('user');
		$this->load->model('countries_db');
		
		if(!$user) redirect('login');
	}
	
	function resetCountry()
	{
		$dbs 	= $this->load->database('core',true);
		$user 	= $this->session->userdata('user');
		
		$dbs->where('userid',$user['id']);
		$query = $dbs->get('available_countries');
		
		//echo "<pre>";print_r($query->result_array());
		
		
		if($user['username'] == "echosigntest83")
		{
			$dbs->where('userid',$user['id']);
			$dbs->delete('available_countries');
			
			
			$dbs->where('id',$user['id']);
			$dbs->delete('splash_page');
			
			echo "Country and splash page reset.";
		}
		exit();
		
	}
	
	/*
    *  MANAGE COUNTRY FIELDS
    */
    
    function getCountries()
    {
        $country_id = $this->input->post('country_id');
        $json['json']['db'] = $this->countries_db->availfields($country_id);
        $this->load->view('template/ajax2',$json);
    }
    
    function setCountryField()
    {
        if(is_ajax())
        {
            $field_id       = $this->input->post('field_id');
            $country_id     = $this->input->post('country_id');
            $boolean        = $this->input->post('bool');
               
            $data['bool'] = "";
            if($boolean)
            {
                $data['bool'] = $boolean;
            }
            
            $db['field_id']     = $field_id;
            $db['country_id']   = $country_id;
            
            $this->countries_db->saveFields($db,$boolean);
            
            $json['json'] = $data;
            $this->load->view('template/ajax2',$json); 
        }
    }
    
    function postSettings()
    {
        if(is_ajax())
        {
            $name       = $this->input->post('fname');
            $bool       = $this->input->post('bool');
            $field_id   = $this->input->post('field_id');
            $textval    = $this->input->post('textval');
               
            $flag = $bool == "true" ? 1 : 2;
            
            $data['response'] = 'Fail'; 
            
            if($bool)
                $db[$name]  = $flag;
            else
                $db[$name]  = $textval; 
                
            $this->countries_db->savecfSettings($db,array('field_id'=>$field_id));
            
            $data['response'] = 'Success';
            
            $json['json'] = $data;
            $this->load->view('template/ajax2',$json);     
        }
    }
    
}
