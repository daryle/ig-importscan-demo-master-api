<?php

class Graph extends Controller {

	function Graph()
	{
		parent::Controller();	
	}
	
	
	function render($ctype,$cname,$qid=0,$entryid=0)
	{
	
		if (!$cname) show_404();
	
		$this->load->model('Shipping2','xdb');
		
		$data['qid'] = $qid;
		$data['entryid'] = $entryid;
		
		$data['cname'] = $cname;
		$data['ctype'] = $ctype;
		
		$data['content'] = $this->load->view('panes/graph',$data,true);
		$this->load->view('template/popup',$data);
		
	}

}
