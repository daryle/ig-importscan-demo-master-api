<?php


class Sendleads extends Controller {

	function Sendleads()
	{
		parent::Controller();
				
	}
	
	function index()
	{
		$this->load->model('Sub_lead_db');
		$this->load->model('Shipping2','xdb');
		$rs = $this->Sub_lead_db->getSaves('frequency > 0');
		
		//echo "<pre>";
		//print_r($rs);
		
		$log['nonewresults'] = 0;
		$log['errorsend'] = 0;
		$log['sphinxerror'] = 0;
		$log['totolprocessed'] = 0;
		
		if (!$rs) return false;
		
		foreach ($rs['db'] as $su)
		{
			
			if ($su['lstatus']>1)
				{
				continue;
				}
			
		
			//$q = $this->xdb->getQuery($su['su_aid']);
			//$q['qto'] = time();
			
			$ds = $this->xdb->getEntries($su['query'],$su['query_from'],$su['query_to']);
			//print_r($ds);
			
			if ($ds['error']!=false) 
				{
				//add error logging here
				$this->Sub_lead_db->saveLog($su['id'],0,$ds['error']);
				
				$log['sphinxerror'] += 1;
				continue;
				}

			$qresult = $su['query_result'];
			//add override from last log result
			//if ($qresult==0) $qresult = $q['qresult'];


			if ($ds['total_found']>$qresult)
				{
				//update sub
				$this->Sub_lead_db->updateSub("id = ".$su['id'],array('query_result'=>$ds['total_found'],'last_sent'=>date('Y-m-d')));
				
				//send email	

				$this->load->library('email');
				
				$config['mailtype'] = 'html';
				
				$this->email->initialize($config);		
				
				$this->email->from('info@importgenius.com', 'ImportGenius Team');
				$this->email->to($su['email']);
				//$this->email->to('lovely@importgenius.com');
				
				$subject = str_replace("{search_name}",$su['name'],$su['subject']);
				$message = $this->load->view('email/alertleads',$su,true);
				//$e['content'] = $message;
				//$dmessage = $this->load->view('template/emailv2',$e,true);
				
				$this->email->subject($subject);
				$this->email->message($message);
				
				if ($this->email->send())
					{
					//log if sent
					$this->Sub_lead_db->saveLog($su['id'],1,'Sent',$ds['total_found']);
					
					} else {
					//log if not sent
					$this->Sub_lead_db->saveLog($su['id'],0,'Email error');
					
					$log['errorsend'] += 1;
					}
				
				}
			else
				{
				//log if no new result
				$this->Sub_lead_db->saveLog($su['id'],0,'No new results',$ds['total_found']);
				$log['nonewresults'] += 1;
				
				}			
			
			$log['totolprocessed'] += 1;
		} //end loop
		
		echo "<pre>".print_r($log,true)."</pre>";
		exit;
		
	}
	
}	
