<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 * @property Alibaba_alert_db $Alibaba_alert_db
 */
class Alibaba_alert extends Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Alibaba_alert_db');
    }

    public function hourly($hours_ago = FALSE) {
        if ($hours_ago === FALSE) {
            $start_date = date('Y-m-d H:00:00', strtotime('-1 hour'));
            $end_date = date('Y-m-d H:59:59', strtotime('-1 hour'));
        }
        else {
            $start_date = date('Y-m-d H:00:00', strtotime('-' . $hours_ago . ' hour'));
            $end_date = date('Y-m-d H:59:59', strtotime('-' . $hours_ago . ' hour'));
        }
        $records = $this->Alibaba_alert_db->get_records($start_date, $end_date);


        echo "Start: {$start_date}<br />";
        echo "End: {$end_date}<br />";
        if (count($records) > 0) {
            $html = "Hi,\n\n";
            $html .= "The following interacted with our AliSourcePro pop out.\n\n";

            $html .= "<table width=\"100%\" border=\"1\">";
            $html .= "<thead>"
                . "<th>Date/Time</th>"
                . "<th>Username</th>"
                . "<th>Full Name</th>"
                . "<th>Keyword(s)</th>"
                . "<th>Action</th>"
                . "</thead>";
            foreach ($records as $record) {
                if (trim($record['keyword']) === '') continue;
                $record['datetime'] = date("F j, Y, g:i a", strtotime($record['datetime']));
                switch ($record['action']) {
                    case 0: {
                            $record['action'] = 'Clicked Skip';
                            break;
                        }
                    case 1: {
                            $record['action'] = 'Clicked Try It Now';
                            break;
                        }
                    case 2: {
                            $record['action'] = 'Clicked Never';
                            break;
                        }
                    case 3: {
                            $record['action'] = 'Successfully registered: <br />' . $record['message'] . '';
                            break;
                        }
                    case 4: {
                            $record['action'] = 'Failed to register: <br />' . $record['message'] . '';
                            break;
                        }
                    case 5: {
                            $record['action'] = 'Closed the pop out form.';
                            break;
                        }
                    case 6: {
                            $record['action'] = 'ExistMemRFQ.';
                            break;
                        }
                }

                $html .= "<tr>"
                    . "<td>{$record['datetime']}</td>"
                    . "<td>{$record['username']}</td>"
                    . "<td>{$record['firstname']} {$record['lastname']}</td>"
                    . "<td>{$record['keyword']}</td>"
                    . "<td>{$record['action']}</td>"
                    . "</tr>";
            }
            $html .= "</table>";
            $html = nl2br($html);

            $this->load->library('email');
            $this->email->initialize(array('mailtype' => 'html'));
            $this->email->from('scripts@codeninja.co', 'Code Ninja Scripts');

            if ($_SERVER['SERVER_NAME'] === 'erickson.importgenius.com') {
                $this->email->to('erickson@importgenius.com');
                $this->email->cc('rgustafson@importgenius.com');
                $this->email->subject('Hourly ImportScan - AliSourcePro Report [Test Only]');
            }
            else {
                $this->email->to('paulo@importgenius.com');
                $this->email->cc('rgustafson@importgenius.com, kanko@importgenius.com');
                $this->email->bcc('erickson@importgenius.com');
                $this->email->subject('Hourly ImportScan - AliSourcePro Report');
            }
            $this->email->message($html);
            $this->email->send();

            echo $html;
        }
        else {
            echo "No records found.";
        }

        die();
    }

}
