<?php


class Help extends Controller
{
    public function index(){
        $this->default_page();
    }
    /**
     * The first page
     */
    public function  default_page()
    {
        $this->load->view('help/default');
    }

    /**
     * Load the tips and tricks
     */
    public function tips_and_tricks()
    {
        $this->load->view('help/tips_and_tricks');
    }

    /**
     * FAQ Endpoint
     */
    public function faq()
    {
        $this->load->view('help/faq');
    }

    /**
     * Get the Video Library
     */
    public function video_library()
    {
        $this->load->view('help/video_library');
    }

    /**
     * Get the Glossary
     */
    public function glossary()
    {
        $this->load->view('help/glossary');
    }

    /**
     * Get the Additional Infos
     */
    public function additional_info()
    {
        $this->load->view('help/additional_info');
    }

    /**
     * Get the Overseas Suppliers
     */
    public function overseas_suppliers()
    {
        $this->load->view('help/overseas_suppliers');
    }

    /**
     * Get the importers
     */
    public function importers()
    {
        $this->load->view('help/importers');
    }

    /**
     * Get the importers
     */
    public function logistics_provider()
    {
        $this->load->view('help/logistics_provider');
    }

    /**
     * Get the investors
     */
    public function investors()
    {
        $this->load->view('help/investors');
    }
}


