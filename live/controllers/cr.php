<?php


class Cr extends Controller {

    private $country;

	function __construct()
	{
		parent::__construct();

        $this->country = "cr";

		$method = $this->uri->rsegment(2);

		if ($method == 'exstatus' || $method == 'cancelexport') return true;

		$this->load->model('Users_db');
        $this->load->model('countries_db');
		//validate

		$user = $this->session->userdata('user');

		if ($user)
		{
		$sid = $user['session_id'];

		if (isset($user['multi']))
			$m = $user['multi'];
		else
			$m = 0;

		array_splice($user,19);

		if (!$m)
			$user['session_id'] = $sid;

		$user['multi'] = $m;

		//update rules

		$rules = $this->session->userdata('rules');
		if (isset($rules['dlimit']))
		{
		$rules['tlimit'] = time();
		if ($rules['dlimit']>0)
			{
			$rules['flimit'] = time()-(60*60*24*$rules['dlimit']);
			}
		$this->session->set_userdata('rules',$rules);

		}







                $g_user  = $this->Users_db->getUser($user);

                if (!$g_user)
                {
                        setcookie("isloggedin", '', time() - 3600, "/");
                        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
                        {
                                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
                                header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
                                header("Cache-Control: no-cache, must-revalidate" );
                                header("Pragma: no-cache" );
                                header("Content-type: text/x-json");
                                echo 'Your session has been signed out. <a href="'.site_url('login').'">Click here to sign-in again</a><div style="display:none">'. gmdate( "D, d M Y H:i:s" ) .'</div>';
                                exit;
                        } else {
                                $this->session->sess_destroy();
                                redirect('login');
                        }
                }

                } else {
                    $this->session->sess_destroy();
                    redirect('login');
                }

	}


	function webinar()
	{

		$user = $this->session->userdata('user');

		$this->load->library('email');

		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$this->email->from('info@importgenius.com', 'ImportGenius Team');
		$this->email->to('info@importgenius.com');
		$this->email->cc('paulo@importgenius.com');

		$this->email->subject('IG Webinar Request on '.date('m/d/Y H:i:s')." by $user[username]");
		$this->email->message("<p>$user[username] is requesting a Webinar. Please contact him by:</p> <p>Name: $user[lastname], $user[firstname] <br /> Email: $user[email] <br /> Phone: $user[phone]</p>");

		$this->email->send();


		$this->load->view("popup/webinar");
	}

	function test($less=0)
	{
		//show_404();

		//function for testing Sphinx Queries

		$this->load->model('Shipping2','xdb');
		//$query = " @masterbilloflading 'CHHKXMNNYC902928'";
		$query = " @countryoforigin \"CHINA\"";
		#$query .= " @fport SHANGHAI";
		$query = "@countryoforigin (CHINA) @fport (SHANGHAI)";

		$addr = "161 W VICTORIA ST. SUITE 220,LONG BEACH CA-90805TEL:310 762 2676";

		$c = str_word_count($addr,0);
		$oc = $c;

		if ($less) $c -= $less; else $c -= 1;
		if ($c<=0) $c = 1;

		$query = "@consaddr \"$addr\"/$c";
		//$from = strtotime("2007-01-01");
		//$to = strtotime("2007-01-31");
		$result = $this->xdb->getEntries($query,0,0,0,30,'uport_s','ASC','consname_s');
		$db = $this->xdb->getRows($result['entries'],"consname_n, consname, consaddr, zipcode, uport");

		echo "<pre>$addr\nPossible words: $oc \nMinimum word matches: $c \nTotal found: ";
		print_r($result['total_found']."\n\n");
		print_r($db);
		echo "</pre>";
		exit;
	}

	function welcome($id=1)
	{
		if (!$this->session->userdata('showWelcome')&&$id!='optimizer') return true;
		$this->session->set_userdata('showWelcome',false);
		$v_data['content'] = $this->load->view('template/welcome','',true);

		//code to add card data
		$user = $this->session->userdata('user');
		$data['username'] = $user['username'];
		$this->load->model('Users_db');

		//code to add visitor data
		$this->load->library('encrypt');
		$this->load->model('Site_db');

		$uid = $this->input->cookie("IG_ID");

		if ($uid)
		{
		$uid = $this->encrypt->decode($uid);
		$vi = $this->Site_db->getVisitor($uid);
			if ($vi)
				{
				$data['referrer'] = $vi['v_ref'];
				$data['firstpage'] = $vi['v_fpage'];
				$data['firsttime'] = $vi['v_time'];
				}
		}

		if (!isset($data['referrer']))
			{
			$data['referrer'] = $user['referrer'];
			$data['firstpage'] = $user['firstpage'];
			$data['firsttime'] = $user['firsttime'];
			}


		//get ptracker data
		$data['user'] = $user;
		$cards = $this->Users_db->getCard($data['user']['username']);
		if (!$cards)
		{

			$fields = array(
						'cid'
						,'firstname'
						,'lastname'
						,'street'
						,'city'
						,'state'
						,'country'
						,'zip'
						,'ctype'
						,'cardno'
						,'refno'
						,'seccode'
						,'expiry'
						);

			foreach ($fields as $field) $cards[$field] = '';
		}
		$data['cardtrans'] = $cards;

		//if not optimizer send email

		if ($id!='optimizer')
		{
		$v_data['ptracker'] = $this->load->view('popup/ptracker',$data,true);

		$user = $this->session->userdata('user');

		$this->load->library('email');

		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$this->email->from('info@importgenius.com', 'ImportGenius Team');
		$this->email->to('david@importgenius.com');
		$this->email->cc('paulo@importgenius.com');

		$data['subject'] = 'First IG Login on '.date('m/d/Y H:i:s')." by $user[username]";

		$this->email->subject($data['subject']);

		$data['content'] = $this->load->view("email/firstlogin",$data,true);
		$message = $this->load->view('template/email',$data,true);

		$this->email->message($message);

		$this->email->send();
		}

		$this->load->view('template/popup',$v_data);
	}

	function upgrade()
	{
		$data['content'] = $this->load->view('panes/upgrade','',true);
		$this->load->view('template/popup',$data);
	}

	function index()
	{
		$this->load->model('users_db');


		$data['nav'] = 1;

		$rules = $this->session->userdata('rules');

		$data['ptitle'] = $rules['atype']." Search";
        $data['current_country'] = "cr";

        $data['filter_opt'] = search_filters('cr');

		$this->load->model('users_db');


		$user = $this->session->userdata('user');

		$data['isavailable'] = $this->users_db->isAvailable($this->country);

		$check_country = $this->countries_db->getAvailableCountries(array('userid'=>$user['id'],'c.country_avre'=>$this->country),true);
		$curcountry = $this->countries_db->getList(array('country_avre'=>"in"),'country_id','desc');
		$lastcountry = $this->users_db->getlastcountry();

		if($user['utype'] != 2)
		{
			if(!$check_country)
			{

				if($curcountry && is_array($curcountry))
				{
					if(isset($curcountry['country_avre']))
						redirect($curcountry['country_avre']);
				}
				else
				{
					if($lastcountry)
					{
						redirect($lastcountry);
					}
					else
					{
						redirect();
					}

				}
			}
			else
			{
				if($lastcountry)
				{
					if($this->country != "cr")
						redirect($lastcountry);
				}
			}
		}

		$this->load->model('promo_db');
		$user = $this->session->userdata('user');
		$promoseen = $this->session->userdata('promoseen');
		//$promoseen = false;
		if ($user['promo']&&$promoseen=='')
		{
		$pdb = $this->promo_db->getHis(array('promoid'=>$user['promo'],'uid'=>$user['id']));
		if (!$pdb) $data['promo'] = $user['promo'];
		if ($pdb)
			{
			if ($pdb['response']==3) $data['promo'] = $user['promo'];
			}
		$this->session->set_userdata('promoseen',$user['promo']);
		}

		$this->load->model('shipping2');

		$nstotal = $this->shipping2->totalSearchbyUserId($user['id']);
		$nsleft = ($rules['qlimit']-$nstotal);

		$data['nstotal'] = $nstotal;
		$data['nsleft'] = $nsleft;
		$data['rules'] = $rules;
		$data['saved'] = 0;

		$popup = $this->session->flashdata('popup');

		if ($popup) $data['popup'] = $popup;

		//load messages if any
		$this->load->model('site_db');

        $bool = true;

		$page = $this->uri->rsegment(1);
		$data['page'] = "cr";

        //$data['countries'] = $this->countries_db->getList(array('status'=>1),'country_name','asc',$bool);
		$data['countries'] = $this->countries_db->getList2(array('status'=>1));
		$data['acountries'] = $this->countries_db->available_countries();
		$data['country'] = $this->countries_db->getCountry(array('country_avre'=>"cr"));

        $data['fields'] = $this->countries_db->fields($this->country,$this->country);
		$data['cfields'] = $this->countries_db->cfields();

        $cdetail = $this->countries_db->cfield(array('country_avre'=>$this->country));

        $data['vmapAllow'] = false;
		$data['datatype'] = false;

		if(isset($cdetail['export']) && $cdetail['export'] == 1)
		{
			$data['datatype'] = true;
		}

        if(isset($cdetail['vmap']) && $cdetail['vmap'])
        {
            $data['vmapAllow'] = true;
        }

		$data['cname'] = "cr";

        $data['privilege'] = false;
        if(in_array($user['email'],config_item('user_privilege')))
            $data['privilege'] = true;

		$message = $this->site_db->getNewMessage();

		if ($message) $data['message'] = $message;


		$this->load->model('Export_db');
		$r = $this->Export_db->get_latest_incomplete($user['id']);

		if ($r)
			$data['pid'] = $r['p_id'];
		else
			$data['pid'] = 0;

		$r2 = $this->Export_db->get_latest_complete($user['id']);

		if ($r2)
			$data['xpid'] = $r2['p_id'];
		else
			$data['xpid'] = 0;


    $this->load->model('Alibaba_db');
    $data['aliMemberId'] = $this->Alibaba_db->get_alibaba_id($user['id']);
    $data['never_ask_again'] = $this->Alibaba_db->get_alibaba_never_ask_again($user['id']);
		$data['content'] = $this->load->view('panes/home',$data,true);
		$this->load->vars($data);
		$this->load->view('default_view');


	}


	function email()
	{

		$email = $this->input->post('email');
		$exf = $this->input->post('exf');
		$user = $this->session->userdata('user');
		$username = $user['username'];
		$fn = "../export/$username/$exf";

		if (!$email)
			{
				$data['response'] = "Please enter a valid email address";
				$data['action'] = 'retry';
				$json['json'] = $data;
				$this->load->view('template/ajax2',$json);
				return true;
			}

		$fz = ceil(filesize($fn)/10485760);

		if ($fz>10)
			{
				$data['response'] = "You're file is $fz Mb. We apologize, but we can't send attachments bigger than 10Mb, please generate a smaller export for attachment, then try again";
				$data['action'] = 'retry';
				$json['json'] = $data;
				$this->load->view('template/ajax2',$json);
				return true;
			}


		$this->load->library('email');

		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$this->email->from('info@importgenius.com', 'ImportGenius Team');
		$this->email->to($email);

		$message = $this->load->view('email/export','',true);

		$e['subject'] = 'ImportGenius / Exported Data on '.date('m/d/Y')." by $username";
		$e['content'] = $message;

		$dmessage = $this->load->view('template/email',$e,true);

		$this->email->subject($e['subject']);

		//$this->email->message('<img src="'.base_url().'media/images/rsshead.jpg" /><p>Thank you for using ImportGenius.</p><p>Attach is an exported data of your search results.</p>');
		$this->email->message($dmessage);
		$this->email->attach($fn);

		$this->email->send();

		$data['response'] = "The e-mail was successfully sent to $email with <b>$exf</b> attached.";
		$data['action'] = 'reset';
		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);
	}

	function download($filename)
	{
		$user = $this->session->userdata('user');
		$filename = str_replace('_','.',$filename);

		$this->load->helper('download');

		$fn = "../export/".$user['username']."/$filename";

		if (!file_exists($fn)) show_404();

		$data = file_get_contents($fn); // Read the file's contents

		force_download($filename, $data);

	}

	function export()
	{

		$data['content'] = $this->load->view('panes/export','',true);
		$this->load->view('template/popup',$data);

	}

	function get_his()
	{

		$this->load->model('Shipping2','xdb');
		$cmd = $this->input->post('cmd');
		$user = $this->session->userdata('user');


		$page = $this->input->post('page');
		$rp = $this->input->post('rp');
		$sid = $this->input->post('sid');

		$order = "qdate desc";

		$sdb = $this->xdb->getQueries("userid = $user[id] AND qlite !=''",$page,$rp,$order,50);

		$data['total'] = $sdb['total'];

		$data['page'] = $sdb['page'];

		$db = $sdb['db'];

		$this->load->helper('str2');

		$rows = array();

				if (is_array($db)) foreach ($db as $row)
				{

				$rows[] = array(
						"id" => $row['aid'],
						"cell" => array(
							$row['qdate']
							,buildResponse($row)
						)
					);
				}


		$data['rows'] = $rows;

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);
	}


	function get_saves()
	{

		$this->load->model('Saved_search_db','xdb');
		$cmd = $this->input->post('cmd');
		$user = $this->session->userdata('user');


		$page = $this->input->post('page');
		$rp = $this->input->post('rp');
		$sid = $this->input->post('sid');

		if ($sid) $this->xdb->deleteSave("sv_id = $sid");

		$sdb = $this->xdb->getSaves("sv_userid = $user[id]",$page,$rp);

		$data['total'] = $sdb['total'];
		$data['page'] = $sdb['page'];

		$db = $sdb['db'];

		$rows = array();

				if (is_array($db)) foreach ($db as $row)
				{

				$rows[] = array(
						"id" => $row['sv_aid'],
						"cell" => array(
							$row['sv_date']
							,$row['sv_name']
							,"<input type='button' id='delsearchb' class='subb' onclick='deleteSearch($row[sv_id]); return false;' title='Delete saved search?' />"
						)
					);
				}


		$data['rows'] = $rows;

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);
	}


	function search_save()
	{

		$this->load->model('Saved_search_db','xdb');
		$this->load->helper('str2');

		$user = $this->session->userdata('user');

		$sv['sv_name'] = trim($this->input->post('reportname'));
		$sv['sv_aid'] = $this->input->post('sid');
		$sv['sv_date'] = date('Y-m-d h:m:s');
		$sv['sv_userid'] = $user['id'];

		//init vars
		$data['response'] = "Please generate a search before saving.";
		$data['action'] = 'saverep';

		if(!$sv['sv_aid'])
		{
		json_exit($data);
		}

		if (!$sv['sv_name'])
		{
		$data['response'] = "Please provide a name for your search.";
		json_exit($data);
		}

		$db = $this->xdb->getSave(array("sv_name"=>$sv['sv_name']));

		if ($db)
		{
		$data['response'] = "<b class='hlite'>$sv[sv_name]</b> already exists.";
		json_exit($data);
		}

		$db = $this->xdb->getSave(array("sv_aid"=>$sv['sv_aid'],"sv_userid"=>$user['id']));

		if ($db)
		{
		$data['response'] = "This search has already been saved as <b class='hlite'>$db[sv_name]</b>";
		json_exit($data);
		}

		$this->xdb->saveSearch($sv);

		$data['action'] = "closereset";
		$data['response'] = "<b class='hlite'>$sv[sv_name]</b> successfully saved.";

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);

	}

	function get_subs()
	{

		$this->load->model('Sub_search_db','xdb');
		$cmd = $this->input->post('cmd');
		$user = $this->session->userdata('user');


		$page = $this->input->post('page');
		$rp = $this->input->post('rp');
		$sid = $this->input->post('sid');

		$status = $this->input->post('status');
		if ($status=='true') $status = 1; else $status = 0;

		if ($sid&&$cmd=='delete') $this->xdb->deleteSave("su_id = $sid");
		if ($sid&&$cmd=='statchange') $this->xdb->updateSub("su_id = $sid",array('su_status'=>$status));

		$sdb = $this->xdb->getSaves("su_userid = $user[id]",$page,$rp);

		$data['total'] = $sdb['total'];
		$data['page'] = $sdb['page'];

		$db = $sdb['db'];

		$rows = array();

				if (is_array($db)) foreach ($db as $row)
				{

				$checked = "checked='checked'";
				if ($row['su_status']==0) $checked = "";

				$rows[] = array(
						"id" => $row['su_aid'],
						"cell" => array(
							$row['su_date']
							,$row['su_name']
							,$row['su_sentto']
							,"<input type='checkbox' class='subb' onclick='statSub(this); return false;' $checked value='$row[su_id]' title='Enable/Disable subscription?' />"
							,"<input type='button'  class='subb' onclick='deleteSub($row[su_id]); return false;' title='Delete saved subscription?' />"
						)
					);
				}


		$data['rows'] = $rows;

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);
	}

	function sub_save()
	{

		$this->load->model('Sub_search_db','xdb');
		$this->load->model('Users_db');
		$this->load->helper('str2');

		$user = $this->session->userdata('user');
		$user = $this->Users_db->getUser(array('id'=>$user['id']));

		//print_r($this->db->last_query()); exit;

		$sv['su_name'] = trim($this->input->post('name'));
		$sv['su_aid'] = $this->input->post('sid');
		$sv['su_date'] = date('Y-m-d h:m:s');
		$sv['su_userid'] = $user['id'];
		$sv['su_sentto'] = $this->input->post('email');
		$sv['su_status'] = 1;

		//init vars
		$data['response'] = "Please generate a search before subscribing.";
		$data['action'] = 'savesub';


		if (in_array($user['utype'],array(14,6,8,21,22))&&$user['allowalerts']!=1)
		{
			switch ($user['utype']) {
                case 21:
                    $alert_limit = 5;
                    break;

                case 22:
                    $alert_limit = 10;
                    break;

                default:
                   $alert_limit = 3;
                    break;

			}

			if ($user['alert_limit']) $alert_limit = $user['alert_limit'];

			$sdb = $this->xdb->getSaves("su_userid = $user[id]",1,1);
			if ($sdb['total']>=$alert_limit)
				{
				$data['response'] = "Your account only allows up to $alert_limit subscriptions";
				json_exit($data);
				}

		}

		if(!$sv['su_aid'])
		{
		json_exit($data);
		}

		if (!$sv['su_name'])
		{
		$data['response'] = "Please provide a name for your subscription.";
		json_exit($data);
		}

		$db = $this->xdb->getSave(array("su_name"=>$sv['su_name'],"su_userid"=>$user['id']));

		if ($db)
		{
		$data['response'] = "<b class='hlite'>$sv[su_name]</b> already exists.";
		json_exit($data);
		}

		$db = $this->xdb->getSave(array("su_aid"=>$sv['su_aid'],"su_sentto"=>$sv['su_sentto']));

		if ($db)
		{
		$data['response'] = "This subscription has already been saved as <b class='hlite'>$db[su_name]</b>";
		json_exit($data);
		}

		$this->xdb->saveSearch($sv);

		$data['action'] = "closereset";
		$data['response'] = "<b class='hlite'>$sv[su_name]</b> successfully saved.";

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);

	}

	function exportstatus()
	{


		$r = $this->session->userdata('ex-remain');

		if ($r)
		{
		$data['response'] = "<div class='progtext'>".$r[0]."% complete. $r[1] remaining."."</div>";
		$data['percent'] = $r[0];
		$data['action'] = "continue2";
		} else {
		$data['response'] = "";
		$data['action'] = "done";
		}
		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);


	}

	function generate2()
	{

		if (!count($_POST)) return true;

		//$this->benchmark->mark('code_start');
		$data['response'] = 'Error generating results';
		$data['action'] = 'exmail';
		$dt = '';

		$this->load->model('Shipping2','xdb');
		$this->load->helper('str2');
		$this->load->helper('file');


		$user = $this->session->userdata('user');
		$username = $user['username'];
		set_time_limit(8000);

		//init variables
		$template = $this->input->post('template');
		$format = $this->input->post('format');
		$sid = $this->input->post('sid');
		$rec = $this->input->post('rec');
		$nfrom = strtonumber($this->input->post('nfrom'),'.',',');
		$nto = strtonumber($this->input->post('nto'),'.',',');

		$filename = trim($this->input->post('fname'));

		if (!$filename) $filename = $template;


		$filename = preg_replace("/[^a-zA-Z0-9 -]/", "", $filename);

		if (!is_dir("../export/$username")) mkdir("../export/$username",0755);

		$fn = "../export/$username/$filename.$format";
		$prog = $this->input->post('prog');


/*
		write_file($fn, "paulo", 'a');
		$data['response'] = $fn;
		json_exit($data);
*/


		if ($prog==0)
			{
			$this->session->set_userdata('extime',time());
			}

		if ($format=='xls'||$format=='xlsx')
			{
				/** Include path **/
				set_include_path(get_include_path() . PATH_SEPARATOR . config_item('resource_folder') .'lib/phpexcel176/Classes/');

				/** PHPExcel */
				include 'PHPExcel.php';

				/** PHPExcel_Writer_Excel2007 */
				if ($format=='xlsx') include 'PHPExcel/Writer/Excel2007.php';
				if ($format=='xls') include 'PHPExcel/Writer/Excel5.php';

				//$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_discISAM;
				$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod);


			}

		//delete old
		if (file_exists($fn)&&!$prog) unlink($fn);

		//switch if invalid
		if ($nfrom>$nto)
		{
		$nto = $nfrom;
		$nfrom = (int)$this->input->post('nto');
		}

		//load query
		$q = $this->xdb->getQuery($sid);

		if (!$q)
			{
			json_exit($data);
			}


		$max_result = 100000;

		if ($format=='xls'||$format=='xlsx')
			{
			$max_result = 10000;
			}

		$start = 0;
		$rp = $q['qresult'];

		if ($rec==1)
			{
			$nfrom -= 1;
			if ($nfrom>$start) $start = $nfrom;
			if ($nto<$rp) $rp = $nto;
			}

		if ($rp == 0) $rp = $max_result;

		if (($rp-$start)>$max_result)
			{
			$data['response'] = rp("Maximum records for this type of export is $max_result only. Please modify your export and try again.",1);
			json_exit($data);
			}

		if ($template=='shipmentdetail')
			{
            // Excel
			$select = "
				shipname, shipaddr
				,consname, consaddr
				,bl
				,adate
				,grossweight
				,unit
				,port
				,origin
				,vessel
				,product
				";


			$hdr = array(
				"SHIPPER","SHIPPER ADDRESS"
				,"CONSIGNEE","CONSIGNEE ADDRESS"
				,"BILL OF LADING"
				,"ARRIVAL DATE"
                ,"Q.UNIT"
				,"PORT"
				,"COUNTRY OF ORIGIN"
				,"M.UNIT"
				,"VESSEL NAME"
				,"PRODUCT DETAILS"

				);

			}

		//write headers

		if (!$prog)
		{

		$q['print'] += 1;
		$this->xdb->updateQuery($sid,$q);

		if ($format=='csv')
			{

			//write hdr
			write_file($fn, "\"".implode("\",\"",$hdr)."\"\n", 'a');
			}

			if ($format=='xls'||$format=='xlsx')
				{

				// Create new PHPExcel object
				//echo date('H:i:s') . " Create new PHPExcel object\n";
				$objPHPExcel = new PHPExcel();

				// Set properties
				$objPHPExcel->getProperties()->setCreator("$user[firstname] $user[lastname]");
				$objPHPExcel->getProperties()->setLastModifiedBy("$user[firstname] $user[lastname]");
				$objPHPExcel->getProperties()->setTitle('Shipment Detail');
				$objPHPExcel->getProperties()->setSubject("Shipment Detail");
				$objPHPExcel->getProperties()->setDescription("Shipment Detail");


				$objPHPExcel->setActiveSheetIndex(0);

				// Add a drawing to the worksheet
				if ($format=='xls')
				{
				$objDrawing = new PHPExcel_Worksheet_Drawing();
				$objDrawing->setName('Logo');
				$objDrawing->setDescription('Logo');
				$objDrawing->setPath(config_item('resource_folder').'media/images/logo2.gif');
				$objDrawing->setHeight(95);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
				}

				//Add headers
				$col = ord('A');

				foreach ($hdr as $fld)
				{

					if ($col<= ord('Z'))
						$fl = chr($col)."7";
					else
						$fl = "A".chr($col-26)."7";

					switch ($fld)
						{
						case 'SHIPPER':
						case 'NOTIFY':
						case 'CONSIGNEE':
							$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col))->setWidth(40);
							break;
						case 'SHIPPER ADDRESS':
						case 'NOTIFY ADDRESS':
						case 'CONSIGNEE ADDRESS':
						case 'PRODUCT DETAILS':
						case "MARKS AND NUMBERS":
							$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col))->setWidth(60);
							break;
						default:
							if ($col<= ord('Z'))
							$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col))->setWidth(20);
							else
							$objPHPExcel->getActiveSheet()->getColumnDimension("A".chr($col-26))->setWidth(20);
							//print_r(chr($col)); exit;
						}

					$objPHPExcel->getActiveSheet()->setCellValue($fl,$fld);
					$objPHPExcel->getActiveSheet()->getStyle($fl)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$objPHPExcel->getActiveSheet()->getStyle($fl)->getFill()->getStartColor()->setARGB('FF333333');
					$objPHPExcel->getActiveSheet()->getStyle($fl)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle($fl)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

					$col++;
				}


				$objPHPExcel->getActiveSheet()->setCellValue('B2','Import Genius - Trade Secrets Revealed Through U.S. Customs Data');
				$objPHPExcel->getActiveSheet()->setCellValue('B3','www.importgenius.com');
				$objPHPExcel->getActiveSheet()->setCellValue('B4','info@importgenius.com');
				$objPHPExcel->getActiveSheet()->setCellValue('B5','888-843-0272');

				$rownum = 8;

				// Rename sheet
				$objPHPExcel->getActiveSheet()->setTitle('Shipment Detail');

				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				// Save Excel5 file
				if ($format=='xls') $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
				if ($format=='xlsx') $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
				$objWriter->save($fn);

				}

		}

		//init loop
		$tstart = $start;

		if ($prog) $tstart = $prog;

		$trp = 3000;
		$end = $rp;

		if (strpos(base_url(),'ig.com'))
			{
				$trp = 3000;
				//sleep(1);
			}


		//loop to get data and append to file
		if ($tstart < $end)
		{

		if (($tstart+$trp)>$end) $trp = $end-$tstart;

		//json_exit($q);

		$rs = $this->xdb->getEntries($q['query'],$q['qfrom'],$q['qto'],$tstart,$trp,$q['qsortname'],$q['qsortorder'],0,0,0,unserialize($q['filters']));

		$tstart += $trp;

		//get data

		//$select = "entryid";

		if (!$rs['entries'])
			{
			$data['action'] = "cancelexport";
			json_exit($data);
			}

		$db = $this->xdb->getRows($rs['entries'],$select," FIELD(entryid,".implode(',',$rs['entries']).' )',false,false,'igalldata_'.$this->country);

/*
		$data['response'] = "<pre>".print_r($rs['entries'],true)."</pre>";
		$data['response'] = $tend." <= $max";
		json_exit($data);
*/


		//write file
			if ($format=='csv')
				{

				//write data
				foreach ($db as $row)
					{
						$row = cleanR3($row);
						//fwrite($fp, implode(',',$row)."\n");
						write_file($fn, implode(',',$row)."\n", 'a');
					}

				}

			if ($format=='xls'||$format=='xlsx')
				{

				$objPHPExcel = PHPExcel_IOFactory::load($fn);

				$rownum = $objPHPExcel->getActiveSheet()->getHighestRow() + 1;

				foreach ($db as $row)
				{

				$row = cleanR4($row);

				$col = ord('A');
					foreach ($row as $fld)
					{
					if ($col<= ord('Z'))
						$fl = chr($col).$rownum;
					else
						$fl = "A".chr($col-26).$rownum;
					$val = $fld;

					$objPHPExcel->getActiveSheet()->setCellValue($fl,$val);

					if (chr($col)=='J'||chr($col)=='K')
						{
						$val = str_replace(",", "", $val);
						$objPHPExcel->getActiveSheet()->setCellValue($fl,$val);
						$objPHPExcel->getActiveSheet()->getStyle($fl)->getNumberFormat()->setFormatCode('#,##0.00');
						}

					if (chr($col)=='I')
						{
						$objPHPExcel->getActiveSheet()->setCellValue($fl,PHPExcel_Shared_Date::PHPToExcel(strtotime($val)));
						$objPHPExcel->getActiveSheet()->getStyle($fl)->getNumberFormat()->setFormatCode('mm/dd/yyyy');
						}


					$col++;
					}
				$rownum++;
				}

				// Save Excel5 file
				if ($format=='xls') $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
				if ($format=='xlsx') $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
				$objWriter->save($fn);


				}

				$total = $end - $start;
				$percent = number_format((($tstart-$start)/$total)*100);
				$extime = $this->session->userdata('extime');

				$elapse = time() - $extime;

				$remain = floor(($elapse/($tstart-$start)) * ($end-$tstart));

				$rtext = toTime($remain);

				$data['response'] = "<div class='progtext'>".$percent."% complete with $rtext remaining."."</div>";
				$data['prog'] = $tstart;
				$data['percent'] = $percent;
				$data['action'] = "continue";

		} else {

		//$data['response'] = 'hi';
		//json_exit($data);

		//zip a file
		$fname = substr($fn,strlen("../export/$username/"));
		if (file_exists($fn)) $output = shell_exec("cd ../export/$username; rm -f \"$fname.zip\"; zip \"$fname.zip\" \"$fname\"");

		//$this->benchmark->mark('code_end');
		//$elapse = $this->benchmark->elapsed_time('code_start', 'code_end');
		$extime = $this->session->userdata('extime');
		$this->session->unset_userdata('extime');

		$elapse = time() - $extime;

		$total_elapse = toTime($elapse);

		if (file_exists($fn)&&file_exists("$fn.zip"))
			{

			$fz1 = ceil(filesize($fn)/1024);
			$fz2 = ceil(filesize($fn.".zip")/1024);

			$fname = substr($fn,strlen("../export/$username/"));

			$data['response'] = "Export generated in $total_elapse.";

			$dlink = site_url("download/$fname");

			$data['response'] .= "<p><b>Download</b> your report by <b>clicking the file name</b> below<br />or <b>select file</b> to send to your email by </b>clicking the option</b></p><table width='400' id='edata'><tbody>";
			$data['response'] .= "<tr><td><input type='radio' name='exf' checked='checked' value='$fname' /></td><td><a href='$dlink' class='$format' >$fname </a></td> <td><button type='button' class='button' onclick = 'location.href=\"$dlink\"'><span><b>Download</b></span></button></td> <td>$fz1 Kb</td></tr>";
			$data['response'] .= "<tr><td><input type='radio'  name='exf' value='$fname.zip' /></td><td><a href='$dlink.zip' class='zip' >$fname.zip </a></td> <td><button type='button' class='button' onclick = 'location.href=\"$dlink.zip\"'><span><b>Download</b></span></button></td> <td>$fz2 Kb</td></tr>";
			$data['response'] .= "</tbody></table>";

			} else {
			if (isset($output)) $data['response'] = rp($output,1);
			}

		}

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);


	}


	function entry($ig = "",$id = "", $dtype = "")
	{
		if(empty($ig))
			$ig = $this->input->post('ig');

		if(empty($id))
			$id = $this->input->post('entryid');

		$user = $this->session->userdata('user');

		if ($user['utype']==17)
		{
		$allowedentries = $this->session->userdata('allowedentries');
		if (!in_array($id,$allowedentries)) redirect('promo/show/3');

		}

		$this->load->model('Shipping2','mdb');
		$this->load->helper('str2');

		$data['row'] = $this->mdb->getEntry($id,'igalldata_'.$this->country,$dtype);

		$data['cfields'] = $this->mdb->countryFields($this->country);

		$row = $this->mdb->getEntry2($id,$this->country);

		if (!$data['row']) show_404();
		$data['ig'] = $ig;
		$data['query'] = $this->mdb->getQuery($ig);

		$data['gmap_country'] = 'igalldata_'.$this->country;


		if (isset($data['query']['qlite']))
			$data['row'] = hlite2($data['row'],unserialize($data['query']['qlite']));

		$data['country'] = $this->country;

		$data['gmap_country'] = 'igalldata_'.$this->country;
		$data['vmap'] = $this->mdb->hasVMap($this->country);

		$data['dtype'] = $dtype;

		$data['content'] = $this->load->view('panes/entry',$data,true);
		$this->load->view('template/popup',$data);
	}


	function map($ig,$id,$field)
	{

		$this->load->model('Shipping2','mdb');
		$data['row'] = $this->mdb->getEntry($id,$this->country);
		$data['field'] = $field;
		$data['content'] = $this->load->view('panes/map',$data,true);
		$this->load->view('template/popup',$data);
	}

	//new maketable function
	function maketable2()
	{

		//get user info
		$user = $this->session->userdata('user');

		//load plugin n model
		$this->load->helper('str2');
		$this->load->model('Shipping2','xdb');

		// Disable temporarily
		//$data["response"] = rp("We are sorry for the inconvenience. Colombia records are currently being updated. Kindly give us 4-5 hours to complete update. Thank you.");
		//json_exit($data);

        $this->xdb->country = $this->country;

		//get common post vars
		$page = $this->input->post('page');
		$rp = $this->input->post('rp');
		$sortname = $this->input->post('sortname');
		$sortorder = strtoupper($this->input->post('sortorder'));

		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$start = (($page-1) * $rp);
		$filters = false;

		//load rules
		$rules = $this->session->userdata('rules');
		$flimit = $rules['flimit']-(60*60*24);
		$tlimit = $rules['tlimit'];
		$qlimit = $rules['qlimit'];

		//init response
		$data['response'] = rp('Error processing your query! Please, try again.',1);
		$data['action'] = 'newsearch';
		$data['nstotal'] = $this->xdb->totalSearchbyUserId($user['id']);
		$data['nsleft'] = $qlimit - $data['nstotal'];
		$data['from'] = $from;
		$data['to'] = $to;
		$data['page'] = 0;
		$data['total'] = 0;
		$data['rows'] = array();


		//does search have sid?
		$sid = $this->input->post('sid');
		$stype = $this->input->post('stype');
		$dtype = $this->input->post('datatype');

		if($dtype == "ex")
		{
			$from = $this->input->post('from_ex');
			$to = $this->input->post('to_ex');
		}

		$data['sid'] = $sid;

		if (!$sid)
			{

			//if no sid build query

			//handle if pass limits for new queries
			if ($data['nsleft']<=0)
				{
				if ($user['utype']>12 &&  $user['utype'] != 23)
				{
					$data['response'] = rp("You have reached your daily limit of <b>$qlimit</b> searches. Request for an <a href='#' onclick='$(\"a.promo2\").trigger(\"click\"); return false;' ><b class='hlite'>upgrade</b></a> now!",1);
				}
				else
					$data['response'] = rp("You have reached your daily limit of <b>$qlimit</b> searches. Request for an <b>upgrade</b> now!",1);
				json_exit($data);
				}

			//return if no range
			if (!$from||!$to)
				{
				$ds['response'] = 'Error with range data';
				$ds['action'] = 'newsearch';
				json_exit($data);
				}

			$q['qfrom'] = strtotime($from);
			$q['qto'] = strtotime($to);

			//switch if wrong
			if ($q['qfrom']>$q['qto'])
				{
				$data['from'] = $to;
				$data['to'] = $from;
				$q['qfrom'] = $to;
				$q['qto'] = strtotime($from);
				}

			//handle if range passes limits
			// No Limit for Latin Data Removing this function
			/*
			if ($q['qfrom']<$flimit)
				{
				$data['response'] = rp("You are searching for records that are <strong>beyond</strong> the range of your account type. Please request an <strong>upgrade</strong>  or select a range of dates more recent than <b>".date("m/d/Y",$flimit)."</b>.",1);
				json_exit($data);
				}

			if ($q['qto']>$tlimit)
				{
				$data['response'] = rp("You are searching for records that are <strong>beyond</strong> the range of your account type. Please request an <strong>upgrade</strong>  or select a range of dates more recent than <b>".date("m/d/Y",$tlimit)."</b>.",1);
				json_exit($data);
				}
			*/

			$qlite = array();

			//get variables
				$fcond = $this->input->post('cond');
				$qtype = $this->input->post('qtype');
				$qry = $this->input->post('qry');
				$mtype = $this->input->post('mtype');
				$qzip = $this->input->post('qzip');
				$qkeyname = $this->input->post('qkeyname');
				$qzip1 = $this->input->post('qzip1');
				$qzip2 = $this->input->post('qzip2');
				$qmiles = $this->input->post('qmiles');
				$exclude = $this->input->post('exclude');
				$hvm = $this->input->post('hvm');

				if ($exclude)
					{
/*
					$fcond[] = "NOT";
					$qtype[] = "consname";
					$qry[] = "NOT AVAILABLE";
					$mtype[] = "EXACT";
					$qzip[] = "";
					$qzip1[] = "";
					$qzip2[] = "";
					$qmiles[] = "";
					$qkeyname[] = "";

					$fcond[] = "NOT";
					$qtype[] = "consname";
					$qry[] = "N/A";
					$mtype[] = "EXACT";
					$qzip[] = "";
					$qzip1[] = "";
					$qzip2[] = "";
					$qmiles[] = "";
					$qkeyname[] = "";
*/
					$filters[] = array(
								'field' => 'consid'
								,'values' => array(2663619)
								,'exclude' => true
								);

					}

				if ($hvm)
					{
					$fcond[] = "AND";
					$qtype[] = "masterbillofladingind";
					$qry[] = "$hvm";
					$mtype[] = "EXACT";
					$qzip[] = "";
					$qzip1[] = "";
					$qzip2[] = "";
					$qmiles[] = "";
					$qkeyname[] = "";
					}

				//build qlite
				for ($n=0;$n<count($qtype);$n++)
				{
					if (!isset($qtype[$n])) continue;
					if (!isset($qry[$n])) continue;

					if($qtype[$n] == 'origin')
					{
						if(strtoupper($qry[$n]) == "EEUU" || strtoupper($qry[$n]) == "E.E.U.U")
						{
							$qry[$n] = 'Estados Unidos';
							$data['search'] = $qry[$n];
						}
					}

					if ($qtype[$n]=='distancefromzip')
						{
						//zip code query
						$numallow = "/[^0-9]/i";
						$qzip[$n] = preg_replace($numallow,"",$qzip[$n]);
						$qmiles[$n] = preg_replace($numallow,"",$qmiles[$n]);
						if ($qzip[$n]!=''&&$qmiles[$n]!='')
							{
							//limit 25 miles for now

							while (strlen($qzip[$n])<5)
								{
								$qzip[$n] = "0".$qzip[$n];
								}

							if (strlen($qzip[$n])>5)
								{
								$data['response'] = rp('Please enter the correct Zip Code 5 digit format.',1);
								json_exit($data);
								}

							if ($qmiles[$n]>5)
								{
								$data['response'] = rp('Please limit Zip Code feature to 5 miles.',1);
								json_exit($data);
								}


							$zcodes = $this->xdb->getZipCodes($qzip[$n],$qmiles[$n]);
							if ($zcodes)
								{
								$qlite['zipcode'][] = array($fcond[$n],implode(' | ',$zcodes),'ANY');
								}

							}
						//json_exit($zcodes);
						}
					else if ($qtype[$n]=='ziprange')
						{

						//zip code query
						$numallow = "/[^0-9]/i";
						$qzip1[$n] = preg_replace($numallow,"",$qzip1[$n]);
						$qzip2[$n] = preg_replace($numallow,"",$qzip2[$n]);

						if ($qzip1[$n]>$qzip2[$n])
							{
							$qzip1temp = $qzip1[$n];
							$qzip1[$n] = $qzip2[$n];
							$qzip2[$n] = $qzip1temp;
							}

						if ($qzip1[$n]!=''&&$qzip2[$n]!='')
							{

							$filters[] = array(
										'field' => 'zipcode_s'
										,'from' => $qzip1[$n]
										,'to' => $qzip2[$n]
										);
/*

							$limit = 500;

							$zcodes = $this->xdb->getZipRange($qzip1[$n],$qzip2[$n],1000);

							//json_exit($zcodes);

							if (count($zcodes)>$limit)
							{
							$data['response'] = rp("The zip code range generates exceed the $limit zipcodes limit allowed by the system, please modify your range to generate a result.",1);
							json_exit($data);
							}

							if ($zcodes)
								{
								$qlite['zipcode'][] = array($fcond[$n],implode(' | ',$zcodes),'ANY');
								}
*/
							}


						}
					else
						{
						//normal query
						$allowed = "/[^a-z0-9* -]/i";
						$qry[$n] = preg_replace($allowed," ",$qry[$n]);
						if (trim($qry[$n])) $qlite[$qtype[$n]][] = array($fcond[$n],$qry[$n],$mtype[$n],$qkeyname[$n]);
						}

				}

				$query = '';

				$this->load->helper('sphinx');
				$cl = new SphinxClient();

				//build from qlite -> consider exporting to a function
				foreach ($qlite as $qkey => $ql)
				{
					//$query .= " @$qkey ";
					$qc = 0;
					foreach ($ql as $qt)
						{
							$qc += 1;

							$qi = $qt[1];

							//$qi = $cl->EscapeString($qi);

							if ($qt[2]=='EXACT') $qi = '"'.$qi.'"';
							if ($qt[2]=='STARTS') $qi = "^$qi";
							if ($qt[2]=='ENDS') $qi = $qi."$";

							$qi = "($qi)";

							if ($qt[0]=='OR'&&$qc>1) $qi = " | $qi ";
							if ($qt[0]=='NOT') $qi = " -$qi ";

							if ($qt[0]=='OR'&&$qc==1&&$query!='') $query .= " | ";
							if ($qc==1 && $qkey != 'all') $query .= " @$qkey ";

							$query .= $qi;


                            //json_exit($query);
						}
				}

				if ($query==''&&!$filters)
					{
					$data['response'] = rp('No results have been found using the search criteria provided.  Please modify your search terms and try again.',1);
					json_exit($data);
					}

				//show query
				//$data['response'] = rp(print_r($qlite,true));
				//json_exit($data);

				$q['userid'] = $user['id'];
				$q['query'] = $query;
				$q['qlite'] = serialize($qlite);

				$q['qsortname'] = $sortname;
				$q['qsortorder'] = $sortorder;
				$q['qdate'] = date('Y-m-d H:i:s');
				$q['session'] = $this->session->userdata('session_id');
				$q['ip'] = $_SERVER['REMOTE_ADDR'];
				$q['view'] = 1;

				if (is_array($filters))
					$q['filters'] = serialize($filters);

			} // end of build query
			else
				{
				$q = $this->xdb->getQuery($sid);
				$q['qsortname'] = $sortname;
				$q['qsortorder'] = $sortorder;
				$q['view'] += 1;
				$qlite = unserialize($q['qlite']);
				if (!is_array($qlite)) $qlite = array();

				if ($q['filters'])
					$filters = unserialize($q['filters']);

				$dtype = $q['dtype'];

				if ($stype==2) //sub search
					{
					$sid = 0;
					unset($q['aid']);
					$q['qdate'] = date('Y-m-d H:i:s');
					$q['session'] = $this->session->userdata('session_id');
					$q['qto'] = time();
					$q['ip'] = $_SERVER['REMOTE_ADDR'];
					$q['view'] = 1;
					$q['print'] = 0;
					}

				if ($stype==3) //sub search
					{
					$sid = 0;
					unset($q['aid']);
					$q['qdate'] = date('Y-m-d H:i:s');
					$q['session'] = $this->session->userdata('session_id');
					$q['ip'] = $_SERVER['REMOTE_ADDR'];
					$q['view'] = 1;
					$q['print'] = 0;
					}

					//ticket 1261
					$this->load->model('Saved_search_db');
					$sv = $this->Saved_search_db->getSave(array('sv_aid'=>$sid));
					if ($sv)
					{
						if (date('Y-m-d',$q['qto']) == date('Y-m-d',strtotime($sv['sv_date']))) $q['qto'] = time();
					}

				}

			//get entryids

			$pagemax = 100000;

			if (($start+$rp)>$pagemax)
				{
				$data['page'] = $page;
				$data['total'] = $q['qresult'];
				$data['response'] = rp('Maximum page limit reached. Retrieve remaining results by refining your search.',1);
				json_exit($data);
				}


			$rs = $this->xdb->getEntries($q['query'],$q['qfrom'],$q['qto'],$start,$rp,$sortname,$sortorder,0,0,0,$filters,$dtype);


			//$data['sortname'] = $sortorder;
            //json_exit($data);
/*
			$data['response'] = print_r($rs['entries'],true);
			json_exit($data);
*/


			if (!$rs['error'])
				{

				$q['qresult'] = $rs['total_found'];
			    $q['qtime'] = $rs['time'];
			    $q['co_id'] = $this->country;
			    $q['s_id'] = defined('S_ID') ? S_ID : "";
			    $q['dtype'] = $dtype;

				if (!$sid)
					{
					$sid = $this->xdb->saveQuery($q);
					$data['sid'] = $sid;
					$data['nstotal'] = $this->xdb->totalSearchbyUserId($user['id']);
					$data['nsleft'] = $qlimit - $data['nstotal'];
					}

				//update search history
				$this->xdb->updateQuery($sid,$q);


				//get rows
				$rows = array();
				$db = array();

				if ($q['qresult']) $db = $this->xdb->getRows($rs['entries'],false," FIELD(entryid,".implode(',',$rs['entries']).' )',false,false,'igalldata_'.$this->country,$dtype);

                //json_exit($db);
				//print_r($rs['entries']); exit;

				if ($user['utype']==17)
				{
					$this->session->unset_userdata('allowedentries');
					$allowedentries = array();
				}


				if($dtype == "ex")
					$dtype = "/ex";
				else
					$dtype = "/im";


				foreach ($db as $row)
				{

				//trial app user stop at 3 rows
				if ($user['utype']==17&&count($rows)>2)
				{


				$rows[] = array(
						"id" => $row['entryid'],
						"cell" => array(
							"<a href=\"".site_url("promo/show/3")."/?TB_iframe=true&height=370&width=600\" class=\"view view2\" title=\"Upgrade From Trial Account Now!\" ></a>"
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: 50px; margin-left: 10px;"></div>'
							,'<div class="blur blur2" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,50).'px"></div>'
							,'<div class="blur blur2" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,50).'px"></div>'
							,'<div class="blur blur2" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,50).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,40).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(0,30).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: 5px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
							,'<div class="blur" style="background-position: -'.rand(0,200).'px center; width: '.rand(50,150).'px"></div>'
						)
					);


				continue;
				}

				if ($user['utype']==17)
				{
					$allowedentries[] = $row['entryid'];
					$this->session->set_userdata('allowedentries',$allowedentries);
				}

				//hlite rows
				$avfilters = search_filters($this->country);
				$avfilter = array();
				foreach($avfilters as $k=>$avf)
				{
					$avfilter[] = $k;
				}


				//hlite rows
				$row = hlite2($row,'',$avfilter);

                //$data['q'] = $row;

                $flds = array();
                $fields = $this->countries_db->fields($this->country,$this->country);

                foreach($fields as $f)
                {
					if($dtype == "/ex")
					{
						if($f['field_name'] == "countries_name")
						{
							$f['field_name'] = "destination";
						}
					}

                    $flds[] = utf8_decode($row[$f['field_name']]);
                    //$flds[] = iconv('UTF-8', 'ASCII//TRANSLIT', $row[$f['field_name']]);
                }

                //json_exit($flds);
                unset($flds[0]);

                for($i=1;$i<count($flds);$i++)
				{
					if($flds[$i] == "0" || $flds[$i] == "0.00")
					{
						$flds[$i] = "-";
					}
				}

                $links = array("<a href=\"".site_url($this->country."/entry/$sid/".$row['entryid']).$dtype."/?TB_iframe=true&height=480&width=760\" class=\"view\" title=\"View Shipment Details - ".substr(strip_tags($row['consname']),0,30)."\" ></a>");
                //$links = array("<a href=\"#".$sid."/".$row['entryid']."\" class=\"view\" title=\"View Record ".$row['entryid']." - ".substr(strip_tags($row['consname']),0,30)."\" ></a>");
                $xfields = array_merge($links,$flds);

                //json_exit($xfields);

				$rows[] = array(
						"id" => $row['entryid'],
						"cell" => $xfields
					);

				}


				$scount = $this->session->userdata('fissue');

				if($scount >= 4)
				{
					$data['response']  = buildResponseError();
				}
				else
				{
					$c = country_name($this->country);
					$dt = $dtype == "/im" ? 'import' : 'export';
					$extra = "of {$c} {$dt} data ";
					$data['response'] = rp(buildResponse($q,$extra));
				}

				$data['scount'] = $scount;

				$data['rows'] = $rows;
				$data['total'] = $q['qresult'];

				if ($start>$data['total']) $page = 1;

				$data['page'] = $page;



			} else {
				$data['response'] = rp($rs['error'],1);
			}


		//load data to JSON
		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);
	}

	function pdf($ig,$id)
    {

		$user = $this->session->userdata('user');

        if ($user['utype']==17)
        {
        $allowedentries = $this->session->userdata('allowedentries');
        if (!in_array($id,$allowedentries)) redirect('promo/show/3');

        }

        $this->load->model('Shipping2','mdb');
        $this->load->helper('str2');

		//$this->mdb->country = $this->country;

        $data['row'] = $this->mdb->getEntry($id,'igalldata_'.$this->country);

/*
        echo "<pre>";
        print_r($data); exit;
*/

        if (!$data['row']) show_404();
        $data['ig'] = $ig;
        $data['query'] = $this->mdb->getQuery($ig);

        if ($data['row']['bl'])
            {
            $result = $this->mdb->getEntries("@billofladingnbr \"".$data['row']['masterbilloflading']."\"",0,0,0,1);
            if (is_array($result['entries']))
                {
                $data['row']['masterbilloflading_id'] = $result['entries'][0];
                }
            }



        if (isset($data['query']['qlite']))
            $data['row'] = hlite2($data['row'],unserialize($data['query']['qlite']));


        $data['date'] = date('Y-m-d H:i:s');
        //$data['company_name'] = 'INTERNATIONAL ASPECTS INC';

        //print_r($data['row']);
        $this->load->view('export/pdf',$data);
    }


}
