<?php

class Training extends Controller
{
	function Training()
	{
		parent::Controller();
	}
	
	function _check_daterange($date)
	{
		$date = explode('-',$date);
		$newdate = $date[2].'-'.$date[0].'-'.$date[1];
		
		$date = strtotime($newdate);
		$now = strtotime(date('Y-m-d'));
		
		if($date < $now)
		{
			$this->form_validation->set_message('_check_daterange','Past date is not allowed');
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function post()
	{
		if(!is_ajax()) exit();

		$this->load->library('form_validation');
		$user = $this->session->userdata('user');
		
		$this->form_validation->set_rules('date','Date','required|callback__check_daterange');
		$this->form_validation->set_rules('ptime','Time','required');
		$this->form_validation->set_rules('timezone','Timezone','required');
		//$this->form_validation->set_rules('description','Data interested in','required');

		if($this->form_validation->run() == FALSE)
		{
			$data['json']['action'] = "retry";
			$data['json']['errors'] = $this->form_validation->_error_array;
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			
			$data['json']['message'] = "";
			$data['json']['message'] .= "<div class='error_msg'>";
			$data['json']['message'] .= "<h3 class='h3'>PLEASE CORRECT THE FOLLOWING ERRORS:</h3>";
			$data['json']['message'] .= "<ul>".validation_errors()."</ul>";
			$data['json']['message'] .= '<button type="button" class="button" onclick="showForm()"><span><b>OK</b></span></button>';
			
			$data['json']['message'] .= "</div>";
			
		}
		else
		{
			$this->load->model('users_db');
			
			$db['clientid'] 	= $user['id'];
			$db['atype'] 		= 6; // type = training session.
			$db['reason'] 		= 2; // reason = training.
			$db['timezone'] 	= $this->input->post('timezone');
			$db['ttime'] 		= $this->input->post('ptime');
			$db['tdate'] 		= $this->input->post('date');
			$db['notes'] 		= $this->input->post('description');
			$db['category'] 	= 0;
			$db['status'] 		= 0;
			
			$cdate = date('F d, y',strtotime($db['tdate']));
			$time = date('h:i a',strtotime($db['ttime']));
			
			$date = explode('-',$db['tdate']);
			$newdate = $date[2].'-'.$date[0].'-'.$date[1];
			
			$cdate = date('F d, Y',strtotime($newdate));
			$time = date('h:i a',strtotime($db['ttime']));
			
			$db['tdate'] = $newdate;
			
			
			$timezone = $this->users_db->getTimezone(array('id'=>$db['timezone']));
			
			$data['json']['action'] = "success";
			$data['json']['message']  = "";
			//$data['json']['message'] .= "<br/><div class='success_msg'><p>A customer success team member from ImportGenius.com will call you on {$cdate} at {$time} {$timezone['name']}. Please be sure to be near a computer so they can share their screen with you.</p></div>";
			//$data['json']['message'] .= "<br/><div class='success_msg'><p>Thanks for your interest in a training session to learn more about our application. One of our customer success team members will contact you shortly to confirm the time with you.</p></div>";
			$data['json']['message'] .= "<br/><div class='success_msg'><p>Thank you for your interest in a training session to learn more about our application. One of our fantabulous Account Managers will contact you shortly to confirm the time that works best for you.</p></div>";

			$data['json']['message'] .= "<br/><br/><button type=\"button\" class=\"button\" onclick=\"location.href='index.html'\"><span><b>GO BACK</b></span></button>";
						
			$this->users_db->save_activity($db);

			$this->load->library('email');
		
			$config['mailtype'] = 'html';
		
			$this->email->initialize($config);		
		
			$this->email->from('info@importgenius.com', 'ImportGenius Team');
			//$this->email->bcc('jinggo@importgenius.com', 'ImportGenius Team');
			$this->email->to('csr@importgenius.com', 'ImportGenius Team');
			
			$data['timezone'] 	= $timezone;
			$data['username'] 	= $user['username'];
			$data['atype'] 		= $user['atype'];
			$data['firstname'] 	= $user['firstname'];
			$data['lastname'] 	= $user['lastname'];
			$data['email'] 		= $user['email'];
			$data['datainterest'] = $this->input->post('description');
			$data['business'] = $user['business'];
			$data['phone'] = $user['phone'];
			$data['schedule'] = $cdate.' at '.$time;
			

			$message = $this->load->view('email/training_notif',$data,true);
		
			$e['subject'] = $user['username'].' has requested a training session';
			$e['content'] = $message;

			$dmessage = $this->load->view('template/email',$e,true);

			$this->email->subject($e['subject']);
		
			//$this->email->message('<img src="'.base_url().'media/images/rsshead.jpg" /><p>Thank you for using ImportGenius.</p><p>Attach is an exported data of your search results.</p>');
			$this->email->message($dmessage);
		
			$this->email->send();
			
		}
		
		$this->load->view('template/ajax2',$data);
	}
}
