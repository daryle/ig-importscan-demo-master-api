<?php 
	class Ccart extends Controller
	{
		function Ccart()
		{
			parent::Controller();
			
			$this->load->library('cart');
			$this->load->model('countries_db');
			$this->load->model('payment_db');
			$this->load->model('splash_db');
		}
		
		function post_order()
		{
			$country_id = $this->input->post('country_id');
			
			if($country_id != 0)
			{
				$country = $this->countries_db->getList(array('status'=>1,'country_id'=>$country_id),false,false,false);
				$price = $country[0]['price'];
				$name = $country[0]['country_name'];
			}
			else
			{
				$this->cart->destroy();
				
				$price = 199.00;
				$name = 'all countries shown above';
			}
			
			$data = array(
               'id'      => $country_id,
               'qty'     => 1,
               'price'   => $price,
               'name'    => $name,
            );

			$this->cart->insert($data);
		
			$data['items'] = $this->cart->total_items();
			$data['total'] = number_format($this->cart->total(),2);
			
			$this->sessCountries();
			$data['scountries'] = implode(', ',$this->session->userdata('scountries'));
			
			$data['json'] = $data;
			$this->load->view('template/ajax2',$data);
			
		}
		
		function remove_item()
		{
			$rowid = $this->input->post('rowid');
			
			$data = array(
				'rowid' => $rowid,
				'qty'	=> 0
			);
			
			$this->cart->update($data);
			
			$data['items'] = $this->cart->total_items();
			$data['total'] = number_format($this->cart->total(),2);
			
			$data['json'] = $data;
			$this->load->view('template/ajax2',$data);
		}
		
		function remove_items()
		{
			$this->cart->destroy();
			
			$data['total'] = $this->cart->total();
			$data['json'] = $data;
			$this->load->view('template/ajax2',$data);
		}
		
		function sessCountries()
		{
			$items = $this->cart->contents();
			$i = array();
			$scountries = "";
			
			foreach($items as $item)
			{
				$i[] = $item['name'];
			}
			
			$countries = $this->session->set_userdata('scountries',$i);
		}
		
		function destroy_cart()
		{
			$this->cart->destroy();
		}
		
		function get_items()
		{
			$items = $this->cart->contents();
			
			$data['items'] = $items;
			$data['total'] = number_format($this->cart->total(),2);
			$data['json'] = $data;
			$this->load->view('template/ajax2',$data);
		}
		
		function process_order()
		{
			$this->load->model('users_db');
			//sleep(2);
			
			$arbtrans	= false;
			$debug		= true;
			$perday		= 0;
			$user 		= $this->session->userdata('user');
			$card 		= $this->users_db->getCard($user['username']);
			$cart_total = $this->cart->total();
			$diffdays	= 0;
			$charge 	= 0;
			$amount		= $user['amount'];
			
			
			$data['action'] = "";
			
			if($card)
			{
				$arbtrans = $this->users_db->getArbtrans($card['cid']);
				
				if($arbtrans)
				{
					$year		= date('Y');
					$month 		= date('m');
					$day 		= date('d',strtotime($arbtrans['datecharge']));
					$amount		= $user['amount'];
					
					$mktime		= mktime(12,0,0,$month,$day,$year);
					$lastdate	= date('Y-m-d',$mktime);
					$diffdays	= (int)date('d') - (int)$day;

					if($amount == 0)
					{
						// Get Amount
						$cond 	 = array('data_code'=>'U_TYPE','data_value'=>$user['utype']);
						$dataset = $this->users_db->getUtypeAmount($cond);
						$amount  = $dataset['description']; 
					}
					
					if($diffdays > 0)
					{
						$perday	= ((int)$diffdays / 30);
						$charge = $perday * $cart_total;
					}
					else
					{
						$charge = $cart_total;
					}

					
				}
				else
				{
					$charge = $cart_total;
				}
				
				$debug = true;
				
				if($debug)
				{
					$data['debug'] = array(
							'diffdays' 	=> $diffdays,
							'charge'	=> $charge,
							'amount'	=> $amount,
							'perday'	=> $perday,
							'card'		=> $card,
							);
				}
				
				$response = true;
				$response = $this->payment_db->validateCard($card['cid'],$charge);

				//If successfully paid add to available countries
				
				if($response)
				{
					
					
					$this->users_db->addCountryToAccount($user['id']);
					$this->cart->destroy();
					
					
					$udb['amount'] = $charge + $amount;
					$this->users_db->updateUser($udb,array('id'=>$user['id']));
					
					
					$sdb['noshow'] = 1;
					$sdb['id']  = $user['id'];
					
					$this->splash_db->saveSplash($sdb,$user['id']);
					
					$data['scountries'] = implode(', ',$this->session->userdata('scountries'));
					$data['action'] = "success"; 
				}
				
				$data['response'] = $response;
				
			}
			else
			{
				// Save Pending Order
				$data['msg'] = "Payment not found";
			}
	
			$data['json'] = $data;
			$this->load->view('template/ajax2',$data);
		}
		
		function pinfo()
		{
			echo phpinfo();
		}
	}
?>
