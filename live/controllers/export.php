<?php


class Export extends Controller {

	var $total_export;

	function __construct()
	{
		parent::__construct();
/*
		//require https
		if (config_item('proto')=='http'&&config_item('domain')=='importgenius.com')
			header("Location: ".str_replace('p://','ps://',site_url($this->uri->uri_string())));
*/
		$method = $this->uri->rsegment(2);

		

		if ($method == 'exstatus' || $method == 'cancelexport') return true;

		$this->load->model('Users_db');
		$this->load->model('export_db');

		//validate

		$user = $this->session->userdata('user');

		if ($user)
		{
		$sid = $user['session_id'];

		if (isset($user['multi']))
			$m = $user['multi'];
		else
			$m = 0;

		array_splice($user, 19);

		if (!$m)
			$user['session_id'] = $sid;

		$user['multi'] = $m;

		//update rules

		$rules = $this->session->userdata('rules');

		if (isset($rules['dlimit']))
		{
		$rules['tlimit'] = time();
		if ($rules['dlimit']>0)
			{
			$rules['flimit'] = time()-(60*60*24*$rules['dlimit']);
			}
		$this->session->set_userdata('rules',$rules);

		}

		}

		$g_user  = $this->Users_db->getUser($user);

		if (!$g_user)
		{

			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			{
				header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
				header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
				header("Cache-Control: no-cache, must-revalidate" );
				header("Pragma: no-cache" );
				header("Content-type: text/x-json");
				echo 'Your session been signed out. <a href="'.site_url('login').'">Click here to sign-in again</a><div style="display:none">'. gmdate( "D, d M Y H:i:s" ) .'</div>';
				exit;
			} else {
				$this->session->sess_destroy();
				redirect('login');
			}
		}

	}

	function generate()
	{
		if (!count($_POST)) return true;

		$data['response'] = 'Error generating results';
		$data['action'] = 'retry';
		$dt = '';

		$this->load->helper('str2');

		$user = $this->session->userdata('user');
		$username = $user['username'];
		$userid = $user['id'];

		//init variables
		$template = $this->input->post('template');
		$format = $this->input->post('format');
		$sid = $this->input->post('sid');
		$rec = $this->input->post('rec');
		$nfrom = strtonumber($this->input->post('nfrom'),'.',',');
		$nto = strtonumber($this->input->post('nto'),'.',',');
		$notify = $this->input->post('notify_me');
		$notify_email = $this->input->post('notify_email');
		$country = $this->input->post('country');

		$rules = $this->session->userdata('rules');



		/**
		 *  EXPORT HISTORY
		 */

		// $xlimit = $this->export_db->get_export_limit(array('client_id'=>$userid));

		// if($xlimit)
		// {
		// 	$remaining_export = $xlimit['remaining_export'];

		// 	if($total_search > $remaining_export)
		// 	{
		// 		// Reaches the total export
		// 		$max_limit = $xlimit['remaining_export'];
		// 		$data['response'] = rp("Sorry, you only have {$max_limit} export credits in your account.",1);
		// 		json_exit($data);
		// 	}

		// }

		// New Plan



		$datatype = $this->input->post('datatype');
		$export_fields = $this->input->post('export_fields');

		if (!valid_email($notify_email))
			{
			$notify_email = $user['email'];
			}

		$filename = trim($this->input->post('fname'));

		if (!is_dir("../export/$username")) mkdir("../export/$username",0755);

		//check if invalid
		if ($nfrom>$nto&&$rec==1)
		{
/*
		$nto = $nfrom;
		$nfrom = (int)$this->input->post('nto');
*/

		$data['response'] = rp("The <u>From</u> Field needs to be less than the <u>To</u> Field, these fields determine where your export will begin and end.",1);
		json_exit($data);


		}


		$this->load->model('Shipping2','xdb');

		//load query
		$q = $this->xdb->getQuery($sid);

		if (!$q)
			{
			$data['response'] = rp("There is no valid search result to be exported.");
			json_exit($data);
			}

		$q['print'] += 1;
		$this->xdb->updateQuery($sid,$q);

		$max_result = $this->session->userdata('max_export_result') ? $this->session->userdata('max_export_result') : 100000;

		$start = 0;
		$rp = $q['qresult'];

		$nfrom -= 1;

		$trp = $nto - $nfrom;

		$this->total_export = $trp;

		if ($rec==0)
			{
			$trp = $rp;
			}

		if ($rec==1)
			{
			$start = $nfrom;

			if ($start>$rp)
				{
				$rp = number_format($rp,0);
				$data['response'] = rp("The <u>From</u> Field needs to be less than $rp, this field determine where your export will begin.",1);
				json_exit($data);
				}

			if (($start+$trp)>$rp)
				$trp = $rp-$start;

			}

		if(in_array($user['utype'],array(26,27,28,29)))
		{
			$xremained = (int)$this->session->userdata['xremained']; // 1000
			$addonexport = (int)$this->session->userdata['addonexport'];

			$xremained += $addonexport;

			$data['xremained'] = $xremained;
			$data['trp'] = $trp;

			if($trp > $xremained)
			{
				$upgrade_msg = "Request for an <a href=\"#\"><b class=\"hlite upgrade\">upgrade</b></a> now.";

				if(in_array($user['utype'],array(28,29)))
				{
					
					$upgrade_msg = "<div class=\"addmore-exprow\">
						<p>To add 20,000 more export rows for $250 please click on the button below.</p>
					    <a id=\"addexport\" class=\"thickbox hlight tbnt\" title=\"Add More Export Rows\" href=\"#\" onclick=\"$('#addexport').hide();$('#addexportproceed').show();\">Add More Export Rows</a>
					    <div class=\"clear\"></div>
					    <div id=\"addexportproceed\" style=\"display:none;\" class=\"addprd\"><p>Add 20,000 export row credits to your account for $250. Proceed?</p>
					        <a class=\"thickbox hlight proceed\" id=\"proceed_addon\" title=\"Add More Credits Now\" href=\"#\">Add More Credits Now</a>
					    </div>
					</div>";
				}

				$data['response'] = rp("<p class='export_error'>Sorry, you only have {$xremained} export credits in your account.</p> $upgrade_msg",1);
				
				json_exit($data);
			}

		}

		if (($trp)>$max_result)
			{
			$nfrom += 1;

			$nfrom = number_format($nfrom,0);
			$nto = number_format($nto,0);
			$max_result = number_format($max_result,0);

/*
			$data['response'] = rp("
			Your export request starts from $nfrom up to $nto. <br />
			This will generate $trp results. <br />
			Maximum results per export has been currently set to $max_result only. <br /> <br />
			This is to allow faster exports. <br />
			You can group your export results to $max_result per export <br /><br
			Please modify your export and try again.",1);
*/
			$data['response'] = rp("

			Sorry, your export request exceeded the limit of {$max_result} records per
			download. We put this in place to make exporting work more smoothly
			for all clients. You're best bet is to break this export request into
			a series of files each under 100,000 shipments. Apologies for the
			inconvenience. We're working to upgrade our infrastructure to allow
			ever larger data exports but in the meantime, this change is making
			downloads go a lot faster for everybody.

			",1);


			json_exit($data);
			}

		//record start time
		$extime = time();

		//create export status
		$this->load->model('Export_db');

		$filename = preg_replace("/[^a-zA-Z0-9 -]/", "", $filename);

		if (!$filename)
			{
				$filename = buildFilename($q);
			}

		$stat['sid'] = $sid;
		$stat['uid'] = $user['id'];
		$stat['percent'] = 0;
		$stat['file'] = $filename;
		$stat['extime'] = $extime;
		$stat['ltime'] = $extime;
		$stat['format'] = $format;
		$stat['server_id'] = S_ID;
		$stat['status'] = 0;
		$stat['notify'] = $notify;
		$stat['notify_email'] = $notify_email;
		$stat['from'] = $start;
		$stat['to'] = $trp;
		$stat['qdesc'] = buildResponse($q);
		$stat['ftime'] = $extime;
		$stat['retries'] = 0;
		$stat['cc_id'] = $country;
		$stat['export_fields'] = $export_fields;


		$pid = $this->Export_db->save_export(0,$stat);

		$fn = "../../../../export/$username/$pid";

		//if (!is_dir("../export/$username")) mkdir("../export/$username",0755);

		//system("cd ../apps/iscan4l/live/perl; nice -n 10 perl $format"."2.pl $sid $start \"$trp\" \"$fn\" $pid $country $datatype > /dev/null 2>&1 &");
		
		$path = "/grab/generate/".md5($pid);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://" .config_item('export_url'). $path);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		
		$data['pid'] = $pid;
		$data['action'] = 'continue2';
		$data['response'] = "";
		$data['percent'] = 0;
		$data['country'] = $stat['cc_id'];
		$data['script'] = "cd ../apps/iscan4l/live/perl; nice -n 10 perl $format"."2.pl $sid $start \"$trp\" \"$fn\" $pid  $country $datatype > /dev/null 2>&1 &";
		$data['export_url'] = "http://" .config_item('export_url'). $path;
		$json['json'] = $data;

		// if($xlimit)
		// {
		// 	$xdb['client_id'] 	= $userid;
		// 	$xdb['payment_type'] = $xlimit['payment_type'];
		// 	$xdb['payment_id'] 	= $xlimit['payment_id'];
		// 	$xdb['plan_type'] 	= $xlimit['plan_type'];
		// 	$xdb['start_date'] 	= $xlimit['start_date'];
		// 	$xdb['end_date'] 	= $xlimit['end_date'];
		// 	$xdb['remaining_export'] = $xlimit['remaining_export'] - $trp;
		// 	$xdb['pid'] = $pid;

		// 	$this->export_db->put_export_limit($xdb);

			

		// }

		// $json['json']['nto'] = $nto;
		// $json['json']['nfrom'] = $nfrom;

		// $json['json']['total_search'] = $trp;

		// $json['json']['total_export'] = $this->export_db->getTotalExport(array('uid'=>$userid));
		// $json['json']['date'] = date('Y-m-d');
		// $json['json']['total_remained'] = $xremained;
		// $json['json']['utype'] = $user['utype'];

		$this->load->view('template/ajax2',$json);
	}

	function retry($pid="",$xpid="")
	{

		$user = $this->session->userdata('user');
		$username = $user['username'];


		$this->load->helper('str2');
		$this->load->model('Export_db');

		$data['pid'] = $pid;
		$data['action'] = "done";

		$s = $this->Export_db->get_export($pid);

		//cancel current if there is
		if ($xpid)
			{
			$x = $this->Export_db->get_export($xpid);

			if ($x)
				{
				$nx['status'] = 11;
				$this->Export_db->save_export($xpid,$nx);
				}

			}

		//re run this export
		if ($s)
		{
			//reset a few settings
			$u['status'] = 0;
			$u['extime'] = time();
			$u['ltime'] = time();
			$u['server_id'] = S_ID;
			$u['percent'] = 0;
			$u['retries'] = $s['retries']+1;

			if ($s['notify']==2)
				$u['notify'] = 1;

			$this->Export_db->save_export($pid,$u);

			//retrieve settings for perl script
			$sid = $s['sid'];
			$start = $s['from'];
			$trp = $s['to'];
			$username = $user['username'];
			$format = $s['format'];

			$fn = "../../../../export/$username/$pid";
			//if (!is_dir("../export/$username")) mkdir("../export/$username",0755);
			$cc_id = $s['cc_id'];

			$datatype = "im";

			$slog = $this->Export_db->getSearchLog(array('aid'=>$s['sid']));


			if($slog)
			{
				if(!empty($slog['dtype']))
				{
					$datatype = $slog['dtype'];
				}
			}

			$path = "/grab/retry/".md5($pid);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://" .config_item('export_url'). $path);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);

			//system("cd ../apps/iscan4l/live/perl; nice -n 10 perl $format"."2.pl $sid $start \"$trp\" \"$fn\" $pid $cc_id $datatype > /dev/null 2>&1 &");

			$data['pscript'] = "perl $format"."2.pl $sid $start \"$trp\" \"$fn\" $pid $cc_id $datatype";
			$data['ccid'] = $cc_id;
			$data['script'] = "cd ../apps/iscan4l/live/perl; nice -n 10 perl $format"."2.pl $sid $start \"$trp\" \"$fn\" $pid  $cc_id $datatype > /dev/null 2>&1 &";
			$data['export_url'] = "http://" .config_item('export_url'). $path;
			$data['action'] = "continue2";

		}

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);

	}

	function status($pid = "")
	{

		$user = $this->session->userdata('user');
		$username = $user['username'];


		$this->load->helper('str2');
		$this->load->model('Export_db');

		$data['pid'] = $pid;


		$s = $this->Export_db->get_export($pid);

		if ($s)
		{

		$rtext = "";

		$p = $s['percent'];

		if ($p>1)
		{

		$elapse = $s['ltime'] - $s['extime'];

		$remain = floor(($elapse/$p) * (100-$p));

		$rtext = toTime($remain);
		$rtext = " with $rtext remaining..";

		if ($p==100)
			{
			$rtext = ". Preparing for download...";
			}

		}

		$data['response'] = $s['percent']."% complete $rtext";

		if ($s['percent']==0)
		{

			if ($s['status']==0)
				$data['response'] = "Please wait as your export <b class='hlite'>$s[file].$s[format]</b> request is now being queued.";

			if ($s['status']==1)
				$data['response'] = "We are creating your file <b class='hlite'>$s[file].$s[format]</b> to export.";

		}

		$data['percent'] = $s['percent'];
		$data['action'] = "continue2";

       if ($s['status']==2)
            redirect("export/prep_for_download/$pid");


		}
		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);


	}

	function status2($pid="")
	{

		$user = $this->session->userdata('user');
		$rules = $this->session->userdata('rules');

		$username = $user['username'];


		$this->load->helper('str2');
		$this->load->model('Export_db');

		$data['pid'] = $pid;

		$s = $this->Export_db->get_export($pid);

		if ($s)
		{

		$rtext = "";

		$p = $s['percent'];

		if ($p>1)
		{

		$elapse = $s['ltime'] - $s['extime'];

		$remain = floor(($elapse/$p) * (100-$p));

		$rtext = toTime($remain);
		$rtext = " with $rtext remaining..";

		if ($p==100)
			{
			$rtext = ". Preparing for download...";
			}

		}

		$data['response'] = $s['percent']."% complete $rtext";

		if ($s['percent']==0)
		{

			if ($s['status']==0)
				$data['response'] = "Please wait as your export <b class='hlite'>$s[file].$s[format]</b> request is now being queued.";

			if ($s['status']==1)
				$data['response'] = "We are creating your file <b class='hlite'>$s[file].$s[format]</b> to export.";

		}

		$data['percent'] = $s['percent'];
		$data['action'] = "continue2";

		//if ($s['status']==3) $data['action'] = 'cancelexport';
		//$data['status'] = $s;


		if ($s['status']>1 && $s['status'] < 11)
			{
			
			$msg = "";

			if(in_array($user['utype'],array(26,27,28,29)))
			{
				$get_export = $this->export_db->get_export($pid);
				
				$total_export = ($get_export['from'] + $get_export['to']) - $get_export['from'];

				$xremained = $this->session->userdata('xremained'); // 1000
				$addonexport = $this->session->userdata('addonexport'); 
				$export_left = $xremained + $addonexport;

				$sess_xremained = $this->session->userdata('xremained');
				$sess_addonexport = $this->session->userdata('addonexport');
				$sess_left = $sess_xremained + $sess_addonexport;

				if($xremained >= $total_export)
				{
					$xremained = $xremained - $total_export;

					$this->session->set_userdata('xremained',$xremained);

					$sess_xremained = $this->session->userdata('xremained');

					$user_export['current_export'] = $xremained;				
					$this->Users_db->updateUser($user_export, array('id'=>$user['id']));

					$sess_left = $sess_xremained + $sess_addonexport;
				}
				else
				{
					$deduct_addon = $total_export;
					if ($xremained>0)
					{
						$deduct_addon = $total_export - $xremained;
						$this->session->set_userdata('xremained',0);

						$user_export['current_export'] = 0;				
						$this->Users_db->updateUser($user_export, array('id'=>$user['id']));

					}
					$addonexport = $addonexport - $deduct_addon;
					$this->session->set_userdata('addonexport',$addonexport);

					$user_export['addon_export'] = $addonexport;	
					$this->Users_db->updateUser($user_export, array('id'=>$user['id']));

					$sess_xremained = $this->session->userdata('xremained');
					$sess_addonexport = $this->session->userdata('addonexport');
					$sess_left = $sess_xremained + $sess_addonexport;

				}

				$total_export_month = $this->export_db->getTotalExport(array('uid'=>$user['id'],'status'=>3), $user);
				
				$total_exports = $total_export_month['total'] + $total_export ;
				
				$msg = " You have made <b class='hlite'>{$total_exports} export(s)</b> this month, and you have <b class='hlite'>{$sess_left} export(s)</b> left.";

				
			}
			
			$data['response'] = rp("Success! Your file <b class='hlite'>$s[file].$s[format]</b> is ready for download.".$msg);
			$data['action'] = "exdone";
			
			}

		}
		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);


	}

	function viewed($pid='')
	{
	$this->load->model('Export_db');

	$s = $this->Export_db->get_export($pid);

/*
	echo "<pre>";
	print_r($s); exit;
*/

	if (!$s) show_404();

	$u['status'] = 3;
	$this->Export_db->save_export($pid,$u);

	}

	function cancel($pid='')
	{

		$user = $this->session->userdata('user');
		$username = $user['username'];


		$this->load->helper('str2');
		$this->load->model('Export_db');

		$data['pid'] = $pid;

		$s = $this->Export_db->get_export($pid);

		if ($s)
		{

		$u['status'] = 11;

		$this->Export_db->save_export($pid,$u);

		$data['action'] = 'cancelexport';
		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);

		}
	}

	function prep_for_download($pid="")
	{

		$user = $this->session->userdata('user');
		$username = $user['username'];
		$data['action'] = 'exmail';

		$this->load->helper('str2');

		$this->load->model('Export_db');

		$s = $this->Export_db->get_export($pid);

		if (!$s)
			{
			json_exit($data);
			}


		$fname = $s['file'].".$s[format]";

		$elapse = $s['ltime'] - $s['extime'];

		$data['total_elapse'] = toTime($elapse);

		$data['fz1'] = fsize($s['fz1']);
		$data['fz2'] = fsize($s['fz2']);
		$data['qdesc'] = $s['qdesc'];

		$data['pid'] = $s['p_id'];

		$data['dlink'] = site_url("export/download/".time()."/$s[p_id]");
		$data['format'] = $s['format'];

		$data['fname'] = $fname;

		$data['response'] = $this->load->view("template/export_response",$data,true);

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);



	}

	function email($pid)
	{

		$email = $this->input->post('email');
		$exf = $this->input->post('exf'); //format
		$user = $this->session->userdata('user');
		$username = $user['username'];

		$this->load->model('Export_db');
		$s = $this->Export_db->get_export($pid);

		if (!$s)
			{
				$data['response'] = "Please select an export to send";
				$data['action'] = 'retry';
				$json['json'] = $data;
				$this->load->view('template/ajax2',$json);
				return true;
			}

		$u['status'] = 3;
		$u['downloaded'] = $s['downloaded']+2;
		$this->Export_db->save_export($pid,$u);

		if (!$email)
			{
				$data['response'] = "Please enter a valid email address";
				$data['action'] = 'retry';
				$json['json'] = $data;
				$this->load->view('template/ajax2',$json);
				return true;
			}

		$fz = ceil($s['fz'.$exf]/1048576);

		if ($fz>10)
			{
				$data['response'] = "You're file is $fz Mb. We apologize, but we can't send attachments bigger than 10Mb, please generate a smaller export for attachment, then try again";
				$data['action'] = 'retry';
				$json['json'] = $data;
				$this->load->view('template/ajax2',$json);
				return true;
			}

		if (!$s) show_404();

		$url = "http://".config_item('export_url')."/grab/email/".md5($user['id'])."/$s[p_id]/$email";

		$fn = $s['file'].".".$s['format'];

		if ($exf==2)
			{
				$url .= "/zip";
				$fn .= ".zip";
			}

		$rp = @file_get_contents($url,false); // Read the file's contents

		$data['url'] = $url;

		$data['response'] = "The e-mail was successfully sent to $email with <b>$fn</b> attached.";
		$data['action'] = 'reset';
		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);
	}

	function download($seed,$pid,$zip="")
	{
		$user = $this->session->userdata('user');


		$this->load->helper('download');


		$this->load->model('Export_db');

		$s = $this->Export_db->get_export($pid);

		if (!$s) show_404();

		$u['status'] = 3;
		$u['downloaded'] = $s['downloaded']+1;
		$this->Export_db->save_export($pid,$u);

		$url = "http://".config_item('export_url')."/grab/direct/".md5($user['id'])."/$s[p_id]/$zip";

		$data = @file_get_contents($url,false); // Read the file's contents

		$filename = "$s[file].$s[format]";

		if ($zip)
			$filename .= ".zip";

		force_download($filename, $data);

		echo $filename;

	}

	function get_items()
	{

		$user = $this->session->userdata('user');
		$this->load->helper('array');

		$page = $this->input->post('page');
		$rp = $this->input->post('rp');
		$id = $user['id'];

		$this->load->model('Export_db');

		$rs = $this->Export_db->get_exports($id,$page,$rp);

		$data['total'] = $rs['total'];
		$data['page'] = $rs['page'];
		$data['rows'] = array();

		$this->load->helper('str2');

		foreach ($rs['db'] as $row)
			{
			$rid = $row['p_id'];
			$cell = array();


			switch ($row['status'])
				{
				case 0:
				case 1:
					$cell[] = "<a href='#' onclick='$(\"#progcancel\").trigger(\"click\"); $(\"#ehistory\").flexigrid(ehis).flexReload(); return false;' class='q-icon dbutton'> Cancel</a>";
					break;

				case 2:
				case 3:
					$dlink = site_url("export/download/".time()."/$row[p_id]");

					$fz1 = fsize($row['fz1']);
					$fz2 = fsize($row['fz2']);

					$cell[] = "Download: <a href='$dlink' class='$row[format] dbutton' >$fz1</a> <a href='$dlink/zip' class='zip dbutton' >$fz2</a> <a href='#' onclick='emailTo(".$row['p_id']."); return false;' class='emailto dbutton' >Email</a>";

					break;

				case 11:

					$cell[] = "<a href='#' onclick='retryExport(".$row['p_id']."); return false;' title='Retry $row[file].$row[format]' class='retry-icon dbutton'>Retry</a>";
					break;

				default:

					$cell[] = "";

					break;

				}

			$country = $this->Export_db->getCountry(array('aid'=>$row['sid']));

			$desc  = "<span style='white-space: normal; margin: 0'>";
			$desc .= $row['qdesc']."<br /><br />";
			if ($row['status']==2||$row['status']==3) $desc .= "Filename: $row[file].$row[format] <br/>";
			if ($row['notify']) $desc .= "Notify: $row[notify_email] <br/>";
			$desc .= "Export Range: ".($row['from']+1)." - ".($row['from']+$row['to'])."<br/>";
			if($country) $desc .= "From Country: {$country} ";

			$desc .= "</span>";

			$cell[] = $desc;

			//$cell[] = $row['file'].".$row[format]";

			switch ($row['status'])
				{
				case '0': $stat = 'In Queue'; break;
				case '1': $stat = 'In Progress with '.$row['percent'].'%'; break;
				case '2':
				case '3': $stat = 'Completed'; break;
				case '11': $stat = 'Cancelled'; break;
				default:
					$stat = "";
					break;
				}


			$cell[] = fwrap($stat." after ".toTime($row['ltime']-$row['extime']));

			$cell[] = fwrap(date("M d, Y ",$row['extime'])."<br />".date("h:i a",$row['extime'])." EST");


			$data['rows'][] = array(
						'id'=>$rid
						,'cell'=>$cell
					);
			}

		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);

	}

	function addcredits()
	{

		$user = $this->session->userdata('user');
		$paytype = $user['paytype'];

		$this->load->model('Users_db');

		$usercard['clientid'] = $user['id'];
		$usercard['onlitle'] = 1;
		$usercard['active'] = 1;
		$card = $this->Users_db->getLatestCard($usercard);

		$this->load->library('email');
		$config['mailtype'] = 'html';
		$this->email->initialize($config);	

		if ($card)
		{
			$amount = "250";
			$valid = $this->processlitle($card,$amount);

			if (!isset($valid['code']))
			{
				//email IG
				$this->email->from('info@importgenius.com', 'ImportGenius Team');
				$this->email->to('lovely@codeninja.co');

				$this->email->subject('Additional Export Credit - Declined');
				$message = $this->load->view('email/exportrequestdecline',$user,true);
				$this->email->message($message);
				
				$this->email->send();

				$data['response'] = 0;
			}
			else 
			{


				if ($valid['code']=='000')
				{
					
					$current = $this->Users_db->getUser(array('id'=>$user['id']));
					if ($current)
					{
						$user_export['addon_export'] = $current['addon_export'] + 20000;				
						$this->Users_db->updateUser($user_export, array('id'=>$user['id']));

						$addonexport = $user_export['addon_export'];
						$this->session->set_userdata('addonexport',$addonexport);

						//save logs
						$log['tran_id'] = $user['id'];
						$log['tran_no'] = $user['username'];
						$log['module_name'] = 'client';
						$log['event_name'] = "+20,000 export credits";
						$log['salesperson'] = $current['salesperson'];
		                $log['status'] = 1;
		                $log['amount'] = "250";
		                $log['utype'] = $current['utype'];
		                $log['terms'] = $current['terms'];
		                $log['payment_method'] = '2';
		                $log['closed_date'] = $current['dateclosed'];
		                $log['payment_schedule'] = $current['payschedule'];
						$this->Users_db->savelog($log);

					}

					//email IG
					$this->email->from('info@importgenius.com', 'ImportGenius Team');
					if (S_ID == 'aws')
					{
						$this->email->to('info@importgenius.com');
						$this->email->subject('Additional Export Credit - Approved');
					}
					else
					{
						$this->email->to('lovely@codeninja.co, rgustafson@importgenius.com');
						$this->email->subject('[Test] Additional Export Credit - Approved');
					}
					
					$message = $this->load->view('email/exportrequestcharge',$user,true);
					$this->email->message($message);
					
					$this->email->send();


					//email user
					$this->email->from('info@importgenius.com', 'ImportGenius Team');
					if (S_ID == 'aws')
					{
						$this->email->to($user['email']);
						$this->email->subject('ImportGenius - Additional Export Credit');
					}
					else
					{
						$this->email->to('lovely@codeninja.co, rgustafson@importgenius.com');
						$this->email->subject('[Test] ImportGenius - Additional Export Credit');
					}
					
					$message = $this->load->view('email/exportrequestchargeuser',$user,true);
					$this->email->message($message);
					
					$this->email->send();

					$data['response'] = 1;
					$data['export'] = $addonexport;
				}
				else
				{
					//email IG
					$this->email->from('info@importgenius.com', 'ImportGenius Team');
					if (S_ID == 'aws')
					{
						$this->email->to('info@importgenius.com');
						$this->email->subject('Additional Export Credit - Declined');
					}
					else
					{
						$this->email->to('lovely@codeninja.co, rgustafson@importgenius.com');
						$this->email->subject('[Test] Additional Export Credit - Declined');
					}
					
					$message = $this->load->view('email/exportrequestdecline',$user,true);
					$this->email->message($message);
					
					$this->email->send();

					$data['response'] = 0;
				}
			}
		}
		else
		{
			//email IG
			$this->email->from('info@importgenius.com', 'ImportGenius Team');

			if (S_ID == 'aws')
			{
				$this->email->to('info@importgenius.com');
				$this->email->subject('Additional Export Credit - Request');
			}
			else
			{
				$this->email->to('lovely@codeninja.co, rgustafson@importgenius.com');
				$this->email->subject('[Test] Additional Export Credit - Request');
			}
			
			$message = $this->load->view('email/exportrequest',$user,true);
			$this->email->message($message);
			
			$this->email->send();

			$data['response'] = 2;

		}
		


		$json['json'] = $data;
		$this->load->view('template/ajax2',$json);


	}

	function processlitle($card,$amount)
	{

		if ($card)
		{
			 return $this->Users_db->validateCard($card['cid'],$amount);

		}
	}

}
