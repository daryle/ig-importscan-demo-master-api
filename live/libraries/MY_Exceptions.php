<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Exceptions extends CI_Exceptions {

	//disable logging of 404s

	function show_404($page = '')
	{	
		$heading = "404 Page Not Found";
		$message = "The page you requested was not found.";

		//log_message('error', '404 Page Not Found --> '.$page);
		echo $this->show_error($heading, $message, 'error_404', 404);
		exit;
	}

	function log_exception($severity, $message, $filepath, $line)
	{	
		$severity = ( ! isset($this->levels[$severity])) ? $severity : $this->levels[$severity];

		$fpage = "";

		if (isset($_SERVER['SERVER_NAME'])&&isset($_SERVER['REQUEST_URI']))	
			$fpage = "http" . ((!empty($_SERVER['HTTPS'])) ? "s" : "") . "://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		
		if (strpos(" ".$message, "Cannot modify header information")) return true;
		log_message('error', 'Severity: '.$severity.'  --> '.$message. ' '.$filepath.' '.$line.' '.$fpage, TRUE);
	}

	function show_error($heading, $message, $template = 'error_general', $status_code = 503)
	{
		set_status_header($status_code);
		
		$message = '<p>'.implode('</p><p>', ( ! is_array($message)) ? array($message) : $message).'</p>';

		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();	
		}
		ob_start();
		include(APPPATH.'errors/'.$template.EXT);
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}


}