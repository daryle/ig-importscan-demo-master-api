<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2009, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * Input Class
 *
 * Pre-processes global input data for security
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Input
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/input.html
 */
class MY_Input extends CI_Input {

	function MY_Input() {
		parent::CI_Input();
	}

	/**
	 * Fetch the IP Address
	 *
	 * @access	public
	 * @return	string
	 */
	function ip_address() {
		if ($this->ip_address !== FALSE) {
			return $this->ip_address;
		}

		if (config_item('proxy_ips') != '' && $this->server('HTTP_X_FORWARDED_FOR') && $this->server('REMOTE_ADDR')) {
			$proxies = preg_split('/[\s,]/', config_item('proxy_ips'), -1, PREG_SPLIT_NO_EMPTY);
			$proxies = is_array($proxies) ? $proxies : array($proxies);

			$this->ip_address = in_array($_SERVER['REMOTE_ADDR'], $proxies) ? $this->input->ip_address() : $_SERVER['REMOTE_ADDR'];
		} elseif ($this->server('HTTP_X_FORWARDED_FOR')) {
			$this->ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif ($this->server('REMOTE_ADDR') AND $this->server('HTTP_CLIENT_IP')) {
			$this->ip_address = $_SERVER['HTTP_CLIENT_IP'];
		} elseif ($this->server('REMOTE_ADDR')) {
			$this->ip_address = $_SERVER['REMOTE_ADDR'];
		} elseif ($this->server('HTTP_CLIENT_IP')) {
			$this->ip_address = $_SERVER['HTTP_CLIENT_IP'];
		}

		if ($this->ip_address === FALSE) {
			$this->ip_address = '0.0.0.0';
			return $this->ip_address;
		}

//		if (strstr($this->ip_address, ',')) {
//			$x = explode(',', $this->ip_address);
//			$this->ip_address = trim(end($x));
//		}

		$this->ip_address = $this->sanitize_remote_host($this->ip_address);
		if (!$this->valid_ip($this->ip_address)) {
			$this->ip_address = '0.0.0.0';
		}

		return $this->ip_address;
	}

	function sanitize_remote_host($host) {
		if (trim($host) != '') {
			if (strpos($host, ',') !== FALSE) {
				$hosts = array();
				$hosts = explode(',', $host);
				if (is_array($hosts) && count($hosts) > 0) {
					$host = trim($hosts[0]);
				}
			}
		}
		return $host;
	}

}

// END Input class

/* End of file Input.php */
/* Location: ./system/libraries/Input.php */