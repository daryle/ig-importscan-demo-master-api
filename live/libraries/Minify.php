<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Minify {

	var $CI;
 	var $lang;
 	var $version;
 	var $browser;
 	var $css3;
 	var $ssl;
	
	var $minify_mode;

	function Minify()
	{
		$this->CI =& get_instance();
		$this->CI->load->helper('cookie');
		$this->minify_mode = get_cookie('minify_mode');
		
		$this->minify_mode = ($this->minify_mode == 'debugninja_12') ? TRUE : FALSE;
	}

	public function js($js)
	{
		require_once('Minify_JS.php');
		$min = new JSMin();

		if($this->minify_mode)
		{
			return $js;
		}
		
		return $min->minify($js);
	}

 	public function css($css,$options = array())
 	{
		require_once('Minify_CSS.php');
 		$min = new Minify_CSS();

		return $min->minify($css,$options);
 	}


 	public function html($html = false)
 	{


		//return $html;

//		require_once('Minify_HTML.php');
// 		$min = new Minify_HTML();

 		if (!$html)
 			{
 			$exp = $this->CI->config->item('html_expires');
 			if (!$exp) $exp = 0;

 			$html = $this->CI->output->get_output();
 			$this->set_output('text/html',$html,0,$exp);
// 			$this->set_output('text/html',$min->minify($html),0,$exp);
 			return true;
 			}

 		return $min->minify($html);
 	}

 	function greater($x,$y)
 	{
 		if ($x>$y)
 			return $x;
 		else
 			return $y;
 	}

 	function initialize($lang=false,$browser=false,$version=false,$css3=false,$ssl=false)
 	{


 		if (!$lang)
		 	$this->lang = $this->CI->config->item('language');
		 else
		 	$this->lang = $lang;

	 	if (!$browser)
			$this->browser = strtolower($this->CI->agent->browser());
		else
			$this->browser = $browser;

		if ($this->browser == "internetexplorer") $this->browser = "ie";
		if ($this->browser == "internet explorer") $this->browser = "ie";

		if (!$version)
			$this->version = floor($this->CI->agent->version());
		else
			$this->version = $version;


		if ($css3===false)
			$this->css3 = $this->CI->agent->css3();
		else
			$this->css3 = $css3;


		if ($ssl!=false)
			$this->ssl = true;

		return array($lang,$browser,$version,$css3);

 	}

 	function gfiles($type,$id)
	{

		if (!$this->lang) $this->initialize();


		$rpath = $this->CI->config->item('resource_folder');
		$files = $this->CI->config->item($type);
		$lang = $this->lang;

		if (!isset($files[$id])) show_error("Resource $id invalid.");

		$files = $files[$id];

		$url = $this->CI->config->item('static_url');
		
		if ($this->ssl) $url = str_replace('http', 'https', $url);
		if ($this->ssl) $url = str_replace('httpss', 'https', $url);

		$browser = $this->browser;

		$version = $this->version;

		$rfiles = array();

		if (!$files) show_error("Resource $id invalid.");

		foreach ($files as $f)
		{

			//handle remote files
			if (substr($f['file'],0,4)=='http')
				{
				$rfiles[] = array('file'=>$f['file']);
				continue;
				}

			//load default
			$target = $rpath.$f['path'].$f['file'];
			$upath = $url.$rpath.$f['path'];

			if (file_exists($target)) $rfiles[] = array('path'=>$upath,'file'=>$target);

			//load brower specific
			$target = str_replace(".$type",".".$browser.".".$type,$target);

			if (file_exists($target)) $rfiles[] = array('path'=>$upath,'file'=>$target);

			//load browser and version specific
			$target = str_replace(".$type",".".$version.".".$type,$target);

			if (file_exists($target)) $rfiles[] = array('path'=>$upath,'file'=>$target);

			//load language specific
			$target = $rpath.$f['path'].$f['file'];
			$target = str_replace(".$type","_$lang.".$type,$target);

			if (file_exists($target)) $rfiles[] = array('path'=>$upath,'file'=>$target);

			//load css3 specific
			if ($this->css3 && $type =='css')
				{

				$target = $rpath.$f['path'].$f['file'];
				$target = str_replace(".$type",".css3.".$type,$target);

				if (file_exists($target)) $rfiles[] = array('path'=>$upath,'file'=>$target);

				}


		}

		return $rfiles;


	}

 	function mtime($type,$id)
	{


		$files = $this->gfiles($type,$id);



/*
		if ($type=='css')
			{
			echo "<pre>";
			print_r($files);
			exit;
			}
*/


		//print_r($files); exit;

		$ltime = 0;

		if (!$files) show_error("Resource $id invalid.");

		foreach ($files as $f)
		{

			if (isset($f['path']))
				$ltime = $this->greater($ltime,filemtime($f['file']));

		}

//		$ltime .= "/".$this->lang."/".$this->browser."/".$this->version."/".$this->css3;

		return $ltime;


	}

	function mset()
	{

		$proto = $this->CI->config->item('proto');

		$ssl = '';

		if ($proto == 'https') $ssl = '/1';

		return "/".$this->lang."/".$this->browser."/".$this->version."/".$this->css3.$ssl;
	}


	public function outputByGroup($type,$id)
	{


		$this->CI->benchmark->mark("$type-$id"."_start");

		$content = "";

		$files = $this->gfiles($type,$id);


		if (!$files) show_error("Resource $id invalid.");


		//identify unique cached file
		$m = md5(serialize($files));
		$t = $this->mtime($type,$id);

		//echo $m; exit;

		$fn = config_item("cache_path")."static/$id-$m-$t.$type";

		//if exists load
		if (file_exists($fn))
			{
			$content = file_get_contents($fn);
			}


		if ($content == "")
		{

		foreach ($files as $f)
		{

			$string = file_get_contents($f['file']);

			if ($type=='css')
				{
				$content .= $this->css($string,array('prependRelativePath'=>$f['path']));
				}
			else if ($type=='js')
				{
				$content .= $this->js($string);
				}

		} /* end for */


			//write cache

			$this->CI->load->helper('file');

			//store in a subfolder in cache to avoid long processing
			$dir = config_item("cache_path")."static";

			//check if folder exists make if not
			if (!is_dir($dir))
				mkdir($dir,0777);

			//delete previous versions
			foreach (glob("$dir/$id-$m-*.$type") as $fd) unlink($fd);

			write_file($fn,$content);



		} /* end if */

		$this->CI->benchmark->mark("$type-$id"."_end");

		//Display benchmark
		//$content = "/* $fn ".$this->CI->benchmark->elapsed_time()." ".$this->CI->benchmark->memory_usage()." */ ".$content;

		if ($type=='css')
			{
			$this->set_output("text/".$type,$content);
			}
		else if ($type=='js')
			{
			$this->set_output("text/javascript",$content);
			}

	}


	private function set_output($type="",$content="",$time = false,$expires = false)
	{

		if (!$time) $time = time();

		$last_modified_time = $time;
		$etag = md5($content);

		$this->CI->output->set_header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
		$this->CI->output->set_header("Etag: $etag");

		if ($expires===false)
			$expires = $this->CI->config->item('expires');

		$this->CI->output->set_header("Content-type: $type; charset=utf-8");
		$this->CI->output->set_header("Pragma: public");
		$this->CI->output->set_header("Cache-Control: maxage=".$expires);
		$this->CI->output->set_header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');
		$this->CI->output->set_output($content);

		//return true;
	}

}

function mtime($type,$id)
{


	$min = new Minify();

	return $min->mtime($type,$id).$min->mset();

}

function minifyCSS($css,$options = array())
{

	$min = new Minify();

	return $min->css($css,$options);
}

function minifyJS($js)
{

		if($this->minify_mode)
		{
			return $js;
		}

		$min = new Minify();

		return $min->js($js);
}


?>
