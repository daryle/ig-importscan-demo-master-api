<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2009, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * Loader Class
 *
 * Loads views and files
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @author		ExpressionEngine Dev Team
 * @category	Loader
 * @link		http://codeigniter.com/user_guide/libraries/loader.html
 */
class MY_Loader extends CI_Loader {

	function view($view, $vars = array(), $return = FALSE) {


		//if language alternative available load that;
		$lang_view = $this->_ci_view_path . "lang/" . config_item('language') . "/" . "$view" . EXT;
		if (file_exists($lang_view))
			$view = "lang/" . config_item('language') . "/" . $view;

		//add a-b tester loader here
		//print_r($view); exit;

		return $this->_ci_load(array('_ci_view' => $view, '_ci_vars' => $this->_ci_object_to_array($vars), '_ci_return' => $return));
	}

	/**
	 * Database Loader
	 *
	 * @access    public
	 * @param    string    the DB credentials
	 * @param    bool    whether to return the DB object
	 * @param    bool    whether to enable active record (this allows us to override the config setting)
	 * @return    object
	 */
	function database($params = '', $return = FALSE, $active_record = NULL) {
		// Grab the super object
		$CI = & get_instance();

		// Do we even need to load the database class?
		if (class_exists('CI_DB') AND $return == FALSE AND $active_record == NULL AND isset($CI->db) AND is_object($CI->db)) {
			return FALSE;
		}

		require_once(BASEPATH . 'database/DB' . EXT);

		// Load the DB class
		$db = & DB($params, $active_record);

		$my_driver = config_item('subclass_prefix') . 'DB_' . $db->dbdriver . '_driver';
		$my_driver_file = APPPATH . 'libraries/' . $my_driver . EXT;

		if (file_exists($my_driver_file)) {
			require_once($my_driver_file);
			$db = new $my_driver(get_object_vars($db));
		}

		if ($return === TRUE) {
			return $db;
		}

		// Initialize the db variable.  Needed to prevent
		// reference errors with some configurations
		$CI->db = '';
		$CI->db = $db;
	}

	// --------------------------------------------------------------------
}