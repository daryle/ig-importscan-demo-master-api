<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2009, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * CodeIgniter Profiler Class
 *
 * This class enables you to display benchmark, query, and other data
 * in order to help with debugging and optimization.
 *
 * Note: At some point it would be good to move all the HTML in this class
 * into a set of template files in order to allow customization.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/profiling.html
 */
class MY_Profiler extends CI_Profiler {

	/**
	 * Compile Queries
	 *
	 * @access	private
	 * @return	string
	 */
	function _compile_queries() {
		if ($GLOBALS['SQL_BENCHMARKS']) {
			// Load the text helper so we can highlight the SQL
			$this->CI->load->helper('text');

			// Key words we want bolded
			$highlight = array('SELECT', 'DISTINCT', 'FROM', 'WHERE', 'AND', 'LEFT&nbsp;JOIN', 'ORDER&nbsp;BY', 'GROUP&nbsp;BY', 'LIMIT', 'INSERT', 'INTO', 'VALUES', 'UPDATE', 'OR', 'HAVING', 'OFFSET', 'NOT&nbsp;IN', 'IN', 'LIKE', 'NOT&nbsp;LIKE', 'COUNT', 'MAX', 'MIN', 'ON', 'AS', 'AVG', 'SUM', '(', ')');

			$output = "\n\n";
			$output .= '<fieldset style="border:1px solid #0000FF;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
			$output .= "\n";
			$output .= '<legend style="color:#0000FF;">&nbsp;&nbsp;' . $this->CI->lang->line('profiler_queries') . ': ' . count($GLOBALS['SQL_BENCHMARKS']) . '&nbsp;&nbsp;&nbsp;</legend>';
			$output .= "\n";
			$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";

			if (!isset($GLOBALS['SQL_BENCHMARKS']) || count($GLOBALS['SQL_BENCHMARKS']) == 0) {
				$output .= "<tr><td width='100%' style='color:#0000FF;font-weight:normal;background-color:#eee;'>" . $this->CI->lang->line('profiler_no_queries') . "</td></tr>\n";
			} else {
				$output .= "\n\n<tr><td>Host</td><td>Database</td><td>Elapsed</td><td>SQL</td></tr>";
				$elapsed_total = 0;
				foreach ($GLOBALS['SQL_BENCHMARKS'] as $benchmark) {
					$elapsed = number_format($benchmark['elapsed'], 4);
					$sql = trim($benchmark['sql']);
					$sql = preg_replace('/[\s]+/',' ',$sql);
					$sql = highlight_code($sql, ENT_QUOTES);
					$db = $benchmark['database'];
					$host = $benchmark['host'];
					$benchmark = number_format($benchmark['benchmark'], 4);
					$elapsed_total += $elapsed;
					foreach ($highlight as $bold) {
						$sql = str_replace($bold, '<strong>' . $bold . '</strong>', $sql);
					}

					$output .= "<tr>
							<td width='5%' valign='top' style='color:#990000;font-weight:normal;background-color:#ddd;'>" . $host . "&nbsp;&nbsp;</td>
							<td width='5%' valign='top' style='color:#990000;font-weight:normal;background-color:#ddd;'>" . $db . "&nbsp;&nbsp;</td>
							<td width='5%' valign='top' style='color:#990000;font-weight:normal;background-color:#ddd;'>" . $elapsed . "&nbsp;&nbsp;</td>
							<td width='85%' style='width:85%; color:#000;font-weight:normal;background-color:#ddd; word-wrap:break-word;'>" . $sql . "</td>
						</tr>\n";
				}
				$output .= "<tr>
						<td width='5%' valign='top' style='color:#990000;font-weight:normal;background-color:#ddd;' colspan=2>Total Time</td>
						<td width='5%' valign='top' style='color:#990000;font-weight:normal;background-color:#ddd;'><strong>" . $elapsed_total . "</strong>&nbsp;&nbsp;</td>
						<td>&nbsp;</td>
					</tr>\n";
			}

			$output .= "</table>\n";
			$output .= "</fieldset>";
			return $output;
		} else {
			$output = "\n\n";
			$output .= '<fieldset style="border:1px solid #0000FF;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
			$output .= "\n";
			$output .= '<legend style="color:#0000FF;">&nbsp;&nbsp;' . $this->CI->lang->line('profiler_queries') . '&nbsp;&nbsp;</legend>';
			$output .= "\n";
			$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";
			$output .="<tr><td width='100%' style='color:#0000FF;font-weight:normal;background-color:#eee;'>" . $this->CI->lang->line('profiler_no_db') . "</td></tr>\n";
			$output .= "</table>\n";
			$output .= "</fieldset>";

			return $output;
		}
//
//
//		die();
//		echo '<pre>';
//		print_r($GLOBALS['SQL_BENCHMARKS']);
//		die();
//		$dbs = array();
//
//		// Let's determine which databases are currently connected to
//		foreach (get_object_vars($this->CI) as $CI_object) {
//			if (is_object($CI_object) && is_subclass_of(get_class($CI_object), 'CI_DB')) {
//				$dbs[] = $CI_object;
//			}
//		}
//
//		if (count($dbs) == 0) {
//			$output = "\n\n";
//			$output .= '<fieldset style="border:1px solid #0000FF;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
//			$output .= "\n";
//			$output .= '<legend style="color:#0000FF;">&nbsp;&nbsp;' . $this->CI->lang->line('profiler_queries') . '&nbsp;&nbsp;</legend>';
//			$output .= "\n";
//			$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";
//			$output .="<tr><td width='100%' style='color:#0000FF;font-weight:normal;background-color:#eee;'>" . $this->CI->lang->line('profiler_no_db') . "</td></tr>\n";
//			$output .= "</table>\n";
//			$output .= "</fieldset>";
//
//			return $output;
//		}
//
//		// Load the text helper so we can highlight the SQL
//		$this->CI->load->helper('text');
//
//		// Key words we want bolded
//		$highlight = array('SELECT', 'DISTINCT', 'FROM', 'WHERE', 'AND', 'LEFT&nbsp;JOIN', 'ORDER&nbsp;BY', 'GROUP&nbsp;BY', 'LIMIT', 'INSERT', 'INTO', 'VALUES', 'UPDATE', 'OR', 'HAVING', 'OFFSET', 'NOT&nbsp;IN', 'IN', 'LIKE', 'NOT&nbsp;LIKE', 'COUNT', 'MAX', 'MIN', 'ON', 'AS', 'AVG', 'SUM', '(', ')');
//
//		$output = "\n\n";
//
//		foreach ($dbs as $db) {
//			$output .= '<fieldset style="border:1px solid #0000FF;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
//			$output .= "\n";
//			$output .= '<legend style="color:#0000FF;">&nbsp;&nbsp;' . $this->CI->lang->line('profiler_database') . ':&nbsp; ' . $db->database . '&nbsp;&nbsp;&nbsp;' . $this->CI->lang->line('profiler_queries') . ': ' . count($this->CI->db->queries) . '&nbsp;&nbsp;&nbsp;</legend>';
//			$output .= "\n";
//			$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";
//
//			if (count($db->queries) == 0) {
//				$output .= "<tr><td width='100%' style='color:#0000FF;font-weight:normal;background-color:#eee;'>" . $this->CI->lang->line('profiler_no_queries') . "</td></tr>\n";
//			} else {
//				foreach ($db->queries as $key => $val) {
//					$time = number_format($db->query_times[$key], 4);
//
//					$val = highlight_code($val, ENT_QUOTES);
//
//					foreach ($highlight as $bold) {
//						$val = str_replace($bold, '<strong>' . $bold . '</strong>', $val);
//					}
//
//					$output .= "<tr><td width='1%' valign='top' style='color:#990000;font-weight:normal;background-color:#ddd;'>" . $time . "&nbsp;&nbsp;</td><td style='color:#000;font-weight:normal;background-color:#ddd;'>" . $val . "</td></tr>\n";
//				}
//			}
//
//			$output .= "</table>\n";
//			$output .= "</fieldset>";
//		}
//
//		return $output;
	}

}

// END CI_Profiler class

/* End of file Profiler.php */
/* Location: ./system/libraries/Profiler.php */
