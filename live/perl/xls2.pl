use Benchmark;
use Sphinx::Search;
use Data::Dumper;
use Time::Local;
use Mysql;
use PHP::Serialization qw(serialize unserialize);
use Spreadsheet::WriteExcel;
use File::Copy;
use Amazon::S3;
use File::MimeInfo;
use Net::Address::IP::Local;

require "dbconf.pl";

#defautl config
my $limit = 1;
my $server = "127.0.0.1";
my $port = 9935;
my $start = 0;
my $end = 100000;
my $fn = 'shipmentdetail';
my $format = 'xls';
my $logfile = false;
my $stat = false;
my $country		= $ARGV[5];
my $datatype	= $ARGV[6];
my $dfilter 	= 'adate';
my $select_flds = "";
my $fres_fields = ();
my $fres_display = ();

my $address      = Net::Address::IP::Local->public;

if ( $address eq '184.107.58.136' )
{
	$address = "localhost";
}
else
{
	$address = '144.202.219.10';
}

if($country eq "us")
{
	$port 		= 9930;
	$dfilter 	= 'actdate';
}

#start benchmark
$t0 = Benchmark->new;

if($country eq "us")
{
	#open igdata db
	$igdb = Mysql->connect('localhost', 'igdata','import_genius','supergenius');
	$igdb->selectdb('igdata');

}
else
{
	#open igdata db
	$igdb = Mysql->connect($db_host, 'iglatindata',$db_user,$db_password);
	$igdb->selectdb('iglatindata');
}

$igdbr = Mysql->connect('localhost', 'igdata','import_genius','supergenius');
$igdbr->selectdb('igdata');

#open core read db
$rdb = Mysql->connect($address, 'core','core','ajnin_edoc_12');
$rdb->selectdb('core');

#open igdata write db
$igdbw = Mysql->connect($address, 'igdata','core','ajnin_edoc_12');
$igdbw->selectdb('igdata');


#get search log

#die if no log id
if ($ARGV[0] eq '') ## Search log ID (aid)
{
	die "Please specifiy search log to export";
}

if ($ARGV[1] ne '')
{
	$start = sprintf("%d",$ARGV[1]);
}

if ($ARGV[2] ne '')
{
	$end = sprintf("%d",$ARGV[2]);
}

if ($ARGV[3] ne '')
{
	$fn = $ARGV[3];
}

if ($ARGV[4] ne '')
{
	$logfile = $ARGV[4];
}


my $tfn = $fn."_".time();

$id = $ARGV[0];

print "ID: " . $id . "\n";

$rsql = "SELECT a.*, b.username FROM search_log a LEFT JOIN users b ON a.userid = b.id WHERE a.aid = $id LIMIT 1";
$rquery = $rdb->query($rsql);
@slog = $rquery->fetchrow();

#print Dumper(@slog); exit();

#create sphinx obj

my $sph 			= Sphinx::Search->new();
my $userid = $slog[1];
my $nq 				= $slog[2];
my $qsort 		= $slog[5];
my $qorder 		= $slog[6];
my $from 			= $slog[3];
my $to 				= $slog[4];
my $username 	= $slog[19];

print "UserID: " . $userid . "\n";
print "username: " . $username . "\n";

## FOR aws storing

my $aws_access_key_id     = 'AKIAIUVWB3TELNNZOS5A';
my $aws_secret_access_key = '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';

my $s3 = Amazon::S3->new
(
	{
		aws_access_key_id => $aws_access_key_id,
		aws_secret_access_key => $aws_secret_access_key,
	}
);

my $bucket = $s3->bucket('importgenius');

# check queue
my ($count, @results) = callExportCount();

my @fexportCount 	= @results;

cleanExport();

while($count > 1)
{
	sleep(5);

	my ($count, @results) = callExportCount();

	$time = $time + 5;

	print $time."\n";

	if($time >= 900)
	{
		#Compare result if moving...

		if (compareArr(@fexportCount,@results) == true)
		{
			cancelExports(@results);
			print "Export has been cancelled.";
		}

		last;
	}
}

#default fields
$sph->SetMatchMode(SPH_MATCH_EXTENDED2)
	->SetSortMode(SPH_SORT_EXTENDED,"$qsort $qorder")
	->SetServer( $server, $port )
	->SetFilterRange($dfilter, $from, $to + 86399)
	->SetLimits($start,$end,100000)
	;

#additional filters
if ($slog[15] ne '')
{
	$fr = unserialize($slog[15]);
	$c = 0;

	while ($fr->[$c])
	{
	   #print $fr->[$c]{'field'}."\n";
	   if ($fr->[$c]{'values'})
	   	{
	   	$sph->SetFilter($fr->[$c]{'field'},$fr->[$c]{'values'},$fr->[$c]{'exclude'});
	   	}
	   if ($fr->[$c]{'from'})
	   	{
	   	$sph->SetFilterRange($fr->[$c]{'field'},$fr->[$c]{'from'},$fr->[$c]{'to'},$fr->[$c]{'exclude'});
		}

	   $c += 1;
	}

}

#get results
if($country eq "us")
{
	$results = $sph->Query($nq,"sfields");
}
else
{
	if(($datatype eq "") || ($datatype eq "im"))
	{
		$results = $sph->Query($nq,"sfields_".$country);
	}
	else
	{
		$results = $sph->Query($nq,"sfields_".$country."_".$datatype);
	}
}


if($country ne "us")
{
	my $expfield_sql = "SELECT export_fields FROM export_status WHERE p_id = '$logfile' LIMIT 1";
	my $expfield_query = $igdbw->query($expfield_sql);
	my @exfrow = $expfield_query->fetchrow();
	my $export_fields = $exfrow[0];
	my $xfields = "";

	if($export_fields)
	{
		$xfields = "AND field_name IN ($export_fields)";
	}

	$fsql = "SELECT field_name,display_name FROM country_fields2 where sort_order_$country != 0 $xfields ORDER BY sort_order_$country ASC";

	$fquery = $igdb->query($fsql);

	while(@frow = $fquery->fetchrow())
	{
		if($frow[0] ne 'entryid')
		{
			if((grep $_ eq $frow[0],@fres_fields) == false)
			{


				if($datatype eq "ex")
				{
					if($frow[0] eq "countries_name")
					{
						$frow[0] = "destination";
					}
				}

				push(@fres_fields,$frow[0]);
			}

			if((grep $_ eq $frow[1],@fres_display) == false)
			{
				push(@fres_display,uc($frow[1]));
			}
		}
	}

	$select_flds = join(', ',@fres_fields)." ";

}


if($country eq "us")
{
		$ctable = " igalldata ";
		$join = "LEFT JOIN inbondcodes ON igalldata.inboundentrytype = inbondcodes.ib_id
				LEFT JOIN usports ON igalldata.usdisport = usports.portid
				LEFT JOIN carrier_codes_2 ON igalldata.carriercode = carrier_codes_2.scac ";

		$select = "
				shipname, shipaddr
				,consname, consaddr, igalldata.zipcode
				,ntfyname, ntfyaddr
				,billofladingnbr
				,actdate
				,weight
				,weightunit
				,fport
				,uport
				,manifestqty,manifestunit
				,measurement,measurementunit
				,countryname
				,vesselname
				,containernum
				,con_num_count
				,product
				,marks
				,countryoforigin
				,portname
				,masterbillofladingind
				,masterbilloflading
				,carriercode,company_name
				,address,city,state, carrier_codes_2.czipcode as czip
				,placereceipt
				";
}
else
{
		my $dtype = "";

		if($datatype eq "ex")
		{
			$dtype = "_ex";
		}

		$ctable = " igalldata_$country".$dtype." ";

		$join = "LEFT JOIN countries c ON (c.countries_iso_code_2 = igalldata_$country$dtype.destination) ";

		$select = $select_flds;
}

my $trp = 1000;
my $tstart = 0;

my $sql = "SELECT SQL_NO_CACHE $select FROM $ctable $join
				WHERE entryid IN (";

#print $sql; exit();

if ($results->{'total_found'})
{

	#let user know queue has started

	#get status obj
	if ($logfile ne false)
	{

	$stat = getStat();

	if ($stat ne false)
		{

			$stat->{'status'} = 1;
			$stat->{'ltime'} = time();

			logStat($stat);


		}
	}

	#create files
	my $workbook  = Spreadsheet::WriteExcel->new($tfn.".$format");
	my $worksheet = $workbook->add_worksheet();
	my $date_format = $workbook->add_format(num_format => 'mm/dd/yyyy');
	my $hdr_format = $workbook->add_format(bg_color => 'black', color => 'white');
	my $url_format 	= $workbook->add_format(border_color => 'white',fg_color=>'white',color=>'blue');
	my $no_border 	= $workbook->add_format(border_color => 'white');

	$worksheet->insert_image('A1','logo.png',5,5,0.9,1);

	my $mformat = $workbook->add_format(
		border  => 0,
		valign  => 'vcenter',
		align   => 'left',
	  );
	$worksheet->write_url(0,1,'http://www.importgenius.com','ImportGenius.com');
	$worksheet->write_url(1,1,'mailto:info@importgenius.com','info@importgenius.com');
	$worksheet->write(2,1,'Toll Free: 877-915-0828',$no_border);
	$worksheet->write(3,1,'International: +1 480-745-3396',$no_border);

	$worksheet->merge_range('A1:A5', '', $mformat);

	$worksheet->set_column(0, 30,  20);
	$worksheet->set_column(4, 4,  10);
	$worksheet->set_column(8, 11,  10);
	$worksheet->set_column(13, 16,  10);
	$worksheet->set_column(20, 20,  10);
	$worksheet->set_column(25, 25,  10);
	$worksheet->set_column(27, 27,  10);
	$worksheet->set_column(31, 31,  10);

	#add header
	if($country eq "us")
	{
		@hdr = (
			"SHIPPER","SHIPPER ADDRESS"
			,"CONSIGNEE","CONSIGNEE ADDRESS","ZIPCODE"
			,"NOTIFY","NOTIFY ADDRESS"
			,"BILL OF LADING"
			,"ARRIVAL DATE"
			,"WEIGHT (LB)"
			,"WEIGHT (KG)"
			,"FOREIGN PORT"
			,"US PORT"
			,"QUANTITY"
			,"Q.UNIT"
			,"MEASUREMENT"
			,"M.UNIT"
			,"SHIP REGISTERED IN"
			,"VESSEL NAME"
			,"CONTAINER NUMBER"
			,"CONTAINER COUNT"
			,"PRODUCT DETAILS"
			,"MARKS AND NUMBERS"
			,"COUNTRY OF ORIGIN"
			,"DISTRIBUTION PORT"
			,"HOUSE vs MASTER"
			,"MASTER B/L"
			,"CARRIER CODE"
			,"CARRIER NAME"
			,"CARRIER ADDRESS"
			,"CARRIER CITY"
			,"CARRIER STATE"
			,"CARRIER ZIP"
			,"PLACE OF RECEIPT"
			);
		}
		else
		{


			@hdr = @fres_display;


		}
    my $colnum = 0;
    my $rownum = 5;
    foreach my $token (@hdr) {
        $worksheet->write($rownum, $colnum, $token, $hdr_format);
        $colnum++;
    }
    $worksheet->set_row($rownum, 20);
    $rownum++;

	#get data from mysql in shards (sleep if needed)


	while ($tstart < $end)
		{
			$tend = $tstart + $trp;

			if ($tend > $end)
				{
				$tend = $end;
				}

			$cond = "";

			for ($i = $tstart;$i < $tend;$i++)
			{
			if (!$results->{'matches'}[$i]{'doc'})
				{
				last;
				}
			if ($cond ne '')
				{
				$cond .= ",";
				}
			$cond .= $results->{'matches'}[$i]{'doc'};
			}

			if ($cond eq '')
				{
				last;
				}

			$c = $i - $tstart;

			print "$i\n";

			sleep(1);

			#slow down every 10k
			if (($i % 10000)==0)
				{
				sleep(5);
				}

			$tstart = $tend;

			$nsql = $sql;

			$nsql .= "$cond) ORDER BY FIELD(entryid,$cond) LIMIT $c";

			$nquery = $igdb->query($nsql);

			# FOR US LOOP
			if($country eq "us")
			{
				while (@nrow = $nquery->fetchrow())
				{
					$cn = 0;
					$wgt = 0;
					$lb = 0;
					$kg = 0;

					$colnum = 0;
					foreach $col (@nrow)
						{
						$cn += 1;


						#format date
						if ($cn == 9)
							{
							$worksheet->write_date_time($rownum, $colnum, $col."T00:00",$date_format);
							$colnum++;
							next;
							}


						#skip weight
						if ($cn == 10)
							{
							$wgt = $col;
							next;
							}

						$col =~ s/[\n]/ /gi;
						$col =~ s/^\s+//;
						$col =~ s/\s+$//;
						$col =~ s/\s{2,}/ /gi;
						$col =~ s/["]/'/gi;
						$col =~ s/[=]//gi;	#prevent excel to think its a formula

						#process weight
						if ($cn == 11)
							{

							#~ print "[" . $kg . "]\n";
							#~ print "[" . $lb . "]\n";

							if ($col eq 'KG' || $col eq 'K')
								{
								$kg = $wgt;
								$lb = sprintf("%.1f",($wgt * 2.2));
								}

							if ($col eq 'LB' || $col eq 'L')
								{
								$lb = $wgt;
								$kg = sprintf("%.1f",($wgt / 2.2));

								#~ print "[" . $kg . "]\n";
								#~ print "[" . $lb . "]\n";

								}

							if ($col eq 'ET' || $col eq 'E' || $col eq 'MT' || $col eq 'M')
								{
								$kg = sprintf("%.1f",($wgt * 1000));
								$lb = sprintf("%.1f",($wgt * 2205));
								}

							#~ if ($col eq 'LT' || $col eq 'L')
							if ($col eq 'LT')
								{
								$kg = sprintf("%.1f",($wgt * 1016.05));
								$lb = sprintf("%.1f",($wgt * 2240));
								}

							if ($col eq 'ST' || $col eq 'S')
								{
								$kg = sprintf("%.1f",($wgt * 907.185));
								$lb = sprintf("%.1f",($wgt * 2000));
								}

							if ($col eq 'GM' || $col eq 'G')
								{
								$kg = sprintf("%.1f",($wgt * 0.001));
								$lb = sprintf("%.1f",($wgt * 0.00220462));
								}

							if ($col eq 'OZ' || $col eq 'O')
								{
								$kg = sprintf("%.1f",($wgt * 0.0283495));
								$lb = sprintf("%.1f",($wgt * 0.0625));
								}

							#~ print "[" . $kg . "]\n";
							#~ print "[" . $lb . "]\n";


							$worksheet->write($rownum, $colnum, $lb);
							$colnum++;
							$worksheet->write($rownum, $colnum, $kg);
							$colnum++;
							next;
							}

						if ($col eq '')
							{
							$col = ' ';
							}

						#print $rownum."-$colnum $col \n";
						$worksheet->write($rownum, $colnum, $col);

						$colnum++;
						}

    				$worksheet->set_row($rownum, 15);
					$rownum++;

				}
			}
			else
			{
				while (@nrow = $nquery->fetchrow())
				{
					$cn = 0;
					$wgt = 0;
					$lb = 0;
					$kg = 0;

					$colnum = 0;
					foreach $col (@nrow)
						{
						$cn += 1;

						$col =~ s/[\n]/ /gi;
						$col =~ s/^\s+//;
						$col =~ s/\s+$//;
						$col =~ s/\s{2,}/ /gi;
						$col =~ s/["]/'/gi;
						$col =~ s/[=]//gi;	#prevent excel to think its a formula

						if ($col eq '')
							{
							$col = ' ';
							}

						#print $rownum."-$colnum $col \n";
						$worksheet->write($rownum, $colnum, $col);

						$colnum++;
						}

    				$worksheet->set_row($rownum, 15);
					$rownum++;

				}
			}

			#compute and record percentage if enabled
			if ($logfile ne false)
			{

			$stat = getStat();

			#if new job started cancel this one
			if ($stat ne false)
				{

				if ($stat->{'status'} == 11)
					{
					$workbook->close();

					print "New Job!\n";

					unlink ($tfn.".$format");
					exit;
					}

					$stat->{'percent'} = sprintf('%d',($tend / $end ) * 100);
					$stat->{'status'} = 1;
					$stat->{'ltime'} = time();

					logStat($stat);


				}
			}

		}

	$workbook->close();

	#move from tmp file
	#~ move ($tfn.".$format",$fn.".$format");

	## Place it Upload to Amazon S3 below


	my $mimetype = mimetype($tfn . ".$format");

	system("zip -j $tfn.$format.zip $tfn.$format");

	my $mimetypeZip = mimetype($tfn . ".$format.zip");

	print "/export/" . $userid . "/" . $logfile . ".$format" . "\n";
	print "/export/" . $userid . "/" . $logfile . ".$format.zip" . "\n";
	print $tfn . ".$format\n";
	print $tfn . ".$format.zip\n";


	$bucket->add_key_filename ( "/export/" . $userid . "/" . $logfile . ".$format",	$tfn . ".$format", { content_type => $mimetype, },) or die $s3->err . ": " . $s3->errstr;
	$bucket->add_key_filename ( "/export/" . $userid . "/" . $logfile . ".$format.zip",	$tfn . ".$format.zip", { content_type => $mimetypeZip, },) or die $s3->err . ": " . $s3->errstr;

	$fz1 = -s $tfn.".$format";
	$fz2 = -s $tfn.".$format.zip";

	print "File size 1: " . $fz1 . "\n";
	print "File size 2: " . $fz2 . "\n";

	## END upload to amazon s3

	#zip output

	#~ $fname = $fn.".$format";
	#~ system("rm -f \"$fname.zip\"; zip -j \"$fname.zip\" \"$fname\"");
	#~

	## Delete the export

	if ( -e $tfn.".$format" )
	{
		unlink $tfn . ".$format" or warn "Could not delete $tfn: $!\n";
		unlink $tfn . ".$format.zip" or warn "Could not delete $tfn: $!\n";
	}
}

#tell stat that your done
if ($logfile ne false)
{

#sleep(5);

$stat = getStat();

if ($stat ne false)
	{

		if ($stat->{'notify'} == 1)
			{
			system("wget -O /dev/null http://".$stat->{'server_id'}.".importgenius.com/iscan4l.php/grab/notify/".$logfile);
			}

		delete $stat->{'notify'};

		print "File size 1: " . $fz1 . "\n";
		print "File size 2: " . $fz2 . "\n";

		$stat->{'fz1'} = $fz1;
		$stat->{'fz2'} = $fz2;
		$stat->{'percent'} = 100;
		$stat->{'status'} = 2;
		$stat->{'ltime'} = time();
		logStat($stat);



		print Dumper $stat;

	}
}

print trim($nq)."\n";
print $results->{'total_found'}."-".$slog[9]."\n";

if ($results->{'error'})
	{
	print $results->{'error'}."\n";
	}

#print Dumper($results);

$t1 = Benchmark->new;
$td = timediff($t1, $t0);
print "the code took:",timestr($td),"\n";

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub logStat
{

	my $row = shift;

	my $lsql = updateSQL($logfile,$row);

	my $lquery = $igdbw->query($lsql);

}

sub getStat
{

my $lsql = "SELECT percent, extime, ltime, status, notify, notify_email, server_id, fz1, fz2, file FROM export_status WHERE p_id = '$logfile' LIMIT 1";
my $lquery = $igdbw->query($lsql);
my @lrow = $lquery->fetchrow();
my $l;

if (!@lrow)
	{
	return false;
	}

$l->{'percent'} = $lrow[0];
$l->{'extime'} = $lrow[1];
$l->{'ltime'} = $lrow[2];
$l->{'status'} = $lrow[3];
$l->{'notify'} = $lrow[4];
$l->{'notify_email'} = $lrow[5];
$l->{'server_id'} = $lrow[6];
$l->{'fz1'} = $lrow[7];
$l->{'fz2'} = $lrow[8];
$l->{'file'} = $lrow[9];

return $l;
}


sub updateSQL
{
	my ($pid,$fields) = @_;
	my $result = "UPDATE export_status SET ";
	my $sets = '';

	%h = %$fields;



	while (($key, $val) = each(%h))
	{

	if (length($sets)>0)
		{
		$sets .= ',';
		}

	$sets .= $key." = ";
	$sets .= $igdbw->quote(trim($val));
	}

	$result .= " $sets WHERE p_id = $pid  LIMIT 1;";
}


sub callExportCount
{
	my $sql = "SELECT p_id FROM export_status WHERE server_id = '$server_id' AND `status` = 1 LIMIT 0,3";

	my $query = $igdbr->query($sql);
	my $count = 0;
	my $c;
	my $results = ();

	while(@statusCount = $query->fetchrow())
	{
		if((grep $_ eq $statusCount[0],@results) == false)
		{
			push(@results,$statusCount[0]);
		}

		$count ++;
	}

	# Return two variables count and p_ids

	return ($count,@results);
}

sub cancelExports
{
	my @args = @_;

	if(!@args)
	{
		return false;
	}

	foreach $i (@args)
	{
		my $cancelSQL = "UPDATE export_status SET `status` = 11 WHERE p_id = $i LIMIT 1";

		$igdbw->query($cancelSQL);
	}
}

sub cleanExport
{
	my $cancelSQL = "UPDATE export_status SET `status` = 11 WHERE (unix_timestamp() - ltime) >= 900 AND server_id = '$server_id' AND `status` = 1";

	$igdbw->query($cancelSQL);
}

sub uniq
{
	return keys %{{map {$_ => 1} @_ }}
}

sub compareArr
{
	my @arg1 = @_;
	my @arg2 = @_;

	my $count = 0;


	foreach $i (uniq(@arg1))
	{
		if(grep $_ eq $i,uniq(@arg2))
		{
			$count ++;
		}
	}

	if($count >= 3)
	{
		return true;
	}
	else
	{
		return false;
	}
}
