<?php

/**
 * Description of edu_db
 *
 * @author Erickson Reyes
 */

/**
 * @property CI_DB_active_record $db
 * @property CI_DB_active_record $db2
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 */
class Edu_db extends Model {

	private $coredb;

	/**
	 * Class constructor
	 * function edu_db() {
	 * 	parent::Controller();
	 * }
	 */
	function Edu_db() {
		parent::Model();
		$this->coredb = $this->load->database('core', true);
	}

	/**
	 * Returns one University
	 *
	 * @param array $cond
	 * @return array
	 */
	function getEntry($cond) {
		$this->coredb->select('
			edu_id,
			edu_name,
			edu_type,
			edu_price,
			edu_banner,
			edu_days,
			edu_url,
			edu_lang,
			edu_list,
			edu_side_text,
			edu_created,
			edu_modified,
			edu_author,
			edu_publish,
			edu_width,
			edu_height
		');
		$cond['edu_publish'] = 1;
		$this->coredb->where($cond);
		$query =  $this->coredb->get('edu');
		$data = $query->row_array();
		return $data;
	}
}

?>
