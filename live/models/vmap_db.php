<?
class Vmap_db extends Model {

	var 	$db2;
    public 	$country;

    function Vmap_db()
    {
        parent::Model();
		$this->db2 = $this->load->database('bigdata', TRUE);
    }
	
	function switch_db($conn)
	{
		$this->db2 = $this->load->database('bigdata_'.$conn, TRUE);
	}
	
	function getNormal($cname,$ntype='consname',$rp=10,$start=0,$dtype="im")
	{
		
		$atype = 'shipname';
		$addr = 'shipaddr';
		$paddr = 'consaddr';

		if ($ntype=='shipname') 
			{
			$atype = 'consname';
			$addr = 'consaddr';
			$paddr = 'shipaddr';
			}
		
		$ptype = $ntype;	
		$ntype = $ntype."_n";
		
		$query = "@vr_$ntype"." \"$cname\" ";
		
		$result = $this->getVR($query,$start,$rp,0,0,"vr_$atype"."_n as id, vr_$atype as name, vr_$ptype as parent, vr_$paddr as parentaddress, vr_$addr as address",$dtype);

		

		$data['total'] = $result['total'];
		$data['time'] = $result['time'];
		
		if (!isset($result['result'][0])) return false;
		
		$data['parent'] = $result['result'][0]["parent"];
		$data['parentaddress'] = $result['result'][0]["parentaddress"];
		
		$db = $result['result'];

		for ($x=0;$x<count($db);$x++)
		{
			
			unset($db[$x]['parent']);	
			unset($db[$x]['parentaddress']);	
				
			$db[$x]['data']['ntype'] = $atype;
			$db[$x]['data']['count'] = 1;
			$db[$x]['data']['address'] = $db[$x]['address'];
			unset($db[$x]['address']);
			$db[$x]['children'] = array();
		}
		
		$data['db'] = $db;
		
		//var_dump($data); exit;
		
		return $data;	
	
	}


    function getVR($query,$p=0,$rp=15,$sortname=false,$sortorder=false,$select=false,$dtype="im")
    {
		$country = $this->country;
		
		$this->load->helper('sphinx');
		$cl = new SphinxClient();
		
		//$start = (($p-1) * $rp);
		$start = $p;
		
		$cl->SetLimits( (int)$start,(int)$rp,999999);
		
		if ($sortname&&$sortorder)
			$cl->SetSortMode(SPH_SORT_EXTENDED,"$sortname $sortorder");
		
		if($country == "us" || $country == "")
			$port = 9930;
		else	
			$port = 9935;
			
		$cl->SetServer( "127.0.0.1", $port );
	  	$cl->SetMatchMode(SPH_MATCH_EXTENDED2);
		
		if($country == "us" || $country == "")
		{
			$table = "vmap";
		}
		else
		{
			if($dtype == "im")
			{
				$table = "vmap_".$country;
			}
			else
			{
				$table = "vmap_".$country."_ex";
			}
		}	
		//echo $table; exit();
        
		$rs = $cl->Query($query,$table);
		
		$data['query'] = $query;
		$data['error'] = false;
		$data['entries'] = 0;
		$data['total'] = 0;
		$data['time'] = 0;
		$data['result'] = false;
	
/*
		echo "<pre>";
		print_r($rs); exit;
*/


		if (isset($rs['matches']))
			{
			$data['entries'] = array_keys($rs["matches"]);
			$data['total'] = $rs['total_found'];
			$data['time'] = $rs['time'];
			
			if ($select) $this->db2->select($select);
			$this->db2->where_in('vr_id',$data['entries']);
			$this->db2->where('vr_consname_n !=','');
			$this->db2->where('vr_shipname_n !=','');

			$this->db2->ar_orderby[] = " FIELD(vr_id,".implode(',',$data['entries']).' )'; 
			
			if($country == "us")
			{
				$query = $this->db2->get('vmap_relations');
			}
			else
			{
				if($dtype == "im")
					$query = $this->db2->get('vmap_relations_'.$country);
				else
					$query = $this->db2->get('vmap_relations_'.$country.'_ex');
			}	
			$data['result'] = $query->result_array();
			
			}

    		
		return $data;
    }


}
