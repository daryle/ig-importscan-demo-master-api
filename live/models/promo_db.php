<?
class Promo_db extends Model {

    function Promo_db()
    {
        parent::Model();
    }

	function getHis($cond)
	{
		$this->db->where($cond); 
		return $this->db->get('promohistory')->row_array();			
	}

	function getPromo($cond)
	{
		$this->db->where($cond); 
		return $this->db->get('promos')->row_array();			
	}

	function save($promoid,$uid,$response)
	{
		$edb = $this->getHis(array('promoid'=>$promoid,'uid'=>$uid));
		$responses = array('None','Yes','No','Later');
		$promo = $this->getPromo(array('prid'=>$promoid));
		
		$db = array();
		
		$db['history'] = "<b>".date('Y-m-d H:i:s')."</b><div>User responded to promo - ".$responses[$response]."</div><br/><br/>";
		$db['updated'] = date('Y-m-d H:i:s');
		$db['response'] = $response;
		
		if (!$edb)
			{
			$db['uid'] = $uid;
			$db['promoid'] = $promoid;
				
				if($promo)
				{
					$db['promoname'] = $promo['prname'];
					$this->db->insert('promohistory', $db);
				}
			}
		else
			{
			$edb['history'] .= $db['history'];
			$edb['updated'] = $db['updated'];
			$edb['response'] = $db['response'];
			$this->db->where('hid', $edb['hid']);
			$this->db->update('promohistory', $edb);
			}
						
	}

}
