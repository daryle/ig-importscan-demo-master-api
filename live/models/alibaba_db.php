<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @property CI_DB_active_record $db
 * @property CI_DB_active_record $coredb
 * @property CI_DB_active_record $db2
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 */
class Alibaba_db extends Model {

    private $coredb;

    /**
     * Class constructor
     *
     */
    function Alibaba_db() {
        parent::Model();
        $this->coredb = $this->load->database('core', TRUE);
    }

    /**
     * Returns the alibaba member ID
     *
     * @param integer $userid
     * @return integer
     */
    function get_alibaba_id($userid) {
        $table = 'alibaba_accounts';
        $cond = array();
        $cond['userid'] = $userid;
        $this->coredb->where($cond);
        $query = $this->coredb->get($table, 1);

        if ($query) {
            $result = $query->row_array();
            return isset($result['aliMemberId']) ? $result['aliMemberId'] : 0;
        }
        return 0;
    }

    /**
     * Returns the alibaba never_ask_again user choice
     *
     * @param integer $userid
     * @return integer
     */
    function get_alibaba_never_ask_again($userid) {
        $table = 'alibaba_accounts';
        $cond = array();
        $cond['userid'] = $userid;
        $this->coredb->where($cond);
        $query = $this->coredb->get($table, 1);

        if ($query) {
            $result = $query->row_array();
            return isset($result['never_ask_again']) ? $result['never_ask_again'] : 0;
        }
        return 0;
    }

    /**
     * Saves an alibaba account. If $userid already exists then it will update the entry
     *
     * @param integer $userid
     * @param integer $aliMemberId
     * @param integer $never_ask_again
     * @return integer
     */
    function save_account($userid, $aliMemberId = FALSE, $never_ask_again = FALSE) {
        // Initialize variables
        $data = array();
        $cond = array();
        $table = 'alibaba_accounts';

        // Let's check if the $userid already exists
        $cond['userid'] = $userid;
        $this->coredb->reconnect();
        $this->coredb->from($table);
        $this->coredb->where($cond);

        // If record does not exist, then insert
        if ($this->coredb->count_all_results() == 0) {
            $data['userid'] = $userid;
            $data['date_created'] = date("Y-m-d h:i:s", time());

            if ($aliMemberId !== FALSE)
                $data['aliMemberId'] = $aliMemberId;
            if ($never_ask_again !== FALSE)
                $data['never_ask_again'] = $never_ask_again;

            $this->coredb->insert($table, $data);
        }
        else {
            $data['date_updated'] = date("Y-m-d h:i:s", time());

            if ($aliMemberId !== FALSE)
                $data['aliMemberId'] = $aliMemberId;
            if ($never_ask_again !== FALSE)
                $data['never_ask_again'] = $never_ask_again;

            $this->coredb->update($table, $data, $cond);
        }

        return $this->coredb->affected_rows();
    }

    /**
     * Saves an Alibaba request and response
     *
     * @param integer $id
     * @param integer $userid
     * @param integer $aliMemberId
     * @param integer $rid_rd
     * @param string $json_request
     * @param string $json_response
     * @return integer
     */
    function save_request($id, $userid, $aliMemberId = FALSE, $rid_rd = FALSE, $success = FALSE, $json_request = FALSE, $json_response = FALSE) {
        // Initialize variables
        $table = 'alibaba_requests';
        $cond = array();
        $data = array();

        // Check first if $id exists
        $cond['id'] = $id;
        $this->coredb->reconnect();
        $this->coredb->from($table);
        $this->coredb->where($cond);

        // If record does not exist, then insert
        if (intval($this->coredb->count_all_results()) === 0) {
            $data['userid'] = $userid;
            $data['date_created'] = date("Y-m-d h:i:s", time());
            $data['aliMemberId'] = $aliMemberId;
            $data['rid_rd'] = $rid_rd;
            $data['success'] = $success;
            $data['json_request'] = $json_request;
            $data['json_response'] = $json_response;
            $this->coredb->insert($table, $data);
            return $this->coredb->insert_id();
        } else {
            $data['id'] = $id;
            $data['date_updated'] = date("Y-m-d h:i:s", time());
            $data['userid'] = $userid;

            if ($aliMemberId !== FALSE)
                $data['aliMemberId'] = $aliMemberId;
            if ($userid !== FALSE)
                $data['userid'] = $userid;
            if ($rid_rd !== FALSE)
                $data['rid_rd'] = $rid_rd;
            if ($success !== FALSE)
                $data['success'] = $success;
            if ($json_request !== FALSE)
                $data['json_request'] = $json_request;
            if ($json_response !== FALSE)
                $data['json_response'] = $json_response;

            $this->coredb->update($table, $data, $cond);
            return $id;
        }
    }

    /**
     * Sends data to Alibaba
     *
     * $data parameters
     *      aliMemberId
     *      countryName
     *      firstName
     *      lastName
     *      email
     *      companyName
     *      telephone
     *
     *      rfqName
     *      rfqDetail
     *      quantity
     *      quantityUnit
     *      source
     *      ip
     *
     * Return codes
     *      aliMemberId
     *      email
     *      error_code
     *      error_message
     *      rfqDetailUrl
     *
     *
     * @param array $data
     * @return array
     */
    function send_request($data) {
        $this->config->load('alibaba');

        // Initialize variables
        $config = config_item('alibaba');
        $url = $config['url'];
        $appKey = $config['key'];
        $appSecret = $config['secret'];
        $sign_str = "";

        if ($data['userInfo']['aliMemberId'] === 0 || $data['userInfo']['aliMemberId'] === '0' || $data['userInfo']['aliMemberId'] === '') {
            $data['userInfo']['aliMemberId'] = '';
            unset($data['userInfo']['aliMemberId']);
        }

        if (trim($data['userInfo']['telephone']) === '') {
            $data['userInfo']['telephone'] = '0';
        }


        $data['rfqInfo']['quantity'] = (int) $data['rfqInfo']['quantity'];

        $json = str_replace('"', "'", json_encode($data));
        $data['json_request'] = $json;
        $apiInfo = 'param2/1/alibaba.rfq.open/rfqRequestPostOpenRemoteService/' . $appKey;
        $code_arr = array('jsoninfo' => $json);

        ksort($code_arr);

        foreach ($code_arr as $key => $val)
            $sign_str .= $key . $val;

        $sign_str = $apiInfo . $sign_str;

        $code_sign = strtoupper(bin2hex(hash_hmac("sha1", $sign_str, $appSecret, true)));

        //Generate the api request url
        $apiUrl = $url . '/' . $apiInfo;

        //Encode the json object
        $encoded_json = urlencode($json);

        //Create the request URL
        $datauest_url = $apiUrl . '?jsoninfo=' . $encoded_json . '&_aop_signature=' . $code_sign;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $datauest_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $content = trim(curl_exec($ch));
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);

        if ($curl_errno > 0) {
            $data['error_code'] = $curl_errno;
            $data['error_message'] = $curl_error;
            $data['json_response'] = '';
        } else {
            $content = stripslashes($content);

            if (strpos($content, '"') !== FALSE) {
                $content = substr($content, 1);
            }

            if (substr($content, strlen($content) - 1) === '"') {
                $content = substr($content, 0, strlen($content) - 1);
            }

            $content = trim($content);
            $data['json_response'] = $content;

            if ($this->isJson($content)) {
                $json = json_decode($content, TRUE);
                $data = array_merge($data, $json);
            }
            else {
                $data['error_code'] = 503;
                $data['message'] = $content;
                $data['error_message'] = $content;
                $data['action'] = 'retry';
            }
        }

        return $data;
    }

    private function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    function save_log($data) {
        $table = 'alibaba_log';
        $this->coredb->insert($table, $data);
    }
}
