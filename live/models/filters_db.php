<?php

class Filters_db extends Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function getFilters($cond)
	{
		$this->db->where($cond);
		$query = $this->db->get('search_filter');
		
		$result = $query->result_array();
		
		return $result;
	}
}
