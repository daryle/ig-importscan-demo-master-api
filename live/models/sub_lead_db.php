<?
class Sub_lead_db extends Model {

    	function Sub_lead_db()
    	{
        parent::Model();
    	}


	
	function getSaves($cond,$page=false,$rp=false)
	{
		$this->db->where($cond); 
		$this->db->from('sub_lead_search');
		$this->db->join('site_leads','sub_lead_search.leadid = site_leads.lid','left');
		$this->db->join('email_templates','sub_lead_search.tid = email_templates.id','left');
		$this->db->join('core_users','site_leads.salesperson = core_users.id','left');
		$data['total'] = $this->db->count_all_results();
		
		if ($page)
		{
		$start = (($page-1) * $rp);

		if ($start>$data['total']) 
			{
			$start = 0; 
			$page = 1;
			}
		}
		
		$this->db->select("sub_lead_search.*, site_leads.lid, site_leads.lstatus, site_leads.fullname, email_templates.subject, email_templates.message, CONCAT_WS(' ',core_users.firstname, core_users.lastname) as salesperson",FALSE);
		$this->db->where($cond); 
		$this->db->join('site_leads','sub_lead_search.leadid = site_leads.lid','left');
		$this->db->join('email_templates','sub_lead_search.tid = email_templates.id','left');
		$this->db->join('core_users','site_leads.salesperson = core_users.id','left');
		if ($page)  $this->db->limit($rp,$start);
		$dt =  $this->db->get('sub_lead_search');
		$data['db'] = $dt->result_array();

		
		if ($page) $data['page'] = $page;
		
		//print_r($data); exit;
				
		return $data;
	}
	

	function saveLog($suid,$stat,$result,$count=0)
	{
		$data['sub_lead_id'] = $suid;
		$data['status'] = $stat;
		$data['result'] = $result;
		$data['date'] = date('Y-m-d H:i:s');
		$data['count'] = $count;
		$this->db->insert('sub_lead_log', $data);

		return $this->db->insert_id(); 	
	}

	
	function updateSub($cond,$data)
	{
		$this->db->where($cond);
		$this->db->update('sub_lead_search',$data);

	}

	function deleteSave($data)
	{
		$this->db->where($data);
		$this->db->delete('sub_lead_search');    		
	}
    	
    	
}    	