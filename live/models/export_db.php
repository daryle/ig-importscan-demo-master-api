<?
class Export_db extends Model {

	var $dbr;
	var $dbw;
	var $db2;
	var $core;
	
	public $country;
	
    function Export_db()
    {
        parent::Model();
        
        $this->dbr = $this->load->database('igsave2', TRUE);
		$this->dbw = $this->load->database('igsave2', TRUE);
		$this->dbw2 = $this->load->database('bigdata_us', TRUE);
		$this->core = $this->load->database('core', TRUE);

		/**
		 * @author Jinggo
		 * Change Configuration of igdata save and write 
		*/
		if(config_item('static_url') == "http://kanban.importgenius.com/")
		{
			$this->dbw = $this->load->database('igsave_k', TRUE);
			$this->dbr = $this->load->database('igsave_k', TRUE);
		}
    }

	function get_export($id)
	{
			$this->dbr->where('p_id',$id);
			$dt =  $this->dbr->get('export_status');
			return $dt->row_array();
	}
	
	function get_export_md5($id)
	{
		$this->dbr->where('md5(p_id)',$id);
		$dt =  $this->dbr->get('export_status');
		return $dt->row_array();
	}

	function save_export($id,$data)
	{
		if (!$id)
		{
				$this->dbw->insert('export_status', $data);
				return $this->dbw->insert_id(); 	
		}
		else
		{
				$this->dbw->where('p_id',$id);
				$this->dbw->update('export_status',$data);
				return $id;
		}	
	}
	
	function get_latest_incomplete($id)
	{
		$this->dbr->where('uid',$id);
		$this->dbr->where('status <',3);
		$this->dbr->order_by('p_id','desc');
		$this->dbr->limit(1);
		
		$dt =  $this->dbr->get('export_status');
		
		return $dt->row_array();
	
	}

	function get_latest_complete($id)
	{
		$this->dbr->where('uid',$id);
		$this->dbr->where('status >',1);
		$this->dbr->where('status !=',11);
		$this->dbr->order_by('p_id','desc');
		$this->dbr->limit(1);
		
		$dt =  $this->dbr->get('export_status');
		
		return $dt->row_array();
	}
		
		
	function get_exports($id,$page=0,$rp=0)
	{

		$this->dbr->where('uid',$id);
		$this->dbr->from('export_status');
		$data['total'] = $this->dbr->count_all_results();


		$this->dbr->where('uid',$id);
		$this->dbr->order_by('status','asc');
		$this->dbr->order_by('p_id','desc');

		if ($page)
		{
		$start = (($page-1) * $rp);

		if ($start>$data['total']) 
			{
			$start = 0; 
			$page = 1;
			}
			
		
		$this->dbr->limit($rp,$start);	
			
		}
		
		$data['page'] = $page;
		
		$dt =  $this->dbr->get('export_status');
		
		$data['db'] = $dt->result_array();
		
		return $data;
		
	}	
	
	function getCountry($cond)
	{
		$this->core->where($cond);
		$this->core->select('co_id, s_id');
		$qt = $this->core->get('search_log');
		
		$row = $qt->row_array();
		
		if($row)
			$country = country_name($row['co_id']);
		else
			$country = "";

		return $country;
	}
	
	function getSearchLog($cond)
	{
		$this->core->where($cond);
		$this->core->select('co_id, s_id, dtype');
		$qt = $this->core->get('search_log');
		
		$row = $qt->row_array();
		
		return $row;
	}

	function get_export_limit($cond)
	{
		$this->core->where($cond);
		$this->core->limit(1);
		$this->core->order_by('id','desc');
		$qry = $this->core->get('export_limit');

		$row = $qry->row_array();

		return $row;
	}

	function put_export_limit($db)
	{
		$this->db->insert('export_limit',$db);
	}

	function getTotalExport($cond,$user)
	{
		$today = date('Y-m-d');

		$this->core->where('tran_id', $user['id']);
		$this->core->where('log_date >', '2014-01-20');
		$events = array('Upgrade', 'Change Plan');
		$this->core->where_in('event_name', $events);
		$this->core->limit(1);
		$this->core->order_by('log_id','desc');
		$qry = $this->core->get('logs');

		$row = $qry->row_array();	

		if ($row) 
		{	
			$closed_date = $row['log_date'];

			$date1 = date(strtotime($closed_date));
			$date2 = date(strtotime($today));

			$difference = $date2 - $date1;
			$months = floor($difference / 86400 / 30 );

			if ($months > 0) $exportDate = date('Y-m-d', strtotime("+$months months", strtotime($closed_date)));
			else $exportDate = $closed_date;

			//where export is greater than the date of Upgrade from logs
			$this->dbr->where('from_unixtime(ftime) >',$exportDate);

		}
		else
		{
			$closed_date = $user['dateclosed'];
			if ($closed_date=='0000-00-00') $closed_date = date('Y-m-d',strtotime($user['created']));

			$date1 = date(strtotime($closed_date));
			$date2 = date(strtotime($today));

			$difference = $date2 - $date1;
			$months = floor($difference / 86400 / 30 );

			if ($months > 0) $exportDate = date('Y-m-d', strtotime("+$months months", strtotime($closed_date)));
			else $exportDate = $closed_date;

			//where export date is based from closed date
			$this->dbr->where('date(from_unixtime(ftime)) >=',$exportDate);


		}


		$this->dbr->select('sum((`to`+`from`) -`from`) as total');
		$this->dbr->where($cond);

		$qry = $this->dbr->get('export_status');

		return $qry->row_array();

		//return $this->dbr->count_all_results();

	}
}    
