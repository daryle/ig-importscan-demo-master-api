<?php

class Splash_db extends Model
{
		private $db2;
		
		function Splash_db()
		{
				parent::Model();
				$this->db2 = $this->load->database('core',TRUE);
		}
		
		function saveSplash($db,$id)
		{
				$query = $this->db2->get_where('splash_page',array('id'=>$id));
				
				$n = $query->num_rows();
				
				if($n > 0)
				{
						$this->db2->where('id',$id);
						$this->db2->update('splash_page',$db);
				}
				else
				{
						$this->db2->insert('splash_page',$db);
				}
		}
		
		function showSplash()
		{
				$user = $this->session->userdata('user');
				$show = true;
				
				$query = $this->db->get_where('splash_page',array('id'=>$user['id']));
				
				$n = $query->num_rows();
				
				if($n > 0)
				{
						$row = $query->row_array();
						
						if($row['noshow'] == 1)
							$show = false;
				}
				
				$splash = $this->session->userdata('splash');
				
				if(!empty($splash))
					$show = false;
				
				return $show;
				
		}
}
