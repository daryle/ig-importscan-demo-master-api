<?
class Saved_search_db extends Model {

    	function Saved_search_db()
    	{
        parent::Model();
    	}


	function getSave($data)
	{
		$this->db->where($data);
		$dt =  $this->db->get('saved_search');

		return $dt->row_array();
	}

	function getSaves($cond,$page=false,$rp=false)
	{
		
		$this->db->where($cond);
		$this->db->from('saved_search');
		$this->db->join('search_log', 'search_log.aid = saved_search.sv_aid', 'left');
		$data['total'] = $this->db->count_all_results();

		if ($page)
		{
		$start = (($page-1) * $rp);

		if ($start>$data['total'])
			{
			$start = 0;
			$page = 1;
			}
		}
		
		
		$this->db->order_by('qdate','desc');
		$this->db->where($cond);
		if ($page)  $this->db->limit($rp,$start);
		$this->db->join('search_log', 'search_log.aid = saved_search.sv_aid', 'left');
		$this->db->group_by('saved_search.sv_aid');
		$dt =  $this->db->get('saved_search');
		$data['db'] = $dt->result_array();
		
		//echo $this->db->last_query();


		if ($page)
			$data['page'] = $page;
		else
			$data['page'] = 1;

		return $data;
	}

	function saveSearch($data)
	{
		$this->db->insert('saved_search', $data);
		return $this->db->insert_id();
	}

    	function deleteSave($data)
    	{
    		$this->db->where($data);
		$this->db->delete('saved_search');
    	}


}
