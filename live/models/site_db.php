<?
class Site_db extends Model {

    function Site_db()
    {
        parent::Model();
    }
    
    function getVisitor($uid)
    {

		if (!$uid) return false;
	
		$this->db->where('v_id',$uid); 
		return $this->db->get('site_visitors')->row_array();		
    
    }
    
    function getNewMessage()
    {
    		
    		$cond['m_publish'] = 1;
    		$bet = 'date(m_pubdate) <= date("'.date("Y-m-d").'")';
    		$bet .= ' AND date(m_expdate) > date("'.date("Y-m-d").'")';
    		
    		$this->db->where($cond);
    		$this->db->where($bet);
    		$row = $this->db->get('messages')->row_array();
    		
    		//print_r($this->db->last_query()); exit;
    		
    		return $row;
    		
    }
    
}    