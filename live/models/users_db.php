<?

class Users_db extends Model {

	private $db2;
	private $core;
	private $dbus;

	function Users_db() {
		parent::Model();
		$this->db2 = $this->load->database('igsave', true);
		$this->core = $this->load->database('core', true);
		$this->dbus = $this->load->database('bigdata_us', TRUE);
	}

	function getUser($data) {
		if (!$data)
			return false;

		$this->core->where($data);
		$user = $this->core->get('users')->row_array();
		return $user;

	}

	function getMUser($data, $select = false) {
		if (!$data)
			return false;

		if ($select)
			$this->db->select($select);
		$this->db->where($data);
		return $this->db->get('users')->row_array();
	}

	function getLatestCard($data) {
		$this->db->where($data);
		$this->db->orderby('cid', 'desc');
		return $this->db->get('cardtrans')->row_array();
	}

	function getActivateData($data) {
		$this->db->where($data);
		$this->db->where(array('event_name' => 'Activate'));
		$this->db->orderby('log_id', 'desc');
		return $this->db->get('logs')->row_array();
	}

	function getLogData($data) {
		$this->db->where($data);
		$this->db->orderby('log_id', 'desc');
		return $this->db->get('logs')->row_array();
	}

	function updateUser($user, $cond) {
		$this->db->where($cond);
		//if (isset($user['tempdb'])) unset($user['tempdb']);
		$this->db->update('users', $user);
	}

	function getCpStore($data) {
		$this->db->where($data);
		return $this->db->get('cp_store')->row_array();
	}

	function getCard($uname) {
		$this->db->where('username', $uname);
		$this->db->where('active', 1);
		$this->db->where('onlitle', 1);
		$this->db->orderby('cid', 'desc');
		return $this->db->get('cardtrans')->row_array();
	}

	function saveCard($db) {
		if ($db['cid'] == "") {

			$this->db->insert('cardtrans', $db);
			$cid = $this->db->insert_id();

			//encrypt cardno and seccode
			if (isset($db['cardno'])) {
				$edb['cp_key'] = md5($cid);
				$edb['cp_val'] = $this->encrypt->encode($db['cardno'], $cid);
				//$edb['cp_end'] = $this->encrypt->encode($db['seccode'],$cid);

				$this->db->insert('cp_store', $edb);

				$edata['cardno'] = "CCXXXX" . substr($db['cardno'], -4, 4);
				$this->db->where('cid', $cid);
				$this->db->update('cardtrans', $edata);
			}
		} else {
			if (strpos($db['cardno'], "*", 1))
				$db['cardno'] = $this->input->post('cardno');
			$cid = $db['cid'];
			$this->db->where(array('cid' => $cid));
			$this->db->update('cardtrans', $db);

			//encrypt cardno and seccode
			if (isset($db['cardno'])) {
				$this->db->where('cp_key', md5($cid));
				$cprow = $this->db->get('cp_store')->row_array();

				$edb = array();
				$edb['cp_val'] = $this->encrypt->encode($db['cardno'], $cid);
				//$edb['cp_end'] = $this->encrypt->encode($db['seccode'],$cid);

				if ($cprow) {
					$this->db->where('cp_key', md5($cid));
					$this->db->update('cp_store', $edb);
				} else {
					$edb['cp_key'] = md5($cid);
					$this->db->insert('cp_store', $edb);
				}

				$edata['cardno'] = "CCXXXX" . substr($db['cardno'], -4, 4);
				$this->db->where('cid', $cid);
				$this->db->update('cardtrans', $edata);
			}
		}


		if (isset($cid)) {
			if (isset($db['clientid'])) {
				$this->db->where('clientid', $db['clientid']);
				$this->db->where('onlitle', 1);
				$this->db->where('active', 1);
				$active_card = $this->db->get('cardtrans')->row_array();
				if ($active_card) {
					$arbdata['cid'] = $cid;
					$this->db->where('datecharge >=', date("Y-m-d"));
					$this->db->where('status', 0);
					$this->db->where('cid', $active_card['cid']);
					$this->db->update('arbtrans', $arbdata);

					$cardata['onlitle'] = "0";
					$cardata['active'] = "0";
					$this->db->where('clientid', $db['clientid']);
					$this->db->update('cardtrans', $cardata);

					$ucardata['onlitle'] = "1";
					$ucardata['active'] = "1";
					$this->db->where('cid', $cid);
					$this->db->update('cardtrans', $ucardata);
				}
			}

			return $cid;
		}
	}

	function dropSimilarAccounts($username) {
		$this->db->where('session_id!=', $this->session->userdata('session_id'));
		$this->db->like('session_data', "\"$username\"");
		$this->db->delete('ig_sessions');
	}

	function logSession($data) {
		$user = $this->session->userdata('user');
		if (!$user)
			return true;
		$db['user_id'] = $user['id'];
		$db['action'] = $data;
		$db['session_id'] = $this->session->userdata('session_id');
		$db['ip_address'] = $this->input->ip_address();
		if (isset($_SERVER['HTTP_USER_AGENT']))
			$db['user_agent'] = $_SERVER['HTTP_USER_AGENT'];

		$db['created'] = date('Y-m-d H:i:s');
		$this->db->insert('session_log', $db);
	}

	function getFirstSession($userid) {
		$this->db->where(array('user_id' => $userid));
		$this->db->orderby('logid', 'asc');
		$this->db->limit(1);
		return $this->db->get('session_log')->row_array();
	}

	function dropOldTables() {
		$tables = $this->db->list_tables();

		foreach ($tables as $table) {
			if (substr($table, 0, 3) == 'tmp') {
				$age = (date('U') - substr($table, 4, 10)) / 3600;
				if ($age > 24)
					$this->db->query("DROP TABLE IF EXISTS $table");
			}
		}
	}

	function getMyCountry($country = "us") {
		$user = $this->session->userdata('user');

		$this->db->where('userid', $user['id']);
		$this->db->where('astatus', 1);
		$this->db->where('cl.country_avre', $country);
		$this->db->order_by('country_name', 'asc');
		$this->db->join('country_lists cl', 'cl.country_id = ac.country_id');
		$acountry = $this->db->get('available_countries ac');

		if ($acountry->num_rows() > 0) {
			$r = 0;
		} else {
			$this->db->where('userid', $user['id']);
			$this->db->where('astatus', 1);
			$this->db->order_by('country_name', 'asc');
			$this->db->join('country_lists cl', 'cl.country_id = ac.country_id');
			$acountry = $this->db->get('available_countries ac');

			$r = $acountry->row_array();
		}



		return $r;
	}

	function getlastcountry() {
		$user = $this->session->userdata('user');

		$lastcountry = $this->db->where('userid', $user['id'])->get('country_lastused');

		$n = $lastcountry->num_rows();

		if ($n > 0) {
			$r = $lastcountry->row_array();
			$lcountry = $r['country_name'];
		} else {
			$lcountry = false;
		}

		return $lcountry;
	}

	function isAvailable($country_name) {
		$user = $this->session->userdata('user');


		$this->db2->where('country_avre', $country_name);
		$country = $this->db2->get('country_lists');

		$crow = $country->row_array();

		if (count($crow) > 0) {
			$this->db->where('country_id', $crow['country_id']);
			$this->db->where('userid', $user['id']);
			$this->db->where('astatus', 1);
			$acountry = $this->db->get('available_countries');

			$n = $acountry->num_rows();

			$flag = false;

			if ($n > 0) {
				$flag = true;

				// Record last country used
				$lastcountry = $this->db->where('userid', $user['id'])->get('country_lastused');

				if ($lastcountry->num_rows() === 0) {
					$db['userid'] = $user['id'];
					$db['country_name'] = $country_name;
					$db['updated'] = date('Y-m-d H:i:s');

					$this->db->insert('country_lastused', $db);
				} else {
					$db['country_name'] = $country_name;
					$db['updated'] = date('Y-m-d H:i:s');

					$this->db->where('userid', $user['id']);
					$this->db->update('country_lastused', $db);
				}
			}
		} else {
			$flag = false;
		}


		return $flag;
	}

	function dropMyTable() {
		$user = $this->session->userdata('user');
		if ($user) {
			$table = $user['tempdb'];
			$this->db->query("DROP TABLE IF EXISTS $table");
		}
	}

	function getArbtrans($cid) {
		$query = $this->db->order_by('arbid', 'desc')->limit(1)->get_where('arbtrans', array('cid' => $cid, 'status' => 1, 'success' => 1));

		return $query->row_array();
	}

	function addCountryToAccount($userid) {
		$contents = $this->cart->contents();


		foreach ($contents as $item) {
			if ($item['id'] == 0) {

				$allcountries = $this->db->get('country_lists')->result_array();

				foreach ($allcountries as $ac) {
					$check_country = $this->db->get_where('available_countries', array('country_id' => $item['id'], 'userid' => $userid))->num_rows();

					if ($check_country < 1) {
						$db['userid'] = $userid;
						$db['country_id'] = $ac['country_id'];
						$db['created'] = date('Y-m-d');

						$this->db->insert('available_countries', $db);
					}
				}
				break;
			} else {
				$check_country = $this->db->get_where('available_countries', array('country_id' => $item['id'], 'userid' => $userid))->num_rows();
				if ($check_country < 1) {
					$db['userid'] = $userid;
					$db['country_id'] = $item['id'];
					$db['created'] = date('Y-m-d');

					$this->db->insert('available_countries', $db);
				}
			}
		}

		return true;
	}

	function getUtypeAmount($cond) {
		$this->db->where($cond);
		$q = $this->db->get('dataset');

		return $q->row_array();
	}

	function getTimezone($cond) {
		$this->db->where($cond);
		$query = $this->db->get('timezones');

		return $query->row_array();
	}

	function save_log($user) {
		$log['user_id'] = 22;
		$log['tran_id'] = $user['id'];
		$log['module_name'] = 'reactivation';
		$log['log_date'] = date("Y-m-d H:i:s");

		/*
		  if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		  {
		  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

		  $local_ip = explode(',',$ip);

		  if(is_array($local_ip))
		  {
		  $ip = gethostbyaddr($local_ip[0]);
		  }

		  $log['machine_name'] = $ip;
		  $log['ip_address'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
		  }
		  else
		  {
		  $log['machine_name'] = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		  $log['ip_address'] = $_SERVER['REMOTE_ADDR'];
		  } */

		$log['ip_address'] = $this->input->ip_address();

		$log['event_name'] = 'Login Attempt';
		$this->db->insert('logs', $log);
	}

	function savelog($log) {
		$log['user_id'] = 22;
		$log['log_date'] = date("Y-m-d H:i:s");
		$log['ip_address'] = $this->input->ip_address();
		$this->db->insert('logs', $log);
	}

	function save_activity($db) {
		$this->db->insert('users_activities', $db);
	}

	function autoAddCountry($user) {
		$latin_utype = array('20','29');
		if (!in_array($user['utype'],$latin_utype)) {
			$cond = array(
				'userid' => $user['id'],
				'country_id' => 8
			);

			$query = $this->core->where($cond)->get('available_countries');
			$row = $query->row_array();

			if (!$row) {
				$db['userid'] = $user['id'];
				$db['country_id'] = 8;
				$db['astatus'] = 1;
				$db['date_modified'] = date('Y-m-d H:i:s');
				$db['created'] = date('Y-m-d H:i:s');

				$this->core->insert('available_countries', $db);
			} else {
				$cond = array(
					'userid' => $user['id'],
					'country_id' => 8,
					'astatus' => 0
				);
				$this->core->where($cond)->set(array('astatus' => 1))->update('available_countries');
			}
		}
	}

	function getFollowedCompanies($clientid) {
		$where['clientid'] = $clientid;
		$where['deleted'] = 0;
		$followed = $this->core->get_where('followed_companies', $where)->result_array();

		if ($followed) {
			foreach($followed as $idx => $row) {
				$ni = $this->dbus->get_where('normal_index', array('n_id' => $row['n_id']))->row_array();
				if ($ni) {
					$followed[$idx]['n_sample'] = $ni['n_sample'];
				}
				else {
					$followed[$idx]['n_sample'] = 'N/A';
				}
			}
		}
		return $followed;
	}

	function getUserCount($cond)
	{
		$this->db->where($cond);
		$this->db->from('users');
		$count = $this->db->count_all_results();

		return $count;
	}

	function getUsers($cond,$select="*")
	{
		$this->db->where($cond);
		$this->db->select($select);
		$query = $this->db->get('users');

		return $query;
	}

    /**
     * Get the current account manager
     *
     * @param int $account_manager
     * @return mixed
     */
    function get_user_account_manager_info($account_manager = 0)
    {
        $this->core->select("*");
        $this->core->from("account_manager_info");
        if (0 == $account_manager ) {
            $this->core->where('nick_name','Import Genius');
            $this->core->limit(1);
        }
        else
        {
            $this->core->where("user_id",$account_manager);
        }

        return $this->core->get()->row();
}



    function validateCard($db, $amount) {
		$this->load->library('xmlrpc');
		$user = $this->session->userdata('user');

		//s5
		//$server_url = "https://s5.importgenius.com/api.php/api_charge_cert";
		//$this->xmlrpc->server($server_url, 80);
		//live
		  if (S_ID == 'aws')
           $server_url = "http://api.importgenius.com/api.php/api_charge";
        else
           $server_url = "http://api.importgenius.com/apidev.php/api_charge";

		//$server_url = "http://api.importgenius.com/api.php/api_charge"; //return $server_url;
		$this->xmlrpc->server($server_url, 80);

		$this->xmlrpc->method('CardValidation');

		$cid = md5($db);
		$userid = 22;

		$request = array($cid, $userid, $amount);
		//$this->xmlrpc->set_debug(TRUE);
		$this->xmlrpc->request($request);		

		if (!$this->xmlrpc->send_request()) {
			return $this->xmlrpc->display_error();
			//return false;
		} else {
			return $this->xmlrpc->display_response();
		}
	}

	function getRowTypes($datavalue, $datacode) {

		$data['data_value'] = $datavalue;
		$data['data_code'] = $datacode;
		$this->db->select('data_id,data_display,description');
		$this->db->where($data);
		$query = $this->db->get('dataset');
		return $query->row_array();
	}

}
