<?
class Labs_db extends Model {

	var $db2;
	var $db3;

    function Labs_db()
    {
        parent::Model();
        $this->db2 = $this->load->database('bigdata', TRUE);
        $this->db3 = $this->load->database('igsave', TRUE);
    }
    
    function getNormal($cond)
    {
    		
    		//$this->db->select('n_id');
    		$this->db2->where($cond);
    		$query = $this->db2->get('normal_index');
    		
    		return $query->row_array();
    }
    
    function updateNormal($cond,$data)
    {
    		$this->db3->where($cond);
		$query = $this->db3->update('normal_index', $data); 
		return $query;

    }
    
    function getMaxNormal()
    {
    
    $this->db2->select_max('n_id');
    $query = $this->db2->get('normal_index');
    
    return $query->row_array();
    
    }

    function getEntry($cond)
    {
    		
    		$this->db2->where($cond);
    		$this->db2->limit(1);
    		$query = $this->db2->get('igalldata');
    		
    		return $query->row_array();
    }

    function getEntriesbyId($cond)
    {
    		
    		$this->db2->where_in('entryid',$cond);
    		$this->db2->limit(count($cond));
    		$query = $this->db2->get('igalldata');
    		
    		return $query->result_array();
    }


    function getMaxEntry()
    {
    
    $this->db2->select_max('entryid');
    $query = $this->db2->get('igalldata');
    
    return $query->row_array();
    
    }


    function insertVmap($data)
    {
    		$str = $this->db3->insert_string('vmap_relations', $data);
    		$str = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $str); 
		$query = $this->db3->query($str); 
		return true;
    }


    function getCompanies($query,$p=1,$rp=15,$sortname=false,$sortorder=false,$ntype=false,$select=false)
    {
    
		$this->load->helper('sphinx');
		$cl = new SphinxClient();
		
		$start = (($p-1) * $rp);
		
		$cl->SetLimits( (int)$start,(int)$rp,999999);
		
		if ($sortname&&$sortorder)
			$cl->SetSortMode(SPH_SORT_EXTENDED,"$sortname $sortorder");
			
		$cl->SetServer( "127.0.0.1", 9940 );
	  	$cl->SetMatchMode(SPH_MATCH_EXTENDED2);
	  	
	  	$from = 1;
	  	$to = 9999999;

		if ($ntype=="importer")
			$cl->SetFilterRange ('is_consignee',1,2);
			
		if ($ntype=="supplier")
		
			$cl->SetFilterRange ('is_shipper',1,2);


		//$cl->SetFilterRange ('n_count',(int)$from,(int)$to);
	  	
		$rs = $cl->Query($query,"normalize");
		
		$data['query'] = $query;
		$data['error'] = false;
		$data['entries'] = 0;
		$data['total'] = 0;
		$data['time'] = 0;
		$data['result'] = false;
	
/*
		echo "<pre>";
		print_r($rs); exit;
*/


		if (isset($rs['matches']))
			{
			$data['entries'] = array_keys($rs["matches"]);
			$data['total'] = $rs['total_found'];
			$data['time'] = $rs['time'];
			
			if ($select) $this->db2->select($select);
			$this->db2->where_in('n_id',$data['entries']);
			$this->db2->ar_orderby[] = " FIELD(n_id,".implode(',',$data['entries']).' )';
			$query = $this->db2->get('normal_index');
			
			$data['result'] = $query->result_array();
			
			}
			
		//print_r($data); exit;	
    		
		return $data;
    }

    function getVR($query,$p=1,$rp=15,$sortname=false,$sortorder=false,$select=false)
    {
    
		$this->load->helper('sphinx');
		$cl = new SphinxClient();
		
		$start = (($p-1) * $rp);
		
		$cl->SetLimits( (int)$start,(int)$rp,999999);
		
		if ($sortname&&$sortorder)
			$cl->SetSortMode(SPH_SORT_EXTENDED,"$sortname $sortorder");
			
		$cl->SetServer( "127.0.0.1", 9940 );
	  	$cl->SetMatchMode(SPH_MATCH_EXTENDED2);
	  	
	  	
		$rs = $cl->Query($query,"vmap");
		
		$data['query'] = $query;
		$data['error'] = false;
		$data['entries'] = 0;
		$data['total'] = 0;
		$data['time'] = 0;
		$data['result'] = false;
	
/*
		echo "<pre>";
		print_r($rs); exit;
*/


		if (isset($rs['matches']))
			{
			$data['entries'] = array_keys($rs["matches"]);
			$data['total'] = $rs['total_found'];
			$data['time'] = $rs['time'];
			
			if ($select) $this->db2->select($select);
			$this->db2->where_in('vr_id',$data['entries']);
			$this->db2->ar_orderby[] = " FIELD(vr_id,".implode(',',$data['entries']).' )';
			$query = $this->db2->get('vmap_relations');
			
			$data['result'] = $query->result_array();
			
			}
			
		//print_r($data); exit;	
    		
		return $data;
    }

    
}    