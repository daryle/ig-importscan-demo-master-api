<?php
class Shipping2 extends Model {

  public $country;

	var $db2;
	var $dbus;
	var $core;

	function Shipping2()
	{
		parent::Model();

		$this->db2 = $this->load->database('bigdata', TRUE);
		$this->dbus = $this->load->database('bigdata_us', TRUE);
		$this->core = $this->load->database('core', TRUE);

	}

	function switch_db($conn)
	{
		$this->db2 = $this->load->database('bigdata_'.$conn, TRUE);
	}

	function getCarrier($scac)
	{

		$this->dbus->where('scac', $scac);
		$data =  $this->dbus->get('carrier_codes_2');

		return $data->row_array();
	}


	function getNormal($cname,$ntype='consname',$rp=10,$start=0,$country = "us",$dtype = "im")
	{

		$atype = 'shipname';

		$addr = 'shipaddr';
		$paddr = 'consaddr';

		if ($ntype=='shipname')
			{
			$atype = 'consname';
			$addr = 'consaddr';
			$paddr = 'shipaddr';
			}

		$ptype = $ntype;
		$ntype = $ntype."_n";

		$query = "@$ntype"." ^\"$cname\"$";

		$result = $this->getEntries($query,0,0,$start,$rp,0,0,$atype."_a",false,false,false,$dtype);

		$data['total'] = $result['total_found'];
		$data['time'] = $result['time'];

		$db = false;


		if (!is_array($result['entries']))
		{
			return false;
		}


		$db = $this->getRows($result['entries'],"$atype"."_n as id,$atype as name, $addr as address, $ptype as parent, $paddr as parentaddress",' FIELD(entryid,'.implode(',',$result['entries']).')',' '," $atype"."_n != ''", $country,$dtype);

		for ($x=0;$x<count($db);$x++)
		{
			if (!isset($data['parent']))
				{
				$data['parent'] = $db[$x]['parent'];
				$data['parentaddress'] = $db[$x]['parentaddress'];
				}

			unset($db[$x]['parent']);
			unset($db[$x]['parentaddress']);

			$db[$x]['data']['ntype'] = $atype;
			$db[$x]['data']['count'] = $result['count'][$x];
			$db[$x]['data']['address'] = $db[$x]['address'];
			unset($db[$x]['address']);
			$db[$x]['children'] = array();
		}


		$data['db'] = $db;

		//print_r($data); exit;

		return $data;

	}

	function getEntry($id,$table='igalldata',$dtype = "im")
	{
		if($table == "igalldata")
		{
			$this->dbus->reconnect();
			$this->dbus->where('entryid', $id);
			$this->dbus->join('inbondcodes','igalldata.inboundentrytype=inbondcodes.ib_id','left');
			$this->dbus->join('usports','igalldata.usdisport=usports.portid','left');
			$data = $this->dbus->get('igalldata');
		}
		else
		{
			if($dtype == "im")
			{
				$this->db2->reconnect();
				$this->db2->where('entryid', $id);
				$this->db2->join('countries',"countries.countries_iso_code_2 = {$table}.destination");
				$data = $this->db2->get($table);
			}
			else
			{
				$this->db2->reconnect();
				$this->db2->where('entryid', $id);
				$this->db2->join('countries',"countries.countries_iso_code_2 = {$table}_ex.destination","left");
				$data = $this->db2->get($table."_ex");
			}
		}
		return $data->row_array();
	}

	function getEntry2($id,$country)
	{
		$countryFields = $this->countryFields($country);

		if($countryFields && $country != "in")
		{
			$this->db2->select($countryFields[1]);
		}

		$this->db2->where('entryid', $id);
		$this->db2->join('countries',"countries.countries_iso_code_2 = igalldata_{$country}.destination");
		$data = $this->db2->get("igalldata_{$country}");

		$row = $data->row_array();
		return $row;
	}

	function countryFields($country)
	{
		/*
		if($country != "ar")
		{
			$fields = $this->db2->select('field_name,display_name')->where('sort_order_'.$country.' !=',0)->order_by('sort_order_'.$country,' asc')->get('country_fields');
		}
		else
		{
			$fields = $this->db2->select('field_name,display_name')->order_by('sort_order','asc')->get('country_fields');
		}
		*/

		$this->db2->reconnect();
		$fields = $this->db2->select('field_name,display_name')->where('sort_order_'.$country.' !=',0)->order_by('sort_order_'.$country,' asc')->get('country_fields2');

		$f = array();

		$fields_array = $fields->result_array();

		if($fields_array)
		{
			foreach($fields_array as $field)
			{
				$f[] = array($field['field_name'],$field['display_name']);
			}
		}

		return $f;
	}

	function getCompany($id)
	{
		$this->db2->where('n_keyword', $id);
		$this->db2->limit(1);
		$data =  $this->db2->get('normal_index');

		return $data->row_array();
	}


    	function getRows($entries,$select=false,$sortname=false,$sortorder=false,$cond=false,$table='igalldata',$dtype="im")
    	{
    		if ($select)
    		{
    			$this->db2->select($select);
    			$this->dbus->select($select);
			}

			$this->db2->where_in('entryid',$entries);
			$this->dbus->where_in('entryid',$entries);

    		if ($cond)
    		{
				$this->db2->where($cond,NULL,false);
				$this->dbus->where($cond,NULL,false);
			}

    		if ($sortname)
			{
				if (in_array($sortname,array("inboundentrytype_s","masterbilloflading_s","masterbillofladingind_s")))
					$sortname = str_replace("_s", "", $sortname);
			}

    		if ($sortname&&$sortorder)
    		{
				$this->db2->order_by("$sortname $sortorder");
				$this->dbus->order_by("$sortname $sortorder");
			}

    		if (strpos($sortname, 'FIELD(') !== FALSE || strpos($sortname, 'FIELD (') !== FALSE)
	        {
	            $this->db2->ar_orderby[] = $sortname;
	            $this->dbus->ar_orderby[] = $sortname;
	        }

            // Removed unused links
            if($table == "igalldata")
			{
				$this->dbus->join('inbondcodes','inboundentrytype = inbondcodes.ib_id','left');
				$this->dbus->join('usports','usdisport = usports.portid','left');
				$this->dbus->join('carrier_codes_2','carriercode = carrier_codes_2.scac','left');
				$result = $this->dbus->get('igalldata',false)->result_array();
			}
			else
			{
				if($dtype == "im")
				{
					$this->db2->join('countries',"countries.countries_iso_code_2 = {$table}.destination","left");
					//$this->db2->join('countries c2',"c2.countries_iso_code_2 = {$table}.origin","left");
					//if  ($table=='igalldata_pa') $this->db2->select("{$table}.*,  countries.countries_name as countries_name");
					//else

					$this->db2->select("{$table}.*,  countries.countries_name as countries_name");

					// if($table != 'igalldata_pa')
					// {
					// 	$this->db2->select("{$table}.*,  countries.countries_name as countries_name");
					// }

					$result = $this->db2->get($table,false)->result_array();
				}
				else
				{
					$this->db2->join('countries',"countries.countries_iso_code_2 = {$table}_ex.destination","left");
					$this->db2->join('countries c2',"c2.countries_iso_code_2 = {$table}_ex.origin","left");
					//if  ($table=='igalldata_pa') $this->db2->select("{$table}_ex.*,  countries.countries_name as destination, c2.countries_name as countries_name");
					$result = $this->db2->get($table."_ex",false)->result_array();
				}
    		}

    		return $result;
    	}


	function getEntries($query,$from=0,$to=0,$start=0,$rp=15,$sortname=false,$sortorder=false,$groupby=false,$distinct=false,$groupbydate=false,$filters=false,$dtype="im")
	{
		if($to > 0) $to += 86399;

		$country = $this->country;

		$this->load->helper('sphinx');
		$cl = new SphinxClient();

		$cl->SetLimits( (int)$start,(int)$rp,100000);

		if ($sortname!='entryid'&&$sortname&&$sortorder)
			$cl->SetSortMode(SPH_SORT_EXTENDED,"$sortname $sortorder");


		if($country == "us" || $country == "")
		{
			//$ip = config_item('server1_ip');
			$ip = "127.0.0.1";
			$port = 9930;
		}
		else
		{
			$ip = "127.0.0.1";
			$port = 9935;
		}
		$cl->SetServer( $ip, $port );
		$cl->SetMatchMode(SPH_MATCH_EXTENDED2);

		if ($groupby&&!$groupbydate) $cl->SetGroupBy($groupby, SPH_GROUPBY_ATTR,'@count desc');
		if ($distinct) $cl->SetGroupDistinct ($distinct);
		if ($groupbydate) $cl->SetGroupBy($groupby, $groupbydate);



		if(empty($country)) $country = config_item('default_country');

		if($country == "us")
			$arival_date = "actdate";
		else
			$arival_date = "adate";

		if ($from&&$to) $cl->SetFilterRange ($arival_date,(int)$from,(int)$to);

		if (is_array($filters))
			{
			foreach ($filters as $f)
				{
				if (!isset($f['exclude'])) $f['exclude'] = false;
				if (isset($f['values']))
					{
					$cl->SetFilter ($f['field'],$f['values'],$f['exclude']);
					}
				else
					{
					//json_exit($f);
					$cl->SetFilterRange ($f['field'],(int)$f['from'],(int)$f['to'],$f['exclude']);
					}
				}
			}

		if($country == "us")
		{
			$rs = $cl->Query($query,"sfields");
		}
		else
		{
			if($dtype == "im")
				$rs = $cl->Query($query,"sfields_".$this->country);
			else
				$rs = $cl->Query($query,"sfields_".$this->country."_ex");
		}
		$data['error'] = false;
		$data['entries'] = 0;
		$data['total_found'] = 0;
		$data['time'] = 0;

		//echo "<pre>";
		//print_r($rs); exit;

		if ($cl->GetLastError())
		{
			$sphinx_error = $cl->GetLastError();
			log_message('error',$sphinx_error);
			$data['error'] = "Oops. Something went wrong with our database. We're looking into this now. Please try again with a shorter date range in the meantime.";
			$data['error'] = $sphinx_error.$query;
			return $data;
		}


		if (isset($rs['matches']))
			{
			$data['entries'] = array_keys($rs["matches"]);
			$data['total_found'] = $rs['total_found'];

			if ($groupby)
				{
				foreach ($rs['matches'] as $entry )
					{
					$data['count'][] = $entry['attrs']['@count'];
					}
				}

			if ($distinct)
				{
				foreach ($rs['matches'] as $entry )
					{
					$data['distinct'][] = $entry['attrs']['@distinct'];
					}
				}

			$data['time'] = $rs['time'];
			}


		return $data;
	}


	function getQuery($sid)
	{
		$this->db->reconnect();
		$this->db->where('aid', $sid);
		return $this->db->get('search_log')->row_array();
	}

	function getQueries($cond,$page=false,$rp=false,$order=false,$max=false)
	{
		$this->db->where($cond);
		$this->db->from('search_log');
		$data['total'] = $this->db->count_all_results();

		if ($data['total']>$max) $data['total'] = $max;

		if ($page)
		{
		$start = (($page-1) * $rp);

		if ($start>$data['total'])
			{
			$start = 0;
			$page = 1;
			}
		}

		if ($order) $this->db->order_by($order);
		$this->db->where($cond);
		if ($page)  $this->db->limit($rp,$start);
		$dt =  $this->db->get('search_log');
		$data['db'] = $dt->result_array();

		if ($page)
			$data['page'] = $page;
		else
			$data['page'] = 1;

		return $data;
	}

    	function saveQuery($data)
    	{
			$this->db->insert('search_log', $data);
			$aid =  $this->db->insert_id();

			$this->search_issue($aid,$data);

			return $aid;
    	}

    	function updateQuery($sid,$data)
    	{
		$this->db->where('aid', $sid);
		$this->db->update('search_log', $data);
    	}

	function totalSearchbyUserId($userid)
	{
		$this->db->where('date(qdate)',date('Y-m-d'));
		$this->db->where('userid',$userid);
		$this->db->from('search_log');

		return $this->db->count_all_results();
	}


	function getZipCodes($zipcode,$miles)
	{

		$rdb = array();

		$this->db2->where('zipcode',$zipcode);
		$start = $this->db2->get('zipcode')->row_array();

		if (!$start)
		{
		$this->db2->like('zipcode',substr($zipcode,0,strlen($zipcode)-1),'after');
		$start = $this->db2->get('zipcode')->row_array();
		}

		if ($start)
		{

			$db = $this->db2->get('zipcode')->result_array();

			foreach ($db as $row)
			{
				if ($this->distance ($start['lat'], $start['longitude'], $row['lat'], $row['longitude'], "m") <= $miles)
				$rdb[] = $row['zipcode'];
			}

		}

		return $rdb;
	}

	function getZipRange($zipfrom,$zipto,$limit=100)
	{
/*

		$this->db2->select('zipcode');
		$this->db2->where('zipcode >=',$zipfrom);
		$this->db2->where('zipcode <=',$zipto);

		if ($limit) $this->db2->limit($limit);
		$db = $this->db2->get('zipcodedata')->result_array();
		
		foreach ($db as $row)
		{ 
			$rdb[] = $row['zipcode'];
		}

		return $rdb;
		*/

		$rdb = array();

		$this->db2->select('zipcode');
		$this->db2->where('zipcode >=',$zipfrom);
		$this->db2->where('zipcode <=',$zipto);
		$this->db2->limit($limit);
		$start = $this->db2->get('zipcodedata')->result_array();

		if (!$start)
		{
		$start = $this->db2->get('zipcodedata')->row_array();
		}

		if ($start)
		{

			//$db = $this->db2->get('zipcodedata')->result_array();

			foreach ($start as $row)
			{
				$rdb[] = $row['zipcode'];
			}

		}

		return $rdb;
	}

	function distance($lat1, $lon1, $lat2, $lon2, $unit)
	{


		//json_exit("$lat1, $lon1, $lat2, $lon2, $unit");



  		$theta = $lon1 - $lon2;
  		$dist = sin(deg2rad((double)$lat1)) * sin(deg2rad((double)$lat2)) +  cos(deg2rad((double)$lat1)) * cos(deg2rad((double)$lat2)) * cos(deg2rad((double)$theta));
  		$dist = acos($dist);
  		$dist = rad2deg($dist);
  		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

  		if ($unit == "K") {
    		return ($miles * 1.609344);
  		} else if ($unit == "N") {
      		return ($miles * 0.8684);
    	} else {
        return $miles;
      	}
	}

	function checkSearchCount($clientid)
	{
		$this->db->where('sdate',date('Y-m-d'));
		$this->db->where('scount >=',2);
		$this->db->where('clientid',$clientid);
		$query = $this->db->get('search_issues');

		if($query->num_rows() > 0)
		{
			$query = $query->row_array();
			$res = $query['scount'];
		}
		else
		{
			$res = 0;
		}

		return $res;
	}

	function search_issue($aid,$data)
	{
		if ($data['qresult']==0)
		{
			//4 consecutive 0 results.
			$f_issue = $this->session->userdata('fissue');

			if(empty($f_issue))
			{
				$this->session->set_userdata('fissue',1);
			}
			else
			{
				$f_issue += 1;
				$this->session->set_userdata('fissue',$f_issue);
			}

			//2 searches
			$s_issue = $this->session->userdata('issue');
			if (!$s_issue) $s_issue = $this->session->set_userdata('issue',1);
			else $s_issue += 1;

			$issue['clientid'] = $data['userid'];
			$issue['stype'] = 2;
			$issue['sdate'] = date('Y-m-d');
			$issue['scount'] = $s_issue;

			$search['clientid'] = $data['userid'];
			$search['stype'] = 2;
			$search['sdate'] = date('Y-m-d');
			$this->db->where($search);
			$dt =  $this->db->get('search_issues');
			$uissue = $dt->row_array();

			if (!$uissue)
			{
				$issue['searchids'] = $aid;
				$this->db->insert('search_issues', $issue);
			}
			else
			{
				$issue['searchids'] = $uissue['searchids'].", ".$aid;
				$this->db->where('id', $uissue['id']);
				$this->db->update('search_issues', $issue);

			}

			//4 searches
			$issue4['clientid'] = $data['userid'];
			$issue4['stype'] = 1;
			$issue4['sdate'] = date('Y-m-d');
			$issue4['scount'] = 1;
			$issue4['searchids'] = $aid;
			$this->db->insert('search_issues', $issue4);
		}
		else
		{
			$this->session->unset_userdata('fissue','');

			$issue['sdate'] = date('Y-m-d');
			$issue['stype'] = 2;
			$issue['clientid'] = $data['userid'];
			$this->db->where($issue);
			$this->db->delete('search_issues');

			$sql = ("SELECT * FROM search_issues WHERE stype = 1
				AND sdate BETWEEN date(now()) - INTERVAL 7 DAY AND date(now())
				AND clientid = '".$data['userid']."'
				GROUP BY searchids");

			$num = $this->db->query($sql);
			$num = $num->num_rows();

			if ($num >= 4)
			{
				$dt4 = $this->db->query($sql);
				$uissue4 = $dt4->result_array();

				$newdate = strtotime ( '-7 day' , strtotime ( date('Y-m-d') ) ) ;
				$last7days = date ( 'Y-m-d' , $newdate );

				//4 searches
				$issue4['clientid'] = $data['userid'];
				$issue4['stype'] = 4;
				$issue4['sdate'] = $last7days;
				$issue4['scount'] = $num;
				$issue4['searchids'] = "";
				foreach ($uissue4 as $row) $issue4['searchids'] .= $row['searchids'].", ";
				$this->db->insert('search_issues', $issue4);
			}
			else
			{
				$dissue['stype'] = 1;
				$dissue['clientid'] = $data['userid'];
				$this->db->where($dissue);
				$this->db->where("sdate BETWEEN date(now()) - INTERVAL 7 DAY AND date(now())");
				$this->db->delete('search_issues');
			}


			$this->session->unset_userdata('issue');

		}


	}

	function hasVMap($country_avre) {
		$this->db2->where('country_avre',$country_avre);
		$result = $this->db2->get('country_lists')->row_array();

		return isset($result['vmap']) && $result['vmap'] == 1 ? TRUE : FALSE;
	}

	function getNormalByKeyword($n_id) {
		return $this->dbus->get_where('normal_index', array('n_keyword' => $n_id))->row_array();
	}

    function getUnits() {
        $this->dbus->order_by('units_abbr', 'asc');
        $query = $this->dbus->get('units_abbr3');
        return $query->result_array();
    }
}


