<?php
  class Countries_db extends Model
  {

		private $db2;
		private $db3;
      
      function Countries_db()
      {
        parent::Model();
        $this->db2 = $this->load->database('igsave',true);  
		$this->db3 = $this->load->database('core',true);

		if(config_item('static_url') == "http://kanban.importgenius.com/")
		{
			$this->db2 = $this->load->database('bigdata_k',true);  
		}
					
					
      }
      
	function allow_country($country_id)
	{	
		$user = $this->session->userdata('user');

		$query = $this->db3
						->where('userid',$user['id'])
						->where('country_lists.country_avre',$country_id)
						->where('astatus',1)
						->select('country_avre,country_name')
						->join('country_lists','country_lists.country_id = available_countries.country_id')
						->get('available_countries');
		
		$num = $query->num_rows();
		
		if($num > 0)
		{
			$rows = $query->result_array();
			$r = array();
			
			foreach($rows as $ro)
			{
				$r[] = $ro['country_avre'];
			}
			
			
			$last_used = $this->db3
						->where_in('country_name',$r)
						->where('userid',$user['id'])
						->order_by('lid','desc')
						->limit(1)
						->get('country_lastused');
	
			$row_lastused = $last_used->row_array();
			
			if($row_lastused)
			{
				return $row_lastused['country_name'];
			}
			else
			{
				$row = $query->row_array();
				
				return $row['country_avre'];
			}
		}
		else
		{
			return false;
		}
	}
      
      function getList($cond,$order = false,$sortorder = false,$check_country = true)
      {
		  $user = $this->session->userdata('user');
		  
          $countries = $this->db3
							->select('country_id')
							->get_where('available_countries',array('userid'=>$user['id'],'astatus'=>1))
							->result_array();
							
		  
		  $c = array();
		  
		  foreach($countries as $country)
		  {
			$c[] = $country['country_id'];
		  }
		  
		  $this->db3->where($cond);
          
          if($order)
            $this->db3->order_by($order,$sortorder);

		  if($check_country)
		  {
			if(count($countries) > 0)
			{
			
				$this->db3->where_in('country_id',$c);
				$q = $this->db3->get('country_lists');
				$res =  $q->result_array();
			}
			else
			{
				$res = false;
			}
		  }
		  else
		  {
			$q = $this->db3->get('country_lists');
			$res =  $q->result_array();
		  }
          
		  return $res;
      }
	  
	  function getList2($cond)
	  {
			$this->db->where($cond);
			$this->db->or_where('status',1);
			//$this->db->order_by('country_name','asc');
			$this->db->order_by('sort_order','asc');
			$q = $this->db->get('country_lists');
			
			return $q->result_array();
	  }
      
	  function getListStore($cond)
	  {
		$this->db->where($cond);
		$countries = $this->db->get('available_countries')->result_array();
		
		$c = array();
		
		
		if($countries)
		{
			foreach($countries as $co)
			{
				$c[] = $co['country_id'];
			}
			$this->db->where_not_in('country_id',$c);
		}
		$q = $this->db->get('country_lists');
		
		
		return $q->result_array();
	  }
	  
	  
	function getUserCountry()
	{
		$user = $this->session->userdata('user');
		
		$userid = $user;

		$this->db->where('userid',$userid);
		$this->db->where('astatus',1);
		$this->db->select('country_lists.country_avre');
		$this->db->join('country_lists','country_lists.country_id = available_countries.country_id');
		$db = $this->db->get('available_countries');
		
		$row = $db->row_array();
		
		return $row;
	}
	  
	  function available_countries()
	  {
			$user = $this->session->userdata('user');
			
			$this->db->where('userid',$user['id']);
			$this->db->where('astatus',1);
			$this->db->select('country_id');
			$db = $this->db->get('available_countries');
			$arr = array();
			
			foreach($db->result_array() as $q)
			{
				$arr[] = $q['country_id'];
			}

			return $arr;
	  }
	  
      function fields($str,$country_sort=false)
      {
			$sortby = "sort_order";
			$where 	= "";
			
			if($country_sort)
			{
				$sortby = "sort_order_".$country_sort;
				$where  = "AND cf.sort_order_".$country_sort." != 0 ";
			}
			
			$sql = "SELECT
                        cf.field_name,cf.display_name,cf.width,cf.sortable,cf.align,cf.hide,sortfields FROM `fields_available` AS fa 
                  LEFT JOIN 
                        country_fields2 AS cf ON (cf.field_id = fa.field_id)
                  LEFT JOIN 
                        country_lists AS cl ON (cl.country_id = fa.country_id) WHERE cl.country_avre = ? AND cf.status = 1 
                  $where    
                  ORDER BY 
                        cf.{$sortby} ASC
                  ";
							
          $q = $this->db2->query($sql,$str);
          
          return $q->result_array();
      }
      
      function availfields($country_id)
      {
          $sql = "SELECT 
                        cf.field_name
                  FROM 
                        `fields_available` AS fa 
                  LEFT JOIN 
                        country_fields2 AS cf ON (cf.field_id = fa.field_id)
                  LEFT JOIN 
                        country_lists AS cl ON (cl.country_id = fa.country_id) WHERE fa.country_id = ?
                  ORDER BY 
                        cf.sort_order ASC
                  ";
                        
          $q = $this->db2->query($sql,$country_id);
          
          return $q->result_array();
      }
      
      function cfields()
      {        
          $this->db2->order_by('field_name','asc');
          $query = $this->db2->get('country_fields2');
          
          return $query->result_array();
      }
      
      function cfield($cond)
      {        
          $this->db2->where($cond);
          $query = $this->db2->get('country_lists');
          
          return $query->row_array();
      }
      
      function savecfSettings($db,$cond)
      {
          $this->db2->where($cond);
          $this->db2->update('country_fields2',$db);
      }
      
      function saveFields($db,$bool)
      {
          if($bool)
          {
              $this->db2->insert('fields_available',$db);
          }
          else
          {
              $this->db2->where('field_id',$db['field_id']);
              $this->db2->where('country_id',$db['country_id']);
              $this->db2->delete('fields_available');
          }
      }
	  
	  function getAvailableCountries($cond,$single=false)
	  {
		$this->db->where($cond);
		$this->db->where('astatus',1);
		$this->db->where('c.status',1);
		$this->db->select('c.country_id,c.country_name');
		$this->db->join('country_lists c','c.country_id = ac.country_id');
		$q = $this->db->get_where('available_countries ac');
		
		if($single)
			$results = $q->row_array();
		else
			$results = $q->result_array();
		
		return $results;
	  }
	  
	  function getCountry($cond)
	  {
		$this->db3->reconnect();
		$q = $this->db3->get_where('country_lists',$cond)->row_array();
		
		return $q;
	  }
	  
	  function get_iso($cond,$like_opt="match")
	  {
		  $this->db3->like('countries_name',$cond,$like_opt);
          $query = $this->db3->get('countries');
          
          return $query->row_array();
	  }
	  
	  
  }
?>
