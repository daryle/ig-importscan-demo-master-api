<?
class Payment_db extends Model {

    function Payment_db()
    {
        parent::Model();
    }

	function validateCard($db, $amount = '0')
	{
		$this->load->library('xmlrpc');
		$user = $this->session->userdata('user');

		$server_url = "https://beta.importgenius.com/api.php/api_charge_cert";
		//$server_url = "http://api.importgenius.com:90/api.php/api_charge_cert";
		$this->xmlrpc->server($server_url, 80);

		$this->xmlrpc->method('CardValidation');

		if (isset($_POST['amount'])) $amount = $this->input->post('amount');

		$cid = md5($db);
		$userid = $user['id'];

		$request = array($cid,$userid,$amount);
		//$this->xmlrpc->set_debug(TRUE);
		$this->xmlrpc->request($request);

		if (!$this->xmlrpc->send_request())
		{
			return $this->xmlrpc->display_error();
			//return false;
		}
		else
		{
			return $this->xmlrpc->display_response();
		}

	}
	
	
	

}
