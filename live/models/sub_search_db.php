<?
class Sub_search_db extends Model {

    	function Saved_search_db()
    	{
        parent::Model();
    	}


	function getSave($data)
	{
		$this->db->where($data);
		$this->db->join('search_log', 'search_log.aid = sub_search.su_aid', 'left');
		$dt =  $this->db->get('sub_search');

		return $dt->row_array();
	}

	function getSaves($cond,$page=false,$rp=false)
	{
		$this->db->reconnect();
		$this->db->where($cond);
		$this->db->from('sub_search');
		$this->db->join('users','sub_search.su_userid = users.id','left');
		$this->db->join('search_log', 'search_log.aid = sub_search.su_aid', 'left');
		//$this->db->group_by('sub_search.su_aid');
		$data['total'] = $this->db->count_all_results();

		if ($page)
		{
		$start = (($page-1) * $rp);

		if ($start>$data['total'])
			{
			$start = 0;
			$page = 1;
			}
		}

		$this->db->where($cond);
		$this->db->join('users','sub_search.su_userid = users.id','left');
		$this->db->join('search_log', 'search_log.aid = sub_search.su_aid', 'left');
		$this->db->group_by('sub_search.su_aid');
		if ($page)  $this->db->limit($rp,$start);
		$dt =  $this->db->get('sub_search');
		$data['db'] = $dt->result_array();


		if ($page) $data['page'] = $page;
		
		//echo $this->db->last_query();

		//print_r($data); exit;

		return $data;
	}

	function saveSearch($data)
	{
		$this->db->reconnect();
		$this->db->insert('sub_search', $data);
		return $this->db->insert_id();
	}

	function saveLog($suid,$stat,$result,$count=0)
	{
		$data['sb_su_id'] = $suid;
		$data['sb_stat'] = $stat;
		$data['sb_result'] = $result;
		$data['sb_date'] = date('Y-m-d H:i:s');
		$data['sb_count'] = $count;
		$this->db->reconnect();
		$this->db->insert('sub_log', $data);
		return $this->db->insert_id();
	}


	function updateSub($cond,$data)
	{
		$this->db->reconnect();
		$this->db->where($cond);
		$this->db->update('sub_search',$data);
	}

    	function deleteSave($data)
    	{
    		$this->db->where($data);
		$this->db->delete('sub_search');
    	}


}
