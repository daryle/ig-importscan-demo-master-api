<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @property CI_DB_active_record $db
 * @property CI_DB_active_record $coredb
 * @property CI_DB_active_record $db2
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 */
class Alibaba_alert_db extends Model {

    private $coredb;

    /**
     * Class constructor
     *
     */
    function Alibaba_alert_db() {
        parent::Model();
        $this->coredb = $this->load->database('core', TRUE);
    }

    function get_records($start_date, $end_date) {
        $sql = "SELECT log.*, u.username, u.firstname, u.lastname FROM `alibaba_log` as log INNER JOIN users AS u ON log.userid = u.id WHERE userid NOT IN (37439) AND `log`.`datetime` >= '{$start_date}' AND `log`.`datetime` <= '{$end_date}' ORDER BY `log`.`datetime` ASC;";
        $query =$this->coredb->query($sql);
        return $query->result_array();
    }
}
