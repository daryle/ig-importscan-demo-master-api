<?php

class Benchmark {

	var $CI;
	var $use;

	function Benchmark() {
		$this->CI = & get_instance();
		$this->use = isset($_COOKIE['devmode']) && $_COOKIE['devmode'] == 'edocajnin_12';
	}

	private function microtime() {
		list ($msec, $sec) = explode(' ', microtime());
		$microtime = (float) $msec + (float) $sec;
		return $microtime;
	}

	private function get_mime_type() {
		$type = 'unknown';
		$headers = $this->CI->output->headers;
		if ($headers) {
			foreach ($headers as $header) {
				if (strpos($header[0], 'Content-type') !== FALSE) {
					if (strpos($header[0], 'css') !== FALSE) {
						$type = 'css';
						continue;
					}
					if (strpos($header[0], 'html') !== FALSE) {
						$type = 'html';
						continue;
					}
					if (strpos($header[0], 'javascript') !== FALSE) {
						$type = 'javascript';
						continue;
					}
					if (strpos($header[0], 'json') !== FALSE) {
						$type = 'json';
						continue;
					}
				}
			}
		}

		return $type;
	}

	private function show_mini_profiler($pre, $suf, $exec_time, $memory, $peak_memory) {
		echo "\n\n{$pre}\n\n";
		echo "Page Execution Time: {$exec_time} Seconds\n";
		echo "Memory Consumed: {$memory}\n";
		echo "Peak Memory Usage: {$peak_memory}\n";

		if (isset($GLOBALS['SQL_BENCHMARKS'])) {
			echo "Query Benchmarks\n\n";
			foreach ($GLOBALS['SQL_BENCHMARKS'] as $ctr => $benchmark) {
				$ctr++;
				$benchmark['elapsed'] = round($benchmark['elapsed'], 4);
				$benchmark['memory'] = round($benchmark['memory'] / 1048576, 4);

				echo "SQL Command #{$ctr} : {$benchmark['sql']}\n";
				echo "Host : {$benchmark['host']}\n";
				echo "Database : {$benchmark['database']}\n";
				echo "Execution Time: {$benchmark['elapsed']} Second(s)\n";
				echo "Memory Consumed: {$benchmark['memory']} MByte(s)\n\n";
			}
		}
		echo "\n{$suf}";
	}

	/**
	 * Use or not use the profiler
	 */
	function profiler() {
		// If dev mode cookie is not detected, let's get out.
		if (!$this->use)
			return;

		// Use only if output is html
		if ($this->get_mime_type() == 'html')
			$this->CI->output->enable_profiler($this->use);
	}

	function display_override() {
		// If dev mode cookie is not detected, let's get out.
		if (!$this->use)
			return;

		// Applicable to json only
		if ($this->get_mime_type() == 'json') {
			$output = $this->CI->output->get_output();

			// End timer
			if (!defined('POST_CONTROLLER_TIME'))
				define('POST_CONTROLLER_TIME', $this->microtime());

			// Get execution time
			$exec_time = round(POST_CONTROLLER_TIME - PRE_CONTROLLER_TIME, 4);

			// Get memory usage
			$memory = round(memory_get_usage() / 1048576, 2);

			// Get peak memory usage
			$peak_memory = round(memory_get_peak_usage() / 1048576, 2);

			$output = json_decode($output, TRUE);
			$output['benchmark']['exec_time'] = $exec_time;
			$output['benchmark']['memory'] = $memory;
			$output['benchmark']['peak_memory'] = $peak_memory;


			if (isset($GLOBALS['SQL_BENCHMARKS'])) {
				foreach ($GLOBALS['SQL_BENCHMARKS'] as $ctr => $benchmark) {
					$ctr++;
					$benchmark['elapsed'] = round($benchmark['elapsed'], 4);
					$benchmark['memory'] = round($benchmark['memory'] / 1048576, 4);
					$output['benchmark']["sql"]["[{$benchmark['elapsed']}] {$benchmark['sql']} "] = array(
						'host' => $benchmark['host'],
						'database' => $benchmark['database'],
						'memory' => $benchmark['memory']
					);
				}
			}
			$output = json_encode($output);
			$this->CI->output->set_output($output);
		}
		$this->CI->output->_display();
	}

	function start() {
		// If dev mode cookie is not detected, let's get out.
		if (!$this->use)
			return;

		// Start timer
		define('PRE_CONTROLLER_TIME', $this->microtime());
	}

	/**
	 * Since CI profiler is not applicable to JS and CSS files, let's make our own.
	 *
	 */
	function end() {
		// If dev mode cookie is not detected, let's get out.
		if (!$this->use)
			return;

		// End timer
		if (!defined('POST_CONTROLLER_TIME'))
			define('POST_CONTROLLER_TIME', $this->microtime());

		// Get execution time
		$exec_time = round(POST_CONTROLLER_TIME - PRE_CONTROLLER_TIME, 4);

		// Get memory usage
		$memory = round(memory_get_usage() / 1048576, 2);

		// Get peak memory usage
		$peak_memory = round(memory_get_peak_usage() / 1048576, 2);

		// Make sure only css and js output will be profiled
		switch ($this->get_mime_type()) {
			case 'css': {
					$pre = '/** ';
					$suf = ' */';
					$this->show_mini_profiler($pre, $suf, $exec_time, $memory, $peak_memory);
					break;
				}
			case 'javascript': {
					$pre = '/** ';
					$suf = ' */';
					$this->show_mini_profiler($pre, $suf, $exec_time, $memory, $peak_memory);
					break;
				}
		}
	}

}