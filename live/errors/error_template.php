<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('base_url')) {

	function base_url()
	{
		$url = $_SERVER['SCRIPT_NAME'];
		$url = substr($url,0,strpos($url,".php"));
		$url = substr($url,0,(strlen($url) - strpos(strrev($url),"/")));
		$url = ((empty($_SERVER['HTTPS']) OR $_SERVER['HTTPS'] === 'off') ? 'http' : 'https')."://".$_SERVER['HTTP_HOST'].$url;
		
		$config['base_url']	= $url;
		return $url;
	}

}

if (!function_exists('site_url')) {
	
	function site_url($s='')
	{
		return "http://www.importgenius.com/".$s;
	}

}

if (!function_exists('lang')) {
	
	function lang($s='')
	{
		$language = config_item('language');
		
		$x = "";
		$lang = array();
		
		$filename = APPPATH."language/$language/default_lang.php";
		
		if (file_exists($filename)) include($filename);
		
		if (isset($lang[$s])) $x = $lang[$s];
		
		return $x;
	}

}


if ( ! function_exists('resource_url'))
{
	function resource_url($uri = '')
	{
		
		$url = config_item('static_url');
		
		if (!$url) $url = base_url();
		
		return $url."resources/site/live/".$uri;
	}
}


if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
{
				$data['response'] = $message;
				$data['action'] = 'retry';
				echo json_encode($data);
				exit;
}


/*
if (!isset($ecode))
	{
	header("HTTP/1.0 404 Not Found");
	}
else
	{
	if ($ecode==503)
		{
		header('HTTP/1.1 503 Service Temporarily Unavailable');
		header('Status: 503 Service Temporarily Unavailable');
		header('Retry-After: 7200'); 
		}
	}	
*/

	
$toh = 20;

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?=$heading?> - Import Genius  : U.S. Customs Database and Competitive Intelligence Tools</title>
		<link rel="stylesheet" href="<?=resource_url('css/default/style.css')?>" />
		<link rel="stylesheet" href="<?=resource_url('css/default/header.css')?>" />
		<link rel="stylesheet" href="<?=resource_url('css/default/error.css')?>" />
		<!--[if IE 6]>
		<link rel="stylesheet" href="<?=resource_url('css/default/style.ie.6.css')?>" />
		<![endif]-->
		<link rel="shortcut icon" href="<?=resource_url('favicon.png')?>" />
		<link rel="apple-touch-icon" href="<?=resource_url('favicon-apple.png')?>"/>
		<style>
			body
				{
				background: #fff;
				}
		</style>
		<script type="text/javascript">var root="<?=site_url('')?>";var toh=<?=$toh?>;</script>
	</head>
	<body>
		
		<div class="container">
		
			<div class="topmenu">
				<div class="tile">				
				
					<span class="callus"><span id="phone" class="icon"></span> <?=lang('call_number')?></span>
					<a href="http://server.iad.liveperson.net/hc/5774993/?cmd=file&file=visitorWantsToChat&site=5774993&byhref=1" class="chatlive"><span class="icon"></span> <span class="label"><?=lang('live_chat')?></span></a>
					<a href="<?=site_url("rss")?>" class="rss"><span class="icon rss-icon"></span> <span class="label"><?=lang('subscribe')?></span></a>
					
				</div>
			</div> <!-- topmenu -->
			
			<div class="header">
				<div class="tile">
				
					<a href="<?=site_url()?>" class="logo" title="Import Genius"></a>
					
					<div class="nav">
							<a href="<?=site_url()?>" id="nav1" title="<?=lang('home')?>"><span><?=lang('home')?></span></a>
							<a href="<?=site_url('tour')?>" id="nav2" title="<?=lang('tour')?>"><span><?=lang('tour')?></span></a>
							<a href="<?=site_url('signup')?>" id="nav3" title="<?=lang('plans_n_pricing')?>"><span><?=lang('plans_n_pricing')?></span></a>
							<a href="<?=site_url('contact')?>" id="nav4" title="<?=lang('contact')?>"><span><?=lang('contact')?></span></a>
					</div>
					
				</div>
			</div> <!-- header -->
			<div class="body">
			
				<div class="tile">
						<div class="col1of3">
								<div class="lost <? if (isset($error_class)) echo $error_class?>"></div>
						</div>
						
						<div class="col2of3">
							
							<h2><?=$heading?></h2>
							
							<p><?=$message?></p>
							
							<? if (isset($more_messages)) { ?>
								
							<div class="more-messages">
								<?=$more_messages?>
							</div>
							<div class="shadow"><div class="s1"></div><div class="s2"></div></div>
								
							<? } ?>
							
						</div>
				</div>
				
			</div> <!-- body -->
</body>
</html>