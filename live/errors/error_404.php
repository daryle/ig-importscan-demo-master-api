<?
$more_messages = "<p>It may still be under construction or permanently removed, as we are constantly improving the site.</p>"; 

if (isset($_SERVER['HTTP_REFERER']))
	$more_messages .= "<p>Please go <a href=\"javascript:history.back();\" class=\"button\"><span class=\"b1\"><span class=\"b2\">Back</span></span></a> or select any link above to learn more about us.</p>";
else
	$more_messages .= "<p>Please select any link above to learn more about us.</p>";

$heading = "Page Not Found";

include('error_template.php');
?>