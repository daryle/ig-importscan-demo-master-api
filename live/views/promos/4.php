<link rel="stylesheet" href="<?=resource_url("css/iscan2/promo.css")?>" />

<img src="<?=resource_url("media/promo/2.jpg")?>" align="left" style="position: absolute; top: 3.5%; left: 2%; width: 42%;" />

<div class="prpanel" style="width: auto; margin-left: 47%; margin-top: 10%; float: none;">

	<div class="promo" style="width: 340px;">
        <h1>
            <b style="color: red; letter-spacing: -1px; font-size: 24px;">Welcome to Your Trial</b><br />
            <div style="font-size: 24px;">
             of  <b>Import Genius!</b>
            
            </div>
        </h1>
        <h3>We give you tools to search the shipping manifests for over 50 million U.S. ocean freight shipments.</h3>
        <b>Here you can:</b>
        <ul type="circle" class="check" style="letter-spacing: 0px; margin-left: 0px;">
        	<li>Find out where competitors source their products</li>
            <li>Learn who else imports from your suppliers</li>
            <li>Research sources of almost any product type</li>
        </ul>
    </div>
    
     <div class="opbuttons">
		<button class="button" type="button" onclick="location.href='/resources/iscan4/live/help/index.html';"><span><b>Let's Get Started!</b></span></button>
	</div>

</div>

