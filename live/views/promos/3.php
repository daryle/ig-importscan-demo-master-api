

<link rel="stylesheet" href="<?=resource_url("css/iscan2/promo.css")?>" />

<img src="<?=resource_url("media/promo/2.jpg")?>" align="left" style="position: absolute; top: 3.5%; left: 2%; width: 38%;" />

<div class="prpanel" style="width: auto; margin-left: 45%; margin-top: 10%; float: none;">

	<div class="promo" style="width: auto;">
        <h1>
            <b style="color: red; letter-spacing: -1px; font-size: 24px;">Sign-Up Now!</b><br />
            <div style="font-size: 24px;">
            for <b>instant access</b>

            </div>
        </h1>
        <h3>To our data on U.S. ocean freight imports</h3>
        <ul type="circle" class="check" style="letter-spacing: 0px; margin-left: 0px;">
        	<li>Find reliable sources for any product based on actual shipping data</li>
            <li>Identify suppliers used by the leaders in your industry</li>
            <li>Monitor your own suppliers to ensure they don't sell to your rivals</li>
        </ul>
    </div>

    <?


	$user = $this->session->userdata('user');

	if (isset($log['log_date']))
		$created = date('j',strtotime($log['log_date']));
	else
		$created = date('j',strtotime($user['created']));

	$arb =  $created - date('d');
	if ($arb < 0)
		{
		$arb = date('t') - date('d') + $created;
		}

	$dom = date('t');

	if ($arb==0) $arb = $dom;

	$prorate = (99/$dom*($dom-$arb)) + ((199/$dom)*$arb) - 0;
	$prorate = number_format($prorate,2);


	?>

    <div class="areyousure" style="display:none; width: 300px">
       	<h2>If you agree to upgrade:</h2>
        <p>The following changes will be applied to your account in 1 business day</p>
        <ul class="check">
        <li><? if ($card) { ?>Your credit card ending in <b>(<?=substr($card['cardno'],strlen($card['cardno'])-4)?>)</b><? } else { ?>You<? } ?> will be charged the pro-rated amount of <strong>$<? echo $prorate ?> <a href="#prorate">*</a></strong> within 24 hours</li>
        <li>Thereafter, your monthly subscription on the <strong><?=$created?>th</strong> of each month will be <strong>$199</strong></li>
        </ul>

        <p>
        <b>Pro-rated calculation:</b><br />
        New monthly rate: $ 199.00<br />
        <b class='hlite' title="Formula: ((current rate / days of month ) * days since last bill) + ((new rate / [days of month]) * (days till next bill)) - already paid">Calculation</b>:  (($99 / <?=$dom?> days) * (<?=$dom-$arb?> days)) + ((199 / <?=$dom?> days * <?=$arb?> days)) - $0
        <br />
        Pro-rate: $ <?=$prorate?>
        </p>


    </div>

    <div style="display:none" id="pmessage">

	<p><?=$user['username']?> accepted our offer to upgrade from <?=isset($planname)?$planname:'Trial';?> to Standard Account in order to access our new Visual Mapping technology.</p>
	<p>
	<b>Action is Required:</b> Please follow standard operating procedure for upgrading an account, including signing into the CMS to make the change and editing the ARB with our credit card processor.
	</p>

	<p>
	    	<b>TASK</b>: Administrator must also charge $<?=$prorate?> as a one-time fee for the
	balance of this month, then change ARB to $199 and quickly
	modify account to STANDARD type.
	</p>

        <p>
        <b>Pro-rated calculation:</b><br />
        New monthly rate: $ 199.00<br />
        <b class='hlite' title="Formula: ((current rate / days of month ) * days since last bill) + ((new rate / [days of month]) * (days till next bill)) - already paid">Calculation</b>:  (($99 / <?=$dom?> days) * (<?=$dom-$arb?> days)) + ((199 / <?=$dom?> days * <?=$arb?> days)) - $99
        <br />
        Pro-rate: $ <?=$prorate?>
        </p>


    </div>
     <div class="opbuttons">
		<button class="button" type="button" onclick="parent.location.href='<?=site_url('users/logout_signup')?>';"><span><b>Upgrade from <?=isset($planname)?$planname:'Trial';?> Now!</b></span></button>
<!--
		<button class="button" type="button" onclick="$('.callform').show(); $(this).parent().hide(); $('input[name:called]').trigger('focus');"><span><b>Call me now</b></span></button>
-->
	</div>

    <form class="form callform" style="display:none;">

	    <div class="fields">

		<h3>Type your phone number below</h3>
		<input type="text" name="called" title="Phone Number" class="rtb" />
		<input type="hidden" name="action" value="promo/post" />
		<button type="submit" class="button"><span><b>Call me now</b></span></button>
		<a href="#" onclick="$('.callform').hide(); $('.opbuttons').show(); return false;"><b>Cancel</b></a>

	    </div>
	    <div class="response"></div>
    </form>


<script type="text/javascript">
	$('.prbuttons a').click
	(
		function ()
			{

			var gurl = 'promo/post';
			var val = this.rel;
			var promoid = '2';

			if (val==1&&$('.areyousure:visible').length==0)
			{
				$('.areyousure').show();
				$('.promo').hide();
				$('#qverify').text('Are you ready to upgrade?');
				this.blur();
				return false;
			}

			$('.prbuttons').hide();
			$('.areyousure').html('Processing...Please wait...');

			$.ajax({
			url: mod+gurl
			,type: 'POST'
			,dataType: 'json'
			,data: {response:val,promoid:promoid, pmessage: $('#pmessage').html()}
			,success:
				function (data)
				{

				//if (parent)
				//	parent.$('#TB_closeWindowButton').trigger('focus').trigger('click');
				$('.areyousure').html("<b>Upgrade request sent.</b><p>Thank you for upgrading from the <?=isset($planname)?$planname:'Trial';?> to the Standard account type with ImportGenius.com.  We will process your request as soon as possible, so that you can start using our new Visual Mapping technology and other features of your account.  </p><p>Please note that our office hours are Monday to Friday 8:30 am to 5:00 pm Eastern.  If you've upgraded your account outside those hours, it will be processed on the next business day.  If you're in a rush, feel free to give us a call at 1-888-843-0272 so we can accelerate the process.</p>");

				}
			});

			return false;

			}
	);
</script>


</div>


