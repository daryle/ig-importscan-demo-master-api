<?

	$newrate = 175;
	
?>
<link rel="stylesheet" href="<?=resource_url("css/iscan2/promo.css")?>" />

<img src="<?=resource_url("media/promo/2.jpg")?>" align="left" style="margin-right:10px" width="240" height="360" />

<div class="prpanel" style="width: 300px;">

	<div class="promo" style="width: 300px;">
        <h1>
            <b style="color: red">Save Money!</b><br />
            <div style="font-size: 28px;">
            Upgrade to <br />
            <b style="color: blue">Standard Account</b>
            </div>
        </h1><br />
        <h2>Receive 25% off to upgrade</h2>
        <ul type="circle" class="check">
        	<li>Promo price now $<?=$newrate?>/month <span style="color:red; text-decoration: line-through;">($199/month)</span></li>
            <li>More visibility: View one year's worth of shipments</li>
            <li>80 searches per day</li>
        </ul>
    </div>
    
    <?
	
	$user = $this->session->userdata('user');
	
	if (isset($log['log_date']))
		$created = date('j',strtotime($log['log_date']));
	else
		$created = date('j',strtotime($user['created']));
		
	$arb =  $created - date('d');
	if ($arb < 0) 
		{
		$arb = date('t') - date('d') + $created;
		}
		
	$dom = date('t');
	
	if ($arb==0) $arb = $dom;

	$prorate = (99/$dom*($dom-$arb)) + (($newrate/$dom)*$arb) - 99;
	$prorate = number_format($prorate,2);
	
	
	?>    
    
    <div class="areyousure" style="display:none; width: 300px">
       	<h2>If you agree to upgrade:</h2>
        <ul class="check">
        <li><? if ($card) { ?>Your credit card ending in <b>(<?=substr($card['cardno'],strlen($card['cardno'])-4)?>)</b><? } else { ?>You<? } ?> will be charged <strong>$<? echo $prorate ?></strong> within 24 hours</li>
        <li>Thereafter, your monthly subscription on the <strong><?=$created?>th</strong> of each month will be <strong>$<?=$newrate?></strong></li>
        </ul>
        <p>The following changes will be applied to your account in 1 business day</p>

        <p>
        <b>Pro-rated calculation:</b><br />
        New monthly rate: $ <?=$newrate?>.00<br />
        <b class='hlite' title="Formula: ((current rate / days of month ) * days since last bill) + ((new rate / [days of month]) * (days till next bill)) - already paid">Calculation</b>:  (($99 / <?=$dom?> days) * (<?=$dom-$arb?> days)) + (($newrate / <?=$dom?> days * <?=$arb?> days)) - $99 
        <br />
        Pro-rate: $ <?=$prorate?>
        </p>
        
        
    </div>
    
    <div style="display:none" id="pmessage">
    	<b>TASK</b>: Administrator must now charge $<?=$prorate?> as a one-time free for the 
	balance of this month, then change ARB to $<?=$newrate?> and quickly 
	modify account to STANDARD type.
    </div>
    
    
    <div class="prbuttons">
    <p><strong id="qverify">Upgrade Now?</strong></p>
    <? $this->load->view('promos/buttons') ?> 				      
    </div>
    
    <div style="color: #777; margin-top: 10px; clear:both;">
    
    Current plan: <b>Limited Account</b>. Restricted to viewing past 45 days of shipments. ($99/mo)
    
    </div>

</div>

