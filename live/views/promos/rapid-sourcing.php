<link rel="stylesheet" href="<?=resource_url("css/iscan2/widget-sourcing.css?v=1")?>" />
<?php
    $this->load->library('user_agent');
    if (strtolower($this->agent->browser()) === 'internet explorer' && intval($this->agent->version()) < 8) {
        ?><link rel="stylesheet" href="<?=resource_url("css/iscan2/widget-sourcing.ie." . intval($this->agent->version()) . ".css?v=" . rand())?>" /><?
    }
?>
    <div class="b-border">&nbsp;</div>
    <div class="w-col1 b-right">

        <div class="inner-fsection">
              <img src="<?=resource_url("media/promo/alibaba-ig-logo.png")?>" title="Alibaba Rapid Sourcing" alt="Alibaba Rapid Sourcing">
            <h2>More Effective, Less Hassle</h2>
            <p>Get free quotations from verified suppliers on Alibaba.com, the world's leading wholesale platform for small businesses!</p>
            3 simple steps:
            <ul>

                <li>Tell us what you're looking for</li>
                <li>We'll gather quotes from suppliers</li>
                <li>Matches are then sent to your email*</li>

            </ul>
            <a href="http://sourcing.alibaba.com/buyer/guide.html" target="_blank" class="alibaba-learn-more">Learn More</a>

            <div style="clear: both">&nbsp;</div>
            <div class="alibaba-not-member">*If you are not an existing Alibaba.com member, an account will be created for you automatically.</div>
        </div>
    </div>
     <div class="w-col2">
        <link rel="stylesheet" href="<?= resource_url("css/iscan2/widget-sourcing.css") ?>" />
        <div class="alibaba-float-arrow"></div>
        <div class="areyousure hidden">Processing...Please wait...</div>
        <form name="frmAlibaba" action="<?= site_url('alibaba/post'); ?>" method="post" class="frmAlibaba">
    <input type="hidden" name="action" value="subscribe" />
    <input type="hidden" name="aliMemberId" value="<?= $aliMemberId === 0 || $aliMemberId === '0' ? '' : $aliMemberId ?>" />
            <div class="rs-form">
                    <h2>Tell Suppliers What You Want</h2>
                    <div class="fsection">
                        <!--<label for="pname">Product Name:</label>-->
                        <input type="text" id="pname" name="rfqName" placeholder="Product Name" class="rfqName" value="<?=isset($param2) ? urldecode($param2) : '';?>">
                    </div>
                    <div class="fsection">
                        <!--<label for="pdetail">Product Detailed Specification:</label>-->
                        <textarea id="pdetail" placeholder="Briefly describe your requirements" name="rfqDetail" class="rfqDetail"></textarea>
                    </div>
                    <div class="fsection fsection2">
                        <!--<label for="pquantity">Estimated Order Quantity:</label>-->
                        <!--<div class="w51">-->
                            <input type="text" id="pquantity" class="quan-input rfqQuantity" placeholder="Estimated Order Quantity" name="quantity">
<!--                        </div>
                        <div class="w52">-->
                            <select name="quantityUnit" class="quantityUnit">
                                <?php
                                    if (isset($units) && $units) {
                                        foreach($units as $unit) {
                                            $selected = $unit['units_abbr'] === 'Pieces' ? 'selected' : '';
                                            echo "<option value=\"{$unit['units_abbr']}\" {$selected} >{$unit['units_abbr']}</option>";
                                        }
                                    }
                                ?>
                        </select>
                        <!--</div>-->
                        <!--<div style="clear:both"></div>-->
                    </div>

            </div>

            <div class="widget-inner">
                <!-- <p><input type="checkbox" id="alibaba-offer-agree" value="" />&nbsp; I agree to the Alibaba.com <a href="http://news.alibaba.com/article/detail/help/100453670-1-alibaba.com-free-membership-agreement.html" target="_blank">Free Membership Agreement</a> and the use of my name, email, and registered country for account creation.</p> -->
                <?php if($aliMemberId === 0 || $aliMemberId === '0') {?>
                    <p><input type="checkbox" id="alibaba-offer-agree" value="" />&nbsp; I have read and agreed to the <a href="http://news.alibaba.com/article/detail/help/100453670-1-alibaba.com-free-membership-agreement.html" target="_blank">Alibaba.com Free Membership Agreement</a> and <a href="" class="show-info-collect">Information Collection Statement</a>. <!-- the use of my name, email, and registered country for account creation. --></p>
                    <?php $this->load->view('panes/alibaba_infocollection');?>
                <? } ?>

                <div class="b-bottom">
                    <button type="submit" <?php echo $aliMemberId === 0 || $aliMemberId === '0' ? "disabled='disabled'" : '' ?>>Submit</button>
                    <a href="#" class="cancel-sourcing">Cancel</a>
                    
                    <a href=" http://us.sourcing.alibaba.com/rfq/request/post_buy_request.htm?src=ibdm_d01_impgen2" target="_blank" class="post-full">Post full request form</a>
                </div>

            </div>
        </form>
    </div>


<script type="text/javascript">
$(document).ready(function() {
    if (window.parent.aliMemberId != '0') {
        $('input[name=aliMemberId]').val(window.parent.aliMemberId);
    }

    $('form[name=frmAlibaba]').submit(function(event) {
        if( $("input#alibaba-offer-agree").attr('checked') == false ){
            var btn = '<div class="widget-inner widget-inner2"><button type="button" id="try-again">Try again</button></div>';
            $('.areyousure').removeClass('black').addClass('red');
            $('.frmAlibaba').hide('fast');
            $('.areyousure').html('Please agree to the Alibaba.com <a href="http://news.alibaba.com/article/detail/help/100453670-1-alibaba.com-free-membership-agreement.html" target="_blank">Free Membership Agreement</a>.' + btn).show('fast');

            window.parent.Alibaba.message = 'Failed to agree to the Alibaba.com Free Membership Agreement';
            window.parent.log_action('4');
            return false;
            event.preventDefault();
        }

        var url = $(this).attr('action');
        var dt = $(this).serializeArray();

        $('.frmAlibaba').hide('fast');
        $('.areyousure').html('Processing... Please wait...').show('fast');

        $.ajax({
            url: url
            , type: 'POST'
            , dataType: 'json'
            , data: dt
            , success: function(data) {
                $('.areyousure').removeClass('black').addClass('red');

                var btn = '<div class="widget-inner widget-inner2"><button type="button" id="try-again">Try again</button></div>';
                if (data.message) {
                    window.parent.Alibaba.message = data.message;

                    // Failed
                    if (data.action === 'retry') {
                        $('.areyousure').html(data.message + btn);

                        if (data.message.indexOf('You are already a member at Alibaba.com') >= 0) {
                            window.parent.log_action('6');
                        }
                        else {
                            window.parent.log_action('4');
                        }
                    }
                    // OK
                    else {
                        $('.areyousure').removeClass('red').addClass('black');
                        btn = '<div class="widget-inner widget-inner2"><button type="button" id="ok-button">OK</button></div>';
                        $('.areyousure').html(data.message + btn);
                        window.parent.aliMemberId = data.aliMemberId;


                        if (data.message.indexOf('You are already a member at Alibaba.com') >= 0) {
                            _gaq.push(['_trackEvent', 'AliSourcePro', 'AliSourceProExisingMemberSignUp', 'Existing AliSourcePro Member Signup', 80, true]);
                            window.parent.log_action('6');
                        }
                        else {
                            // Google Tracker
                            _gaq.push(['_trackEvent', 'AliSourcePro', 'AliSourceProCompletedSignUp', 'Completed AliSourcePro Signup', 100, true]);
                            window.parent.log_action('3');
                        }
                    }
                }
                else {
                    $('.areyousure').removeClass('red').addClass('black');
                    $('.areyousure').html(data.message + btn);
                }
            }
        });
        event.preventDefault();
    });

    $('#try-again').live('click', function() {
        $('.areyousure').hide('fast');
        $('.frmAlibaba').show('fast');
    });

    $('#ok-button,.cancel-sourcing').live('click', function() {
        window.parent.$('#TB_closeWindowButton').trigger('click');
        window.parent.$('#alibaba-offer-widget-close').trigger('click');
    });

    $('#alibaba-offer-agree').live('click', function() {
        if(this.checked)
        {
            $('.b-bottom button').removeAttr('disabled');
        }
        else
        {
            $('.b-bottom button').attr('disabled',true);
        }
    });

    $('.show-info-collect').live('click', function(){
        $('#alibaba-info-collect').show();
    });

    $('.show-info-collect').live('click', function(e){
        e.preventDefault();
        $('#alibaba-info-collect').show();
    });

    $('#alibaba-info-collect-close').live('click', function(e){
        e.preventDefault();
        $('#alibaba-info-collect').hide();
    });
});

</script>