
<link rel="stylesheet" href="<?=resource_url("css/iscan2/promo.css")?>" />

<img src="<?=resource_url("media/promo/2.jpg")?>" align="left" style="margin-right:10px" width="240" height="360" />

<div class="prpanel" style="width: 340px;">

	<div class="promo" style="width: 340px;">
        <h1>
            <div style="font-size: 24px;">
            Upgrade to <br />
            <b style="color: blue">the Enterprise Account</b>
            </div>
        </h1>
        <h3>With the Enterprise Account You Can</h3>
        <ul type="circle" class="check" style="letter-spacing: 0px; margin-left: 0px;">
            <li>Expand from 50 to 100 searches per day</li>
			<li>Access Latin America Data</li>
			<li>Access India Data</li>
        </ul>
    </div>
    
    <?
	
	
	$user = $this->session->userdata('user');
	
	if (isset($log['log_date']))
		$created = date('j',strtotime($log['log_date']));
	else
		$created = date('j',strtotime($user['created']));
		
	$arb =  $created - date('d');
	if ($arb < 0) 
		{
		$arb = date('t') - date('d') + $created;
		}
		
	$dom = date('t');
	
	if ($arb==0) $arb = $dom;
	
	$prorate = (399/$dom*($dom-$arb)) + ((899/$dom)*$arb) - 399;
	$prorate = number_format($prorate,2);
	
	
	?>    
    
    <div class="areyousure" style="display:none; width: 300px">
       	<h2>If you agree to upgrade:</h2>
        <p>The following changes will be applied to your account in 1 business day</p>
        <ul class="check">
        <li><? if ($card) { ?>Your credit card ending in <b>(<?=substr($card['cardno'],strlen($card['cardno'])-4)?>)</b><? } else { ?>You<? } ?> will be charged the pro-rated amount of <strong>$<? echo $prorate ?> <a href="#prorate">*</a></strong> within 24 hours</li>
        <li>Thereafter, your monthly subscription on the <strong><?=$created?>th</strong> of each month will be <strong>$899</strong></li>
        </ul>
        
        <p>
        <b>Pro-rated calculation:</b><br />
        New monthly rate: $ 899.00<br />
        <b class='hlite' title="Formula: ((current rate / days of month ) * days since last bill) + ((new rate / [days of month]) * (days till next bill)) - already paid">Calculation</b>:  (($399 / <?=$dom?> days) * (<?=$dom-$arb?> days)) + ((899 / <?=$dom?> days * <?=$arb?> days)) - $399 
        <br />
        Pro-rate: $ <?=$prorate?>
        </p>


    </div>
    
    <div style="display:none" id="pmessage">

	<p><?=$user['username']?> accepted our offer to upgrade from Standard to Enterprise Account in order to access our new Visual Mapping technology.</p>
	<p>
	<b>Action is Required:</b> Please follow standard operating procedure for upgrading an account, including signing into the CMS to make the change and editing the ARB with our credit card processor.
	</p>
	
	<p>
	    	<b>TASK</b>: Administrator must also charge $<?=$prorate?> as a one-time fee for the 
	balance of this month, then change ARB to $899 and quickly 
	modify account to Enterprise type.
	</p>

        <p>
        <b>Pro-rated calculation:</b><br />
        New monthly rate: $ 899.00<br />
        <b class='hlite' title="Formula: ((current rate / days of month ) * days since last bill) + ((new rate / [days of month]) * (days till next bill)) - already paid">Calculation</b>:  (($399 / <?=$dom?> days) * (<?=$dom-$arb?> days)) + ((899 / <?=$dom?> days * <?=$arb?> days)) - $399 
        <br />
        Pro-rate: $ <?=$prorate?>
        </p>

	
    </div>
    
    
    <div class="prbuttons">
    <p><strong id="qverify">Upgrade Now?</strong></p>

<a href="#" rel="1" class="pryes"><span>Yes</span></a>
<a href="#" rel="3"><span>Remind me Later</span></a>
<a href="#" rel="2"><span>No Thanks</span></a>

<script type="text/javascript">
	$('.prbuttons a').click
	(
		function ()
			{

			var gurl = 'promo/post';
			var val = this.rel;
			var promoid = '7';
			
			if (val==1&&$('.areyousure:visible').length==0)
			{
				$('.areyousure').show();
				$('.promo').hide();
				$('#qverify').text('Are you ready to upgrade?');
				this.blur();
				return false;
			}
			
			$('.prbuttons').hide();
			
			if (val==1)
			{
				$('.areyousure').html('Processing...Please wait...');
				
				$.ajax({
				url: mod+gurl
				,type: 'POST'
				,dataType: 'json'
				,data: {response:val,promoid:promoid, pmessage: $('#pmessage').html()}
				,success: 
					function (data) 
					{
					
					//if (parent)
					//	parent.$('#TB_closeWindowButton').trigger('focus').trigger('click');
					$('.areyousure').html("<b>Upgrade request sent.</b><p>Thank you for upgrading from the Standard to the Enterprise (899) account type with ImportGenius.com.  We will process your request as soon as possible, so that you can start searching Latin America data and other features of your account.  </p><p>Please note that our office hours are Monday to Friday 8:30 am to 5:00 pm Eastern.  If you've upgraded your account outside those hours, it will be processed on the next business day.  If you're in a rush, feel free to give us a call at 855-573-9976 so we can accelerate the process.</p>");
					
					}							
				});			
			}
			else
			{
				$('.promo').show();
				$('.areyousure').hide();
			}
			
			return false;	
			
			}
	);
</script>


    </div>
    
    <div style="color: #777; margin-top: 10px; clear:both;">
    
    Current plan: <b>Standard Account</b>. Restricted to 50 searches. ($399/mo)
    
    </div>

</div>

