
					<tr>
						<td style="background: #bbb; color: #333; " valign="top">
							
							<b style="font-size: 18px; font-weight: bold;">Tip</b>
							<p>
							In order to avoid missing important correspondence from ImportGenius.com regarding your account, please add us to the Safe-Senders list in your email.
							</p>
							
						</td>
						<td style="background: #bbb; color: #333; " colspan="2" valign="top">
							<p>
							If you use Outlook 2007 perform the following steps:
							</p>
							<p>
							Click on
							</p>
							<ol>
							<li>Actions in the toolbar</li>
							<li>Junk Email</li>
							<li>Junk Email Options</li>
							<li>Add Sender's Domain (@importgenius.com) to Safe Sender's List</li>
							</ol>
							
						</td>
					</tr>
