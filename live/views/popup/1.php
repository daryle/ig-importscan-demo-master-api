

<img src="<?=base_url()?>media/promo/2.jpg" align="left" style="margin-right:15px" width="240" height="360" />

<div style="float: left; width: 325px;">

<span style="font-size: 24px; color: #677a89; letter-spacing: -1px;">Welcome to the new <b>ImportGenius.com</b></span>
<p>We've made some major updates, and we think you're going to like them.</p>

<span style="font-size: 18px; color: #c00; letter-spacing: -1px;">What's <b>new</b>?</span>

	<p>
	<b>Visual Mapping</b> -  Use our stunning new graphical interface to browse the entire universe of companies in your industry.  Search by any company name to be immersed in a visual map of their trading connections.
	</p>
	<p>
	<b>e-Mail Alerts</b> - Subscribe to the results of any search in our new e-mail alerts tab. Choose to receive e-mails anytime a competitor imports a product, or whenever there are new shipments containing product keywords you specify.
	</p>
	<p>
	<b>Search History</b> - Now your recent searches will be saved automatically, so you'll have quick access to the data that interests you most.
	</p>
	<p>
	Dive in and tell us what you think.
	</p>

</div>