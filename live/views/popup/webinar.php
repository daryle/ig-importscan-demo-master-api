<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Splash Screen</title>
<link href="<?=base_url()?>help/css/styles.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>help/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
	<style type="text/css">
    div#Container .Top-Head, div#Container .Top-Head .close, div#Container .Main-Content, div#Container .Main-Content .Right-Content div.Action-Button, span.ImportersIcon, span.LogisticsIcon, span.OverseasIcon, span.InvestorsIcon, div#Container #BottomCurve, img { behavior: url(images/iepngfix.htc) }
	</style>
<![endif]-->
</head>



<body>

<!-- Start of Wrapper -->
<div id="wrapper">
	
	<!-- Start of Content Wrapper -->
	<div id="Container">

		<div class="Top-Head">
			<div class="back-link"><a href="javascript:history.back()">Back</a></div>
		</div>

		
		<!-- Start of Main Content -->		
		<div class="Main-Content">
			<div class="Title-Head"><h2>Getting Started with Importgenius.com</h2></div>
			
			<!-- Start of Left Content -->
			<div class="Left-Content">
				<div class="TitleNum1"><h2>Watch Video Introduction</h2></div>
				<div class="clearer"></div>
				<div class="Video">
				<object type="application/x-shockwave-flash"
                data="http://www.youtube.com/v/-zu0Vp_pNDc?fs=1&amp;hl=en_US" width="339" height="274">
               <param name="movie" value="http://www.youtube.com/v/-zu0Vp_pNDc?fs=1&amp;hl=en_US" />
			   </object>					
			
</div>
				
			</div>
			<!-- End of Left Content -->
			
			<!-- Start of Right Content -->
			<div class="Right-Content">
				<div class="TitleNumHome"><h2>Webinar Request - Processed</h2></div>
				<div class="clearer"></div>
				
				<div class="List-Content">
				<p>
				We host webinars every week to help you get the most out of your Import Genius account.
				</p>
				<p>
				One of our trade data specialists will contact you shortly to schedule a time that works for you.
				</p>
				<p>
				In the meantime, give us a call if you have any questions.
				</p>
				
				</div>
				
			</div>	
			<!-- End of Right Content -->
				
			
			<div id="Bootom-Container">
				<div class="check-box">
					<span><input name="everytime" type="checkbox" value="1" id="everytime" /></span>
					<span><label for="everytime">Show this everytime I login</label></span>
				</div>
				
				<div class="Bottom-Link"><b>Still confused?</b> <span class="BottomLink"> <a href="../is/main/webinar">Attend a Webinar</a> |<a href="http://www.importgenius.com/contact" target="_parent">Contact Us</a></span></div>
			<div class="clearer"></div>
			</div>	
		
			
		
		</div>
		<!-- End of Main Content -->
 
	</div>
	<!-- End of Content Wrapper -->
	

	

</div>
<!-- End of Wrapper -->
<script type="text/javascript" src="../lib/jquery/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>help/script.js"></script>
</body>
</html>