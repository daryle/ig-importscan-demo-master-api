	    
    <style>
			.frow label { width: 100px; }
			.fpane
			{
			border: 1px solid #ccc;
			padding: 10px;
			display: none;
			}
			.ptabs
				{
				height: 27px;
				position: relative;
				z-index: 2;
				}
			.ptabs a
				{
				float: left;
				padding: 6px 10px;
				height: 14px;
				color: #000;
				background: #ddd url(images/topbg.png) repeat-x;
				border: 1px solid #ccc;
				border-bottom: 0px;
				margin-right: -1px;
				font-weight: bold;
				font-size: 11px;
				text-transform: uppercase;
				white-space: nowrap;
				}
			
				.ptabs a:hover
					{
					background: #ddd;
					height: 15px;
					}	
			
				.ptabs a.sel
					{
					background: #ebf3fa;
					height: 15px;
					}
				table.tbl_countries
				{
					width: 100%;
					border-collapse: collapse;
					border: 1px solid #ddd;
				}
				
				table.tbl_countries th
				{
					padding: 6px;
					background: #ddd;
				}
				
				table.tbl_countries td
				{
					padding: 0 6px;
				}
				
				table.tbl_countries th,
				table.tbl_countries td
				{
					text-align: left;
					border-bottom: 1px solid #ddd;
				}
				
				table.tbl_countries td.right,
				table.tbl_countries th.right
				{
					text-align: right;
				}
				.cart
				{
					float: right;
					display: block;
					width: 250px;
					height: 25px;
					line-height: 25px;
				
					border-radius: 5px;
					color: #000;
					font-size: 12px;
					text-align: center;
					font-weight: normal;
					text-shadow: -1px 1px #fff;
					display: none;
				}
				.cart button
				{
					margin: 0 5px;
				}
				
				.xpreloader
				{
					background: #fff url(<?=resource_url('css/iscan2/images/vload.gif')?>) no-repeat center;
					width: 100%;
					height: 100%;
					top: 0;
					left: 0;
					position: absolute;
					opacity: 0.6;
					display: none;
				}
				
				button:disabled
				{
					color: green;
				}
	</style>
	
	
	<div class="fpane cartpane" style="position: relative;">
		<div class="xpreloader">
		
		</div>
		<div class="countries">
			
			<h3>Countries in My Account</h3>
			<ul>
				<li>U.S. Import Data</li>
				<?php foreach($acountries as $ac){?>
				<li><?=$ac['country_name']?> - Imports </li>
				<? } ?>
			</ul>
			
			<?php 
				$ccontents = $this->cart->contents();
				$items = $this->cart->total_items();
				$c = array();
				$showcart = 'none';
				
				foreach($ccontents as $cc)
				{
					$c[] = $cc['id'];
				}
				
				if(count($ccontents) > 0)
				{
					$showcart = 'block';
				}
				
			?>
			
			<div class="all_countries">
				<h3>
					Add Countries & Data Sets 
					<!--
					<span class="cart" style="display: <?=$showcart?>; width: 330px">You have <b><?=$items?></b> selected countries. <button name="view_cart">View Countries</button></span>
					-->
				</h3>
				<table class="tbl_countries">
				<thead>
					<tr>
						<th>Countries</th>
						<th class="right">Price</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					
						<td width="70%">All Countries Below</td>
						<td class="right" width="20%"><b>$199.00</b></td>
						<td align="right" width="10%"><button class="btn_cart2 itemid0" name="0">Add</button></td>
					</tr>

					<?php foreach($countries as $country) { ?> 
					
						<?php if($country['country_id'] != 8) {?> 

							<tr>
								<td><?=$country['country_name']?> - Imports </td>
								<td class="right">$99.00</td>
								<?php if(in_array($country['country_id'],$c)) { ?> 
									<td align="right"><button class="btn_cart itemid<?=$country['country_id']?>" name="<?=$country['country_id']?>" disabled="disabled">Add</button></td>
								<? } else {?> 
									<td align="right"><button class="btn_cart itemid<?=$country['country_id']?>" name="<?=$country['country_id']?>">Add</button></td>
								<? } ?>
								
							</tr>
						
						<? }?>
					
					<? }?>
				
				</tbody>
			</table>
			</div>
			
			<div class="shopping_cart" style="display: none">
				<h3>Items in Your Cart
					<span style="float: right;"> <button name="go_back">Go Back</button></span>
				</h3>
				<table class="tbl_countries">
				<thead>
					<tr>
						<th>Countries</th>
						<th class="right">Price</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody id="allitems">
					
				</tbody>
				</table>
			</div>

			
			<!--<div class="note">
				<p>You're one click away from adding "Country" Trade Data to my Import Genius. Just type your name here to consent to allowing us to charge an additional <b>$<span class="content_price"><?=$carttotal?></span></b> per month to the card on file.</p>
				<p>Your Name : <input type="text" name="fullname"/> <button name="accept"<?=$carttotal > 0 ? '' : 'disabled="disabled"'?>>Accept</button></p>
			</div>-->
			
			<div class="note">
				<p><input type="checkbox" name="agreement"/>I agree that my card will be charged an additional <b>$<span class="content_price"><?=$carttotal?></span></b> per month for access to the Import Genius database of shipping manifests <span class="scountries"></span></p>
				<p>
					<button type="submit" class="button"><span><b>Learn More</b></span></button>
					<button name="accept"<?=$carttotal > 0 ? '' : 'disabled="disabled"'?> class="button" type="button"><span><b>Add to My Account</b></span></button>
					<button type="button" name="startover" class="button"><span><b>Start Over</b></span></button>
					<button type="button" name="goback" class="button"><span><b>Go Back</b></span></button>
				</p>
			</div>
			
			
			
		</div>
		<div class="confirmation" style="display: none">
			<p>A Whole New World of Trade Awaits.
				We've just added <span class="scountries"></span> trade data to your Import Genius account.  You can switch between countries with the dropdown menu at the top of the application.  Thanks for your business, please let us know if we can help! 
			</p>
			<p><b>TIP:</b> The shipping manifests are written in the local language in each country, so when you search by product be sure to use the Spanish word in Latin America.</p>
			<br/>
			<button type="button" name="reloadpage" class="button"><span><b>Reload Page</b></span></button>
		</div>
	</div>
	
	<div class="splash-wrapper">
		<div class="splash-image">
			

		</div>

		<div class="showsplash">
			<input type="checkbox" name="splashOption" alt="<?=site_url('splash/save')?>"/> Do not show this message again.
		</div>

		<div style="text-align: center; padding: 4px 0">
			<button type="button" class="button upgrade_now"><span><b>Upgrade Now</b></span></button>
			<button type="button" class="button"><span><b>Learn More</b></span></button>
		</p>
	</div>

<script>
 
$('button[name=reloadpage]').click(function(e){
	parent.location.reload();
})
 
 $('button[name=accept]').click(function(e){
	var agreement = $('input[name=agreement]:checked');
	
	if(agreement.length == 0)
	{
		alert('Agreement field is required');
		return false;
	}
	else
	{
		$.ajax({
			url: '<?=site_url('ccart/process_order')?>',
			dataType: 'json',
			type: 'post',
			success: function(data)
			{
				try
				{
					if(data)
					{
						if(data.action == "success")
						{
							$('.scountries').html(data.scountries);
							$('.countries').hide();
							$('.confirmation').show();
						}
					}
				}
				catch(e)
				{
				
				}
			}
		})
	}
 });
 
 
$('button[name=startover]').click(function(e){
	e.preventDefault();
	
	$.ajax({
		url: '<?=site_url('ccart/remove_items')?>',
		dataType: 'json',
		type: 'post',
		success: function(data)
		{
			$('.btn_cart').removeAttr('disabled');
			$('.btn_cart2').removeAttr('disabled');
			$('.content_price').html(0);
			$('.scountries').html('');
			if(data.total > 0)
			{
				$('button[name=accept]').removeAttr('disabled');
			}
			else
			{
				$('button[name=accept]').attr('disabled','disabled');
			}
		}
	})
	
})

$('.xpreloader').ajaxStart(function(e){
	$(this).show();
})

$('.xpreloader').ajaxStop(function(e){
	$(this).hide();
})
 
 $('button[name=view_cart]').click(function(e){
	$('.all_countries').fadeOut('fast',function(){
		$('.shopping_cart').fadeIn('fast');
		
		$.ajax({
			url: '<?=site_url('ccart/get_items')?>',
			dataType: 'json',
			type: 'post',
			success: function(data)
			{
				try
				{
					if(data)
					{
						var i = data.items;
						var html = "";
						var html2 = "";
						$('#allitems').html('');
						
						$.each(i,function(x,y){
							//console.log(y);
							html = "";
							html += "<tr id='"+y.rowid+"'>";
							html += 	"<td width=\"70%\">";
							html += 		y.name + ' Imports & Exports';
							html += 	"</td>";
							html += 	"<td class='right' width=\"20%\">";
							html += 		'$'+y.price;
							html += 	"</td>";
							html += 	"<td class='right' width=\"10%\">";
							html += 	"<button class='btn_remove itemid"+y.id+"'  name=\""+y.rowid+"\">Remove</button>";
							html += 	"</td>";
							html +="</tr>";
							
							$('#allitems').prepend(html);
						})
						
						html2 = "";
						html2 += "<tr colspan='2'>";
						html2 += "<td class=\"right\">Total</td>";
						html2 += "<td class=\"right\" id=\"total_amount\">$"+data.total+"</td>";
						html2 += "<td class=\"right\" style='padding: 5px 0;'>&nbsp;</td>";
						html2 += "</tr>";
						$('#allitems').append(html2);
						
						$('.content_price').html(data.total);
					}
				}
				catch(e)
				{
				
				}
			}
		})
		
	});
	
 })
 
 $('button[name=go_back]').click(function(e){
	$('.shopping_cart').fadeOut('fast',function(){
		$('.all_countries').fadeIn('fast');
	});
 })
 
 $('button.btn_remove').live('click',function(e){
	var rowid = $(this).attr('name');
	var cls = $(this).attr('class');
	var cls = cls.split(' ');
	
	$('.'+cls[1]).removeAttr('disabled');
	
	if(cls[1] == 'itemid0')
	{
		$('.btn_cart').removeAttr('disabled');
	}

	$.ajax({
		url: '<?=site_url('ccart/remove_item')?>',
		dataType: 'json',
		data: {rowid:rowid},
		type: 'post',
		success: function(data)
		{
			$('#'+rowid).fadeOut('slow',function(){
				$('#'+rowid).remove();
			})
			$('.cart b').html(data.items);
			$('.content_price').html(data.total);
			$('#total_amount').html('$'+data.total);
			
			if(data.total > 0)
			{
				$('button[name=accept]').removeAttr('disabled');
			}
			else
			{
				$('button[name=accept]').attr('disabled','disabled');
			}
		}
	})
	
 })
 
 $('.btn_cart, .btn_cart2').live('click',function(){
	var id = $(this).attr('name');
	var cls = $(this).attr('class');
	var cls = cls.split(' ');

	$(this).attr('disabled','disabled');
	if(cls[0] == "btn_cart2") 
		$('.btn_cart').attr('disabled','disabled');
	
	$.ajax({
		url: '<?=site_url('ccart/post_order')?>',
		dataType: 'json',
		data: {country_id : id },
		type: 'post',
		success: function(data)
		{
			//console.log(data);
			try
			{
				if(data)
				{
					if(data.items)
					{
						$('.cart').slideDown('fast');
						
						$('.cart b').slideUp('fast',function(){
							$('.cart b').slideDown('fast').html(data.items);
						});
						
						$('.content_price').html(data.total);
						
						if(data.total > 0)
						{
							//if(parseInt(data.total) == 198)
							//{
							//	$('.btn_cart2').trigger('click');	
							//}
							$('button[name=accept]').removeAttr('disabled');
							$('.scountries').html('from '+data.scountries);
						}
					}
				}
			}
			catch(e)
			{
			
			}
		}
	})
 
 })
 
	$('button[name=goback]').click(function(e){
		$('.cartpane').hide();
		$('.splash-wrapper').show();
	})
 
 </script>   
  
