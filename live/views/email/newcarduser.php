
<p>Thank you for providing new credit card information to ImportGenius.com. We will activate your account within one business day and then contact you when it is live. In the meantime, if you have any questions, you can contact us anytime at 1-888-843-0272 or by e-mailing info@importgenius.com.</p>

<b>Account Details</b>
<p>
	User Name : <?=$user['username']?><br />
    Account Type : <?=$user['atype']?>
</p>

<p>
	Your transaction reference number is : <b><?=$card['refno']?></b>.
</p>

<b>New Credit Card Details</b>
<p>
    Fullname: <?=$card['firstname']?> <?=$card['lastname']?><br />
    Email: <?=$user['email']?><br />
    Street Address: <?=$card['street']?><br />
    City: <?=$card['city']?><br />
    State: <?=$card['state']?><br />
    Zip: <?=$card['zip']?><br />
    Credit Card: <?=$card['ctype']?><br />
    Credit Card No.: ****-****-****-<?=substr($card['cardno'],strlen($card['cardno'])-3)?><br />
    Expiration (mm-yy): <?=$card['expiry']?>
</p>

<p>    
	Thank you.
</p>

<p>
	<b>ImportGenius.com</b>
</p>