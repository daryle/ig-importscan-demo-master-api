<p>Hi,</p>
<p>
Thank you for upgrading from the Trial account type with ImportGenius.com.  We will process your request as soon as possible, so that you can start using our new Visual Mapping technology and other features of your account.  Please note that our office hours are Monday to Friday 8:30 am to 5:00 pm Eastern.  If you've upgraded your account outside those hours, it will be processed on the next business day.  If you're in a rush, feel free to give us a call at 1-888-843-0272 so we can accelerate the process.  
</p>
<p>
Best regards,
</p>
<p>
The Import Genius Team<br />
www.importgenius.com<br />
info@importgenius.com<br />
888-843-0272
</p>