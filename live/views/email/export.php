Hi <?=$firstname?>,

<p>
We've attached the data that you requested from our system.
</p>

<p>
If you have any trouble viewing this file, please contact us through the orange "Customer Service" button or by using the info provided below.
</p>

<p>
Thank you for using ImportGenius!
</p>
