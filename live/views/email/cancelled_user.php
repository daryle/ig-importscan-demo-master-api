<p>
Hi,
</p>

<?
					$stype = "cancelled";
					if ($suspended==1) $stype = "suspended";
?>

<p>
A <?=$stype?> user just tried signing into their account.
Please call this person to try and win back their business.
</p>

<p><a href="https://core.importgenius.com/ccards#<?=$username?>">https://core.importgenius.com/ccards#<?=$username?></a></p>

<b>Account Details</b>
<p>
    User Name : <?=$username?><br />
    First Name : <?=$firstname?><br />
    Last Name : <?=$lastname?><br />
    Account : <?=$atype?><br />
    Phone : <?=$phone?><br />
    Email : <?=$email?><br />
    Business Name : <?=$business?><br />
    Signed Up : <?=$created?><br />
    <? if ($suspended!=1) { ?>
    Cancelled : <?=$datecancelled?><br />
    <? } else if (isset($suspend_date)) { ?>
    Suspended : <?=$suspend_date?><br />
    <? } ?>
</p>

<p>
Thanks,
</p>

<p>
Your Friendly ImportGenius.com Robot
</p>