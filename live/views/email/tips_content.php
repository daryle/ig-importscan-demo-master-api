<p>Hi <?=$firstname?>,</p>

<p>Welcome to ImportGenius.com. The content below contains some tips and tricks we've found helpful when using our database.</p>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
<tr>
<td align="left" valign="middle" bgcolor="#f9f9f9"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Use Appropriate Categories</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
    <li>Baseball gloves - search under Products, Pottery Barn- search under Consignee.</li></ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Use asterisk (*) As A Wild Card</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>If you do a search for "bat*" you will get results for bat, bats, battery, batman, bathroom, batch, baton, battalion, etc.</li>
      <li>Without the * you would only get results for "bat". - It also helps if you are not sure how to spell a word or company name.</li>                                      
    </ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle" bgcolor="#f9f9f9"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Do not use the full company name when doing a search</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>If you search for Awesome Designs Ltd, you will not get results for Awesome Designs Limited.</li>
    </ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Always do a second search for the company address</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>You may find additional results for the same company, but under a different name.</li>
    </ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle" bgcolor="#f9f9f9"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Do not use the complete address</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>Use as few words as possible to generate more results</li>
    </ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">To Eliminate too many search results:</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>Use a 2nd search filter by clicking on the green + sign at the end of the search criteria.</li>
      <li>Add a 2nd filter and choose "not" rather than "and" then the product description.</li>
    </ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td width="12" align="left" valign="middle" bgcolor="#f9f9f9"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Calendar default is a 1 year search</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>Enterprise Plan Members - be sure to extend your dates to give you more results.</li>
      <li>Limited Plan Members - default is 45 days and cannot be changed.</li>
    </ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Visual Mapping Tool</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>Available to Standard and Enterprise Plan Members</li>
      <li>Choose consignee or shipper</li>
      <li>Enter company name</li>
      <li>Instantly view a company's trade partners</li>
    </ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle" bgcolor="#f9f9f9"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Email Alerts</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>Available to Standard and Enterprise Plan Members</li>
      <li>After performing a search , save and subscribe to that search to be notified when shipments matching your search 
criteria are uploaded into our system</li>
    </ul></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Download your search results to Excel or in CSV format to manipulate as necessary</td>
      </tr>
    </table></td>
  </tr>
</table></td>
</tr>
<tr>
<td align="left" valign="middle" bgcolor="#f9f9f9"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
        <td align="left" valign="middle" style=" font-size:13px; color:#4168aa; font-weight:bold;">Having trouble getting results you desire?</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    <ul style="margin:0px; padding:0px; padding-left:13px;">
      <li>Perhaps the plan you subscribe to does not give enough access to the data.</li>
      <li>Ensure the import records you wish to access are U.S. ocean freight import records.</li>
    </ul></td>
  </tr>
</table></td>
</tr>
</table>
<p>
You may contact us any time if you have questions or trouble finding what you're looking for.
</p>

<p>
U.S.: 888-843-0272<br />
International: 202-595-3101<br />
LiveChat: Upper right corner of the application<br />
Email: info@importgenius.com<br />
</p>