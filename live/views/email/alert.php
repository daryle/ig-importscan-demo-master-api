<p>Hi,</p>
<p>
A new shipment just triggered your e-mail alert for "<?=$su_name?>" in the ImportGenius.com database.
</p>

<? if (isset($data_entry) && $cname == 'United States') {?>


<table style="text-align: left; margin: auto; font-family: Arial; font-size: 12px;  color:#333">
<tr><td width="120px;" valign="top">Date of Arrival:</td><td valign="top"> <?=$data_entry['actdate']?></td></tr>
<tr><td valign="top">Consignee:</td><td valign="top"><?=$data_entry['consname']?></td></tr>
<tr><td valign="top">Consignee Address: </td><td valign="top"><?=$data_entry['consaddr']?></td></tr>
<tr><td valign="top">Shipper:</td><td valign="top"><?=$data_entry['shipname']?></td></tr>

<tr><td valign="top">Shipper Address:</td><td valign="top"> <?=$data_entry['shipaddr']?></td></tr>
<tr><td valign="top">Product:</td><td valign="top"><?=character_limiter($data_entry['product'],200)?></td></tr>
<tr><td>Weight: </td><td valign="top"><?=number_format($data_entry['weight'],0)." ".$data_entry['weightunit']?></td></tr>
<tr><td valign="top">Number of Units:</td><td valign="top"> <?=number_format($data_entry['manifestqty'],0)." ".$data_entry['manifestunit']?></td></tr>
<tr><td valign="top">US Port:</td><td valign="top"> <?=strtoupper($data_entry['uport'])?></td></tr>
<tr><td valign="top">Foreign Port:</td><td valign="top"> <?=strtoupper($data_entry['fport'])?></td></tr>
</table>
<? } ?>

<p>
To view more details for this shipment:
</p>

<ol>
<li>Login to: <a href="http://app.importgenius.com/">http://app.importgenius.com/</a></li>
<li>Go to Email Alerts tab</li> 
<li>Select <b><?=$su_name?></b>, then click Search</li>
</ol>

<p>
Thanks for subscribing!
</p>
<!--
<p>
<b>ImportGenius</b><br />
<a href="http://www.importgenius.com/">ImportGenius.com</a><br />
<a href="mailto:info@importgenius.com">info@importgenius.com</a>
</p>
-->