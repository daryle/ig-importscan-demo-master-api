<p>Hi,</p>
<p>
A new shipment just triggered your e-mail alert for "<?=$su_name?>" in the ImportGenius.com database.
</p>



<table style="text-align: left; margin: auto; font-family: Arial; font-size: 12px;  color:#333; width: 100%">

<?php if(isset($data_entry['adate']) && $data_entry['adate']){?>
	<tr><td width="120px;" valign="top">Date of Arrival:</td><td valign="top"> <?=$data_entry['adate']?></td></tr>
<?php } ?>

<?php if(isset($data_entry['consname']) && $data_entry['consname']) {?> 
	<tr><td valign="top">Consignee:</td><td valign="top"><?=$data_entry['consname']?></td></tr>
<?php } ?>

<?php if(isset($data_entry['consaddr']) && $data_entry['consaddr']) { ?> 
	<tr><td valign="top">Consignee Address: </td><td valign="top"><?=$data_entry['consaddr']?></td></tr>
<?php } ?>

<?php if(isset($data_entry['shipaddr']) && $data_entry['shipaddr']) {?> 
	<tr><td valign="top">Shipper:</td><td valign="top"><?=$data_entry['shipname']?></td></tr>
<?php } ?>

<?php if(isset($data_entry['shipaddr']) && $data_entry['shipaddr']) {?> 
	<tr><td valign="top">Shipper Address:</td><td valign="top"> <?=$data_entry['shipaddr']?></td></tr>
<?php } ?>

<?php if(isset($data_entry['product']) && $data_entry['product']) { ?> 
<tr><td valign="top">Product:</td><td valign="top"><?=character_limiter($data_entry['product'],200)?></td></tr>
<?php } ?>


<?php if(isset($cfields) && $cfields) { ?>

	<?php foreach($cfields as $cf){?>
		<?php if( isset($data_entry[$cf[0]]) && $data_entry[$cf[0]] != "" && ! in_array($cf[0],array('entryid','product','consname','shipname','adate'))) {?>
		<?php //if($data_entry[$cf[0]] != "" && $cf[0] != "entryid" && $cf[0] != "product" && $cf[0] != "consname" && $cf[0] != "shipname" ){ ?>
			<tr>
				<td>
				<?=$cf[1]?>
				</td>
				<td>
				<?php if($data_entry[$cf[0]] != '0'){?>
				<?=iconv("UTF-8", "UTF-8//IGNORE",$data_entry[$cf[0]])?>
				<? } else { ?>
				-
				<? } ?>
				</td>
			</tr>
		  <? } ?>

	<? } ?>

<? } ?>

</table>

<p>
To view more details for this shipment:
</p>

<ol>
<li>Login to: <a href="http://app.importgenius.com/">http://app.importgenius.com/</a></li>
<li>Go to Email Alerts tab</li> 
<li>Select <b><?=$su_name?></b>, then click Search</li>
</ol>

<p>
Thanks for subscribing!
</p>
<!--
<p>
<b>ImportGenius</b><br />
<a href="http://www.importgenius.com/">ImportGenius.com</a><br />
<a href="mailto:info@importgenius.com">info@importgenius.com</a>
</p>
-->