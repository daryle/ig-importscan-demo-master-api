<p>Team,</p>

<p>A customer has requested a training session to learn more about how they can use Import Genius.</p>
<br/>
<a href="http://core.importgenius.com/ccards#<?=$username?>">http://core.importgenius.com/ccards#<?=$username?></a>
<br/>

<p>
    Company: <?=$business?><br />
	Contact Name: <?=$firstname?> <?=$lastname?><br />
    Email: <?=$email?><br />
	Phone: <?=$phone?><br />
    <?php if(isset($timezone['name'])){ ?>
    Timezone: <?=$timezone['name']?>
    <? } ?>
</p>	

<p>Requested Time/Date: <?=$schedule?></p>
<p>Note: <?=nl2br($datainterest)?>

<br/>
<br/>

<p>
	Thanks! 
	<br/>
	<br/>
	<b>ImportGenius.com</b>
</p>
