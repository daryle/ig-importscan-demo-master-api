			<?
			
			$dlink = "http://s6.importgenius.com/iscan4l.php/export/download/".time()."/$p_id";
			$fz1 = fsize($fz1);
			$fz2 = fsize($fz2);
			$fname = $file.".".$format;
			
			?>
			
			<b>This exported report is from:</b>
			<p style="padding:5px; background: #eee"><?=$qdesc?></p>
			
			<p>Export Range: From results <?=$from+1?> to <?=$from+$to?></p>
			
			<p>Your export was generated in <?=toTime($ltime-$extime)?>.</p>

			<p><b>Download</b> your report by <b>clicking the file name</b> below:</p>
			
			<table width='400' cellpadding="5" cellspacing="0">
			<tbody>
			<tr>
				<td><a href='<?=$dlink?>' class='<?=$format?>' ><?="$fname"?> </a></td> 
				<td><?=$fz1?></td>
			</tr>
			<tr>
				<td><a href='<?=$dlink?>/zip' class='zip' ><?=$fname?>.zip </a></td> 
				<td><?=$fz2?></td>
			</tr>
			</tbody>
			</table>
			
			<p>If you are prompted to sign-in, please follow the instructions below:</p>
			
			<ol>
				<li>Enter your username and password</li>
				<li>Click 'Export/Email Report link</li>
				<li>Click the format you wish to download</li>
			</ol>
			
			<br /><br />
