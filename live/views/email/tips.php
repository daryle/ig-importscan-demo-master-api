<p>Hi <?=$firstname?>,</p>

<p>
Welcome to ImportGenius.com.  The attached PDF contains some tips and tricks we've found helpful when using our database.
</p>
<p>
You may contact us any time if you have questions or trouble finding what you're looking for.
</p>

<p>
U.S.: 888-843-0272<br />
International: 202-595-3101<br />
LiveChat: Upper right corner of the application<br />
Email: info@importgenius.com<br />
</p>

<p>
Thanks,<br /><br />

ImportGenius.com<br />
Customer Success Team<br />
info@importgenius.com
</p>