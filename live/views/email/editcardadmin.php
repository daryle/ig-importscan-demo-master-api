<p>We have a current user that provided us with updated credit card details</p>

<p><a href="https://core.importgenius.com/ccards#<?=$user['username']?>">https://core.importgenius.com/ccards#<?=$user['username']?></a></p>


<b>Account Details</b>
<p>
	User Name : <?=$user['username']?><br />
    Account Type : <?=$user['atype']?><br />
    Website : <?=$user['website']?>
</p>

<p>
	Reference number is : <b><?=$card['refno']?></b>.
</p>

<b>Credit Card Details</b>
<p>
    Fullname: <?=$card['firstname']?> <?=$card['lastname']?><br />
    Email: <?=$user['email']?><br />
    Street Address: <?=$card['street']?><br />
    City: <?=$card['city']?><br />
    State: <?=$card['state']?><br />
    Zip: <?=$card['zip']?><br />
    Credit Card: <?=$card['ctype']?><br />
    Credit Card No.: ****-****-****-<?=substr($card['cardno'],strlen($card['cardno'])-3)?><br />
    Expiration (mm-yy): <?=$card['expiry']?>
</p>

<p>
	<b>ImportGenius.com</b>
</p>