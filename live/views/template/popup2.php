<?
$this->output->set_header("Content-type: text/html");
$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
$this->output->set_header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate" );
$this->output->set_header("Cache-Control: post-check=0, pre-check=0",false);
$this->output->set_header("Pragma: no-cache" );
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-itunes-app" content="app-id=781279942"/>
<title>ImportScan</title>

<?

		$links[] = array(
			'href'=>static_url("mini/css/entry/".mtime('css','entry'))
			,'rel'=>'stylesheet'
		);

		foreach ($links as $link) echo link_tag($link);
?>
<!--[if IE]>
<link rel="shortcut icon" href="<?=base_url();?>favicon.ico" type="image/x-icon" />
<![endif]-->
<link rel="shortcut icon" href="<?=base_url();?>favicon.ico" type="image/x-icon"/>

<script type="text/javascript">var root = '<?=base_url()?>'; var mod = '<?
if (!isset($mod)) $mod = site_url('');

if (substr($mod,-1)!='/') $mod .= '/';

	echo $mod;

?>';

var tb_pathToImage = "<?=resource_url("css/iscan2/images/tload.gif")?>";


</script>

<? if (!isset($jsgroup)) $jsgroup = 'entry'; ?>
<script type="text/javascript" src="<?=static_url("mini/js/$jsgroup/".mtime('js',$jsgroup))?>"></script>

</head>

<body class>
            <div class="errorbox" style="display:none" >
            		<div class="errortitle">Error Message</div>
            		<div class="errorcontent"></div>
                    <div class="errorcontrol">
                    	If this error is unexpected please email us.
                    	<button type="button" class="button" onclick="location.reload()"><span><b>Reload Page</b></span></button>
                    	<button type="button" class="button" onclick="$(this).parents('.errorbox').hide()"><span><b>Close</b></span></button>
                    </div>
            </div>



<? if (isset($content)) echo $content ?>



</body>

</html>
