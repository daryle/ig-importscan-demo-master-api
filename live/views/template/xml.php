<?

	$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" ); 
	$this->output->set_header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" ); 
	$this->output->set_header("Cache-Control: no-cache, must-revalidate" ); 
	$this->output->set_header("Pragma: no-cache" );
	$this->output->set_header("Content-type: text/xml");

	$this->load->plugin('str');
	
	$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	$xml .= "<rows>";
				
		$xml .= "<page>$page</page>";
		$xml .= "<total>$total</total>";
		
		foreach ($db as $row) {
			foreach ($row as $fld)
			{
				list($v1) = each($row);
				if ($row[$v1]=='') $row[$v1] = 'N/A';
			}
			$url = $row['tbname'];
			$xml .= "<row id='".$row['entryid']."'>";
			$xml .= "<cell><![CDATA[<a href='".site_url('main/entry/'.$url.'/'.$row['entryid'])."' class='view' title='View Record ".$row['entryid']." - ".$row['consname']."'></a>]]></cell>";		
			$xml .= "<cell><![CDATA[".hlite($row['consname'],'consname')." ]]></cell>";		
			$xml .= "<cell><![CDATA[".hlite($row['shipname'],'shipname')."]]></cell>";		
			$xml .= "<cell><![CDATA[".$row['actdate']."]]></cell>";		
			$xml .= "<cell><![CDATA[".hlite($row['product'],'product')."]]></cell>";		
			$xml .= "<cell><![CDATA[".$row['weight']." ".$row['weightunit']."]]></cell>";		
			$xml .= "<cell><![CDATA[".strtoupper(hlite($row['fport'],'fport'))."]]></cell>";		
			$xml .= "<cell><![CDATA[".strtoupper(hlite($row['uport'],'uport'))."]]></cell>";		
			$xml .= "</row>";		
		}
		

	$xml .= "</rows>";

	echo $xml;	

?>