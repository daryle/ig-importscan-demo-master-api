<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<title><? if (isset($subject)) echo $subject; ?></title>
	</head>
	<body>
	<div style="text-align: center; padding: 40px; background: #90BDDF;">
			<table cellpadding="20" cellspacing="0" width="700" style="text-align: left; margin: auto; font-family: Arial; font-size: 12px;">
				<tbody>
					<tr>
						<td style="font-size: 24px; color: #fff; background: #3D3D3D; -moz-border-radius-topleft: 10px; border-top-left-radius: 10px;" colspan="2">
							<?=$subject?>
						</td>
						<td style="background: #3D3D3D; -moz-border-radius-topright: 10px; border-top-right-radius: 10px;">
							<a href="http://www.importgenius.com">
							<img src="http://static.importgenius.com/resources/site/live/css/default/images/logo.jpg" alt="ImportGenius.com" title="ImportGenius.com" border="0" />
							</a>
						</td>
					</tr>
					<tr>
						<td style="background: #ccc; color: #333" colspan="3">
							Import Genius : Empowering Information
						</td>
					</tr>
					<tr>
						<td style="background: #fff; color: #555" valign="top" colspan="3">
							
							
							<?=$content?>
							
							<p>Sincerely,</p><br />
							<p>
							<b>The Import Genius Team</b><br />
							<a href="http://www.importgenius.com" style="text-decoration:none; color: #2176B9;">www.ImportGenius.com</a><br />
							<a href="mailto:info@importgenius.com" style="text-decoration:none; color: #2176B9;">info@importgenius.com</a><br />
							Phone: 855-573-9976<br />
							International: +1 480-745-3396
							</p>
						
						</td>
					</tr>
					<? $this->load->view('tiles/email_tip'); ?>
					<tr>
						<td style="background: #3d3d3d; color: #ccc;" valign="top" width="40%">
						
									<b>Customer Support Team</b> : <br /><br />

									Tel: <b><span style="color:#ccc">1-855-573-9976</span></b><br/>
									International: <span style="color:#ccc">+1 480-745-3396</span><br/>
									Fax: <span style="color:#ccc">1-480-245-5000</span><br/>
									<span style="color:#ccc">info@importgenius.com</span><br /><br />								

									9150 Del Camino Rd Suite 116<br />
									Scottsdale, AZ 85258<br />
									United States of America								
						</td>
						<td style="background: #3d3d3d; color: #ccc;" valign="top" width="30%">
									<b>Shanghai, China</b> :<br/><br />
									Tel: <span style="color:#ccc">+86-21-63806036</span><br/>
									Mobile No.: <span style="color:#ccc">+86-13671737628 china@importgenius.com</span>						
						</td>

						<td style="background: #3d3d3d; color: #ccc;" valign="top" width="30%">

									<b>Corporate Headquarters</b><br /><br />
									DBase VI, LLC<br />
									One Hibiscus Alley<br />
									St. Thomas 00802<br />
									US Virgin Islands<br />

						</td>
					</tr>
					<tr>
						<td colspan="3" style="background: #222; color: #fff; -moz-border-radius-bottomleft: 10px; border-bottom-left-radius: 10px; -moz-border-radius-bottomright: 10px; border-bottom-right-radius: 10px;">
								All Rights Reserved. Copyright &copy; <?=date('Y')?>. 
								<a href="http://www.importgenius.com" style="text-decoration:none; color: yellow;">ImportGenius</a>, 
								Inc.
						</td>
					</tr>
				</tbody>
			</table>
	</div>
	</body>
</html>
