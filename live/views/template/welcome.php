
<link rel="stylesheet" href="<?=base_url()?>css/iscan/promo.css" />

<img src="<?=base_url()?>media/promo/1.jpg" align="left" style="margin-right:10px" width="250" height="360" />

<div class="prpanel">

	<div class="promo">
        <h1>
            Welcome to<br />
            <b>ImportGenius</b>
        </h1>
        <h2>Use Our Competitive Intelligence Tools To:</h2>
        <ul type="circle" class="check">
				<li>Research the shipping activity of competitors and customers</li>
				<li>Source products of any nature from around in the world</li>
				<li>Monitor overseas suppliers activity to ensure exclusive agent compliance</li>
				<li>Evaluate the activity of publicly traded companies</li>
				<li>Investigate and enforce intellectual property rights</li>
        </ul>
    </div>

</div>