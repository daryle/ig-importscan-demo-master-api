<?
$this->output->set_header("Content-type: text/html");
$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
$this->output->set_header("Expires: 0", false);
$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, max-stale=0,post-check=0, private, max-age=0, pre-check=0");
$this->output->set_header("Pragma: no-cache");
$user = $this->session->userdata('user');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="apple-itunes-app" content="app-id=781279942"/>
        <title>ImportGenius <? if ($user) echo " : $user[username] - $ptitle" ?></title>
        <?
        $links[] = array(
            'href' => static_url("mini/css/default/" . mtime('css', 'default'))
            , 'rel' => 'stylesheet'
        );
        foreach ($links as $link)
            echo link_tag($link);
        ?>

		<link rel="shortcut icon" href="http://dj7zlikk81c24.cloudfront.net/resources/site/live/20140312/favicon.ico" type="image/x-icon"/>

        <script type="text/javascript">
            var aliMemberId = '<?= isset($aliMemberId) ? $aliMemberId : 0; ?>';
            var aliNeverAsk = '<?= isset($never_ask_again) ? $never_ask_again : 0; ?>';
            var root = '<?= base_url() ?>';
            var mod = '<?
        if (!isset($mod))
            $mod = site_url('');

        if (substr($mod, -1) != '/')
            $mod .= '/';

        echo $mod;
        ?>';

            var optRoot = '<?= site_url() ?>';

<?php if (strpos(site_url(), '.php')) { ?>

                optRoot = '<?= site_url() ?>/';

<? } ?>

            var tb_pathToImage = "<?= resource_url("css/iscan2/images/tload.gif") ?>";

        </script>

        <script type="text/javascript" src="<?= static_url("mini/js/default/" . mtime('js', 'default')) ?>"></script>

        <script type="text/javascript">
            // Google analytics
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-172279-25']);
            _gaq.push(['_setAllowHash', false]);
            _gaq.push(['_setDomainName', 'none']);
        </script>
    </head>

    <body>


        <div class="box1"><div class="box2">

                <div class="container">

                    <div class="header">
                        <a href="<?= site_url('') ?>" id="logo2"></a>

                        <!--
                        <div class="fb_like" style="float: left; margin: 15px 0 0 220px">
                         <iframe onLoad="$(this).fadeIn();" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/like.php?href=http://www.facebook.com/importgenius&amp;layout=button_count&amp;show_faces=false&amp;width=150&amp;action=like&amp;colorscheme=light&amp;height=21" style="border:none; overflow:hidden; width:150px; height:21px; display:none;" allowtransparency="true" ></iframe>
                        </div>
                        -->

<? if ($user) { ?>


                            <div class="hlight"></div>


                            <div class="memnav">

                                <a href="<?= site_url('help/default_page') ?>/?TB_iframe=true&height=500&width=740" title="Client Services: 855-573-9976" class="thickbox hlight" id="ehelp" >Customer Service</a>
                                <div class="co_dropdown">
                                    <a href="" class="active" id="co_dropdown">
                                        <i class="cicon <?= $cname ?>"></i>
                                        <?php foreach ($countries as $c) { ?>
                                            <?php if ($c['country_avre'] == $cname) { ?>
                                                <?= $c['country_name'] ?>
                                            <? } ?>
    <? } ?>
                                        <div class="dropdown"></div>
                                    </a>
                                    <div class="co_lists">
                                        <div class="caret-dropdown">
                                            <span class="caret-outer"></span>
                                            <span class="caret-inner"></span>
                                        </div>
                                        <ul>
                                            <?php foreach ($countries as $c) { ?>
                                                <?php
                                                $disabled = "";
                                                $url = site_url($c['country_avre']);
                                                $target = "";
                                                $active = "";

                                                if (!in_array($c['country_id'], $acountries)) {
                                                    $disabled = "class='disabled'";
                                                    $url = "http://www.importgenius.com/latin/" . strtolower($c['country_name']);
                                                    $target = "target='_blank'";

                                                    if ($c['country_avre'] == "us") {
                                                        $url = "http://www.importgenius.com/tour";
                                                    }

                                                    if ($c['country_avre'] == "in") {
                                                        $url = "http://www.importgenius.com/tour/" . strtolower($c['country_name']);
                                                    }

                                                    if ($c['country_avre'] == "cr") {
                                                        $url = "http://www.importgenius.com/latin";
                                                    }
                                                } else {
                                                    if ($c['country_avre'] == "us") {
                                                        $url = site_url();
                                                    }
                                                }

                                                if ($c['country_avre'] == $cname) {
                                                    $active = "class='current'";
                                                }

                                                if ($c['country_avre'] == 'in')
                                                    $url = str_replace("/latin", "", $url);

                                                if (defined("S_ID") && S_ID == 'dev')
                                                    $url = str_replace("http://www.importgenius.com/", base_url(), $url);

                                                if (get_domain() == 'app.ig.com')
                                                    $url = str_replace("app.ig.com", "ig.com", $url);
                                                ?>

                                                <li><a href="<?= $url ?>" <?= $disabled ?> <?= $target ?> <?= $active ?>><i class="cicon <?= $c['country_avre'] ?>"></i><?= $c['country_name'] ?></a></li>

                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>

                                <a href="<?= site_url('users/account') ?>TB_iframe=true&height=500&width=740"  title="My Account" class="nleft thickbox" id="myaccount"><span class="myaccount"><b></b>My Account [<?= $user['username'] ?>]</span></a>
                                <a href="<?= site_url('logout/' . $this->session->userdata('rand')); ?>" class="nright" style="float: left; margin: 0 8px 0 0;"><span class="logout"><b></b>Logout</span></a>

                            </div>
<? } ?>

<? if (isset($message)) { ?>

                            <div class="sysalerts">
                                <marquee behaviour="slide" loop="20">
                                    <div class="sysitem">
                                        <b><?= $message['m_title'] ?> : </b>
                                        <span><?= $message['m_message'] ?></span>

                                    </div>
                                </marquee>
                            </div>

<? } ?>

                        <div class="errorbox" style="display:none">
                            <div class="errortitle">Error Message</div>
                            <div class="errorcontent"></div>
                            <div class="errorcontrol">
                                If this error is unexpected please email us.
                                <button type="button" class="button" onclick="location.reload()"><span><b>Reload Page</b></span></button>
                                <button type="button" class="button" onclick="$(this).parents('.errorbox').hide()"><span><b>Close</b></span></button>
                            </div>
                        </div>


                    </div>

                    <div class="body">



