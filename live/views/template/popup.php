<?
$this->output->set_header("Content-type: text/html");
$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
$this->output->set_header("Pragma: no-cache");
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="apple-itunes-app" content="app-id=781279942"/>
        <? if (!isset($pagetitle)) $pagetitle = "ImportScan"; ?>
        <title><?= $pagetitle ?></title>

        <?
        $links[] = array(
            'href' => static_url("mini/css/entry/" . mtime('css', 'entry'))
            , 'rel' => 'stylesheet'
        );

        foreach ($links as $link)
            echo link_tag($link);
        ?>
        <!--[if IE]>
        <link rel="shortcut icon" href="<?= base_url(); ?>favicon.ico" type="image/x-icon"/>
        <![endif]-->
        <link rel="shortcut icon" href="<?= base_url(); ?>favicon.ico" type="image/x-icon" />

        <script type="text/javascript">var root = '<?= base_url() ?>';
            var mod = '<?
        if (!isset($mod))
            $mod = site_url('');

        if (substr($mod, -1) != '/')
            $mod .= '/';

        echo $mod;
        ?>';

            var tb_pathToImage = "<?= resource_url("css/iscan2/images/tload.gif") ?>";
            var pid = 0;
            var xpid = 0;

            var submod = '';
            var colModel = '';
            var arivalDate = '';
            var getFilter = false;

        </script>

<? if (!isset($jsgroup)) $jsgroup = 'entry'; ?>
        <script type="text/javascript" src="<?= static_url("mini/js/$jsgroup/" . mtime('js', $jsgroup)) ?>"></script>

        <script type="text/javascript">
            // Google analytics
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-172279-25']);
            _gaq.push(['_setAllowHash', false]);
            _gaq.push(['_setDomainName', 'none']);
        </script>
    </head>

    <body class>
        <div class="errorbox" style="display:none" >
            <div class="errortitle">Error Message</div>
            <div class="errorcontent"></div>
            <div class="errorcontrol">
                If this error is unexpected please email us.
                <button type="button" class="button" onclick="location.reload()"><span><b>Reload Page</b></span></button>
                <button type="button" class="button" onclick="$(this).parents('.errorbox').hide()"><span><b>Close</b></span></button>
            </div>
        </div>



<? if (isset($content)) echo $content ?>

        <script type="text/javascript" src="<?= site_url("mini/js/script4/" . mtime('js', 'script4')) ?>"></script>

<? if (strpos(base_url(), 'importgenius.com')) { ?>
            <script type="text/javascript">
                        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
            <script type="text/javascript">
                try {
                    var pageTracker = _gat._getTracker("UA-172279-25");
                    pageTracker._trackPageview();
                } catch (err) {
                }</script>
    <?
    if (isset($ptracker))
        echo $ptracker;
    ?>

            <!-- Google Ads -->
            <script type="text/javascript">
                (function() {
                    var gcconv = document.createElement('script'),
                            s2 = document.getElementsByTagName('script')[0];

                    gcconv.type = 'text/javascript';
                    gcconv.async = true;
                    gcconv.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                            '//www.googleadservices.com/pagead/conversion.js';

                    s2.parentNode.insertBefore(gcconv, s2);
                })();
            </script>

            <!-- Google Conversion -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1038283176,
                        google_conversion_language = "en",
                        google_conversion_format = "3",
                        google_conversion_color = "666666",
                        google_conversion_label = "gXdMCOiuggIQqOOL7wM",
                        google_conversion_value = 0;
                /* ]]> */
            </script>


            <!-- Google Custom Search Engine -->
            <script type="text/javascript">
                (function() {
                    var cx = '004684272230815309896:r1nzha4gu1k';
                    var gcse = document.createElement('script');
                    gcse.type = 'text/javascript';
                    gcse.async = true;
                    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                            '//www.google.com/cse/cse.js?cx=' + cx;
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(gcse, s);
                })();
            </script>

            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1038283176/?label=gXdMCOiuggIQqOOL7wM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>

    <!-- BEGIN Invitation Positioning  <script language="javascript" type="text/javascript">var lpPosY = 100;</script><!-- END Invitation Positioning  -->
    <!-- BEGIN HumanTag Monitor. DO NOT MOVE! MUST BE PLACED JUST BEFORE THE /BODY TAG <script language='javascript' src='http://server.iad.liveperson.net/hc/5774993/x.js?cmd=file&file=chatScript3&site=5774993&&imageUrl=http://server.iad.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a'> </script><!-- END HumanTag Monitor. DO NOT MOVE! MUST BE PLACED JUST BEFORE THE /BODY TAG -->
<? } ?>

    </body>

</html>
