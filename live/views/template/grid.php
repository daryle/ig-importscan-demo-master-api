<?
	$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" ); 
	$this->output->set_header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" ); 
	$this->output->set_header("Cache-Control: no-cache, must-revalidate" ); 
	$this->output->set_header("Pragma: no-cache" );
	$this->output->set_header("Content-type: text/x-json");
	
	$this->load->plugin('str');
	
	echo "{";
	echo "page: $page,\n";
	echo "total: $total,\n";
	echo "rows: ";
	$rc = false;
	$rows = array();
	foreach ($db as $row) 
		{
		$rows[] = array(
				"id" => $row['entryid'],
				"cell" => array(
					"<a href=\"".site_url('main/entry/'.$row['tbname'].'/'.$row['entryid'])."\" class=\"view\" title=\"View Record ".$row['entryid']." - ".$row['consname']."\" ></a>"
					,hlite($row['consname'],'consname')
					,hlite($row['shipname'],'shipname')
					,$row['actdate']
					,hlite($row['product'],'product')
					,number_format($row['weight'])." ".$row['weightunit']
					,number_format($row['manifestqty'])." ".$row['manifestunit']
					,hlite(strtoupper($row['fport']),'fport')
					,hlite(strtoupper($row['uport']),'uport')
					,hlite($row['carriercode'],'carriercode')
					,hlite($row['vesselname'],'vesselname')
				)
			);
		}
	if (isset($rows)) echo json_encode($rows);
	echo "\n";
	echo "}";
	
?>

