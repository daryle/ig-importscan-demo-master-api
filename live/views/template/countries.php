<?php if(isset($countries)) {?> 
	<select name="first" class="nright country-selector" id="countries" style="float: left; margin: 0 20px 0 0;">
		<?php if($countries) {?>
		<option value="us">USA</option>
		<?php foreach($countries as $country):?>
			
			<?php 
				if($country['country_avre'] == $page) 
				{
					$selected = 'selected="selected"';    
				} 
				else
				{
					$selected = '';    
				}
			?>
			<option <?=$selected?> value="<?=$country['country_avre']?>"><?=$country['country_name']?></option>      
		<?php endforeach?>
		<?php } else {?>
			<option value="us">USA</option>
		<?php } ?>
	</select>
	
	<? } ?>
