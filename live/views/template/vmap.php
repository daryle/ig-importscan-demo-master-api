
<script type="text/javascript" src="<?=static_url("mini/js/jit/".mtime('js','jit'))?>"></script>

<style>

.tooltip
	{
	background: beige;
	padding: 10px;
	border: 1px solid #ccc;
	width: 200px;
	font-size: 11px;
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	}

</style>

<div id="container">

<div id="left-container">
		<div class="widget">
				<div class="wighead">Legend</div>
				<div class="wigbody">
						<ul type="disc" class="contypes">
								<li class="consleg"><span>Consignee</span></li>
								<li class="shipleg"><span>Shipper</span></li>
						</ul>
				</div>
		</div>
		<div class="widget">
				<div class="wighead">Current Node Info</div>
				<div class="wigbody">
						<div class="wlabel">Company Name</div>
						<div class="winfo" id="w_companyname">&nbsp;</div>
						<div class="wlabel">Company Type</div>
						<div class="winfo" id="w_companytype">&nbsp;</div>
						<div class="wlabel">Featured Address</div>
						<div class="winfo" id="w_companyaddr">&nbsp;</div>
						<div class="wlabel">Connections</div>
						<div class="winfo"  id="w_connections">&nbsp;</div>
						<div class="wlabel">Showing Connections</div>
						<div class="winfo"  id="w_vconnections">&nbsp;</div>
						<?
						$user = $this->session->userdata('user');

						if ($user['utype']<5) {
						?>
						<div class="wlabel">Share Link</div>
						<div class="winfo" id="w_sharelink">&nbsp;</div>

						<? } ?>

				</div>
				<div class="wigbody wigcontrols">
						<div class="wlabel" id="vhelp">&nbsp;</div>

						<a href="#" id="lnnodeb" onclick="goNext(); return false;" style="display:none; margin-top: 10px;" class="button"><span>Load Next Nodes</span></a>

						<a href="#" id="lpnodeb" onclick="goBack(); return false;" style="display:none; margin-top: 10px;" class="button"><span>Load Previous Nodes</span></a>

						<a href="#" id="setrootb" onclick="setRoot(); return false;" id="setrootb" style="margin-top: 10px; display:none;" class="button"><span>Set as Root</span></a>

						<a href="#" id="brootb" style="margin-top: 10px; display:none;" onclick="goRoot(); return false;" class="button"><span class="broot">Back to Root</span></a>

						<a href="#" id="nwb" style="display:none; margin-top: 10px;" class="button" onclick="nw=window.open(this.href,'vmap','location=no,top=10,left=10,scrollbars=yes,height=480,width=760,status=yes');nw.focus(); return false;">
						<span>Printer-Friendly Version</span></a>


				</div>
		</div>
</div>

<div id="center-container">
    <div id="infovis">&nbsp;</div>
</div>

</div>

<script type="text/javascript">


	function goShip(vsroot,vstype,vsname)
		{

		var $dtype = $('input[name=datatype_map]:checked').val();

		//console.log($dtype);

		$('.ptitle a:eq(0)').trigger('click');
		$('.fsearch').trigger('reset');

		if($dtype == "ex")
		{
			$('input[name=datatype]:eq(1)').trigger('click');
		}

		// $('#conditions').empty();
		// $('#clonebank .fcond').clone().appendTo('#conditions').find('select:eq(1),select:eq(2)').remove();
		// $('#conditions .fcond:first').prepend("<input type='text' name='qtype[]' value='"+vstype+"_n' style='display:none' />");
		// $('#conditions .fcond:first').prepend("<input type='text' name='mtype[]' value='EXACT' style='display:none' />");
		// $('#conditions .qtype1 input:eq(0)').val(vsroot).hide();
		// $('#conditions .qtype4 input:eq(0)').val(vsname);
		// $('#conditions .qtype4 b:eq(0)').html(unescape(vsname));

		$('#conditions .qtype1 input:eq(0)').val(unescape(vsname));
		$('#conditions .qtype1 select:eq(0)').val('EXACT');
		$('#conditions .fcond select:eq(1)').val(vstype);

		$('.fsearch').trigger('submit');

		}

    var changeRoot = 0;

    var htset = {
        Node: {
            overridable: true,
            dim: 9,
            color: "#f00"
        },

        Edge: {
            lineWidth: 2,
            color: "#82e2f8"
        },
        onCreateLabel: function(domElement, node){

        var nodcontent = "<p class='lclick'>"+node.name+"</p>";
        var CompanyName = node.name;

        var tipcontent = "<p><b>"+node.name+" <a href=\"http://www.importgenius.com/websearch/?sourceid=chrome&ie=UTF-8&q="+CompanyName.toLowerCase()+"\" class=\"google_search\" target=\"_blank\"></a></b></p><b>Featured Address</b><p>"+node.data.address+"</p>";
        //var tipcontent = "<p><b>"+node.name+"</b></p><b>Featured Address</b><p>"+node.data.address+"</p>";
        tipcontent += "<p><button type='button' onclick='changeRoot=1; $(\"#"+node.id+" .lclick\").trigger(\"click\");return false;' class='button sroot'><span><b>Set as Root</b></span></button></p>";

        if (node.id==jroot)
        	{
        	tipcontent += "<p>";
        	tipcontent += "<button type='button' onclick='goBack(); return false;' class='button lback' style='display:none'><span><b>Back</b></span></button>";
        	tipcontent += "<button type='button' onclick='goNext(); return false;' class='button lnext' style='display:none'><span><b>Next</b></span></button>";
        	tipcontent += "</p>";
        	}

        if ($('#flex1').length) tipcontent += "<div><button type='button' onclick='goShip(\""+node.id+"\",\""+node.data.ntype+"\",\""+escape(node.name)+"\"); return false;' class='button lship'><span><b>View Latest Shipments</b></span></button></div>";

            $(domElement)
            .html(nodcontent)
            .simpletip({content:tipcontent
            })
            .hover
            (
            		function () {
            			$(this).css('z-index','99');
            			}
            		,
            		function ()
            			{
            			$(this).css('z-index','auto');
            			}
            )
		 .find('.lclick').click(function (e) {

            	var obj = (e.target || e.srcElement);

            	if (vloading) return false;

            	if (node.id==nroot&&node.id!=jroot&&changeRoot!=1)
            		{
            		goRoot();
            		return true;
            		}

                if (node.id!=nroot) ht.onClick(node.id);

                setCurrentNode(node);

            })
            ;

        },

        onPlaceLabel: function(domElement, node){
            var style = domElement.style;

            $(domElement).show().css({cursor:'pointer'});

            var wlimit = 80;

            if (node._depth <= 1) {
                $(domElement).css({fontSize:'0.8em',color:'#333'});
            } else if(node._depth == 2){
                $(domElement).css({fontSize:'0.7em',color:'#555'});
                wlimit = 50;
            } else {
                 $(domElement).hide();
                 wlimit = 100;
            }

            if (node.id==jroot)
            	{
            	$(domElement).find('button.sroot').hide();

	  		if ((page+10)<=pagelimit)
	  			$(domElement).find('button.lnext').show();
	  		else
	  			$(domElement).find('button.lnext').hide();

	  		if ((page-10)>=0)
	  			$(domElement).find('button.lback').show();
	  		else
	  			$(domElement).find('button.lback').hide();


            	}
            else
            	$(domElement).find('button.sroot').show();

            var left = parseInt($(domElement).css('left'));
            var w = parseInt($(domElement).width());

            if (w>wlimit)
            	{
            		w = wlimit;
            		$(domElement).width(w);
            	} else {
            		$(domElement).width(100);
            		w = $(domElement).width();
            	}

            left = left - w / 2;

            if (left<0) left = 0;

            $(domElement).css({left: left});
        }

    }

    var cw = 550;
    var ch = 550;

    var ncw = parseInt($(window).width()) - 200;
    var nch = $(window).height()-$('#infovis').get(0).offsetTop-50;

    if ($('.resultindex').length) ncw -= 220;

    if (ncw>cw) cw = ncw;
    if (nch>ch) ch = nch;

    $('#vresults').show();
    var canvas = new Canvas('mycanvas', {
        'injectInto': 'infovis',
        'width': cw,
        'height': ch
    });
    $('#vresults').hide();


    $('#left-container').height(ch+70);
    $('#container').height(ch+30);

	function goRoot()
	{
	if (!vloading) $('#'+jroot+' .lclick').trigger('click'); //$('#'+jroot).trigger('click');
	}

	function goBack()
	{

		var npage = page - 10;

		if (npage<0) return false;

		page = npage;

		getJSON();

		return false;
	}

	function goNext()
	{


		var npage = page + 10;

		if (npage>pagelimit) return false;

		page = npage;

		getJSON();

		return false;
	}

	function setRoot()
	{
		page = 0;
		jroot = nroot;
		ctype = ntype;

		getJSON();

		return false;

	}

	var vjson = 0;

	function resetMap()
	{

        	if (!nroot) return true;

		var subnodes = Graph.Util.getSubnodes(ht.graph.getNode(nroot), 0);

	    	var map = [];

	    	for (var i = 0; i < subnodes.length; i++) {
	        map.push(subnodes[i].id);
	    	}


		ht.op.removeNode(map, {
	          type: 'fade:con',
	          duration: 1000
	        });


	     nroot = '';

	}

	function getJSON()
	{

	if (vloading) return false;

	$('#infovis').addClass('loading');
	var nctype = $('select[name=ctype]').val();

	vloading = true;

    <?php if(!isset($country)) $country = '';?>

    var country = '<?=!is_array($country) ? $country : $cname; ?>';
    var dtype;

    <?php if(isset($datatype) && $datatype == true){?>
		dtype = $('input[name=datatype_map]:checked').val();
    <? } ?>

    if(typeof dtype == "undefined") dtype = "im";

    <?php if(isset($dtype) && $dtype == "ex"){?>
		dtype = "ex";
    <? } ?>

	$.ajax(
	{
		url: mod+'vmap/getjson/'+ ctype + "/" + jroot +"/"+page+"/"+country+"/"+dtype,
		dataType: 'json',
		success: function (data)
		{
			$('#infovis').removeClass('loading');

			var vlink = mod + 'vmap/render2/'+ ctype + "/" + data.xcode;

			if(country != "us")
			{
				vlink = mod + 'vmap/render2/'+ ctype + "/" + data.xcode + "/0/0/"+country+"/"+dtype;

			}

			var slink = '';

			slink += "<a href='"+vlink+"' target='_blank'>Visual Map Link</a><br />";
			slink += "<textarea rows='5' style='width:150px; border: 1px solid #ccc'>"+vlink+"</textarea>";

			$('#w_sharelink').html(slink);

			$('#nwb').attr('href',mod +  'vmap/render/'+country+'/'+ctype+'/'+data.vmap.id);


			vloading = false;

			vjson = data.vmap;


			resetMap();

			setTimeout('loadNewMap()',1500);

			$('#mycanvas').show();

			nroot = vjson.id;
			jroot = vjson.id;
			pagelimit = vjson.data.count;
		}
	 }
	 );

	 return true;

	}

	function loadNewMap()
	{
			ht.loadJSON(vjson);
			ht.refresh();
			setCurrentNode(vjson);

			if($.browser.msie)
            {
            	var cwidth = $('#mycanvas').width() + 10;
            	$('#mycanvas-canvas').css({width:cwidth});
            }
	}

	function setCurrentNode(node)
	{

		if (vloading) return false;

		nroot = node.id;
		ntype = node.data.ntype;

		$('#w_companyname').html(node.name);

		wtype = 'Consignee';

		if (node.data.ntype=='shipname') wtype = 'Shipper';

		$('#w_companytype').html(wtype);

		if (node.data.address)
			$('#w_companyaddr').empty().html(node.data.address);
		else
			$('#w_companyaddr').empty().html('&nbsp;');

		if (node.data.count) $('#w_connections').html(node.data.count);

		var vcon = '0';

		var vhelp = '';

		if (nroot==jroot)
		{

	  		if ((page+10)<pagelimit)
	  			$('#lnnodeb').show();
	  		else
	  			$('#lnnodeb').hide();

	  		if ((page-10)>=0)
	  			$('#lpnodeb').show();
	  		else
	  			$('#lpnodeb').hide();


	  		$('#brootb,#setrootb').hide();

	  		vlast = page + 10;

	  		if (vlast>pagelimit) vlast = pagelimit;

	  		vcon = (page+1) + ' - ' + vlast;

	  		if (vlast<pagelimit) vhelp = 'See more connections using <b>Load Next Nodes</b>';

		} else {

	  		$('#lnnodeb,#lpnodeb').hide();
	  		$('#brootb,#setrootb').show();

	  		var subnodes = Graph.Util.getSubnodes(ht.graph.getNode(nroot), [1,1]);

	  		var scount = subnodes.length - 1;

	  		if (scount)
		  			vcon = '1 - ' + scount;

		  	if (node.data.count>scount) vhelp = 'See more connection by using <b>Set as Root</b>';

		}

		//alert(vcon);

		$('#vhelp').html(vhelp);
		$('#w_vconnections').html(vcon);


		if (changeRoot==1)
			{
			setTimeout("setRoot()",1500);
			changeRoot=0;
			}

	}

     var ht = new Hypertree(canvas, htset);

	var page = 0;
	var pagelimit = 0;
	var nroot = 0;
	var ntype = '';
	var vloading = false;


</script>
