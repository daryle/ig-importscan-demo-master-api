<?php

define('FPDF_FONTPATH','lib/fpdf/font/');
require('lib/fpdf/fpdf.php');

class PDF extends FPDF
{
//Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->SetX(-24);
        //Arial italic 8
        $this->SetFont('Arial','',8);
        //Page number
        $this->Write(4,$this->PageNo());
    }

}


$pdf=new PDF();
$pdf->AddPage();

$pdf->SetTitle('Subscription Agreement');
$pdf->SetAuthor('ImportGenius.com');
$pdf->SetSubject('ImportGenius.com Subscription Agreement');

$pdf->SetTopMargin(24);

//add logo
$pdf->Image("media/images/logo.gif",25,26,50,16,'GIF');

//add header
$pdf->SetFont('Arial','',18);
$pdf->SetXY(77,27);
$pdf->Write(5,'Terms of Service (month to month)');
$pdf->SetLeftMargin(77);
$pdf->Ln(5.5);
$pdf->SetFont('Arial','',9);
$pdf->Write(5,'Please complete and sign form as your earliest convenience.');
/*$pdf->SetFont('Arial','B',9);
$pdf->Write(5,'347 823 8704 (US)');
$pdf->Ln(5);
$pdf->SetFont('Arial','',9);
$pdf->Write(3,'Or scan signed form and email to ');
$pdf->SetTextColor(16,101,238);
$pdf->Write(3,'contracts@importgenius.com','mailto:contracts@importgenius.com');
$pdf->SetTextColor(0,0,0);*/

//add content
$pdf->SetXY(24,47);
$pdf->SetLeftMargin(24);
$pdf->SetRightMargin(20);
$pdf->SetFont('Arial','',10);
$pdf->Ln(5);
$pdf->Write(5,"Business Name");
$pdf->SetX(50);
$pdf->Write(5,": ".$user['business']);
$pdf->Ln(7);
$pdf->Write(5,"Contact Name");
$pdf->SetX(50);
$pdf->Write(5,": ".$user['lastname'].", ".$user['firstname']);
$pdf->Ln(7);
$pdf->Write(5,"Phone Number");
$pdf->SetX(50);
$pdf->Write(5,": ".$user['phone']);
$pdf->Ln(7);
$pdf->Write(5,"Email Address");
$pdf->SetX(50);
$pdf->Write(5,": ".$user['email']);
$pdf->Ln(12.7);

$pdf->Line(53,57,118,57);
$pdf->Line(53,64,118,64);
$pdf->Line(53,71,118,71);
$pdf->Line(53,78,118,78);

$pdf->SetFont('Arial','',12);
$pdf->Write(4,"These Terms of Service govern your use of the ImportGenius.com data and Web interface. These services may include, but are not limited to, business information on entities supplied by ImportGenius.com or any of its affiliates.   As the Service Provider (\"SP\"), ImportGenius.com agrees to provide you, the Service Recipient (\"SR\") with the services described in paragraph 1 hereof, and SR subscribes to such services in accordance with this Agreement.\n\n");
$pdf->SetFont('Arial','',10);

$utype = array('12'=>' '.' ','14'=>' '.' ','16'=>' '.' ');
$utype['14'] = 'x';

class sWrite {

    var $pdf;

    function w($text,$style='')
    {
        $pdf = $this->pdf;
        $pdf->SetFont('Arial',$style,10);
        $pdf->Write(4,$text);
    }
    
    function n($num)
    {
        $pdf = $this->pdf;
        $pdf->SetX(28);
        $pdf->Write(4,"$num.");
        $pdf->SetX(34);
    }
    
    function s($num)
    {
        $pdf = $this->pdf;
        $pdf->SetX(34);
        $pdf->SetFont('Arial','',10);
        $pdf->Write(4,"($num)");
        $pdf->SetX(40);
    }
    
    function c($text,$style='')
    {
        $pdf = $this->pdf;
        $pdf->SetFont('Arial',$style,10);
        $pdf->SetLeftMargin(40);
        $pdf->Write(4,$text);
        $pdf->SetLeftMargin(34);
    }

}

$p = new sWrite();
$p->pdf = $pdf;


$pdf->SetLeftMargin(34);
$p->n("A");
$p->w("Effective Date: ",'B');
$p->w($date."\n\n");
$p->n("B");
$p->w("Initial Data Start Date: ",'B');
$p->w($startdate."\n\n");
//$p->w("\t\tEnd Date: ",'B');
//$p->w($enddate);
$p->n("C");
$p->w("ImportGenius Products: ",'B');
$p->w("The following ImportGenius Products are subject to this Agreement:\n\n");
$p->w("[".$utype['12']."] Limited (data for past 45 days)\n");
$p->w("[".$utype['14']."] Standard (data for past 365 days)\n");
$p->w("[".$utype['16']."] Enterprise (data for 2006 to present)\n\n");
$p->w("Billing Frequency: ",'B');
$p->w("Monthly\n\n");
$p->n("D");
$p->w("Subscription Fee: ",'B');

switch ($user['utype'])
{
    case 12 : $fee = 99; break;
    case 14 : $fee = 199; break;
    case 16 : $fee = 399; break;
    default: $fee = 199;
}

$fee = '$'.$fee;

$p->w("Client shall pay a Subscription Fee for access to the selected ImportGenius Product(s) as follows: $fee per month starting after the conclusion of the 30-day free trial.\n\n");
$p->w("Payment Type: ",'B');
$p->w("[x] Credit Card\n\n");

$p->w("Credit Card Type: ");
$p->w($card['ctype'],'U');
$p->w(" "." "." "."Credit Card Number: ");
$p->w("****-****-****-".substr($card['cardno'],strlen($card['cardno'])-3)."\n\n",'U');

$p->w("Expiration Date: ");
$p->w($card['expiry'],'U');
$p->w(" "." "." "."Security Code: ");
$p->w("***",'U');

$p->w("\n\nName as it appears on the credit card: ");
$p->w($card['lastname'].", ".$card['firstname']."\n\n",'U');
$p->w("Credit card billing address: ");

$address = $card['street'].", ".$card['city'].", ".$card['state'].", ".$card['zip'].", ".$card['country'];
if (strlen($address)>60) $address = substr($address,0,60)."...";

$p->w($address."\n\n",'U');
$p->w("Credit card holder's signature: \n\n");

$pdf->Line(83,218,185,218);
$pdf->Ln(30);
$pdf->Cell(0,0,"(Additional terms on next page with required acknowledgment)",0,0,"R");

$pdf->Image("media/images/signhere.gif",184,200,22,9,'GIF');

$pdf->AddPage();

$p->w("SP agrees to provide to SR the services described in paragraph 1 hereof, and SR subscribes to such services in accordance with this Agreement.\n\n");

$p->n(1);
$p->w("Services.\n",'B');
$p->w("The services provided hereunder (the \"Services\") shall consist of a nonexclusive and nontransferable right to use the ImportScan data and access to the online software.\n");

$p->n(2);
$p->w("Term.\n",'B');
$p->s("a");
$p->c("This Agreement shall be effective from the date it is accepted by SP until the end date (the \"Term\"), unless earlier terminated during the Term or any renewal thereof, as follows: (i) SR shall have the right to terminate this Agreement at any time (ii) SP shall have the right to terminate this Agreement at any time immediately upon written notice to SR if SR breaches any of the provisions of this Agreement.\n");
$p->s("b");
$p->c("Following completion of the initial term, this agreement shall automatically renew as a month-to-month agreement. Under the terms of the renewed contract, SR will continue to be charged each month on the anniversary of their contract until they contact SP to cancel the service. SR must provide 7-days notice to SP prior to cancellation.\n");

$p->n(3);
$p->w("Charges.\n",'B');
$p->s("a");
$p->c("SR agrees to pay SP the fees and charges set forth on each Schedule together with any applicable taxes for the Services.\n");
$p->s("b");
$p->c("SR may cancel the agreement at any time by notifying SP in writing without incurring any cancellation fees. No refunds will be issued for charges already made.\n");

$p->n(4);
$p->w("Hold Harmless\n",'B');
$p->w("SP represents and warrants that (a) the items described in this subscription agreement and the sale or use of them does not infringe, directly or indirectly, any valid patent, copyright or trademark, and that SP will, at SP's cost and expense, indemnify, defend and hold SR harmless from and against any claims, actions, demands and litigation based on actual or alleged infringement thereof; and that (b) that all federal, state, and local regulation and statutes, applicable to furnishing the sale of these items or any material or labor, have been fully complied with. These warranties are in addition to, and shall not be construed as limiting or restricting any warranties of the SP, express or implied, or which are provided by law or exist by operation of law. SP agrees to pay, discharge and hold SR harmless from all claims\n");

$p->n(5);
$p->w("Warranties and Limitations of Liabilities.\n",'B');
$p->s("a");
$p->c("Each time SR uses the Services, SR shall be deemed to represent, warrant and covenant to SP and its Affiliated Companies that: (i) it has all requisite regulatory and legal authority to enter into and be bound by this Agreement; and (ii) its use of the Services complies with all applicable laws, rules and regulations.\n");
$p->s("b");
$p->c("SP AND ITS AFFILIATED COMPANIES MAKE NO WARRANTY, EXPRESS OR IMPLIED, AS TO RESULTS TO BE ATTAINED BY SR OR OTHERS FROM THE USE OF THE SERVICES AND THERE ARE NO EXPRESS OR IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE. The information and data contained in the Services are derived from sources deemed reliable, but SP, its Affiliated Companies and its and their suppliers do not guarantee the correctness or completeness of any programs, data or other information furnished in connection with the Services. To the maximum extent permitted by law, SP and its Affiliated Companies shall not be responsible for or have any liability for any injuries or damages caused by delays or interruptions of the Services. SR is solely responsible for the accuracy and adequacy of the data and information used by it and the resultant output thereof. SP and its Affiliated Companies shall have no liability or responsibility for the security or maintenance of any data input by SR.\n");
$p->s("c");
$p->c("SP, its Affiliated Companies, its and their suppliers and its and their third-party agents shall have no responsibility or liability, contingent or otherwise, for any injury or damages, whether caused by the negligence of SP or its Affiliated Companies or any of its and their employees, subcontractors, agents, equipment vendors or otherwise, arising in connection with the Services rendered under this Agreement and shall not be liable for any lost profits, losses, punitive, incidental or consequential damages or any claim against SR by any other party.\n");
$p->s("d");
$p->c("SR shall indemnify, hold harmless and at SR's expense defend SP, SP's Associated Persons and the Additional Entities against any loss, claim, demand or expense (including reasonable attorneys' fees) arising in connection with a breach of this Agreement by SR or the use of the Services by SR or Customer.\n");
$p->s("e");
$p->c("Limited by paragraph 4(g), to the extent permitted by law, the aggregate liability of SP and SP's  Associated Persons arising in connection with a given product or service for damages, regardless of the form of the action, shall not exceed the fees paid by SR for the ImportScan service subscription(s) of SR enabled for system, product or service in question during the three months preceding the first loss or damage.\n");
$p->s("f");
$p->c("Notwithstanding anything to the contrary in this Agreement, to the extent permitted by law, the aggregate liability of SP and SP's Associated Persons arising in connection with this Agreement, the Services for damages, regardless of the form of the action, shall not exceed the fees paid by SR for the Services during the three months preceding the first loss or damage, and this shall be SR's exclusive remedy.\n");
$p->s("g");
$p->c("No party shall be liable to the other for any default resulting from force majeure, which shall be deemed to include any circumstances beyond the reasonable control of the party or parties affected. No action, regardless of form, arising out of or pertaining to any of the Services may be brought by SR more than one year after the cause of action has accrued.\n");
$p->s("h");
$p->c("Notwithstanding any limitations contained in paragraphs 4(b) through 6(g) to the contrary, SP agrees to indemnify SR and hold it harmless and at SP's expense defend SR against any claim that the programs, data, information and other items provided by SP hereunder infringe any copyright, trademark or other contractual, statutory or common law rights; provided that (i) SR shall promptly notify SP in writing of the claim, (ii) SP shall have sole control of the settlement and defense of any action to which this indemnity relates, (iii) SR shall cooperate in every reasonable way to facilitate such defense, and (iv) if SR becomes aware of any suspected infringement by a third party of any proprietary rights of SP, SR shall promptly notify SP of such activities.\n");

$p->n(6);
$p->w("Remedies.\n",'B');
$p->w("If SR or any of its employees, representatives or affiliates breaches or threatens to breach any provision of this Agreement, SP shall be entitled to injunctive relief to enforce the provisions hereof, but nothing herein shall preclude SP from pursuing any action or other remedy for any breach or threatened breach of this Agreement, all of which shall be cumulative. If SP prevails in any such action, SP shall be entitled to recover from SR all reasonable costs, expenses and attorneys' fees incurred in connection therewith. As reasonable protection of the proprietary rights of SP and others in the information provided through the Services to avoid breach of SP's obligations to providers of such information, and to avoid unnecessary uncertainty, burden, and expense for all parties, SR acknowledges and agrees that the dissemination or distribution by SR of information identical or similar to that provided through the Services shall be deemed a breach of the terms of paragraphs 10(a) through 10(d) hereof and shall give rise to an immediate right of SP to terminate this Agreement or any portion of the Services provided hereunder.\n");

$p->n(7);
$p->w("Parties.\n",'B');
$p->w("SR recognizes that (i) SP, (ii) its Affiliated Companies, (iii) the respective partners and suppliers of SP and its Affiliated Companies, and (iv) the respective affiliates of the entities covered in subparagraph (iii) ((iii) and (iv) together, the \"Covered Entities\"), each have rights with respect to the Services, including the software, data, information and other items provided by SP and its Affiliated Companies by reason of SR's use of the Services. Paragraphs 6 and 7 hereto shall be for the benefit of SP, its Affiliated Companies, the Covered Entities and the respective affiliates, successors, assigns, officers, directors, employees and representatives of the Covered Entities. The term \"SP\" as used in paragraphs 6 and 7 hereto includes SP, its Affiliated Companies and the Covered Entities.\n\n");

$pdf->Ln(15);
$pdf->Cell(0,0,"(Additional terms on next page with required acknowledgment)",0,0,"R");
$pdf->Ln(32);
$p->w("Client Initial:",'B');

$pdf->Line(57,242,75,242);

$pdf->AddPage();

//$p->w("\n\n");

$p->n(8);
$p->w("Scope of Services.\n",'B');
$p->s("a");
$p->c("The Services are solely and exclusively for the use of SR and shall not be used for any illegal purpose or in any manner inconsistent with the provisions of this Agreement. SR acknowledges that the Services were developed, compiled, prepared, revised, selected and arranged by SP and others (including certain information sources) through the application of methods and standards of judgment developed and applied through the expenditure of substantial time, effort and money and constitute valuable industrial and intellectual property and trade secrets of SP and such others. SR agrees to protect the proprietary rights of SP and all others having rights in the Services during and after the Term. SR acknowledges and agrees that it has no ownership rights in and to the Services and that no such rights are granted under this Agreement. SR shall honor and comply with all written requests made by SP or its suppliers to protect their and others' contractual, statutory and common law rights in the Services with the same degree of care used to protect its own proprietary rights, which in no event shall be less than reasonable efforts. SR agrees to notify SP in writing promptly upon becoming aware of any unauthorized access or use by any party or of any claim that the Services infringe upon any copyright, trademark, or other contractual, statutory or common law rights.\n");
$p->s("b");
$p->c("The data, analysis and presentation included in the Services shall not be re-circulated, redistributed or published by SR except for internal purposes without the prior written consent of SP and, where necessary, with certain sources of the information included in the Services.\n");
$p->s("c");
$p->c("SR shall not use any of SP's or its Affiliated Companies' trademarks, trade names, or service marks in any manner which creates the impression that such names and marks belong to or are identified with SR, and SR acknowledges that it has no ownership rights in and to any of these names and marks.\n");
$p->s("d");
$p->c("SR acknowledges and agrees that SP may delegate certain of its responsibilities, obligations and duties under or in connection with this Agreement to a third party or an Affiliated Company of SP, which may discharge those responsibilities, obligations and duties on behalf of SP.\n");
$p->s("e");
$p->c("SR acknowledges and agrees that that only a maximum of 1 specific user is authorized to access the online account throughout the Term. The identity of authorized user is the signer of this Agreement and may not be changed without written consent from SP. A maximum of 6 Internet Protocol (IP) addresses may be associated with user login events. IP's logged and associated with SR's account in excess of this amount may result in user lockout at SP's discretion\n");

$p->n(9);
$p->w("Complete Agreement; Modifications or Waivers; Form; Inquiries.\n",'B');
$p->w("This Agreement, together with the Schedules, which are incorporated herein by reference, is the complete and exclusive statement of the agreements between the parties with respect to the subject matter hereof and supersedes any oral or written communications or representations or agreements relating thereto. No changes, modifications or waivers regarding this Agreement shall be binding unless in writing and signed by the parties hereto; provided, however, that SP may amend the provisions of this Agreement relating to provide additional services by providing written notice to SR. This Agreement, including the Schedules, and any modifications, waivers or notifications relating thereto, may be executed and delivered by facsimile or electronic mail. Any such facsimile or electronic mail transmission shall constitute the final agreement of the parties and conclusive proof of such agreement. For inquiries, SR should contact ImportScan operating agent of SP, at 10450 N. 74th Street, St. 130, Scottsdale, AZ 85258, Telephone: (888) 843-0272, or any successor operating agent or other party as specified by SP from time to time.\n");

$p->n(10);
$p->w("Validity\n",'B');
$p->w("SP and SR intend this Agreement to be a valid legal instrument. If any provision of this Agreement shall be held invalid, the remainder of this Agreement shall not be affected and shall be valid and enforceable to the fullest extent permitted by law. The invalid provision shall be reformed to the minimum extent necessary to correct any invalidity while preserving to the maximum extent the rights and commercial expectations of the parties. The headings in this Agreement are intended for convenience of reference and shall not affect its interpretation.\n");

$p->n(11);
$p->w("Governing Law\n",'B');
$p->w("This Agreement and the legal relations among the parties hereto shall be governed by and construed in accordance with the laws of the State of Arizona regardless of the laws that might otherwise govern under applicable choice-of-law principles. The parties hereto agree to submit to the jurisdiction of each of the federal and state courts located in Maricopa County, Arizona in connection with any matters arising out of this Agreement and not to assert a defense of forum non convenience, sovereign immunity, Act of State or analogous doctrines in connection with any action.\n");

$pdf->Ln();
$pdf->Cell(0,0,"(Additional terms on next page with required acknowledgment)",0,0,"R");
$pdf->Ln();
$p->w("Client Initial:",'B');

$pdf->Line(57,276,75,276);

$pdf->AddPage();

$p->n(12);
$p->w("Survival.\n",'B');
$p->w("Survival Paragraphs, 4, 5(e), 6, 7, 8, 9, 10, and 11 hereof shall survive the termination of this Agreement and shall continue in full force and effect. \n");

$pdf->Ln(10);
$p->w("For ImportGenius.com",'B');
$p->w("



Signed:

Print Name: Ryan Petersen

Date: $date

Title: Officer

Phone: 888 843 0272
");

$pdf->SetXY(86,46);
$pdf->SetLeftMargin(110);

$p->w("For Client",'B');
$p->w("



Signed:

Print Name: ".$user['firstname']." ".$user['lastname']."

Date: $date

Title:

Phone: ".$user['phone']."

E-mail: ".$user['email']."
");

$pdf->Image("media/images/blursign2.png",50,53,52,13,'PNG');

$pdf->Line(48,66,100,66);
$pdf->Line(54,74,100,74);
$pdf->Line(45,82,100,82);
$pdf->Line(44,90,100,90);
$pdf->Line(47,98,100,98);

$pdf->Line(124,66,185,66);
$pdf->Line(130,74,185,74);
$pdf->Line(121,82,185,82);
$pdf->Line(120,90,185,90);
$pdf->Line(123,98,185,98);
$pdf->Line(123,106,185,106);

$pdf->Image("media/images/signhere.gif",184,50,22,9,'GIF');

$pdf->Output($fname." Subscription Agreement.pdf",'D');
//$pdf->Output();

?>