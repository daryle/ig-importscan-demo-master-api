

<body onLoad="document.getElementById('username').focus();" class="fh">
<table width="100%" height="100%"  border="0" class="fh"> 
  <tr> 
    <td align="center" valign="middle">

        <table width="640" height="427" border="0" cellspacing="0" cellpadding="0" class="login" style="text-align:left">

	   		<tr>
				<td width="340"></td>
				<td width="300" valign="middle">
				
                				<div class="signin">
                                
						    	<form class="xform" method="post" action="<?=site_url('users/auth')?>"> 
                                
                                		<noscript>
                                        
                                        	 <h1 class="important">Browser Support</h1>
                                        
                                        	<p align="center">
                                            	This system requires Browsers with Javascript and Ajax support.
                                            </p>
                                                <style>
													.fields {display:none}
													html, body, .fh { height: 100% }
												</style>
                                        </noscript>
                                        
                                        <script type="text/javascript">
											if (parent.$('.flexigrid').length) 
											{
												$('.fh').hide();
												parent.$('.errorcontent').html('You have been signed out.');
												parent.$('.errorbox').show();
											}
										</script>
										<style>
											
											.exloading
												{
												background: none;
												}
										
                                            .frow label
                                                {
                                                width: 70px;
                                                }
                                                
                                            .frow input.tb
                                                {
                                                width: 140px;
                                                }
                                                                                
                                        </style>

                                <div class="fields">
                                        
                                        <?
										$msg = $this->session->flashdata('login_message');
										if ($msg)
											echo "<span>$msg</span>";
										else
											echo '<h1 class="important">Sign In Here</h1>'."Please enter your username and password below to login.";
										?>
                                        <br /><br />
                                        <input type="hidden" name="action" value="users/auth" />
                                        
                                        <?php if($accounts) {?>
                                        <div class="frow">
                                            <div class="frow">
                                                <label>Usernames:</label>
                                                <select class="stb" name="nusername" class="stb">
                                                    <?php foreach($accounts as $account){?> 
                                                    <option><?php echo $account['username']?></option>
                                                    <? }?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php } else { ?>

                                        <div class="frow">
                                            <label><u>L</u>ogin:</label>
                                            <input type="text" id="username" title="User Name" name="nusername" class="tb rtb" accesskey="l"/>
                                        </div>

                                        <?php } ?>
                                        
                                        <div class="frow">                                
                                            <label>Password:</label>
                                            <input type="password" title="Password" name="password" class="tb" />
                                        </div>
                                        
                                        <div style="padding: 5px; padding-left: 75px; padding-top: 0px;">
                                            <input type="checkbox" checked="checked" automcomplete="off" value="1" name="rememberme" id="rememberme" />
                                            <label for="rememberme">Keep me signed-in</label>
                                        </div>
                                        
                                        <div class="frow">
                                            <label>&nbsp;</label>
                                            <button type="submit" class="button"><span><b>Sign In</b></span></button>
                                        </div>
                                        
                                        <div class="frow" style="margin-top: 10px;">
                                        	<label>&nbsp;</label>
                                        	<a href="https://www.importgenius.com/recover" target="_blank">I forgot my password</a>
                                        </div>
                                        
                                        <div class="frow" style="margin-top: 10px;">
                                        	<label>&nbsp;</label>
                                        	<a href="https://www.importgenius.com/recover/username" target="_blank">I forgot my username</a>
                                        </div>
										
                                        <div class="frow" style="margin-top: 20px;">
                                            Not yet a member?  <a href="https://www.importgenius.com/signup">Join Now.</a>
                                        </div>
        
                                        <div class="frow">
                                            <label>&nbsp;</label>
                                            <a href="#" class="forgot" style="display:none">Forgot Password</a>
                                        </div>

								</div> <!--end of fields-->

								<p class="response"></p>

							    </form>

                   				</div>
                   
				</td>
			</tr>
			</table>


       <? 
       if (isset($message)) { ?>
        
       <div class="sysalerts"> 
       		<div behaviour="slide" loop="20">
       		  <div class="sysitem">
       		  <b><?=$message['m_title']?> : </b> <br />
       		  <span><?=$message['m_message']?></span>
       		  
	            </div>
	          </div>
       </div>

       <? } ?>

            <a href="http://www.importgenius.com">Main Site</a>
            
            
	</td> 
  </tr> 

</table> 

<script type="text/javascript">

	$('html, body').addClass('fh');
	var country = "us";
	
</script>
