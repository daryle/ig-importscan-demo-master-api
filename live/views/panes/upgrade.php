	<style>
		body
			{
			font-size: 11px;
			}
	</style>
    
    <table class="flexme" cellspacing="0" cellpadding="3" style="display:none;">
    	<thead>
    		<tr>
            	<th width="140">Feature</th>
                <th width="100">Demo</th>
            	<th width="100">Standard</th>
    			<th width="100">Premium</th>
                <th width="100">Enterprise</th>
          	</tr>        
        </thead>
        <tbody>
        	<tr>
        		<td>Allowed searches per day</td>
                <td>5</td>
                <td>40</td>
                <td>80</td>
                <td>300</td>
       		</tr>
            <tr>
            	<td>Price</td>
                <td>Free</td>
                <td>$99/month</td>
                <td>$199/month</td>
                <td>$399/month</td>
            </tr>                
        </tbody>
	</table>
    
    <?
	$rules = $this->session->userdata('rules');
	$user = $this->session->userdata('user');
	?>
    <p>
    <b>Current Account Type</b> <?=$rules['atype']?>
    </p>
    
    <p>
    You will soon be able to upgrade your account online through our system.
    </p>
    
    <p>
    For now, call us at <strong>(888) 843-0272</strong>, to upgrade.
    </p>
    
    <script type="text/javascript">
		$('.flexme').flexigrid
			(
				{
					height: 'auto'
					,title: 'Account Packages'
				}
			);
	</script>        