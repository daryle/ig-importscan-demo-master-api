<?php $u = $this->session->userdata('user'); ?>
<div class="new-widget hidden" id="alibaba-offer-widget">
    <span class="lc_top_arrow"></span>
    <div class="section-widget">
        <div class="inner-widget">
            <div class="widget-header">
				<img src="<?=resource_url("media/promo/alibaba-ig-logo.png")?>" title="Alibaba Rapid Sourcing" alt="Alibaba Rapid Sourcing">
                <!--<h2>Amazing Service</h2><a href="#" id="alibaba-offer-widget-close">x</a>-->
                <?php
                    if ($u['id'] % 2 != 0) {
                        ?><h2>Get multiple <span id="alibaba-term"><span class="product-term"></span></span> quotes</h2><!-- <a href="#" id="alibaba-offer-widget-close">x</a> --><?php
                    }
                    else {
                        ?><h2>Find <span id="alibaba-term"><span class="product-term"></span></span> suppliers quickly</h2><!-- <a href="#" id="alibaba-offer-widget-close">x</a> --><?php
                    }
                ?>

            </div>
            <!--<p>Get better <span id="alibaba-term"><span class="product-term"></span></span> quotes &amp; verified suppllier information free at AliSourcePro form Alibaba.com.</p>-->
            <p>Receive quotes from verified suppliers with one simple request.</p>

        </div>
    </div>
    <div class="clear"></div>
    <div class="widget-btn">
        <div class="wcol100">
        <a href="<?=site_url('promo/show/rapid-sourcing/cows/TB_iframe?height=412&amp;width=740&amp;inlineId=AliSourceProFrame');?>" id="alibaba-offer-widget-frame" data-title="<span style='font-size:18px;font-weight:bold;text-align:left;float:left;'>Better Quotes and Verified Supplier Info</span>" class="thickbox nbtn">Try it Now</a>
        &nbsp;
        <button type="button" class="button button2" id="alibaba-offer-widget-skip"><span><b>Skip</b></span></button>
        &nbsp;
        <button type="button" class="button button2" id="alibaba-offer-widget-never-ask-again"><span><b>Never</b></span></button>

<!--        <div style="float:left"><a href="<?=site_url('promo/show/rapid-sourcing/cows/TB_iframe?height=412&amp;width=740');?>" id="alibaba-offer-widget-frame" data-title="<span style='font-size:18px;font-weight:bold;text-align:left;float:left;'>Better Quotes and Verified Supplier Info</span>" class="thickbox nbtn">Try it Now</a></div>
        <div style="float:left"><button type="button" class="button button2" id="alibaba-offer-widget-skip"><span><b>Skip</b></span></button></div>
        <div style="float:left"><button type="button" class="button button2" id="alibaba-offer-widget-never-ask-again"><span><b>Never</b></span></button></div>-->
 
        </div>

    </div>
</div>