	<p>
	Get email alerts when new shipments matching your search clear Customs.
	</p>

<?
	if (!isset($followed_companies))
		$followed_companies = array();
	
		$user = $this->session->userdata('user');

		if (($user['utype'] == 24 || $user['utype'] == 26) && count($followed_companies) >= 3) {
			$user['allowalerts'] = 0;
		}

		if ($user['utype']<5||$user['allowalerts']==1||in_array($user['utype'],array(16,9,20,14,6,8,101,102,21,22,23,27,28,29))) {
?>

    	<form class="form">
        	<input type="hidden" name="action" value="main/sub_save" />
        	<input type="hidden" name="country" value="<?=$cname?>"/>
    		<div class="fields">
    				<b>Email Alerts Options</b><br /><br />
            		<div class="frow">
	            		<label class="label">Subscription Name</label>
	            		<input type="text" class="rtb alpha2" title="Search Name" name="name" maxlength="30" />
                    </div>
            		<div class="frow">
	            		<label class="label">Email alerts to</label>
	            		<input type="text" class="rtb emailtb" title="Email to" name="email" maxlength="50" value="<?=$user['email']?>" />
                    </div>
                    <div class="frow">
                    	<label class="label">&nbsp;</label>
                        <button type="submit" class="button"><span><b>Save</b></span></button>
                  	</div>
            </div>
            <p class="response"></p>
    	</form>

    	<br />

<?
	}
	else if ($user['utype']==12)
	{

?>

	<p>This feature is currently available to <b>Standard</b> and <b>Enterprise</b> users only.  Please contact us about pricing for adding this feature to your account.</p>


<?
	}
	else if (($user['utype'] == 24)  && count($followed_companies) >= 3)
	{

?>

	<!--<p>You have reached the maximum of three <b>E-mail Alerts</b> allowed for this plan. Please upgrade your plan to get more <b>E-mail Alerts</b>.</p>-->
    <div>
	<? $this->load->view('promos/8'); ?>
    </div>

<?
	}
	else if ($user['utype'] == 26)
	{

?>

	<!--<p>You have reached the maximum of three <b>E-mail Alerts</b> allowed for this plan. Please upgrade your plan to get more <b>E-mail Alerts</b>.</p>-->
    <div>
	<? $this->load->view('promos/10'); ?>
    </div>   


<?
	}
	else
	{

?>

	<!-- <p>This feature is currently available to <b>Enterprise</b> users only.  Please contact us about pricing for adding this feature to your account.</p> -->

	<p>You're free trial account does not allow e-mail alerts. Please
	upgrade to a paid account to access this feature</p>
	<button class="button" type="button" onclick="location.href='<?=site_url('users/logout_signup')?>';"><span><b>Upgrade Now!</b></span></button>
<?
	}
?>
