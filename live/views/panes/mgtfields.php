
    <div class="fcond" style="margin-bottom: 10px">
        
    <select name="countries" autocomplete="off">
           <option></option>
          <?php foreach($countries as $country):?>
                <option value="<?=$country['country_id']?>"><?=$country['country_name']?></option>      
            <?php endforeach?>
    </select>

    </div> 

    <table cellpadding="0" cellspacing="0">
        <tr>                                    
            <td>
                <?php foreach($cfields as $cf) {?>
                    <div class="vblock cfields" style="margin-right: 5px; width: 170px">
                        <input type="checkbox" autocomplete="off"  value="<?=$cf['field_id']?>" name="<?=$cf['field_name']?>"><label><?=$cf['display_name']?></label>
                    </div>
                 <? }?>
            </td>
        </tr>
    </table>


<div style="width: 98%; background: #fff; padding: 15px; margin: 10px 0;">
    <h3 style="margin: 0 0 15px 0; padding: 0;">Global Settings</h3>
    <?php foreach($cfields as $cf) {?> 
    <div style="background: whiteSmoke; padding: 5px; margin: 2px;" class="globalSettings" title="<?=$cf['field_id']?>">
        <span style="width: 150px; display: block; float: left;"><strong><?=$cf['display_name']?></strong></span>
        
        <input type="checkbox" name="sortable" id="sortable" <?=$cf['sortable'] == 1 ? "checked='checked'" : ""?>/>
        <label for="sortable">Sortable</label>
        
        <input type="checkbox" name="hide" id="hide" <?=$cf['hide'] == 1 ? "checked='checked'" : ""?>/>
        <label for="hide">Hide</label>
        
        <input type="checkbox" name="status" id="displaysearch" <?=$cf['status'] == 1 ? "checked='checked'" : ""?>/>
        <label for="displaysearch">Display On Search</label>
        
        <input type="text" name="width" style="width: 40px; text-align: center;" value="<?=$cf['width']?>"/>
        <label>Width</label>
        
        <input type="text" name="sort_order" style="width: 40px; text-align: center;" value="<?=$cf['sort_order']?>"/>
        <label>Sort Order</label>
        
        <select name="align">
            <option <?=$cf['align'] == 'center' ? "selected='selected'" : ""?>>center</option>
            <option <?=$cf['align'] == 'left' ? "selected='selected'" : ""?>>left</option>
            <option <?=$cf['align'] == 'right' ? "selected='selected'" : ""?>>right</option>
        </select>
        <label>Align</label>
        
    </div>
    <?php } ?>
</div>