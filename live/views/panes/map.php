<div id="map_canvas" style="height: 280px;"></div>

<form onsubmit="getAddress($('#addr').val());return false;">
<b>Refine Google Map Result</b><br />
<p>Accuracy is dependent on Google Maps and Shipping Data. Refine result by modifying the address below.</p>
<input type="text" value="<?=str_replace("\n"," ",$row[$field."addr"])?>" id="addr" name="addr" size="40" maxlength="255" />
<button type="submit">Go</button>
</form>

<style>
	form
		{
		margin: 0px;
		}
	body, html
		{
		padding: 0px;
		margin: 0px;
		background: none;
		border: 0px solid red;
		width: 300px;
		height: 390px;
		overflow: hidden;
		font-family: Arial;
		font-size: 12px;
		}
</style>
<? if (strpos(base_url(),'dev.importgenius.com')||strpos(base_url(),'localhost')) { ?>
<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAACDPyKOgOyeOfEsqqG1n_qRSr7YRdypExPkJIYYIkh4LrOXdaVhRoEmCFTAEowoyyd7JWj22-R44gMQ" type="text/javascript"></script>
<? } else { ?>
<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAACDPyKOgOyeOfEsqqG1n_qRQ7UPybu1iBGLjm6rg8sCXhQvNWehSTGzY2aSp7Y05hqjbjWP4zoDj6GA" type="text/javascript"></script>
<? } ?>
<script type="text/javascript">

	//live = ABQIAAAACDPyKOgOyeOfEsqqG1n_qRQ7UPybu1iBGLjm6rg8sCXhQvNWehSTGzY2aSp7Y05hqjbjWP4zoDj6GA

	//ABQIAAAACDPyKOgOyeOfEsqqG1n_qRSr7YRdypExPkJIYYIkh4LrOXdaVhRoEmCFTAEowoyyd7JWj22-R44gMQ
	
    var map = null;
    var geocoder = null;

    // function initialize() {
    //   if (GBrowserIsCompatible()) {
    //     geocoder = new GClientGeocoder();
    //   }
    // }	
	
	var addr = '';
	
    function getAddress(address) 
	{
			//alert(address);
		  if (!address.length) return false;
		  addr = address;
		  if (geocoder) 
		  {
				geocoder.getLatLng
				(
					  address,
					  function(point) 
					  {
					  		if (!point)
								{
									//alert(address.substr(0,address.lastIndexOf(' ')));
									var n_add = $.trim(address.substr(0,address.lastIndexOf(' ')));
									//alert(n_add);
									if (n_add.length)
										{
											getAddress(n_add);
										}
								} else {
									map = new GMap2(document.getElementById("map_canvas"),{ size: new GSize(300,280) });
									map.setCenter(point, 13);
									var marker = new GMarker(point);
									map.addOverlay(marker);
									map.addControl(new GSmallMapControl());
									var bottomRight = new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10,10));
									map.addControl(new GMapTypeControl(),bottomRight);							
									marker.openInfoWindowHtml('<strong><?=$row[$field."name"]?></strong><div style="width:200px;">'+address+'</div>');
									$("#map_canvas").show();
								
									//alert(point);
								}
					  }
				);
		  }
    }
	
	var defaddr = '<?=str_replace("\n"," ",$row[$field."addr"])?>';
	
	$(document).ready
	(
		function ()
			{
				initialize();
				getAddress(defaddr);
			}
	);
	
	$('body').unload(function(){GUnload()});		
	
</script>	
