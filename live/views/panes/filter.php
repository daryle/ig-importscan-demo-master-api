
<div class="fcond">

<select class="cond_select" name="cond[]" style="display:none">
	<option value="AND" selected="selected">AND</option>
	<option value="OR">OR</option>
	<option value="NOT">NOT</option>
</select>


<select name="qtype[]" autocomplete="off" class="disable filters" onchange="switchtype(this)">
	<?php if(isset($filter_opt) && is_array($filter_opt)) {?>
		
		<?php foreach($filter_opt as $key=>$filterv) { ?>
			<option value="<?=$key?>"><?=$filterv?></option>
			
		<? } ?>
	<? } else { ?>
		<option value="product">Product</option>
		<option value="consname">Consignee</option>
		<option value="consaddr">Consignee Address</option>
		<option value="shipname">Shipper</option>
		<option value="shipaddr">Shipper Address</option>
		<option value="ntfyname">Notify Party</option>
		<option value="port">Port</option>
		<option value="fport">Foreign Port</option>
		<option value="billofladingnbr">Bill of Lading</option>
		<option value="carriercode">Carrier Code</option>
		<option value="vessel">Vessel Name</option>
		<option value="containernum">Container Number</option>
		<option value="countryoforigin">Country of Origin</option>
		<option value="marks">Marks</option>
		<option value="distancefromzip">Distance from Zipcode</option>
		<option value="ziprange">Zipcode Range</option>
	<? } ?>
</select>

<span class="qtype1 qtypes">
	<input type="text" name="qry[]" autocomplete="off" class="tb ntb alpha2" maxlength="40" title="search keyword" size="20" />
	<select name="mtype[]">
		<option value="ALL" selected="selected">Contains</option>
		<option value="EXACT">Exact Phrase</option>
		<option value="ANY">Any</option>
		<option value="STARTS">Starts with</option>
		<option value="ENDS">Ends with</option>
	</select>
</span>

<span class="qtype2 qtypes" style="display:none">
Zipcode:
<input type="text" name="qzip[]" autocomplete="off" class="tb ntb" maxlength="6" size="10" title="zip code" />
miles:
<!-- <input type="text" name="qmiles[]" autocomplete="off" class="tb ntb numeric" maxlength="6" size="10" title="miles" /> -->

<select name="qmiles[]" autocomplete="off">
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
</select>

</span>


<span class="qtype3 qtypes" style="display:none">
<input type="text" name="qzip1[]" autocomplete="off" class="tb ntb" maxlength="6" size="10" title="from zip code" />
to
<input type="text" name="qzip2[]" autocomplete="off" class="tb ntb" maxlength="6" size="10" title="to zip code" />
</span>

<span class="qtype4 qtypes" >
	<input type="text" name="qkeyname[]" value="" style="display:none" />
   <b class="qkeyname"></b>
</span>
													
<input type="button" value="" class="addb" title="New Search Filter"  />
<input type="button" value="" class="subb" title="Remove This Search Filter"  style="display:none" />



</div> 

