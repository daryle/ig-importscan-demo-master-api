
<div class="etabs">
		<a href="#" class="sel"><span>Create a New Export</span></a>
		<a href="#"><span>Download / Email an Export</span></a>
		<a href="#">Export History</span></a>

</div>

<div class="epane">

<?

$user = $this->session->userdata('user');
$max_result = $this->session->userdata('max_export_result') ? $this->session->userdata('max_export_result') : 100000;
$max_result = number_format($max_result,0);

$new_utypes = array(26,27,28,29);
/**
 * Allow university account to use
 * export function
 */
if ($user['utype'] == 103) {
	$user['contract'] = 1;
}
//print_r($user);


if (!isset($user['noexport'])) $user['noexport'] = 0;

if ($user['noexport']==1) { ?>

	<div class="exoptions">
		<b>Export Disabled</b><br /><br />
		<p>This feature is currently not available for your account.  Please contact us about pricing for enabling this feature.</p>

		<button type="button" class="button" onclick="window.open('https://server.iad.liveperson.net/hc/5774993/?cmd=file&file=visitorWantsToChat&site=5774993&byhref=1','Import_Genius_Live_Chat','height=598,width=470');"><span><b>Live Chat</b></span></button>
	</div>

<? } else if ($user['contract'] == 0 && $user['utype'] > 4 && strtotime($user['created']) > strtotime("2011-4-29") && $user['paytype'] != 6) { ?>

	<div class="exoptions">
		<b>Export Disabled</b><br /><br />
		<p>This feature is currently not available for your account because you haven't e-signed/submitted your contract yet.</p>

		<p>Get instructions on how to e-sign/submit your contract export via the button below:</p>

		<button type="button" class="button" onclick="nw = window.open('http://www.importgenius.com/activate2/<?=md5($user['username'])?>'); nw.focus(); return false;"><span><b>Activate Export Now</b></span></button> or

		<a href="javascript:;" onclick="window.open('https://server.iad.liveperson.net/hc/5774993/?cmd=file&file=visitorWantsToChat&site=5774993&byhref=1','Import_Genius_Live_Chat','height=598,width=470');"><span><b>Talk to us via Live Chat for help</b></span></a>
	</div>


<? } else { ?>

    	<div id="exform">

	<form class="form">

    	<input type="hidden" name="action" value="export/generate" />
    	<input type="hidden" name="country" value="<?=$country['country_avre']?>"/>
    	<input type="hidden" name="export_fields" value=""/>
        <div class="response r2"></div>

		<div class="fields">
				
				<!-- <a href="#" class="upgrade"><b class="hlite">upgrade</b></a> -->
        		
        		<b>Export Options</b><br /><br />
                <div class="f-row">
                	<div class="f-label"><label for="nformat">Format</label></div>
                    <div class="f-input">
                    <select name="format" id="nformat">
                            <option value="xls">Excel</option>
                            <option value="csv">CSV</option>
                    </select>
                    </div>
             	</div> <!-- f-row format -->
				<div class="f-row">
					<div class="f-label">
					<label for="nfname">File Name</label>
					</div>
					<div class="f-input">
					<input type="text" value="" id="nfname" name="fname" maxlength="100" class="tb" autocomplete="off" size="40" /> <span class="tip">(Optional)</span>
					</div>
				</div> <!-- f-row file name -->
                <div class="f-row">

                	<div class="f-label">
                		<label for="rec0">Export Range</label>
                	</div>

                	<div class="f-input">
						<?php
							$disabled = "";
							$selected = "";

							if(in_array($user['utype'],$new_utypes))
							{
								$disabled = "disabled";
								$selected = "checked";
							}
						?>
                        <input type="radio" id="rec0" name="rec" value="0" autocomplete="off" checked="checked" <?php echo $disabled?>/>
                        <label for="rec0">All results <span class="tip">(Will work only if below <?=$max_result;?> results)</span></label>

                        <br />

                        <input <?php echo $selected?> type="radio" id="rec1" name="rec" value="1" autocomplete="off" />
                        <label for="rec1">Custom Range</label>

                        <br />

                        <label for="nfrom" title="This is where the export will start">Start <u>From</u>  </label>
                        <input type="text" id="nfrom" name="nfrom" class="numeric tb" onclick="$('#rec1').attr('checked','checked');" size="6" maxlength="6" value="1"  autocomplete="off" />

                        <label for="nto" title="This is where the export will end">up <u>To</u> </label>
                        <input type="text" id="nto" name="nto" class="numeric tb" onclick="$('#rec1').attr('checked','checked');" size="6" maxlength="6" value="1" autocomplete="off" />

                    </div>

                </div>  <!-- f-row export range -->
                <div class="f-row">

                	<div class="f-label">
                		<label for="notify_me">Notify</label>
                	</div>

                	<div class="f-input">

	                    <input type="checkbox" value="1" name="notify_me" id = "notify_me" /> <label for="notify_me">Email me when done </label><br />
	                    <input type="text" value="<?=$user['email']?>" name="notify_email" class="tb email" size="40" /> <span class="tip">(Optional)</span>

					</div>

                </div> <!-- f-row notify -->
                <div class="f-row">
                		<div class="f-label">&nbsp;</div>
                		<div class="f-input">

                    	<button type="submit" id="ge-submit" class="button"><span><b>Generate Export</b></span></button>
						
						<?php if(!in_array($user['utype'],array(26,27,28,29))){?> 
                    	<br /><br />
	                    <div class="tip">
						<b>Group</b> your export results to <b>20,000</b> or less per export.<br />
						This is to allow faster exports.
	                    </div>
	                    <? } ?>


                		</div>
                </div> <!-- f-row submit -->

                <div style="clear:both"></div>

        </div> <!-- end fields -->

	</form>

	</div> <!-- exform -->

	<div class="prog_div" style="display:none;">

		<div class="progtitle">We are generating your export...</div>
		<div class="progbar">
		  <div class="progbar-inner"></div>
		</div>
		<p id="exstatus" class="progtext">&nbsp;</p>
		<div class="progtext">
					<p>
					<button type="button" id="progcancel" class="button"><span><b>Cancel</b></span></button>
					</p>
		</div>

		<div class="comment">
			<b>Helpful Hints:</b>
			<ul class="hint-list">
				<li>
				If you are exporting a large file you can now close this window or sign-out, then come back for it later.
				</li>
				<li>
				If you wish to make another export, you'll need to finish or cancel this one.
				</li>
			</ul>

		</div>

	</div> <!-- prog_div -->

<? } ?>


</div> <!-- end epane 0 -->

<div class="epane" style="display:none">

	<div id="last-export">

		<div class="comment">No recent export</div>

	</div>

    <div class="exmail" style="display:none">

  	<form class="form"> <!-- email options form -->
	<input type="hidden" name="action" value="export/email" />

    		<div class="fields">
        	<label>Enter an email address to send exported data</label><br />
            <input type="text" value="<?=$user['email']?>" name="email" class="tb email" size="40" />
            <button type="submit" id="sendmail" class="button"><span><b>Send</b></span></button>
           	<button type="reset" class="button"><span><b>Clear</b></span></button>
            <input type="text" value="" name="exf" style="display:none" />

            <br /><br />
            <p><button type='button' id='generate-again-b' class='button' onclick='$(".etabs a:eq(0)").trigger("click"); resetExport();'><span><b>Generate another?</b></span></button></p>

        	</div>
	        <div class="response"></div>

   	</form> <!-- email options form -->

    </div> <!--exmail -->


</div> <!-- epane 1 -->

<div class="epane" style="display:none">

		<table id="ehistory"></table>

</div> <!-- epane 2 -->

<script type="text/javascript">
	var trigger_upgrade = false;
	var trigger_proceed = false;

	$(document).ready(function(){
		// $('.upgrade').click(function(){
		// 	// $('#TB_closeWindowButton').trigger('click');
		// 	// setTimeout(function(){
		// 	// 	$('a.promo2').trigger('click');
		// 	// },500);
		// })



		$('.r2').bind('click', function(e){
			e.preventDefault();
			e.stopPropagation();

			if($(e.target).hasClass('upgrade'))
			{
				$('#TB_closeWindowButton').trigger('click');
				if(trigger_upgrade == false)
				{
					trigger_upgrade = true;
					upgrade();
				}

				
			}

			if($(e.target).hasClass('proceed'))
			{
				if(trigger_proceed == false)
				{
					trigger_proceed = true;
					proceed();
				}

				
			}
		});

		function upgrade() {
			setTimeout(function(){
				trigger_upgrade = false;
				$('.promo_new').trigger('click');
			},500);
		}


		function proceed()
		{
			$('.error').html('').html("<p class='export_error exloading'>Processing, please wait...</p>")

			$.ajax({
			   type: 'POST',
			   url: mod + 'export/addcredits/',
			   dataType: 'json',
			   success: function(data){
			   	if (data.response==1)
			   	{
			   		$('.error').html('').html("<p class='export_error'>You have successfully added 20,000 export credits to your account.</p>");
				}
				else if (data.response==2)
				{
					$('.error').html('').html("<p class='export_error'>Your request for additional export credit has been sent. If you have questions, please call us at: 855-573-9976.</p>");
				}
				else
				{
			   		$('.error').html('').html("<p class='export_error'>We cannot process the additional export request at this time. The credit card was declined. If you have questions, please call us at: 855-573-9976.</p>");
			   	}
			   }
			  });
		}
		
	});

//$.ajax({type: 'POST',url: mod + 'export/addcredits/',dataType: 'json',success: function(data){alert(data.json.response);}});


	// function upgrade() {
	// 	$('#TB_closeWindowButton').trigger('click');
	// 	setTimeout(function(){
	// 		$('a.promo2').trigger('click');
	// 	},500);
	// }
</script>



