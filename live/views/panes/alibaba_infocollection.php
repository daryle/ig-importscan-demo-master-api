<div class="new-widget hidden" id="alibaba-info-collect">
    <span class="lc_top_arrow"></span>
    <div class="section-widget">
        <div class="inner-widget">
            <div class="widget-header">

                <h2>Information Collection Statement</h2><a href="#" id="alibaba-info-collect-close">x</a>

            </div>
            <p>By clicking the “Submit” button on the “Free supplier quotations” pop-up page of <a href="http://www.alibaba.com" target="_blank">Alibaba.com</a> on Import Genius’s website, you agree that Import Genius may disclose to <a href="http://www.alibaba.com" target="_blank">Alibaba.com</a> Hong Kong Limited and its affiliated companies (“<a href="http://www.alibaba.com" target="_blank">Alibaba.com</a>”) the email address, name, and country of registration that you registered with Import Genius at the time you applied for any of the services of Import Genius (the “Information”).  The Information received by <a href="http://www.alibaba.com" target="_blank">Alibaba.com</a> will be used solely for the purpose of processing your use of <a href="http://www.alibaba.com" target="_blank">Alibaba.com</a> services and creating your user account with <a href="http://www.alibaba.com" target="_blank">Alibaba.com</a>. Alibaba.com will not disclose the Information to any other party in a form that would identify you.  You have the right to request access to and correction of the Information held by <a href="http://www.alibaba.com" target="_blank">Alibaba.com</a>.  If you wish to access or correct your information held by <a href="http://www.alibaba.com" target="_blank">Alibaba.com</a>, please visit your member profile page at: <br/><br/><a href="http://us.sourcing.alibaba.com/buyerprofile/manage_buyer_profile.htm">http://us.sourcing.alibaba.com/buyerprofile/manage_buyer_profile.htm</a>.</p>

        </div>
    </div>
    <div class="clear"></div>
    
</div>