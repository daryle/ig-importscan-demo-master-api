    <img src="<?=base_url()?>media/images/logo2.gif" class="vlogo" id="vlogo" style="display:none" />

    <div class="memnav">
    		<? if ($entryid&&$qid) { ?>
    		<a href="<?=site_url("main/entry/$qid/$entryid")?>" class="nleft"><span class="Back"><b></b>Back to Entry</span></a>
    		<? } ?>
    		<a href="#" class="nmid" onclick="window.print();"><span class="Print"><b></b>Print</span></a>
    		<a href="#" class="nright" onclick="location.reload(); return false;"><span class="Refresh"><b></b>Refresh Visual Map</span></a>
    		<!-- <a href="<?=site_url("graph/render/$ctype/$jroot/$qid/$entryid")?>" style="margin-left:10px;"><span class="chart"><b></b>Graph</span></a> -->

		<!-- <a href="#" onclick="jroot=nroot; ctype=ntype; page=0; json=0; resetMap(); getJSON(); return false;"><span><b></b>Reset</span></a> -->

    </div>

    <div style="clear:both"></div>


<?
	$this->load->view('template/vmap');
?>

<style media="print">
	#left-container, .memnav { display: none; }
	img.vlogo
		{
		display: block !important;
		}
</style>

<script type="text/javascript">

	var jroot = '<?=$jroot?>';
	var ctype = '<?=$ctype?>';
	$(document).ready(function() {
		getJSON();
	});

</script>
