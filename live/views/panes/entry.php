<?php if(!isset($country)) echo $country = 'us'?>
    <style media="print">
		.btp, .memnav, .ptogtitle, .gmap, .beta
			{
			display: none !important;
			}

		.bDiv
			{
			height: auto !important;
			}

		.flexigrid
			{
			border: 2px solid #333 !important;
			}

		.flexigrid table, div, td, th
			{
			border: 0px !important;
			background: none !important;
			}

		.flexigrid .mDiv
			{
			border-bottom: 1px solid #333 !important;
			}
	</style>

	<div class="exloading">Loading, Please wait...</div>

    <div class="memnav">
    		<a href="#" class="nleft"><span class="Refresh"><b></b>Refresh</span></a>
    		<a href="#" class="nmid" style="display:none"><span class="Back"><b></b>Back</span></a>
    		<a href="#" class="nmid" style="display:none"><span class="Next"><b></b>Next</span></a>
            <a href="#" class="nmid" style="display:none"><span class="Window"><b></b>New Window</span></a>
<!--    		<a href="#" class="nmid"><span class="pdf"><b></b>Save as PDF</span></a>-->
    		<a href="#" class="nright"><span class="Print"><b></b>Print</span></a>

    		<?



    		    $mquery = serialize($query);

		    $spos = strpos($mquery,'@shipname');
		    $cpos = strpos($mquery,'@consname');

		    $ctype = "consname";

		    if ($spos&&$cpos&&$spos<$cpos) $ctype = "shipname";
		    if ($spos&&!$cpos) $ctype = "shipname";

    	 	$user = $this->session->userdata('user');

    	 	if ($user['utype']<5||$user['allowvmap']==1||in_array($user['utype'],array(16,9,14,6,8))) {

    		if ($row[$ctype.'_n']&&$row[$ctype.'_n']!='notavailable') { ?>
			<?
				if ($vmap) {
					if(!isset($dtype)) $dtype = "im";

					?><a href="<?=site_url("vmap/render/$country/$ctype/".$row[$ctype.'_n']."/$ig/$row[entryid]/".$dtype)?>" style="margin-left: 10px;"><span class="vmap"><b></b>Visual Map</span></a><?
				}
			?>
			<!-- <a href="<?=site_url("graph/render/$ctype/".$row[$ctype.'_n']."/$ig/$row[entryid]")?>" style="margin-left: 10px;"><span class="chart"><b></b>Graph</span></a> -->
		<? }
		}
		?>

    </div>

    <script type="text/javascript">

    if (parent)
    		{
    		if (parent.$('#row<?=$row['entryid']?>').prev().attr('id'))
    			$('.memnav a:eq(1)').show();
    		if (parent.$('#row<?=$row['entryid']?>').next().attr('id'))
    			$('.memnav a:eq(2)').show();
    		if (parent.$('#row<?=$row['entryid']?>').attr('id'))
	    		$('.memnav a:eq(3)').show();
    		}

    $('.memnav a').click
    (
    		function ()
    			{
    				var n = $('.memnav a').index(this);
    				switch (n)
    					{
    				case 0: self.location.reload(); break;
    				case 1:

					rid = parent.$('#row<?=$row['entryid']?>').prev().attr('id');
					nurl = parent.$('#'+rid+' a.view').attr('href');
					nurl = nurl.substr(0,nurl.indexOf('?'));
    					location.href = nurl;
    					break;

    				 case 2:

					rid = parent.$('#row<?=$row['entryid']?>').next().attr('id');
					nurl = parent.$('#'+rid+' a.view').attr('href');
					nurl = nurl.substr(0,nurl.indexOf('?'));
    					location.href = nurl;
    					break;

					case 3:

						nw=window.open(location.href,'<?=$row['entryid']?>','location=no,top=10,left=10,scrollbars=yes,height=480,width=760,status=yes');
						nw.focus();
						parent.$('#TB_closeWindowButton').trigger('click');
						break;

//				   case 4:
//
//                        location.href='<?=site_url($country.'/pdf/'.$ig.'/'.$row['entryid'])?>';
//                        break;
//    				case 5:
//
//    						window.print();
//    					 	break;

    				case 4:

    						window.print();
    					 	break;
    					default:
    						return true;
    					}

    				this.blur();
    				return false;
    			}
    );

    </script>

	<div style="clear:both"></div>

	<div class="tbtitle">CONTACT INFO</div>
    <?php //echo "<pre>"; print_r($row); echo "</pre>"?>
	<table class="flexme" cellspacing="0" cellpadding="3" >
    	<thead>
    		<tr>
            	<th class="label" width="120">Type</th>
            	<th class='label' width="120">Name</th>
    			<th class='label' width="240">Address</th>

                <?php if($row['conscontact']) {?>
                <th class="label" width="60">Phone</th>
                <? }?>
          	</tr>
        </thead>
    	<tbody>
			<?php if($row['consname']){?>
				<tr>
					<td>Consignee</td>
					<td><?=$row['consname']?> <a href="http://www.importgenius.com/websearch/?sourceid=chrome&ie=UTF-8&q=<?=strtolower(strip_tags($row['consname']))?>" target="_blank"><img src="<?=resource_url('css/iscan2/images/google.png')?>" alt=""/></a></td>
					<td><? if ($row['consaddr']!='') {?>
						<span><?=$row['consaddr']?></span>
						<br />
						<?php if($this->agent->browser() != "INTERNET EXPLORER" && $this->agent->version() != "6.0") {?> 
						<span class="gmap" onclick="return showMap(this,'<?=$ig."/".$row['entryid']?>/cons');">Show Map</span>
						<? } ?>
						<? } ?>
					</td>
					<?php if($row['conscontact']) {?>
					<td><?=$row['conscontact']?></td>
					<? } ?>
				</tr>
            <? } ?>
            <?php if($row['shipname']) {?>
        	<tr>
            	<td>Shipper</td>
            	<td><?=$row['shipname']?> <a href="http://www.importgenius.com/websearch/?sourceid=chrome&ie=UTF-8&q=<?=strtolower(strip_tags($row['shipname']))?>" target="_blank"><img src="<?=resource_url('css/iscan2/images/google.png')?>" alt=""/></a></td>
            	<td><? if ($row['shipaddr']) {?>
                	<span><?=$row['shipaddr']?></span>
                	<br />
                	<?php if($this->agent->browser() != "INTERNET EXPLORER" && $this->agent->version() != "6.0") {?> 
                	<span class="gmap" onclick="return showMap(this,'<?=$ig."/".$row['entryid']?>/ship');">Show Map</span>
                    <? } ?>
                    <? } ?>
                </td>
                <?php if($row['shipcontact']) {?>
                <td><?=$row['shipcontact']?></td>
                <? } ?>
            </tr>
            <? } ?>
       	</tbody>
    </table><br />

	<?php if($row['product']){?>
	<div class="tbtitle"> PRODUCT DETAILS</div>
	<table class="flexme" cellspacing="0" cellpadding="3" >
    	<thead>
    		<tr>
    			<th class='label' width="450">Description Area</th>
          	</tr>
        </thead>
    	<tbody>
        	<tr>
            	<td><?=nl2br(utf8_encode($row['product']))?></td>
            </tr>
        </tbody>
    </table><br />
    <? } ?>

	<div class="tbtitle"> TRANSIT DETAILS</div>
	<table class="flexme" cellspacing="0" cellpadding="3" >
    	<thead>
    		<tr>
            	<th class='label' align="right" width="120">Property</th>
    			<th class='label' width="450">Value</th>
          	</tr>
        </thead>
    	<tbody>

			<?php //echo "<pre>";print_r($cfields)?>

			<?php if(isset($cfields) && $cfields) { ?>

				<?php foreach($cfields as $cf){?>

					<?php if($row[$cf[0]] != "" && $cf[0] != "entryid" && $cf[0] != "product" && $cf[0] != "consname" && $cf[0] != "shipname" ){ ?>
						<tr>
							<td>
							<strong>
							<?=$cf[1]?>
							</strong>
							</td>
							<td>
							<?php if($row[$cf[0]] != '0'){?>
							<?=utf8_decode($row[$cf[0]])?>
							<? } else { ?>
							-
							<? } ?>
							</td>
						</tr>
					  <? } ?>

				<? } ?>

			<? } ?>


       	</tbody>
    </table><br />

	<? if (isset($related)) { ?>

	<div class="tbtitle">RELATED B/L</div>
	<table class="flexme" cellspacing="0" cellpadding="3" >
    	<thead>
    		<tr>
    			<th class='label' align="center" width="120">Type (House/Master)</th>
    			<th class='label' width="450">B / L</th>
          	</tr>
        </thead>
    	<tbody>
    	<? foreach ($related as $rel) if ($rel['entryid']!=$row['entryid']) {
    		?>
        	<tr>
        		<td align="center">
        		<?=$rel['masterbillofladingind']?>
        		</td>
            	<td>
            	<a href='<?=site_url("main/entry/$ig/$rel[entryid]")?>'>
            	<?=$rel['billofladingnbr']?>
            	</a>
            	</td>
            </tr>
    	<? } ?>
        </tbody>
    </table><br />


	<? } ?>


<div class="btp" align="center" style="display:none">
	<a href="#">Back to top</a>
</div>

<script type="text/javascript">
	var country = '<?=$gmap_country?>';


	if (parent)
		{
			$('span.Window').show();
			parent.$('#TB_ajaxWindowTitle').html('View Shipment Details <?//=$row['entryid']?> <?//=addslashes(substr(strip_tags($row['shipname']),0,30))?>');
		} else {
			document.title = 'View Record <?=$row['entryid']?> <?=addslashes(strip_tags($row['shipname']))?>';
		}

	$.fn.flexHeight = function(p) { //function to update general options
		return this.each( function() {
				if (this.grid) this.grid.fixHeight();
			});

	};

	function showMap(obj,addr)
	{
		$(obj).siblings('div').toggle();
		if ($(obj).html()=='Show Map')
			{
			$(obj).html('Hide Map');
			if (!$(obj).siblings('div').length)
				$(obj).parent().append('<div style="overflow:hidden;"><iframe src="<?=site_url('main/map')?>/'+addr+'/'+country+'" width="310" height="400" frameborder="0"></iframe></div>');
			}
		else
			$(obj).html('Show Map');

		$(obj).parents('table').flexHeight();
		return false;

	}

	$('.gmap').each
	(
		function ()
			{
				if ($.trim($(this).prev().prev().html())!='')
					$(this).show().parent().append('<i class="beta">Beta</i>');
			}
	);


	$('.exloading').hide();


	$('.flexme').each(
		function ()
			{
				var title = $(this).prev().html();
				if (parseInt($(this).height())<300)
					$(this).flexigrid({showToggleBtn: false, height:'auto',title: title,sortname: 'test',nowrap:false, showTableToggleBtn: true, width: 640});
				else
					$(this).flexigrid({showToggleBtn: false,title: title,sortname: 'test',nowrap:false, showTableToggleBtn: true, width: 640});
			}
	);

	$('.btp').show();


	$('.gmap').each
	(
		function ()
			{
				$(this).parents('tr').unbind('click');
			}
	);


</script>
