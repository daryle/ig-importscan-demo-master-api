
    <style media="print">
		.btp, .memnav, .ptogtitle, .gmap, .beta
			{
			display: none !important;
			}

		.bDiv
			{
			height: auto !important;
			}

		.flexigrid
			{
			border: 2px solid #333 !important;
			}

		.flexigrid table, div, td, th
			{
			border: 0px !important;
			background: none !important;
			}

		.flexigrid .mDiv
			{
			border-bottom: 1px solid #333 !important;
			}

	</style>


	<div class="exloading">Loading, Please wait...</div>

    <div class="memnav">
    		<a href="#" class="nleft"><span class="Refresh"><b></b>Refresh</span></a>
    		<a href="#" class="nmid" style="display:none"><span class="Back"><b></b>Back</span></a>
    		<a href="#" class="nmid" style="display:none"><span class="Next"><b></b>Next</span></a>
    		<a href="#" class="nmid" style="display:none"><span class="Window"><b></b>New Window</span></a>
    		<a href="#" class="nmid" style="display: none"><span class="pdf"><b></b>Save as PDF</span></a>
			<a href="#" class="nright"><span class="Print"><b></b>Print</span></a>

    		<?



    		    $mquery = serialize($query);

		    $spos = strpos($mquery,'@shipname');
		    $cpos = strpos($mquery,'@consname');

		    $ctype = "consname";

		    if ($spos&&$cpos&&$spos<$cpos) $ctype = "shipname";
		    if ($spos&&!$cpos) $ctype = "shipname";

    	 	$user = $this->session->userdata('user');

    	 	if ($user['utype']<5||$user['allowvmap']==1||in_array($user['utype'],array(16,9,14,6,8,21,22,23))) {

    		if ($row[$ctype.'_n']&&$row[$ctype.'_n']!='notavailable') { ?>
    		<a href="<?=site_url("vmap/render/us/$ctype/".$row[$ctype.'_n']."/$ig/$row[entryid]")?>" style="margin-left: 10px;"><span class="vmap"><b></b>Visual Map</span></a>
    		<!-- <a href="<?=site_url("graph/render/$ctype/".$row[$ctype.'_n']."/$ig/$row[entryid]")?>" style="margin-left: 10px;"><span class="chart"><b></b>Graph</span></a> -->
		<? }
		}
		?>

    </div>

    <script type="text/javascript">

    if (parent)
    		{
    		if (parent.$('#row<?=$row['entryid']?>').prev().attr('id'))
    			$('.memnav a:eq(1)').show();
    		if (parent.$('#row<?=$row['entryid']?>').next().attr('id'))
    			$('.memnav a:eq(2)').show();
    		if (parent.$('#row<?=$row['entryid']?>').attr('id'))
	    		$('.memnav a:eq(3)').show();
    		}

    $('.memnav a').click
    (
    		function ()
    			{
    				var n = $('.memnav a').index(this);
    				switch (n)
    					{
    					case 0: self.location.reload(); break;
    					case 1:

						rid = parent.$('#row<?=$row['entryid']?>').prev().attr('id');
						nurl = parent.$('#'+rid+' a.view').attr('href');
						nurl = nurl.substr(0,nurl.indexOf('?'));
    					 	location.href = nurl;
    					 	break;

    					 case 2:

						rid = parent.$('#row<?=$row['entryid']?>').next().attr('id');
						nurl = parent.$('#'+rid+' a.view').attr('href');
						nurl = nurl.substr(0,nurl.indexOf('?'));
    					 	location.href = nurl;
    					 	break;

					case 3:

						nw=window.open(location.href,'<?=$row['entryid']?>','location=no,top=10,left=10,scrollbars=yes,height=480,width=760,status=yes');
						nw.focus();
						parent.$('#TB_closeWindowButton').trigger('click');
						break;
					case 4:
						 location.href='<?=site_url('main/pdf/'.$ig.'/'.$row['entryid'])?>';
    					break;
    				case 5:

    						window.print();
    					 	break;

    					default:
    						return true;
    					}

    				this.blur();
    				return false;
    			}
    );

    </script>

	<div style="clear:both"></div>

	<div class="tbtitle">CONTACT INFO</div>
	<table class="flexme" cellspacing="0" cellpadding="3" >
    	<thead>
    		<tr>
            	<th class="label" width="120">Type</th>
            	<th class='label' width="120">Name</th>
    			<th class='label' width="340">Address</th>
          	</tr>
        </thead>
    	<tbody>
        	<tr>
            	<td>Consignee</td>
            	<td><?=$row['consname']?>  <a href="http://www.importgenius.com/websearch/?sourceid=chrome&ie=UTF-8&q=<?=strtolower(strip_tags(str_replace('&','%26',$row['consname'])))?>" target="_blank"><img src="<?=resource_url('css/iscan2/images/google.png')?>" alt=""/></a></td>
            	<td><? if ($row['consaddr']!='') {?>
                	<span><?=$row['consaddr']?></span>
                	<br />
                    <?php if($this->agent->browser() != "INTERNET EXPLORER" && $this->agent->version() != "6.0") {?> 
                    	<span class="gmap" style="margin-top: 2px" onclick="return showMap(this,'<?=$ig."/".$row['entryid']?>/cons');">
                            Show Map
                        </span> 
                    <? } ?>
                    <? } ?>
                </td>
            </tr>
            <tr>
            	<td>Notify Party</td>
                <td>
					<?=$row['ntfyname']?>
					<?php if($row['ntfyname']){?>

                	<a href="http://www.importgenius.com/websearch/?sourceid=chrome&ie=UTF-8&q=<?=strtolower(strip_tags(str_replace('&','%26',$row['ntfyname'])))?>" target="_blank"><img src="<?=resource_url('css/iscan2/images/google.png')?>" alt=""/></a></span>
                	<? }?>
                </td>
            	<td><? if ($row['ntfyaddr']) {?>
                	<span><?=$row['ntfyaddr']?></span>
                	<br />
                	<?php if($this->agent->browser() != "INTERNET EXPLORER" && $this->agent->version() != "6.0") {?> 
                    <span class="gmap"  style="margin-top: 2px" onclick="return showMap(this,'<?=$ig."/".$row['entryid']?>/ntfy');">Show Map</span>
                    <? } ?>

                    <? } ?>
                </td>
            </tr>
        	<tr>
            	<td>Shipper</td>

            	<td><?=$row['shipname']?> <a href="http://www.importgenius.com/websearch/?sourceid=chrome&ie=UTF-8&q=<?=strtolower(str_replace('&','%26',$row['shipname']))?>" target="_blank"><img src="<?=resource_url('css/iscan2/images/google.png')?>" alt=""/></td>

            	<td><? if ($row['shipaddr']) {?>
                	<span><?=$row['shipaddr']?></span>
                	<br />
                    <?php if($this->agent->browser() != "INTERNET EXPLORER" && $this->agent->version() != "6.0") {?> 
                	<span class="gmap" onclick="return showMap(this,'<?=$ig."/".$row['entryid']?>/ship');">Show Map</span>
                    <?php } ?>
                    <? } ?>
                </td>
            </tr>
       	</tbody>
    </table><br />

	<div class="tbtitle"> PRODUCT DETAILS</div>
	<?php
		$container_num = $row['containernum'];
		$product = $row['product'];
		$x = preg_replace("/\n$/",'',$product);
		$y = explode("\n",$x);


		//print_r($y);

	?>
	<table class="flexme" cellspacing="0" cellpadding="3" >
    	<thead>
    		<tr>
            	<th class='label' width="120">Container No.</th>
    			<th class='label' width="450">Description Area</th>
          	</tr>
        </thead>
    	<tbody>

                <?
                /*
                if ($row['con_num_count'] >1)
				{
					$container_array = explode("\n",$row['containernum']);
					$product_array = explode("\n",$row['product']);

					 foreach ($container_array as $ckey=>$cval)
					 {
						 if (trim($cval) != "") {
							echo "<tr>";
							echo "<td>".$cval."</td>";

							$prod = find_occurences($row['rawdata'], "5".$cval,25,252);

							//$prod = str_replace("0000000000","",str_replace("000000000000000000","",substr(strstr($row['rawdata'],"5".$cval),strlen($cval)+13,252)));

							echo "<td>".trim($prod)."</td>";
							echo "</tr>";
						 }
					 }


				}
				else
				{
					echo "<tr>";
					echo "<td>".$row['containernum']."</td>";
					echo "<td>".nl2br($row['product'])."</td>";
					echo "</tr>";
				}
				*/
				echo "<tr>";
				echo "<td>".$row['containernum']."</td>";
				echo "<td>".nl2br($row['product'])."</td>";
				echo "</tr>";
				?>

        </tbody>
    </table><br />

	<div class="tbtitle"> TRANSIT DETAILS</div>
	<table class="flexme" cellspacing="0" cellpadding="3" >
    	<thead>
    		<tr>
            	<th class='label' align="right" width="120">Property</th>
    			<th class='label' width="450">Value</th>
          	</tr>
        </thead>
    	<tbody>
			<tr>
            	<td>
				<strong>
                Carrier
                </strong>
                </td>
                <td>
                <?=$row['carriercode']?>
                <?
                	if (isset($carrier['company_name']))
                		{
                ?>
                	- <?=$carrier['company_name']?>
                	<p>
                	<b>Address</b><br />
                	<?=$carrier['address']?><br />
                	<?=$carrier['city']?><br />
                	<?=$carrier['state']?>,
                	<?=$carrier['czipcode']?><br />
                	<?=$carrier['country']?>
                	<br /><br />
                	Contact No.: <?=$carrier['phone']?>
                	</p>
                	<br />

                <?
                		}
                ?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Ship Registered In
                </strong>
                </td>
                <td>
                <?=$row['countryname']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Vessel
                </strong>
                </td>
                <td>
                <?=$row['vesselname']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Voyage
                </strong>
                </td>
                <td>
                <?=$row['voyagenumber']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                US Port
                </strong>
                </td>
                <td>
                <?=$row['uport']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Foreign Port
                </strong>
                </td>
                <td>
                <?=$row['fport']?>
                </td>
           	</tr>
           	 <tr>
            	<td>
                <strong>
                Country of Origin
                </strong>
                </td>
                <td>
                <?=$row['countryoforigin']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Place of Receipt
                </strong>
                </td>
                <td>
                <?=$row['placereceipt']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Bill of Lading
                </strong>
                </td>
                <td>
                <?=$row['billofladingnbr']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Arrival Date
                </strong>
                </td>
                <td>
                <?=$row['actdate']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Quantity
                </strong>
                </td>
                <td>
                <?=$row['manifestqty']?> <?=$row['manifestunit']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Container Count
                </strong>
                </td>
                <td>
                <?=$row['con_num_count']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                Weight (LB / KG)
                </strong>
                </td>
                <td>
                <?=toLB($row['weight'],$row['weightunit'])?>
                /
                <?=toKG($row['weight'],$row['weightunit'])?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                CBM
                </strong>
                </td>
                <td>
                <?=$row['measurement']?> <?=$row['measurementunit']?>
                </td>
           	</tr>
            <tr>
            	<td>
                <strong>
                House vs Master
                </strong>
                </td>
                <td>
                <?=$row['masterbillofladingind']?>
                </td>
           	</tr>
            <? if ($row['masterbillofladingind']=='H') {?>
            <tr>
            	<td>
                <strong>
                Master Bill of Lading
                </strong>
                </td>
                <td id="mbol">
                <?=$row['masterbilloflading']?>
                </td>
           	</tr>
           	<? } ?>
           	<? if ($row['inboundentrytype'] && $row['ib_desc']) { ?>
            <tr>
            	<td>
                <strong>
                In-Bond Entry Type
                </strong>
                </td>
                <td>
                <?=$row['inboundentrytype']." - ".$row['ib_desc']?>
                </td>
           	</tr>
           	 <? } ?>
       	</tbody>
    </table><br />

	<div class="tbtitle">OTHER INFO</div>
	<table class="flexme" cellspacing="0" cellpadding="3" >
    	<thead>
    		<tr>
            	<th class='label' align="right" width="120">Container No.</th>
    			<th class='label' width="450">Marks and Numbers Area</th>
          	</tr>
        </thead>
    	<tbody>
        <!--
        	<tr>
            	<td><?=$row['containernum']?></td>
            	<td><?=nl2br($row['marks'])?></td>
            </tr>
            -->
            <?
                if ($row['con_num_count'] >1)
				{
					$container_array = explode("\n",$row['containernum']);
					$marks_array = explode("\n",$row['marks']);

					 foreach ($container_array as $ckey=>$cval)
					 {
						 if (trim($cval) != "") {
							echo "<tr>";
							echo "<td>".$cval."</td>";

							$marks = find_occurences($row['rawdata'], "7".$cval,13,252);
                                                        
                                                        //prepend "80" for the new datas
                                                        if ($marks == FALSE) {
                                                            $marks = find_occurences($row['rawdata'], "80".$cval,13,252);
                                                        }

							//$prod = str_replace("0000000000","",str_replace("000000000000000000","",substr(strstr($row['rawdata'],"5".$cval),strlen($cval)+13,252)));

							echo "<td>".trim($marks)."</td>";
							echo "</tr>";
						 }
					 }


				}
				else
				{
					echo "<tr>";
					echo "<td>".$row['containernum']."</td>";
					echo "<td>".nl2br($row['marks'])."</td>";
					echo "</tr>";
				}
			?>
        </tbody>
    </table><br />

	<script type="text/javascript">var actdate = '<?=$row['actdate']?>';</script>

	<? if ($row['masterbillofladingind']=='M') { ?>
	<table id="<?=trim(strip_tags($row['billofladingnbr']))?>" class="flexme2"></table>


	<? } ?>

	<? if ($row['masterbillofladingind']=='H') { ?>

	<table id="<?=trim(strip_tags($row['masterbilloflading']))?>" class="flexme2"></table>


	<? } ?>


<div class="btp" align="center" style="display:none">
	<a href="#">Back to top</a>
</div>

<span style="color:#fff">{elapsed_time}</span>

<script type="text/javascript">

$(document).ready(function () {
	if (parent) {
		$('span.Window').show();
		parent.$('#TB_ajaxWindowTitle').html('View Shipment Details <?//=addslashes(substr(strip_tags($row['shipname']),0,30))?>');
	} else {
		document.title = 'View Record <?=$row['entryid']?> <?=addslashes(strip_tags($row['shipname']))?>';
	}

	$.fn.flexHeight = function(p) { //function to update general options
		return this.each( function() {
				if (this.grid) this.grid.fixHeight();
			});

	};

	$('.gmap').each(function () {
		if ($.trim($(this).prev().prev().html())!='') {
			$(this).show().parent().append('<i class="beta">Beta</i>');
        }
	});

	$('.exloading').hide();

	$('.flexme').each( function () {
        var title = $(this).prev().html();
		$(this).show();
		if (parseInt($(this).height())<300) {
			$(this).flexigrid({showToggleBtn: false, height:'auto',title: title,sortname: 'test',nowrap:false, showTableToggleBtn: true, width: 640});
        } else {
			$(this).flexigrid({showToggleBtn: false, height:'auto',title: title,sortname: 'test',nowrap:false, showTableToggleBtn: true, width: 640});
        }
	});

	$('.flexme2').flexigrid({
		'height':'auto',
        'dataType': 'json',
        'title': 'RELATED B/L',
        'width': 640,
        'url': mod + 'main/showbills/' + $('.flexme2').attr('id') + '/' + actdate,
        colModel:[
			{display: 'Type (House/Master)', width: 120, name : 'rtype', sortable : false, align: 'center'},
            {display: 'B / L', width: 450, name : 'rbl', sortable : false}
		]
	});

	$('.btp').show();

	$('.gmap').each( function () {
		$(this).parents('tr').unbind('click');
	});

    var $masterBOL = $('#mbol');
    if ($masterBOL.length) {
        var URL = mod + 'main/showmaster/' + $masterBOL.text().trim() + '/' + actdate;

        $.get(URL ,function (data) {
            $masterBOL.find('div').html(data.link);
        });
    }
});

function showMap(obj,addr) {
    $(obj).siblings('div').toggle();

    if ($(obj).html()=='Show Map') {
        $(obj).html('Hide Map');
        if (!$(obj).siblings('div').length) {

            $(obj).parent().append('<div style="overflow:hidden;"><iframe src="<?=site_url('main/map')?>/'+addr+'" width="310" height="400" frameborder="0"></iframe></div>');
        }
    } else {
        $(obj).html('Show Map');
    }

    $(obj).parents('table').flexHeight();
    return false;
}

</script>