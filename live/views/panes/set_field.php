<form action="<?=site_url('main/set_field')?>" method="post">
    <ul style="list-style-type: none;">
        <li style="margin-bottom: 15px">
            <select name="country_id">
                <?php foreach($countries as $c):?>
                <option value="<?=$c['country_id']?>"><?=$c['country_name']?></option>
                <?php endforeach?>
            </select>
        </li>
        <?php foreach($db as $d):?>
            <li style="margin-left: 30px"><input type="checkbox" name="field_id[]" value="<?=$d['field_id']?>" /><?=$d['display_name']?></li>   
        <?php endforeach?>
        <li><input type="submit" value="Submit"/></li>
    </ul>
</form>