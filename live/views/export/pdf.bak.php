<?php

define('FPDF_FONTPATH','resources/iscan4l/live/lib/fpdf/font/');
require('resources/iscan4l/live/lib/fpdf/fpdf.php');

class PDF extends FPDF
{
//Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->SetX(-24);
        //Arial italic 8
        $this->SetFont('Arial','',8);
        //Page number
        $this->Write(4,$this->PageNo());
    }

}


$pdf=new PDF();
$pdf->AddPage();

$pdf->SetTitle('Subscription Agreement');
$pdf->SetAuthor('ImportGenius.com');
$pdf->SetSubject('ImportGenius.com Subscription Agreement');

$pdf->SetTopMargin(24);

//add logo
$pdf->Image("resources/cms/live/media/images/logo.gif",25,26,50,16,'GIF');

//add header
$pdf->SetFont('Arial','B',12);
$pdf->SetXY(77,29);
if ($terms==1) $pdf->Write(5,'Terms of Service (Month-to-Month Agreement)');
else $pdf->Write(5,'Terms of Service ('.$terms.'-month agreement)');
$pdf->SetLeftMargin(77);
$pdf->Ln(5);
$pdf->Write(5,'Please e-sign document to activate your account.');

$pdf->SetXY(24,44);
$pdf->SetLeftMargin(24);
$pdf->SetRightMargin(20);

$pdf->SetFont('Arial','',12);

class sWrite {

    var $pdf;

    function w($text,$style='')
    {
        $pdf = $this->pdf;
        $pdf->SetLeftMargin(24);
        $pdf->SetFont('Arial',$style,10);
        $pdf->Write(5,$text);
    }
    
    function n($num)
    {
        $pdf = $this->pdf;
        $pdf->SetX(24);
        $pdf->Write(5,"$num.");
        $pdf->SetX(28);
    }
    
    function s($num)
    {
        $pdf = $this->pdf;
        $pdf->SetX(24);
        $pdf->SetFont('Arial','',12);
        $pdf->Write(5,"($num)");
        $pdf->SetX(40);
    }
    
    function c($text,$style='')
    {
        $pdf = $this->pdf;
        $pdf->SetFont('Arial',$style,10);
        $pdf->SetLeftMargin(24);
        $pdf->Write(5,$text);
        //$pdf->SetLeftMargin(34);
    }
    
    function l($text,$style='',$y)
    {        
        $pdf = $this->pdf;
        $pdf->SetLeftMargin(36);
        $pdf->SetY($y);
        $pdf->SetFont('Arial',$style,10);
        $pdf->Write(5,$text);
        
    }
    
    function r($text,$style='',$y)
    {
        $pdf = $this->pdf;
        $pdf->SetXY(120,$y);
        $pdf->SetFont('Arial',$style,10);
        $pdf->SetLeftMargin(120);
        $pdf->Write(5,$text);
    }
    
    function t($text,$style='')
    {
        $pdf = $this->pdf;
        $pdf->SetFont('Arial',$style,10);
        $pdf->SetLeftMargin(36);
        $pdf->Write(5,$text);
        //$pdf->SetLeftMargin(34);
    }

}

$p = new sWrite();
$p->pdf = $pdf;

//add content
$p->c("SThese Terms of Service govern your use of the ImportGenius.com Web application.  ImportGenius.com agrees to provide to you, the Service Recipient, the nonexclusive and nontransferable right to use the ImportGenius.com web application and data, and the Service Recipient subscribes to such services in accordance with this Agreement. \n\n");

$p->n(1);
$p->w("Term.\n\n",'B');
if ($terms>1){
$p->c("This Agreement will be effective from the date it is accepted by ImportGenius.com until terminated by the Service Recipient. The Service Recipient has the right to request termination of this Agreement at any time by giving at least 14 days prior written notice to ImportGenius.com and upon payment of any early termination charges that apply as set forth in paragraph 2 hereof. \n\n");
} else {
$p->c("This Agreement will be effective from the date it is accepted by ImportGenius.com until terminated by the Service Recipient. The Service Recipient has the right to request termination of this Agreement at any time by giving at least 14 days prior written notice to ImportGenius.com. \n\n");
}

$p->c("ImportGenius.com will have the right to terminate this Agreement at any time immediately upon written notice to the Service Recipient if the Service Recipient breaches any of the provisions of this Agreement.  \n");

switch ($user['utype'])
{
    case 12 : $fee = 99; break;
    case 14 : $fee = 199; break;
    case 16 : $fee = 399; break;
    case 9 : $fee = 399; break;
    case 8 : $fee = 399; break;
    case 7 : $fee = 199; break;
    case 6 : $fee = 99; break;
    default: $fee = 99;
}

if ($user['amount']>0) $fee = str_replace('.00','',$user['amount']);
$fee = '$'.$fee;

$p->n(2);
$p->w("Charges.\n\n",'B');
$p->t("(a) The Service Recipient agrees to pay ImportGenius.com the monthly subscription fee of $fee per month throughout the duration of this Agreement. \n\n");

if ($terms>1){
$p->t("(b) If the Service Recipient terminates this Agreement prior to the end of the minimum term of $terms months, the Service Recipient will pay ImportGenius.com an amount equal to 50% of the monthly subscription fees remaining under the minimum term.  \n\n");
$p->t("(c) You will be billed the amount in (a) on a monthly basis until you contact us to terminate your agreement. \n\n");
} else $p->t("(b) You will be billed the amount in (a) on a monthly basis until you contact us to terminate your agreement. \n\n");

$p->n(3);
$p->w("Warranties and Limitations of Liabilities \n\n",'B');
$p->t("(a) ImportGenius.com makes no warranty as to results to be attained by the Service Recipient or others from the use of the Services and there are no express or implied warranties of merchantability or fitness for a particular purpose or use. \n\n");

$p->t("(b) ImportGenius.com will have no responsibility or liability, contingent or otherwise, for any injury or damages, whether caused by the negligence of ImportGenius.com or any of its employees, arising in connection with the services rendered under this Agreement and will not be liable for any lost profits, losses, punitive, incidental or consequential damages or any claim against the Service Recipient by any other party. \n\n");

$p->n(4);
$p->w("Scope of Services \n\n",'B');
$p->t("(a) The data included in the Services will not be re-circulated, redistributed or published by the Service Recipient except for internal purposes without the prior written consent of ImportGenius.com. ");

//$p->c("\n\n");
$pdf->SetY(270);
$p->w("Client Initial:",'B');

$pdf->Line(59,274,79,274);
$pdf->SetAutoPageBreak(1,1);
$pdf->AddPage();

$p->t("(b) The Service Recipient acknowledges and agrees that only a maximum of 1 user(s) is authorized to access the online account, or to access data transferred from the online account onto the Service Recipient?s Customer Relationship Management system \"CRM system\", throughout the Term.  The identity of the authorized user is the signatory of this Agreement and may not be changed without prior written consent from ImportGenius.com.  \n\n");


$p->n(5);
$p->w("Survival \n \n",'B');
$p->c("Paragraphs 3 and 4 hereof will survive the termination of this Agreement and will continue in full force and effect.  \n\n\n");

$p->n(6);
$p->w("Service Recipient Info",'B');

$p->c("\n\n");

$p->t("(a) Effective Date: ",'B');
$p->w($date."\n\n");

$pdf->Line(68,98,93,98);

$p->t("(b) Subscription Level ",'B');

$p->c("\n\n");

$utype = array('12'=>' '.' ','14'=>' '.' ','16'=>' '.' ','9'=>' '.' ','8'=>' '.' ','7'=>' '.' ','6'=>' '.' ');
$utype[$user['utype']] = 'x';

$amount = array('12'=>'99','14'=>'199','16'=>'399','9'=>'399','8'=>'399','7'=>'199','6'=>'99');
$amount[$user['utype']] = str_replace('.00','',str_replace('$','',$fee));

if ($user['utype']<'9' && $user['utype']>'5')
{
    $p->t("[".$utype['6']."] Standard Plan - $".$amount['6']." per month\n");
    $p->t("[".$utype['7']."] Deluxe Plan - $".$amount['7']." per month\n");
    $p->t("[".$utype['8']."] Premium Plan - $".$amount['8']." per month\n\n");
}
elseif ($user['utype']=='16')
{
    $p->t("[".$utype['12']."] Limited Plan - $".$amount['12']." per month\n");
    $p->t("[".$utype['14']."] Standard Plan - $".$amount['14']." per month\n");
    $p->t("[".$utype['16']."] Enterprise Plan - $".$amount['16']." per month\n\n");
}
else
{
    $p->t("[".$utype['12']."] Limited Plan - $".$amount['12']." per month\n");
    $p->t("[".$utype['14']."] Standard Plan - $".$amount['14']." per month\n");
    $p->t("[".$utype['9']."] Enterprise Plan - $".$amount['9']." per month\n\n");
    
}


$p->t("(c) Payment Info ",'B');

$p->c("\n\n");

$p->t("Cardholder's Name:    ".$card['firstname']." ".$card['lastname']);
$p->c("\n\n");
$p->t("Cardholder's Signature:    ");
$p->c("\n\n");

$pdf->Line(69,148,162,148);
$pdf->Line(75,158,162,158);

$pdf->Image("resources/cms/live/media/images/signhere.gif",162,145,22,9,'GIF');

$p->t("(d) Agreement ",'B');

$p->c("\n\n");

$p->l("For ImportGenius.com ",'B',173);
$p->l("Dbase VI, LLC,",'',183);
$p->l("dba ImportGenius.com,",'',188);
$p->l("Contact Name:    Ryan Petersen",'',203);
$p->l("Signed:    ",'',213);
$p->l("Date:    $date",'',223);

$p->r("For Service Recipient ",'B',173);
$p->r("Business Name:    ".$user['business'],'',183);
$p->r("",'',193);
$p->r("Contact Name:    ".$user['firstname']." ".$user['lastname'],'',203);
$p->r("Signed:    ",'',213);
$p->r("Date:    $date",'',223);


//$pdf->Line(61,187,120,187);
$pdf->Line(61,207,105,207);
$pdf->Line(50,217,105,217);
$pdf->Line(46,227,105,227);

$pdf->Line(148,187,185,187);
$pdf->Line(145,207,185,207);
$pdf->Line(134,217,185,217);
$pdf->Line(130,227,185,227);

$pdf->Image("resources/cms/live/media/images/blursign2.png",50,208,52,8,'PNG');

$pdf->Image("resources/cms/live/media/images/signhere.gif",182,204,22,9,'GIF');

$p->c("\n\n\n\nPlease e-sign the document for fastest service. If you do not want to e-sign you may print, sign, and fax to +1 480 245 5000 or scan signed form and email to contracts@importgenius.com.");

if ($action=="download") $pdf->Output($fname." Subscription Agreement.pdf",'D');
else $pdf->Output("../export/".$fname." Subscription Agreement.pdf",'F'); 
?>