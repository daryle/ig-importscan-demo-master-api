<?php

define('FPDF_FONTPATH','resources/iscan4l/live/lib/fpdf/font/');
require('resources/iscan4l/live/lib/fpdf/fpdf.php');

class PDF extends FPDF
{
//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetX(-24);
		//Arial italic 8
		$this->SetFont('Arial','',8);
		//Page number
		$this->Write(4,$this->PageNo());
	}

}


$pdf=new PDF();
$pdf->AddPage();

$pdf->SetTitle('Subscription Agreement');
$pdf->SetAuthor('ImportGenius.com');
$pdf->SetSubject('ImportGenius.com Subscription Agreement');

$pdf->SetTopMargin(24);

//IMPORT GENIUS BACKGROUND
$pdf->Image("resources/iscan4l/live/media/images/billlading.jpg",0,0,210,285,'JPG');

//add header
/*
$pdf->SetFont('Arial','B',9);
$pdf->SetXY(110,18);
$pdf->Write(5,'ImportGenius.com  888-843-0272  info@importgenius.com');
$pdf->SetLeftMargin(5);


$pdf->SetFont('Arial','B',25);
$pdf->SetXY(10,38);
$pdf->Write(5,'OCEAN BILL OF LADING');
$pdf->SetLeftMargin(5);

$pdf->SetLineWidth(0.5); 
$pdf->Line(12,49,105,49);  */



$pdf->SetFont('Courier','',7);

// Shipper Name 
$shipper = "-";
if(isset($row['shipname'])) $shipper = $row['shipname'];
 
$pdf->SetXY(11.5,57);
$pdf->SetRightMargin(160);
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,$shipper);


// Address Name 
$shipper_addr = "-";
if(isset($row['shipaddr'])) $shipper_addr = $row['shipaddr'];

$pdf->SetXY(61.5,57);
$pdf->SetRightMargin(0);
$pdf->SetLeftMargin(61.5); 
$pdf->Write(4,$shipper_addr);
    
// Consignee Name
$consname = "-";
if(isset($row['consname'])) $consname = $row['consname'];

$pdf->SetXY(11.5,77);
$pdf->SetRightMargin(170); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,$consname);

// Consignee Address

/* - US Setup
$consaddr = "-";
if(isset($row['consaddr'])) $consaddr = $row['consaddr'];

$pdf->SetXY(45.5,77);                   
$pdf->SetRightMargin(100);
$pdf->SetLeftMargin(45.5); 
$pdf->Write(4,$consaddr);  */


$consaddr = "-";
if(isset($row['consaddr'])) $consaddr = $row['consaddr'];

$pdf->SetXY(45.5,77);
$pdf->SetRightMargin(10);
$pdf->SetLeftMargin(45.5); 
$pdf->Write(4,$consaddr);

// Zip Code   
$zipcode = "-";
if(isset($row['zipcode'])) $zipcode = $row['zipcode']; 

$pdf->SetXY(115.5,77);
$pdf->SetLeftMargin(115.5); 
$pdf->SetRightMargin(10);
$pdf->Write(4,$zipcode);

// BIll of Lading  
$billoflading = "-";
if(isset($row['billofladingnbr'])) $billoflading = $row['billofladingnbr']; 

$pdf->SetXY(115.5,77);
$pdf->SetLeftMargin(168.5); 
$pdf->SetRightMargin(10);
$pdf->Write(4,$billoflading);

// Notify  
$notify = "-";
if(isset($row['secondnotify'])) $notify = $row['secondnotify']; 

$pdf->SetXY(115.5,77);
$pdf->SetLeftMargin(140.5); 
$pdf->SetRightMargin(10);
$pdf->Write(4,$notify);

// Arival Date
$adate = "-";
if(isset($row['actdate'])) $adate = $row['actdate']; 

$pdf->SetXY(11.5,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,$adate);

// Weight LB
/* US SETUP

$weightlb = "-";
if(isset($row['grossweight'])) $weightlb = $row['grossweight']; 

$pdf->SetXY(37.5,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(37.5); 
$pdf->Write(4,$weightlb); 
*/

$weightlb = "-";
if(isset($row['wgt'])) $weightlb = $row['unit']; 

$pdf->SetXY(47.3,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(47.3); 
$pdf->Write(4,toLB($weightlb,$row['unit']));

// Weight KG
/* Us SETUP
$weightkg = "-";
if(isset($row['grossweight'])) $weightkg = $row['grossweight']; 
$pdf->SetXY(60,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(60); 
$pdf->Write(4,$weightkg);  */

$weightkg = "-";
if(isset($row['wgt'])) $weightkg = $row['wgt']; 
$pdf->SetXY(71,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(71); 
$pdf->Write(4,toKG($weightkg,$row['unit'])); 


// Measurement
/*$pdf->SetXY(11.5,118);
$pdf->SetRightMargin(170); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,'2'); */ 

// Foriegn PORT
$fport = "-";

if(isset($row['fport'])) $fport = $row['fport']; 
$pdf->SetXY(80,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(85); 
$pdf->Write(4,$fport);

// US PORT
$uport = "-";

if(isset($row['uport'])) $uport = $row['uport']; 
$pdf->SetXY(123	,101);
$pdf->SetRightMargin(30); 
$pdf->SetLeftMargin(123); 
$pdf->Write(4,$uport);

// QUANTITY
$qty = "-";

if(isset($row['manifestqty'])) $qty = $row['manifestqty']; 
$pdf->SetXY(165,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(165); 
$pdf->Write(4,$qty);  

// UNIT
$unit = "-";

if(isset($row['manifestunit'])) $unit = $row['manifestunit']; 
$pdf->SetXY(185,101);
$pdf->SetRightMargin(0); 
$pdf->SetLeftMargin(185); 
$pdf->Write(4,$unit); 


// SHIP
$ship = "-";

if(isset($row['shipcountry'])) $ship = $row['shipcountry']; 
$pdf->SetXY(160,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(160); 
$pdf->Write(4,$ship);


// Product Description
$description = "-";
if(isset($row['product'])) $description = $row['product'];

$pdf->SetXY(11.5,133);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,strip_tags($description));  

// Marks
$marks = "-";
if(isset($row['marks'])) $marks = $row['marks'];

$pdf->SetXY(11.5,163);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,strip_tags($marks));  

// ORIGIN
$origin = "-";

if(isset($row['origin'])) $origin = $row['origin']; 
$pdf->SetXY(11.5,160);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,$origin);

// DESTINATION
$destination = "-";

if(isset($row['countries_name'])) $destination = $row['countries_name']; 
$pdf->SetXY(76.5,160);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(76.5); 
$pdf->Write(4,$destination);


// DESTINATION
$measurement = "-";

if(isset($row['measurement'])) $measurement = $row['measurement']; 
$pdf->SetXY(76.5,160);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(76.5); 
$pdf->Write(4,$measurement);

$pdf->Output("../export/{$row['consname']}.pdf",'D');
//$pdf->Output("../export/{$company_name}.pdf",'F'); 
?>