<?php

define('FPDF_FONTPATH','resources/iscan4l/live/lib/fpdf/font/');
require('resources/iscan4l/live/lib/fpdf/fpdf.php');

class PDF extends FPDF
{
//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetX(-24);
		//Arial italic 8
		$this->SetFont('Arial','',8);
		//Page number
		$this->Write(4,$this->PageNo());
	}

}


$pdf=new PDF();
$pdf->AddPage();

$pdf->SetTitle('Subscription Agreement');
$pdf->SetAuthor('ImportGenius.com');
$pdf->SetSubject('ImportGenius.com Subscription Agreement');

$pdf->SetTopMargin(24);

//IMPORT GENIUS BACKGROUND
$pdf->Image("resources/iscan4l/live/media/images/billofladinglatin.jpg",0,0,210,285,'JPG');

//add header
/*
$pdf->SetFont('Arial','B',9);
$pdf->SetXY(110,18);
$pdf->Write(5,'ImportGenius.com  888-843-0272  info@importgenius.com');
$pdf->SetLeftMargin(5);


$pdf->SetFont('Arial','B',25);
$pdf->SetXY(10,38);
$pdf->Write(5,'OCEAN BILL OF LADING');
$pdf->SetLeftMargin(5);

$pdf->SetLineWidth(0.5); 
$pdf->Line(12,49,105,49);  */



$pdf->SetFont('Courier','',8);

// Shipper Name 
$shipper = "-";
if(isset($row['shipname'])) $shipper = $row['shipname'];
 
$pdf->SetXY(11.5,57);
$pdf->SetRightMargin(150);
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,$shipper);


// Address Name 
$shipper_addr = "-";
if(isset($row['shipaddr'])) $shipper_addr = $row['shipaddr'];

$pdf->SetXY(61.5,57);
$pdf->SetRightMargin(50);
$pdf->SetLeftMargin(61.5); 
$pdf->Write(4,$shipper_addr);
    
// Consignee Name
$consname = "-";
if(isset($row['consname'])) $consname = $row['consname'];

$pdf->SetXY(11.5,77);
$pdf->SetRightMargin(150); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,$consname);

// Consignee Address

/* - US Setup
$consaddr = "-";
if(isset($row['consaddr'])) $consaddr = $row['consaddr'];

$pdf->SetXY(45.5,77);                   
$pdf->SetRightMargin(100);
$pdf->SetLeftMargin(45.5); 
$pdf->Write(4,$consaddr);  */


$consaddr = "-";
if(isset($row['consaddr'])) $consaddr = $row['consaddr'];

$pdf->SetXY(61.5,77);
$pdf->SetRightMargin(10);
$pdf->SetLeftMargin(61.5); 
$pdf->Write(4,$consaddr);

// Zip Code   
$zipcode = "-";
if(isset($row['zipcode'])) $consaddr = $row['zipcode']; 

$pdf->SetXY(115.5,77);
$pdf->SetLeftMargin(115.5); 
$pdf->SetRightMargin(10);
$pdf->Write(4,$zipcode);

// Arival Date
$adate = "-";
if(isset($row['adate'])) $adate = $row['adate']; 

$pdf->SetXY(11.5,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,$adate);

// Weight LB
/* US SETUP

$weightlb = "-";
if(isset($row['grossweight'])) $weightlb = $row['grossweight']; 

$pdf->SetXY(37.5,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(37.5); 
$pdf->Write(4,$weightlb); 
*/

$weightlb = "-";
if(isset($row['grossweight'])) $weightlb = $row['grossweight']; 

$pdf->SetXY(47.3,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(47.3); 
$pdf->Write(4,toLB($row['grossweight'],$row['unit']));

// Weight KG
/* Us SETUP
$weightkg = "-";
if(isset($row['grossweight'])) $weightkg = $row['grossweight']; 
$pdf->SetXY(60,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(60); 
$pdf->Write(4,$weightkg);  */

$weightkg = "-";
if(isset($row['grossweight'])) $weightkg = $row['grossweight']; 
$pdf->SetXY(71,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(71); 
$pdf->Write(4,toKG($row['grossweight'],$row['unit'])); 


// Measurement
/*$pdf->SetXY(11.5,118);
$pdf->SetRightMargin(170); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,'2'); */ 

// PORT
$port = "-";

if(isset($row['port'])) $port = $row['port']; 
$pdf->SetXY(99,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(99); 
$pdf->Write(4,$port);

// QUANTITY
$qty = "-";

if(isset($row['quantity'])) $qty = $row['quantity']; 
$pdf->SetXY(119.5,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(119.5); 
$pdf->Write(4,$qty);  

// UNIT
$unit = "-";

if(isset($row['unit'])) $unit = $row['unit']; 
$pdf->SetXY(141,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(141); 
$pdf->Write(4,$unit); 


// SHIP
$ship = "-";

if(isset($row['shipcountry'])) $ship = $row['shipcountry']; 
$pdf->SetXY(160,101);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(160); 
$pdf->Write(4,$ship);


// Product Description
$description = "-";
if(isset($row['product'])) $description = $row['product'];

$pdf->SetXY(11.5,120);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,strip_tags($description));  

// ORIGIN
$origin = "-";

if(isset($row['origin'])) $origin = $row['origin']; 
$pdf->SetXY(11.5,160);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(11.5); 
$pdf->Write(4,$origin);

// DETINATION
$destination = "-";

if(isset($row['countries_name'])) $destination = $row['countries_name']; 
$pdf->SetXY(76.5,160);
$pdf->SetRightMargin(10); 
$pdf->SetLeftMargin(76.5); 
$pdf->Write(4,$destination);





$pdf->Output("../export/{$row['consname']}.pdf",'D');
//$pdf->Output("../export/{$company_name}.pdf",'F'); 
?>