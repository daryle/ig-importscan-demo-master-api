

</div>
<!-- End of Main Content -->

</div>
<!-- End of Content Wrapper -->




</div>
<!-- End of Wrapper -->
<script type="text/javascript" src="<?php echo base_url(); ?>resources/iscan4l/live/lib/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/iscan4l/live/help/script.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.video li a').click(
            function () {
                var video_id = $(this).attr('href');

                video_id = video_id.replace('#', '');

                $('#load_video').html('<div class="video_preloader"></div>');

                $.ajax({
                    url: '/iscan4l.php/main/getvideo',
                    dataType: 'json',
                    type: 'post',
                    data: [
                        { name: 'video_id', value: video_id }
                    ],
                    success: function (data) {
                        var vurl = "//www.youtube.com/v/" + video_id;
                        var html = "";
                        var autoplay = 0;

                        if (data) {

                            html = "<object type=\"application/x-shockwave-flash\"";
                            html += "data=\"" + vurl + "?fs=1&amp;hl=en_US&autoplay=" + autoplay + "\" width=\"437\" height=\"302\">";
                            html += "<param name=\"movie\" value=\"" + vurl + "?fs=1&amp;hl=en_US&autoplay=" + autoplay + "\" />";
                            html += "<param name=\"allowFullScreen\" value=\"true\" />";
                            html += "</object>";

                            //$('#load_video').load(function(){
                            $('#load_video').html(html);

                            $('.video-title').fadeOut('fast', function () {
                                $('.video-title').slideDown('fast').html(data.title);
                            });
                            //})


                        }


                    }, error: function (xhr) {

                    }
                })

            }
        );
    });
</script>
</body>
</html>
