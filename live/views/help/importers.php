<?php $this->load->view('help/header'); ?>

    <!-- Start of Main Content -->
    <div class="Main-Content">
        <div class="Title-Head"><h2>Getting Started with Importgenius.com</h2></div>

        <!-- Start of Left Content -->
        <div class="Left-Content">
            <div class="TitleNum1"><h2>Watch Video Introduction</h2></div>
            <div class="clearer"></div>
            <div class="Video">
                <object width="339" height="216">
                    <param name="movie"
                           value="//www.youtube.com/v/edLGoh8OTiQ?fs=1&amp;hl=en_US&amp;rel=0"></param>
                    <param name="allowFullScreen" value="true"></param>
                    <param name="allowscriptaccess" value="always"></param>
                    <embed src="//www.youtube.com/v/edLGoh8OTiQ?fs=1&amp;hl=en_US&amp;rel=0"
                           type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true"
                           width="339" height="216"></embed>
                </object>

            </div>
            <div class="clearer"></div>


        </div>
        <!-- End of Left Content -->

        <!-- Start of Right Content -->
        <div class="Right-Content">
            <div class="TitleNum1"><h2>Benefits for Importers</h2></div>
            <div class="clearer"></div>

            <!-- Start of Bullet List -->
            <div class="List-Content">
                <ul class="check">
                    <li>Find verified overseas suppliers for any product based on verified
                        shipping histories and customer lists
                    </li>
                    <li>Monitor your suppliers shipments to see who else they sell to in the
                        U.S. and enforce exclusivity agreements
                    </li>
                    <li>Discover exactly who your competitors buy from overseas with
                        detailed records showing all their imports
                    </li>
                    <li>Conduct market research to understand key trends affecting your
                        industry including market sizing, shipment growth, market shares, and
                        new product emergence
                    </li>
                </ul>

            </div>
            <!-- End of Bullet List -->

        </div>
        <!-- End of Right Content -->

        <div id="Bootom-Container">
            <div class="check-box">
                <span><input name="everytime" type="checkbox" value="1" id="everytime"/></span>
                <span><label for="everytime">Show this everytime I login</label></span>
            </div>

            <div class="Bottom-Link"><b>Still confused?</b> <span class="BottomLink"> <a
                        href="//www.importgenius.com/contact#livedemo" target="_blank">Contact Us</a></span></div>
            <div class="clearer"></div>
        </div>


    </div>
    <!-- End of Main Content -->
<?php $this->load->view('help/footer'); ?>