<?php $this->load->view('help/header'); ?>
    <!-- Start of Main Content -->
<div class="Main-Content">

    <!-- Start of Content -->
    <div class="faq-titlehead"><h2 style="text-align: center"><b>Top 10 Frequently Asked Questions</b></h2></div>
    <div class="faq-content">
        <div class="main-subheading">
            <h2>1. What data do you provide? </h2></div>
        <div class="faq-content">
            <br/>
            We provide ocean freight shipping manifests in a user-friendly search application. These records include
            all ocean shipments that have passed through a U.S. port at any point in the transit, even if the final
            destination is in another country. You’ll also find a significant number of Canadian import and export
            records, and all shipping data of 10 Latin American countries. The LATAM data is very comprehensive,
            which includes both imports and exports not only by ocean, but also by truck, rail and air. The Latin
            American data also includes fields for HS codes, and values of the shipments. Another dataset available
            is imports into the Port of Mumbai, which covers 80% of India's ocean imports.
        </div>
        <br/>

    </div>

<div class="faq-content">
    <div class="main-subheading">
        <h2>2. Where do you get this information?</h2></div>
    <div class="faq-content">
        <br/>
        Our records come from a variety of trustworthy sources including the U.S. Department of Homeland
        Security, international Customs agencies, other government agencies, private companies, non-profit
        organizations and our in-house data-gathering teams. The shipping manifest data are public records,
        which we have licensed from U.S. Customs and Border Protection.

    </div>
    <br/>

    <div class="faq-content">
    <div class="main-subheading">
        <h2>3. I want import data for countries other than the U.S. do you provide those records?</h2></div>
    <div class="faq-content">
        <br/>
        Import Genius has several other databases, which include the import and export data for Argentina,
        Chile, Colombia, Costa Rica, Ecuador, Panama, Paraguay, Peru, Uruguay, Venezuela, as well as the
        import records for the Port of Mumbai, India (80% of India's imports). Several Canadian shipments
        pass through U.S. ports as well, therefore our U.S. database contains a significant number of
        Canadian import and export records.

    </div>
    <br/>

    <div class="faq-content">
    <div class="main-subheading">
        <h2>4. Do you only provide import data?</h2></div>
    <div class="faq-content">
        <br/>
        We provide export information for all 10 of our Latin countries. If you're interested in
        shipping records for companies importing and exporting from Argentina, Chile, Colombia, Costa
        Rica, Ecuador, Panama, Paraguay, Peru, Uruguay, and Venezuela please contact us at 855-573-9976.
        You can also leave a message for us on our <a href="http://importgenius.com/contact"
                                                      target="_blank">contact page</a></div>
    <br/>

    <div class="faq-content">
    <div class="main-subheading">
        <h2>5. How come I can't find the data on a company who I know is either importing or
            exporting. </h2></div>
    <div class="faq-content">
        <br/>
        There are a number of reasons this may happen.
        <br/><br/>

        <div class="alllist">
            <ul>
                <li>The data exists before the date range your plan allows you to view (for example,
                    the last shipments for a particular company are in 2011, but your plan only lets
                    you view data from the last year).
                </li>
                <li>The company may be exporting or importing under a different name and/or
                    address.
                </li>
                <li>The name the company uses to import/export may be spelled slightly different
                    than how they have it spelled elsewhere (for example, their corporate name may
                    have an "Inc." at the end, but the name they import under may not
                </li>
                <li>The company may not be in our system. However, try searching in other fields,
                    using the "All Search" feature. The reason for this is we do not alter the data
                    given to us by U.S. Customs. If a bill of lading has been incorrectly filed by
                    them we will not alter that bill of lading. For example, a company's name may be
                    labeled under product due to incorrect filing by U.S. Customs. Using the "All
                    Search" will identify the company name in any location bringing their shipping
                    record to your results from the product field!
                </li>
                <li>Also, air cargo is protected by law and is not public information; therefore we
                    will not have that data
                </li>
            </ul>
            <br/>
            Please also View our "Tips and Tricks" tab located in the Customer Service window and
            request a free-training session; we'll share tactics you can use to find some of these
            companie

        </div>
    </div>
    <br/>

    <div class="faq-content">
    <div class="main-subheading">
        <h2>6. How does this information get entered into your database?</h2></div>
    <div class="faq-content">
        <br/>
        The U.S Customs as well as our 11 available countries' offices electronically transmit
        this information directly into our database. Import Genius does not have the opportunity
        to check the information before it enters our database.
    </div>
    <br/>

    <div class="faq-content">
    <div class="main-subheading">
        <h2>7. How often do you update your system?</h2></div>
    <div class="faq-content">
        <br/>
        For U.S. shipments, bills of lading become public record four days after the
        shipment reaches the port. From there, U.S. Customs can take anywhere from 1 to 6
        days to enter the information into our system.
        <br/>
        Our 11 additional countries differ due to their requirements through their Customs
        department. If you have any questions about a country's data update please contact
        us at 855-573-9976. You can also leave a message for us on our <a
            href="http://importgenius.com/contact" target="_blank">contact page</a>.
    </div>
    <br/>

    <div>
    <div class="main-subheading">
        <h2>8. How can I search for a product by its HS code?</h2></div>
    <div class="faq-content">
        <br/>
        The U.S. Customs office does not require an HS code on a bill of lading record
        to be used on products coming in on ocean-based shipments. However, many
        exporters will provide the HS code in the "product" field of the shipping
        manifest, and these records can easily be located with a search in the "Product"
        or "All Search" field.
        <br/>
        <br/>
        The HS code can be used to search for in following countries we offer:
        Argentina, Chile, Colombia, Costa Rica, Ecuador, India, Panama, Paraguay, Peru,
        Uruguay, and Venezuela.
    </div>
    <br/>

    <div>
    <div class="main-subheading">
        <h2>9. Where can I find the phone numbers for a particular
            importer/exporter?</h2></div>
    <div class="faq-content">
        <br/>
        Phone numbers, while sometimes provided, are not required on the Bills of
        Lading. Import Genius has a team dedicated to finding contact information
        for importers and exporters. To find out more information please e-mail your
        account manager or contact us at 855-573-9976. You can also leave a message
        for us on our <a href="http://importgenius.com/contact" target="_blank">contact
            page</a>.
    </div>
    <br/>

    <div>
    <div class="main-subheading">
        <h2>10. Where can I get additional help with using this system?</h2>
    </div>
    <div class="faq-content">
        <br/>
        Every Import Genius client has a dedicated account manager who is
        available to help answer questions and provide trainings to help you
        become more efficient with using our search application. You can contact
        us 24/7 by phone, e-mail, or live chat <a
            href="http://importgenius.com/contact" target="_blank">here</a>.

        <br/>
        <br/>
        Our goal here at Import Genius is to provide world class service by
        educating clients on how to make smarter international trade decisions
        in order to grow and succeed in business by utilizing our exciting
        technology.
    </div>

    <!-- End of Content -->

    <div class="pdf-dload-bg">

        <a href="<?php echo base_url(); ?>resources/iscan4l/live/help/IG-Faq.pdf" target="_blank" class="link">
            <div class="pdf-icondload"></div>
            <div class="pdf-dloadtext">Download IG FAQs</div>
        </a>

    </div>


    <div id="Bootom-Container">
        <div class="check-box">
            <span><input name="everytime" type="checkbox" value="1" id="everytime"/></span>
            <span><label for="everytime">Show this everytime I login</label></span>
        </div>

        <div class="Bottom-Link"><b>Still confused?</b> <span
                class="BottomLink"> <a
                    href="//www.importgenius.com/contact" target="_blank">Contact
                    Us</a></span></div>
        <div class="clearer"></div>
    </div>
<?php $this->load->view('help/footer'); ?>