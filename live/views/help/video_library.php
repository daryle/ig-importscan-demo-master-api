<?php $this->load->view('help/header'); ?>
    <!-- Start of Main Content -->
    <div class="Main-Content">
        <div class="Title-Head"><h2>Video Library</h2></div>

        <div class="col1">

            <div class="main-subheading"><h2>Basic Search</h2></div>
            <div>
                <ul class="video-icon video">
                    <li><a href="#lghuMMp6OBg">How to Search on ImportGenius.com</a></li>

                </ul>

            </div>
            <div class="clearer"></div>

            <div class="main-subheading"><h2>To Search by...</h2></div>
            <div>
                <ul class="video-icon video">
                    <li><a href="#6FchdOLTzdc">Product</a></li>
                    <li><a href="#l30r-jenHes">Consignee (Importer)</a></li>
                    <li><a href="#XJvZFgZ17JA">Consignee Address</a></li>
                    <li><a href="#np2FnnXqtZI">Shipper</a></li>
                    <li><a href="#Yy0BS_qZ3Ks">Shipper Address</a></li>
                    <li><a href="#KFnKf-2wLig">Notify Name</a></li>
                    <li><a href="#p3e5h10jlHo">US Port</a></li>
                    <li><a href="#098P-uT7Zfo">Foreign Port</a></li>
                    <li><a href="#LYyqWgqv7jo">Bill of Lading</a></li>
                    <li><a href="#avfpORPRbpM">Carrier Code</a></li>
                    <li><a href="#Dvo1ngMC-sY">Vessel Name</a></li>
                    <li><a href="#pPmzABKlarI">Container Number</a></li>
                    <li><a href="#_NiDLxDGG80">Country of Origin</a></li>
                    <li><a href="#sKzTmc2kdRM">Marks &amp; Numbers</a></li>
                    <li><a href="#XsEt1JKwMAc">Distance from Zipcode</a></li>
                    <li><a href="#hpM1i3msUPw">Zipcode Range</a></li>
                </ul>

            </div>
            <div class="clearer"></div>

            <div class="main-subheading"><h2>Features/other</h2></div>
            <div>
                <ul class="video-icon video">
                    <li><a href="#TyUPUSenE_k">Visual Map</a></li>
                    <li><a href="#HV-3YotW67o">Email alerts</a></li>
                    <li><a href="#rnGeuaUkPCo">Saving a search</a></li>
                    <li><a href="#kYmNkje8390">Search History</a></li>
                    <li><a href="#n3qLEz5fBS4">Date Range</a></li>
                    <li><a href="#CyilLnLqlnc">Modify Display Field</a></li>
                    <li><a href="#BTFys9eKGIQ">Forgot Password</a></li>
                </ul>

            </div>
            <div class="clearer"></div>

            <div class="main-subheading"><h2>Advanced Search</h2></div>
            <div>
                <ul class="video-icon video">
                    <li><a href="#9IaKZ1JUPmU">Advanced Search Techniques</a></li>

                </ul>

            </div>


        </div>
        <!-- End of col1 -->

        <div class="col2">

            <div>
                <div class="video-title">How to Search on ImportGenius.com</div>

                <div id="load_video">
                    <object type="application/x-shockwave-flash"
                            data="//www.youtube.com/v/-zu0Vp_pNDc?fs=1&amp;hl=en_US&autoplay=1" width="410"
                            height="279">
                        <param name="movie" value="//www.youtube.com/v/-zu0Vp_pNDc?fs=1&amp;hl=en_US&autoplay=1"/>
                        <param name="allowFullScreen" value="true"/>
                    </object>
                </div>
            </div>

        </div>

        <div id="Bootom-Container">
            <div class="check-box">
                <span><input name="everytime" type="checkbox" value="1" id="everytime"/></span>
                <span><label for="everytime">Show this everytime I login</label></span>
            </div>

            <div class="Bottom-Link"><b>Still confused?</b> <span class="BottomLink"> <a
                        href="//www.importgenius.com/contact" target="_blank">Contact Us</a></span></div>
            <div class="clearer"></div>
        </div>



<?php $this->load->view('help/footer');?>