<?php $this->load->view('help/header'); ?>
    <!-- Start of Main Content -->
    <div class="Main-Content">
        <div class="Title-Head"><h2>Getting Started with Importgenius.com</h2></div>


        <!-- Start of Left Content -->
        <div class="Left-Content">
            <div class="TitleNum1"><h2>Watch Video Introduction</h2></div>
            <div class="clearer"></div>
            <div class="Video">
                <object type="application/x-shockwave-flash"
                        data="//www.youtube.com/v/NmWdt9dMzjM?fs=1&amp;hl=en_US" width="339" height="274">
                    <param name="movie" value="//www.youtube.com/v/NmWdt9dMzjM?fs=1&amp;hl=en_US"/>
                    <param name="allowFullScreen" value="true"/>
                </object>
            </div>
            <div class="clearer"></div>


        </div>
        <!-- End of Left Content -->

        <!-- Start of Right Content -->
        <div class="Right-Content">
            <div class="TitleNum1"><h2>Benefits for Logistics Providers</h2></div>
            <div class="clearer"></div>

            <!-- Start of Bullet List -->
            <div class="List-Content">
                <ul class="check">
                    <li>Identify importers who need your services using verified shippng
                        histories to go after the most likely prospects
                    </li>
                    <li>Target companies importing a particular product, along a given trade
                        lane, or using a particular ocean carrier
                    </li>
                    <li> Find importers in any region using our &quot;zip code radius&quot; search</li>
                    <li>Separate house versus master bill of lading for each shipment to
                        identify customers of competing freight forwarders so you can win
                        their business
                    </li>
                </ul>

            </div>
            <!-- End of Bullet List -->

        </div>
        <!-- End of Right Content -->

        <div id="Bootom-Container">
            <div class="check-box">
                <span><input name="everytime" type="checkbox" value="1" id="everytime"/></span>
                <span><label for="everytime">Show this everytime I login</label></span>
            </div>

            <div class="Bottom-Link"><b>Still confused?</b> <span class="BottomLink"><a
                        href="//www.importgenius.com/contact" target="_blank">Contact Us</a></span></div>
            <div class="clearer"></div>
        </div>


    </div>
    <!-- End of Main Content -->
<?php $this->load->view('help/footer'); ?>