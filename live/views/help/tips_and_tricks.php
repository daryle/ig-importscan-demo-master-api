<?php $this->load->view('help/header'); ?>
    <!-- Start of Main Content -->
    <div class="Main-Content">
    <div class="Title-Head"><h2>Tips &amp; Tricks</h2></div>
    <div class="Title-Head"><h4 style="margin-top: 5px; text-align: center;">Still having difficulty searching? Take
            advantage of our
            <a href="javascript:;"
               onclick="window.open('https://server.iad.liveperson.net/hc/5774993/?cmd=file&file=visitorWantsToChat&site=5774993&byhref=1','Import_Genius_Live_Chat','height=598,width=470');"
               class="btn2 bblue">Live Chat</a>
            feature in the orange customer service tab, allowing you to get in touch with us 24/7 via instant
            messaging.</h4>
    </div>
    <!-- Start of main-col left -->
    <div class="main-col">

        <!-- Start of Topic 1 -->
        <div class="main-subheading"><h2>Finding Relevant Search Results</h2></div>
        <div class="main-headsub">Set Your Date Range</div>
        <div class="alllist">
            <ul>
                <li>Use the calendar icons to limit or expand the date range of your search .</li>
            </ul>

        </div>

        <div class="main-headsub">Add Search Filters</div>
        <div class="alllist">
            <ul>
                <li>Press the "+" button to the right of the search field to add search filters.</li>
                <li>Choose "or", "and", or "not" to indicate the search conditions.</li>
            </ul>

        </div>

        <div class="main-headsub">Advanced Search Tip</div>
        <div class="alllist">
            <ul>
                <li>Use an asterisk (*) to get better search results. Search for bat*, and your results will include
                    anything which begins with bat such as: bats, battery, bathroom, etc.
                </li>
            </ul>
        </div>
        <!--Advanced Search Tip -->
        <div class="main-headsub">House vs Master</div>
        <div class="alllist">
            <ul>
                <li>To filter out results with the logistics company listed as the consignee/shipper, check the
                    "House Only" filter next to the date range. To view only the results that include logistics
                    company names, check the "Master Only" filter
                </li>
            </ul>
        </div>
        <!-- House vs Master -->
        <div class="main-headsub">Search by Region</div>
        <div class="alllist">
            <ul>
                <li>To view the shipment activity within a specific region, use the criteria "Zip Code Range"</li>
            </ul>
        </div>
        <!-- Search by Region -->
        <div class="main-headsub">Search by Country</div>
        <div class="alllist">
            <ul>
                <li> If you would like to view shipments of a product coming from a specific country, you can select
                    the criteria, "Shipper Address" and then type the country name into the search box
                </li>
            </ul>
        </div>
        <!-- Search by Country -->
        <div class="main-headsub">Help Avoid Data Entry Errors</div>
        <div class="alllist">
            <ul>
                <li>Can't find the company you're looking for? Try searching by the criteria, "All". Sometimes
                    information on the Bill of Lading is listed inconsistently, such as the company name being
                    listed in the
                    "Marks and Numbers" category instead of under "Consignee" or "Shipper". Searching "All" helps
                    avoid these data entry errors
                </li>
            </ul>
        </div>
        <!-- Help Avoid Data Entry Errors -->

        <!-- end of Topic 1 -->

        <!-- Start of Topic 2 -->
        <div class="main-subheading"><h2>Not Finding the Results You're Looking For?</h2></div>
        <div class="main-headsub">Try A Second Search For the Company Address</div>
        <div class="alllist">
            <ul>
                <li>You may find additional results for the same company, but under a different name.</li>
                <li>Try only using the street name or city name when searching</li>
                <li>Avoid searching for 1 Main St., Aztec, NM. Instead, search for the words Main and Aztec. This
                    way you will pick up 1 Main St., One Main St., and 1 Main Street, Aztec NM, and Aztec, New
                    Mexico.
                </li>
            </ul>
        </div>

        <div class="main-headsub">Try To Find a Consignee Using One Of These Methods</div>
        <div class="alllist">
            <ul>
                <li>Search by "notify name" to see shipments where a consignee was listed as receiver.</li>
                <li>Search for the company name under "consignee address". At times a company will preface their
                    address with their name.
                </li>
                <li>Search by the consignee's shipper. This may reveal alternate
                    names or addresses.
                </li>
            </ul>
        </div>
        <!-- Try To Find a Consignee Using One Of These Methods -->
        <div class="main-headsub">Understanding the Difference Between Country of Origin And Shipper Address</div>
        <div class="alllist">
            <ul>
                <li>Country of Origin is the country of the last foreign port the shipment passed through before reaching destination.
                </li>
                <li>Shipper address is the actual location of where the shipment originated.</li>
            </ul>
        </div>
        <!-- Understanding the Difference Between Country of Origin And Shipper Address -->
        <!-- end of Topic 2 -->
    </div>
    <!-- end of main-col left -->

    <!-- Start of main-col right -->
    <div class="main-col">
        <!-- Start of Topic 1 -->
        <div class="main-subheading"><h2>Maximizing ImportGenius.com's Capabilities</h2></div>
        <div class="main-headsub">Use the Visual Mapping Tool (Plus and Premium Members Only)</div>
        <div class="alllist">
            <ul>
                <li>Located to the right of the "New Search" tab near the top of the screen.</li>
                <li>Instantly view a company's trade partners by searching a consignee or shipper.</li>
            </ul>
        </div>
        <!-- Maximizing ImportGenius.com's Capabilities -->

        <div class="main-headsub">Receive E-mail Alerts (Plus and Premium Members Only)</div>
        <div class="alllist">
            <ul>
                <li>Located on the toolbar directly above where the search results display.</li>
                <li>Click the "Create Email Alert" button after a search to add alerts.</li>
            </ul>
        </div>
        <!-- Receive E-mail Alerts (Plus and Premium Members Only) -->
        <div class="main-headsub">Export Your Search to Excel or CSV Format</div>
        <div class="alllist">
            <ul>
                <li>Located on the left of the toolbar above where the search results display.</li>
                <li>Click this button after a search to save the results to your computer or e-mail.</li>
            </ul>
        </div>
        <!-- Export Your Search to Excel or CSV Format -->
        <div class="main-headsub">Premium Users</div>
        <div class="alllist">
            <ul>
                <li>You can click the year at the top of the calendar to see a list of available years for your date
                    range
                    search. That will let you skip to the year you want rather than tabbing through each month one
                    at a time.
                </li>
            </ul>
        </div>
        <!-- Premium Users -->
        <div class="main-headsub">View Shipment Details</div>
        <div class="alllist">
            <ul>
                <li>Simply click on the magnifying glass in the 'View' Column to bring up a view of an individual
                    shipment's details
                </li>
            </ul>
        </div>
        <!-- View Shipment Details -->

        <div class="main-headsub">View Related Bills of Lading</div>
        <div class="alllist">
            <ul>
                <li>Scroll to the bottom of the 'View Shipment Details' view in order to find the other Bills of
                    Lading on the shipment
                </li>
            </ul>
        </div>
        <!-- View Related Bills of Lading -->

        <div class="main-headsub">Help Preserve Your Searches</div>
        <div class="alllist">
            <ul>
                <li>Open up multiple search lines</li>
                <li>Choose "OR" in the box to the left of each line</li>
                <li>Choose the criteria and product/company you want to search for, and run the search</li>
            </ul>
        </div>
        <!-- Help Preserve Your Searches -->
        <div class="main-subheading"><h2>Do you need more out of your plan? Here are some benefits of
                upgrading!</h2></div>
        <div class="main-headsub">Plus Plan</div>
        <div class="alllist">
            <ul>
                <li>Receive Email Alerts - Under this plan you get 5 email alerts at a time</li>
                <li>Export Feature- Download 10k results/month into Excel</li>
                <li>Date Range- One year rolling date range</li>
            </ul>
        </div>
        <!-- Do you need more out of your plan?  Here are some benefits of upgrading! -->
        <div class="main-headsub">Premium Plan</div>
        <div class="alllist">
            <ul>
                <li>Visual Mapping - Instantly view a company?s trade partners by searching a consignee or shipper
                </li>
                <li>Receive Email Alerts- Under this plan you get 10 email alerts at a time</li>
                <li>Export Feature- Download 25k results/month in Excel</li>
                <li>Date Range- View data going back to 2006</li>
            </ul>
        </div>
        <!-- Do you need more out of your plan?  Here are some benefits of upgrading! -->

        <div class="pdf-dload-bg">
            <a href="<?php echo base_url(); ?>resources/iscan4l/live/help/IG-Tips-Tricks.pdf" class="link">
                <div class="pdf-icondload"></div>
                <div class="pdf-dloadtext">Download IG Tip &amp; Tricks</div>
            </a>

        </div>

    </div>
    <!-- end of main-col right -->

    <div id="Bootom-Container"> 
        <div class="check-box">
            <span><input name="everytime" type="checkbox" value="1" id="everytime"/></span>
            <span><label for="everytime">Show this everytime I login</label></span>
        </div>

        <div class="Bottom-Link"><b>Still confused?</b> <span class="BottomLink"> <a
                    href="//www.importgenius.com/contact" target="_blank">Contact Us</a></span></div>
        <div class="clearer"></div>
    </div>


    </div>
    <!-- End of Main Content -->

<?php $this->load->view('help/footer'); ?>
