<?php $this->load->view('help/header'); ?>
    <!-- Start of Main Content -->
    <div class="Main-Content">
        <div class="Title-Head"><h2>Getting Started with Importgenius.com</h2></div>

        <!-- Start of Left Content -->
        <div class="Left-Content">
            <div class="TitleNum1"><h2>Watch Video Introduction</h2></div>
            <div class="clearer"></div>
            <div class="Video">
                <object type="application/x-shockwave-flash"
                        data="//www.youtube.com/v/8CtrC_OBHIo?fs=1&amp;hl=en_US" width="339" height="274">
                    <param name="movie" value="//www.youtube.com/v/8CtrC_OBHIo?fs=1&amp;hl=en_US"/>
                    <param name="allowFullScreen" value="true"/>
                </object>

                <div class="clearer"></div>


            </div>
        </div>
        <!-- End of Left Content -->

        <!-- Start of Right Content -->
        <div class="Right-Content">
            <div class="TitleNum1"><h2>Benefits for Exporters</h2></div>
            <div class="clearer"></div>

            <!-- Start of Bullet List -->
            <div class="List-Content">
                <ul class="check">
                    <li> Find American companies already importing products like yours so
                        your sales team can build a relationship to win their business
                    </li>
                    <li> Discover U.S. customer lists for your rival manufacturers anywhere
                        in the world
                    </li>
                    <li>Monitor your product segment to identify new players before they
                        become a threat
                    </li>
                    <li>Carry out market research to determine the relative health of key
                        customers and the manufacturers that supply them
                    </li>
                </ul>

            </div>
            <!-- End of Bullet List -->

        </div>
        <!-- End of Right Content -->

        <div id="Bootom-Container">
            <div class="check-box">
                <span><input name="everytime" type="checkbox" value="1" id="everytime"/></span>
                <span><label for="everytime">Show this everytime I login</label></span>
            </div>

            <div class="Bottom-Link"><b>Still confused?</b> <span class="BottomLink"> <a
                        href="//www.importgenius.com/contact" target="_blank">Contact Us</a></span></div>
            <div class="clearer"></div>
        </div>


    </div>
    <!-- End of Main Content -->
<?php $this->load->view('help/footer'); ?>